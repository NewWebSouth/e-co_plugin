<?php

namespace App\Controllers\Admin;

use App\Entities\Section;
use App\Repositories\SectionRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/section")
 */
class SectionController extends AppController
{
    
    /**
     * @Route("/", name="admin.section.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SectionRepository $sectionRepository
     * @return SectionController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, SectionRepository $sectionRepository )
    {
        
        $request->setParameter( 'sections', $sectionRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.section.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return SectionController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        /** @var Section $section */
        
        $request->setParameter( 'section', $this->getEntity( Section::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.section.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createSection
     * @param SectionRepository $sectionRepository
     * @return SectionController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createSection, SectionRepository $sectionRepository )
    {
        if( $this->manageCreate( $request, $createSection, new Section() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Section créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.section.index' );
        }
        
        $request->setParameter( 'sections', $sectionRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.section.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateSection
     * @param SectionRepository $sectionRepository
     * @return SectionController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateSection, SectionRepository $sectionRepository )
    {
        $section = $this->getEntity( Section::class, $request );
        
        if( $this->manageUpdate( $request, $updateSection, $section ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Section mise à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.section.show', [ 'id' => $section->getId() ] );
        }
        
        $request->setParameter( 'context', 'update' );
        $request->setParameter( 'section', $section );
        $request->setParameter( 'sections', $sectionRepository->findAllWithout( $section->getId() ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.section.delete", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteSection
     * @return SectionController|Distributor|array
     * @throws NotFoundException
     */
    public function delete( HttpResponse $response, HttpRequest $request, DeleteInterface $deleteSection )
    {
        if( $this->manageDelete( $request, $deleteSection, $this->getEntity( Section::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Section supprimé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.section.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/section/$templateName.html.twig";
    }
}
