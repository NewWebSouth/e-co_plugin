<?php

namespace App\Controllers\Admin;

use App\Entities\Purchase;
use App\Repositories\PurchaseRepository;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/purchase")
 */
class PurchaseController extends AppController
{
    
    /**
     * @Route("/", name="admin.purchase.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param PurchaseRepository $purchaseRepository
     * @return PurchaseController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, PurchaseRepository $purchaseRepository )
    {
        $request->setParameter( 'purchases', $purchaseRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.purchase.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PurchaseController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'purchase', $this->getEntity( Purchase::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.purchase.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createPurchase
     * @param StockRepository $stockRepository
     * @return PurchaseController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createPurchase, StockRepository $stockRepository )
    {
        if( $this->manageCreate( $request, $createPurchase, $purchase = new Purchase() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Achat créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.purchase.show', [ 'id' => $purchase->getId() ] );
        }
        
        $request->setParameter( 'stocks', $stockRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="purchase.edit", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PurchaseController|Distributor|array
     */
    public function edit( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="purchase.delete", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PurchaseController|Distributor|array
     * @throws NotFoundException
     */
    public function delete( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->redirectLocal( 'purchase.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/purchase/$templateName.html.twig";
    }
}
