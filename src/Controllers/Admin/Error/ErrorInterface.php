<?php


namespace App\Controllers\Admin\Error;


interface ErrorInterface
{

    /**
     * Return route for unavailable resource
     *
     * @return string
     */
    public function resourceUnavailable(): string;
}
