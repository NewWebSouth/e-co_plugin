<?php

namespace App\Controllers\Admin\Error;

use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/error")
 */
class ErrorController extends Distributor
{
    
    /**
     * @Route("/", name="error.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ErrorController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="error.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ErrorController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="error.create", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ErrorController|Distributor|array
     */
    public function create( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="error.edit", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ErrorController|Distributor|array
     */
    public function edit( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete", name="error.create", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ErrorController | Distributor | array
     */
    public function delete( HttpResponse $response, HttpRequest $request )
    {
        return $this->redirectLocal( 'error.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "error/$templateName.html.twig";
    }
}
