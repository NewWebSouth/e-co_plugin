<?php


namespace App\Controllers\Admin\Error;


class Error implements ErrorInterface
{

    /**
     * Return route for unavailable resource
     *
     * @return string
     */
    public function resourceUnavailable(): string
    {
        return 'admin.error.unavailable';
    }
}
