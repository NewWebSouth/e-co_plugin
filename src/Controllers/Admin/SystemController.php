<?php

namespace App\Controllers\Admin;

use App\Packages\Administer\Tools\System\SystemCleanerInterface;
use Newwebsouth\Abstraction\Controller\AppController;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/system")
 */
class SystemController extends AppController
{
    
    /**
     * @Route("/", name="admin.system.reload", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SystemCleanerInterface $systemCleaner
     * @return SystemController|Distributor|array
     * @throws NotFoundException
     */
    public function reload( HttpResponse $response, HttpRequest $request, SystemCleanerInterface $systemCleaner )
    {
        $recoveredMemory = $systemCleaner->clean();
        $this->entityManager->register();
        
        $request->setSuccess( 'Votre système à été nettoyé ( ' . $recoveredMemory . ' octets libérées )' );
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.home' );
    }
    
    
    /**
     * @Route("/software/", name="admin.system.software", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return SystemController|Distributor|array
     */
    public function software( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'IP_SERVER', $request->getServer()['SERVER_ADDR'] );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'software' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/system/$templateName.html.twig";
    }
}
