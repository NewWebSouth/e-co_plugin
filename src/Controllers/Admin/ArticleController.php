<?php

namespace App\Controllers\Admin;

use App\Entities\Article;
use App\Repositories\ArticleRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/article")
 */
class ArticleController extends AppController
{
    
    /**
     * @Route("/", name="admin.article.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ArticleRepository $articleRepository
     * @return ArticleController|Distributor|array
     */
    public function index(
        HttpResponse $response,
        HttpRequest $request,
        ArticleRepository $articleRepository )
    {
        
        $request->setParameter( 'articles', $articleRepository->findAll( Article::class ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.article.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @return ArticleController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'article', $this->getEntity( Article::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.article.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createArticle
     * @return ArticleController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createArticle )
    {
        
        if( $this->manageCreate( $request, $createArticle, new Article() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Article créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.article.index' );
        }
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.article.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateArticle
     * @return ArticleController|Distributor|array
     * @throws NotFoundException
     */
    public function edit(
        HttpResponse $response,
        HttpRequest $request,
        UpdateInterface $updateArticle )
    {
        $article = $this->getEntity( Article::class, $request );
        
        if( $this->manageUpdate( $request, $updateArticle, $article ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Article mis à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.article.show', [ 'id' => $article->getId() ] );
        }
        
        $request->setParameter( 'article', $article );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.article.delete", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteArticle
     * @return ArticleController | Distributor | array
     * @throws NotFoundException
     */
    public function delete(
        HttpResponse $response,
        HttpRequest $request,
        DeleteInterface $deleteArticle )
    {
        if( $this->manageDelete( $request, $deleteArticle, $this->getEntity( Article::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Article supprimé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.article.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/article/$templateName.html.twig";
    }
}
