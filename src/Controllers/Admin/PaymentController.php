<?php

namespace App\Controllers\Admin;

use App\Repositories\PaymentRepository;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/payment")
 */
class PaymentController extends Distributor
{
    
    /**
     * @Route("/", name="admin.payment.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param PaymentRepository $paymentRepository
     * @return PaymentController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, PaymentRepository $paymentRepository )
    {
        $request->setParameter( 'payments', $paymentRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/payment/$templateName.html.twig";
    }
}
