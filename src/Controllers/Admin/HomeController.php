<?php

namespace App\Controllers\Admin;

use App\Packages\Administer\Manage\Ordered\SRevalide;
use App\Repositories\MemoryRepository;
use App\Repositories\OrderedRepository;
use Nomess\Annotations\Route;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/home")
 */
class HomeController extends Distributor
{
    
    /**
     * @Route("/", name="admin.home", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SRevalide $revalide
     * @param EntityManagerInterface $entityManager
     * @param OrderedRepository $orderedRepository
     * @param MemoryRepository $memoryRepository
     * @return HomeController|Distributor|array
     */
    public function index(
        HttpResponse $response,
        HttpRequest $request,
        SRevalide $revalide,
        EntityManagerInterface $entityManager,
        OrderedRepository $orderedRepository,
        MemoryRepository $memoryRepository )
    {
        $revalide->revalide();
        $entityManager->register();
        
        $request->setParameter( 'orders', $orderedRepository->findAllActive() );
        $request->setParameter( 'memories', $memoryRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/home/$templateName.html.twig";
    }
}
