<?php

namespace App\Controllers\Admin;

use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Http\HttpSession;
use Nomess\Manager\Distributor;


/**
 * @Route("/manager")
 */
class AccountController extends AppController
{
    
    /**
     * @Route("/login", name="admin.account.login", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $serviceLoginAdmin
     * @return AccountController|Distributor|array
     * @throws NotFoundException
     */
    public function login( HttpResponse $response, HttpRequest $request, ServiceInterface $serviceLoginAdmin )
    {
        if( $this->manageService( $request, $serviceLoginAdmin, new \stdClass() ) ) {
            return $this->forward( $request, $response )->redirectLocal( 'admin.home' );
        }
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'login' ) );
    }
    
    
    /**
     * @Route("/logout", name="admin.account.logout", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param HttpSession $session
     * @return AccountController|Distributor|array
     * @throws NotFoundException
     */
    public function logout( HttpResponse $response, HttpRequest $request, HttpSession $session )
    {
        $session->kill();
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.account.login' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/account/$templateName.html.twig";
    }
}
