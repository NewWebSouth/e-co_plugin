<?php

namespace App\Controllers\Admin;

use App\Entities\Ordered;
use App\Packages\Administer\Manage\Ordered\SRevalide;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/ordered")
 */
class OrderedController extends AppController
{
    
    /**
     * @Route("/", name="admin.ordered.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SRevalide $revalide
     * @return OrderedController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, SRevalide $revalide )
    {
        $revalide->revalide();
        $this->entityManager->register();
        
        $request->setParameter( 'orders', $this->entityManager->find( Ordered::class ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.ordered.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return OrderedController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        /** @var Ordered $ordered */
        $ordered = $this->getEntity( Ordered::class, $request );
        $ordered->isConsulted();
        $this->entityManager->persists( $ordered )
                            ->register();
        
        $request->setParameter( 'ordered', $ordered );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="ordered.create", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return OrderedController|Distributor|array
     */
    public function create( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.ordered.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateOrdered
     * @return OrderedController|Distributor|array
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateOrdered )
    {
        $ordered = $this->getEntity( Ordered::class, $request );
        
        if( $this->manageUpdate( $request, $updateOrdered, $ordered ) ) {
            $request->setSuccess( 'Commande mise à jour' );
            $this->entityManager->register();
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.ordered.show', [ 'id' => $ordered->getId() ] );
        }
        
        $request->setParameter( 'ordered', $ordered );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="ordered.delete", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return OrderedController|Distributor|array
     * @throws \Nomess\Exception\NotFoundException
     */
    public function delete( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->redirectLocal( 'ordered.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/order/$templateName.html.twig";
    }
}
