<?php

namespace App\Controllers\Admin;

use App\Entities\Stock;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/stock")
 */
class StockController extends AppController
{
    
    /**
     * @Route("/", name="admin.stock.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param StockRepository $stockRepository
     * @return StockController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, StockRepository $stockRepository )
    {
        $request->setParameter( 'stocks', $stockRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.stock.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return StockController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'stock', $this->getEntity( Stock::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.stock.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createStock
     * @return StockController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createStock )
    {
        if( $this->manageCreate( $request, $createStock, new Stock() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Stock créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.stock.index' );
        }
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.stock.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateStock
     * @return StockController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateStock )
    {
        $stock = $this->getEntity( Stock::class, $request );
        
        if( $this->manageUpdate( $request, $updateStock, $stock ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Stock mis à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.stock.show', [ 'id' => $stock->getId() ] );
        }
        
        $request->setParameter( 'stock', $stock );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.stock.delete", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteStock
     * @return StockController | Distributor | array
     * @throws NotFoundException
     */
    public function delete( HttpResponse $response, HttpRequest $request, DeleteInterface $deleteStock )
    {
        if( $this->manageDelete( $request, $deleteStock, $this->getEntity( Stock::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Stock supprimé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.stock.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/stock/$templateName.html.twig";
    }
}
