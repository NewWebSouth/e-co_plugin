<?php

namespace App\Controllers\Admin;

use App\Entities\Reduction;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ReductionRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/reduction")
 */
class ReductionController extends AppController
{
    
    /**
     * @Route("/", name="admin.reduction.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ReductionRepository $reductionRepositoryr
     * @return ReductionController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, ReductionRepository $reductionRepositoryr )
    {
        $request->setParameter( 'reductions', $reductionRepositoryr->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.reduction.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ReductionController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'reduction', $this->getEntity( Reduction::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.reduction.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createReduction
     * @return ReductionController|Distributor|array
     * @throws NotFoundException
     */
    public function create(
        HttpResponse $response,
        HttpRequest $request,
        CreateInterface $createReduction,
        CustomerRepository $customerRepository,
        ProductRepository $productRepository )
    {
        if( $this->manageCreate( $request, $createReduction, new Reduction() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Réduction créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.index' );
        }
        
        $request->setParameter( 'customers', $customerRepository->findAll() );
        $request->setParameter( 'products', $productRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.reduction.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateReduction
     * @return ReductionController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateReduction )
    {
        $reduction = $this->getEntity( Reduction::class, $request );
        
        if( $this->manageUpdate( $request, $updateReduction, $reduction ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Réduction mise à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.show', [ 'id' => $reduction->getId() ] );
        }
        
        $request->setParameter( 'reduction', $reduction );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/disable/{id}", name="admin.reduction.disable", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param ServiceInterface $disableReduction
     * @return ReductionController|Distributor
     * @throws NotFoundException
     */
    public function disable( HttpRequest $request, HttpResponse $response, ServiceInterface $disableReduction )
    {
        $this->setSearchMethod( 'GET' );
        if( $this->manageService( $request, $disableReduction, $this->getEntity( Reduction::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Réduction désactivé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.index' );
    }
    
    
    /**
     * @Route("/enable/{id}", name="admin.reduction.enable", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param ServiceInterface $enableReduction
     * @return ReductionController|Distributor
     * @throws NotFoundException
     */
    public function enable( HttpRequest $request, HttpResponse $response, ServiceInterface $enableReduction )
    {
        $this->setSearchMethod( 'GET' );
        if( $this->manageService( $request, $enableReduction, $this->getEntity( Reduction::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Réduction activé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/reduction/$templateName.html.twig";
    }
}
