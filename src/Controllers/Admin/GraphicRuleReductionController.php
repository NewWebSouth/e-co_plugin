<?php

namespace App\Controllers\Admin;

use App\Entities\GraphicRuleReduction;
use App\Repositories\ReductionRepository;
use App\Repositories\SectionRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Newwebsouth\Exception\UninitializedException;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/graphicrulereduction")
 */
class GraphicRuleReductionController extends AppController
{
    
    /**
     * @Inject()
     */
    private SectionRepository $sectionRepository;
    
    
    /**
     * @Route("/", name="admin.reduction.web.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ReductionRepository $reductionRepository
     * @return GraphicRuleReductionController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, ReductionRepository $reductionRepository )
    {
        $request->setParameter( 'reductions', $reductionRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.reduction.web.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return GraphicRuleReductionController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'graphicRuleReduction', $this->getEntity( GraphicRuleReduction::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create/{id}", name="admin.reduction.web.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createGraphicRuleReduction
     * @return GraphicRuleReductionController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createGraphicRuleReduction )
    {
        if( $this->manageCreate( $request, $createGraphicRuleReduction, new GraphicRuleReduction() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Votre réduction sera publié sur votre site dans quelques instants' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.web.index' );
        }
        
        $request->setParameter( 'sections', $this->sectionRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.reduction.web.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateGraphicRuleReduction
     * @return GraphicRuleReductionController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateGraphicRuleReduction )
    {
        $graphicRuleReduction = $this->getEntity( GraphicRuleReduction::class, $request );
        
        if( $this->manageUpdate( $request, $updateGraphicRuleReduction, $graphicRuleReduction ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Règle graphique mise à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.web.show', [ 'id' => $graphicRuleReduction->getId() ] );
        }
        
        $request->setParameter( 'sections', $this->sectionRepository->findAll() );
        $request->setParameter( 'graphicRuleReduction', $graphicRuleReduction );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.reduction.web.delete", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteGraphicRuleReduction
     * @return GraphicRuleReductionController | Distributor | array
     * @throws NotFoundException
     * @throws UninitializedException
     */
    public function delete( HttpResponse $response, HttpRequest $request, DeleteInterface $deleteGraphicRuleReduction )
    {
        $this->setSearchMethod( 'GET' );
        
        if( $this->manageDelete( $request, $deleteGraphicRuleReduction, $this->getEntity( GraphicRuleReduction::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Règle graphique supprimé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.reduction.web.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/reduction/web/$templateName.html.twig";
    }
}
