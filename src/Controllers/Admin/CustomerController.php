<?php

namespace App\Controllers\Admin;


use App\Entities\Customer;
use App\Repositories\CustomerRepository;
use App\Repositories\OrderedRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/customer")
 */
class CustomerController extends AppController
{
    
    /**
     * @Route("/", name="admin.customer.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CustomerRepository $customerRepository
     * @return CustomerController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, CustomerRepository $customerRepository )
    {
        $request->setParameter( 'customers', $customerRepository->findAll( Customer::class ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.customer.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param OrderedRepository $orderedRepository
     * @return CustomerController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request, OrderedRepository $orderedRepository )
    {
        
        $customer = $this->getEntity( Customer::class, $request );
        
        $request->setParameter( 'orders', $orderedRepository->findByCustomer( $customer->getId() ) );
        $request->setParameter( 'customer', $customer );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.customer.create", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return CustomerController|Distributor|array
     */
    public function create( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.customer.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateCustomer
     * @return CustomerController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateCustomer )
    {
        $customer = $this->getEntity( Customer::class, $request );
        
        if( $this->manageUpdate( $request, $updateCustomer, $customer ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Client mis à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.customer.show', [ 'id' => $customer->getId() ] );
        }
        
        $request->setParameter( 'customer', $customer );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/add/comment/{id}", name="admin.customer.add.comment", methods="POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param ServiceInterface $addComment
     * @return CustomerController|Distributor
     * @throws NotFoundException
     */
    public function addComment( HttpRequest $request, HttpResponse $response, ServiceInterface $addComment )
    {
        $customer = $this->getEntity( Customer::class, $request );
        
        if( $this->manageService( $request, $addComment, $customer ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Commentaire ajouté au client' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.customer.show', [ 'id' => $customer->getId() ] );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.customer.disable", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $disableCustomer
     * @return CustomerController|Distributor|array
     * @throws NotFoundException
     */
    public function disable( HttpResponse $response, HttpRequest $request, ServiceInterface $disableCustomer )
    {
        if( $this->manageService( $request, $disableCustomer, $this->getEntity( Customer::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Client supprimé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.customer.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/customer/$templateName.html.twig";
    }
}
