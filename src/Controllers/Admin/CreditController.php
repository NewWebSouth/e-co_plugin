<?php

namespace App\Controllers\Admin;

use App\Entities\Credit;
use App\Repositories\CreditRepository;
use App\Repositories\InvoiceRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/credit")
 */
class CreditController extends AppController
{
    
    /**
     * @Route("/", name="admin.credit.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreditRepository $creditRepository
     * @return CreditController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, CreditRepository $creditRepository )
    {
        $request->setParameter( 'credits', $creditRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="credit.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return CreditController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.credit.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param InvoiceRepository $invoiceRepository
     * @return CreditController|Distributor|array
     */
    public function create( HttpResponse $response, HttpRequest $request, InvoiceRepository $invoiceRepository, CreateInterface $createCredit )
    {
        if( $this->manageCreate( $request, $createCredit, new Credit() ) ) {
            $this->entityManager->register();
            
            $request->setSuccess( 'Avoir créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.credit.index' );
        }
        
        $request->setParameter( 'invoices', $invoiceRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="credit.edit", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return CreditController|Distributor|array
     */
    public function edit( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="credit.delete", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return CreditController|Distributor|array
     */
    public function delete( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->redirectLocal( 'credit.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/credit/$templateName.html.twig";
    }
}
