<?php

namespace App\Controllers\Admin;

use App\Entities\Product;
use App\Entities\Stock;
use App\Repositories\ProductRepository;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/product")
 */
class ProductController extends AppController
{
    
    /**
     * @Route("/", name="admin.product.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ProductRepository $productRepository
     * @return ProductController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, ProductRepository $productRepository )
    {
        
        $request->setParameter( 'products', $productRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.product.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ProductController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'product', $this->getEntity( Product::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.product.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createProduct
     * @return ProductController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createProduct )
    {
        if( $this->manageCreate( $request, $createProduct, new Product() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Produit créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.product.index' );
        }
        
        $request->setParameter( 'stocks', $this->entityManager->find( Stock::class ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.product.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateProduct
     * @param StockRepository $stockRepository
     * @return ProductController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateProduct, StockRepository $stockRepository )
    {
        $product = $this->getEntity( Product::class, $request );
        
        if( $this->manageUpdate( $request, $updateProduct, $product ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Produit mis à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.product.show', [ 'id' => $product->getId() ] );
        }
        
        $request->setParameter( 'context', 'update' );
        $request->setParameter( 'stocks', $stockRepository->findAll() );
        $request->setParameter( 'product', $product );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/disable/{id}", name="admin.product.disable", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $disableProduct
     * @return ProductController|Distributor
     * @throws NotFoundException
     */
    public function disable( HttpResponse $response, HttpRequest $request, ServiceInterface $disableProduct )
    {
        $this->setSearchMethod( 'GET' );
        if( $this->manageService( $request, $disableProduct, $this->getEntity( Product::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Produit désactivé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.product.index' );
    }
    
    
    /**
     * @Route("/enable/{id}", name="admin.product.enable", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $enableProduct
     * @return ProductController|Distributor
     * @throws NotFoundException
     */
    public function enable( HttpResponse $response, HttpRequest $request, ServiceInterface $enableProduct )
    {
        $this->setSearchMethod( 'GET' );
        if( $this->manageService( $request, $enableProduct, $this->getEntity( Product::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Produit activé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.product.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/product/$templateName.html.twig";
    }
}
