<?php

namespace App\Controllers\Admin;

use App\Controllers\Admin\Error\ErrorInterface;
use App\Entities\Image;
use App\Repositories\ImageRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/image")
 */
class ImageController extends AppController
{
    
    /**
     * @Route("/", name="admin.image.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ImageRepository $imageRepository
     * @return ImageController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, ImageRepository $imageRepository )
    {
        $request->setParameter( 'images', $imageRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.image.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ImageController|Distributor|array
     * @throws \Newwebsouth\Exception\UninitializedException
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'image', $this->getEntity( Image::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.image.create", methods="POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createImage
     * @return ImageController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createImage )
    {
        if( $this->manageCreate( $request, $createImage, new Image() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Image créé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.image.index' );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.image.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateImage
     * @return ImageController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateImage )
    {
        $image = $this->getEntity( Image::class, $request );
        
        if( $this->manageUpdate( $request, $updateImage, $image ) ) {
            $request->setSuccess( 'Image mise à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.image.show', [ 'id' => $image->getId() ] );
        }
        
        $request->setParameter( 'image', $image );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete", name="admin.image.delete", methods="POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteImage
     * @param ErrorInterface $error
     * @param ImageRepository $imageRepository
     * @return ImageController | Distributor | array
     * @throws NotFoundException
     */
    public function delete(
        HttpResponse $response,
        HttpRequest $request,
        DeleteInterface $deleteImage,
        ErrorInterface $error,
        ImageRepository $imageRepository )
    {
        
        if( $request->isValidToken() ) {
            
            $image = $imageRepository->findByPublicFilename( $request->getParameter( 'image' ) );
            
            if( !empty( $image ) ) {
                if( $deleteImage->delete( $request, $image ) ) {
                    $this->entityManager->register();
                    $request->setSuccess( 'Image supprimé' );
                } else {
                    $request->setError( $deleteImage->getRepository( 'error' ) );
                }
            } else {
                return $this->forward( $request, $response )->redirectLocal( $error->resourceUnavailable() );
            }
        } else {
            $request->setError( 'Le formulaire n\'a pas pu être transmit' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.image.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/image/$templateName.html.twig";
    }
}
