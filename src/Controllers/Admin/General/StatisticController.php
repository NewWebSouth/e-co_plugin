<?php

namespace App\Controllers\Admin\General;

use App\Repositories\CustomerRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\OrderedRepository;
use App\Repositories\ProductCustomerRepository;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/general/statistic")
 */
class StatisticController extends Distributor
{
    
    /**
     * @Inject()
     */
    private InvoiceRepository $invoiceRepository;
    /**
     * @Inject()
     */
    private OrderedRepository  $orderedRepository;
    /**
     * @Inject()
     */
    private CustomerRepository $customerRepository;
    /**
     * @Inject()
     */
    private ProductCustomerRepository  $productCustomerRepository;
    
    
    /**
     * @Route("/", name="admin.general.statistic.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return StatisticController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'turnover', $this->getTurnover() );
        $request->setParameter( 'orders', $this->getOrders() );
        $request->setParameter( 'top_customer', $this->getTopClient() );
        $request->setParameter( 'top_product', $this->getTopProduct() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    private function getTopProduct(): array
    {
        $array = array();
        
        $productCustomers = $this->productCustomerRepository->findAll();
        
        if( !empty( $productCustomers ) ) {
            foreach( $productCustomers as $productCustomer ) {
                
                if( !array_key_exists( $productCustomer->getProduct()->getId(), $array ) ) {
                    $array[$productCustomer->getProduct()->getId()] = [
                        'product' => $productCustomer->getProduct(),
                        'total'   => 0 ];
                }
                
                $array[$productCustomer->getProduct()->getId()]['total']++;
            }
        }
        
        usort( $array, function ( $a, $b ) {
            if( $a['total'] === $b['total'] ) {
                return 0;
            }
            
            return ( $a['total'] > $b['total'] ) ? -1 : 1;
        }
        );
        
        return $array;
    }
    
    
    private function getTopClient(): array
    {
        $array = array();
        
        $customers = $this->customerRepository->findAll();
        
        if( !empty( $customers ) ) {
            foreach( $customers as $customer ) {
                if( $customer->isActive() ) {
                    $invoices = $this->invoiceRepository->findAllForCustomer( $customer->getId() );
                    
                    $total = 0;
                    
                    if( !empty( $invoices ) ) {
                        foreach( $invoices as $invoice ) {
                            $total += $invoice->getTotal();
                        }
                    }
                    
                    $array[$customer->getStrName()] = $total;
                }
            }
        }
        
        arsort( $array );
        
        return $array;
    }
    
    
    private function getOrders(): array
    {
        $arrayYears = $this->getArrayMonth();
        $orders     = $this->orderedRepository->findAll();
        
        if( !empty( $orders ) ) {
            foreach( $orders as $order ) {
                if( $order->getStatus() > 0 ) {
                    $orderDate = ( new \DateTime( $order->getCreatedAt() ) )->format( 'n-Y' );
                    
                    foreach( $arrayYears as $key => $value ) {
                        if( $orderDate === $key ) {
                            $arrayYears[$key]++;
                            break 1;
                        }
                    }
                }
            }
        }
        
        return $this->formatArray( $arrayYears );
    }
    
    
    private function getTurnover(): array
    {
        
        $arrayYears = $this->getArrayMonth();
        $invoices   = $this->invoiceRepository->findAll();
        
        if( !empty( $invoices ) ) {
            foreach( $invoices as $invoice ) {
                $invoiceDate = ( new \DateTime( $invoice->getCreatedAt() ) )->format( 'n-Y' );
                
                foreach( $arrayYears as $key => $value ) {
                    if( $invoiceDate === $key ) {
                        $arrayYears[$key] += $invoice->getNetToPay();
                        break 1;
                    }
                }
            }
        }
        
        return $this->formatArray( $arrayYears );
    }
    
    
    private function getArrayMonth(): array
    {
        $array = array();
        
        $currentYears = date( 'Y' );
        $currentMonth = date( 'n' );
        
        // Decrement to 0 for get 12 month on current
        for( $i = 12; $i !== 0; $i-- ) {
            
            if( $currentMonth === 0 ) {
                $currentYears--;
                $currentMonth = 12;
            }
            
            $array[$currentMonth . '-' . $currentYears] = 0;
            
            $currentMonth--;
        }
        
        return $array;
    }
    
    
    private function formatArray( array $monthYears ): array
    {
        $mapper = [
            1  => 'Janvier',
            2  => 'Février',
            3  => 'Mars',
            4  => 'Avril',
            5  => 'Mai',
            6  => 'Juin',
            7  => 'Juillet',
            8  => 'Aout',
            9  => 'Septembre',
            10 => 'Octobre',
            11 => 'Novembre',
            12 => 'Décembre'
        ];
        
        $result = array();
        
        foreach( $monthYears as $key => $value ) {
            $result[$mapper[(int)explode( '-', $key )[0]]] = $value;
        }
        
        return $result;
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/general/statistic/$templateName.html.twig";
    }
}
