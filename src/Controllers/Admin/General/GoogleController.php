<?php

namespace App\Controllers\Admin\General;

use Nomess\Annotations\Route;
use Nomess\Helpers\DataHelper;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/general/google")
 */
class GoogleController extends Distributor
{
    
    use DataHelper;
    
    private const DC_ID_ANALYTIC = 'google_analytic_id';
    
    
    /**
     * @Route("/", name="admin.general.google.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return GoogleController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'user_analytic_id', $this->get( self::DC_ID_ANALYTIC ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/general/google/$templateName.html.twig";
    }
}
