<?php

namespace App\Controllers\Admin;

use App\Packages\Administer\External\SNumerology;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/external")
 */
class ExternalController extends Distributor
{
    
    /**
     * @Inject()
     */
    private EntityManagerInterface $entityManager;
    
    
    /**
     * @Route("/numerology/", name="admin.external.numerology", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SNumerology $numerology
     * @return ExternalController|Distributor|array
     */
    public function numerology( HttpResponse $response, HttpRequest $request, SNumerology $numerology )
    {
        if( $request->isRequestMethod( 'POST' ) ) {
            $numerology->service( $request );
        }
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'numerology' ) );
    }
    
    
    /**
     * @Route("/{id}", name="external.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ExternalController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/external/$templateName.html.twig";
    }
}
