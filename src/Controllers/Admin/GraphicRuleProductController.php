<?php

namespace App\Controllers\Admin;

use App\Entities\GraphicRuleProduct;
use App\Entities\Product;
use App\Repositories\ProductRepository;
use App\Repositories\SectionRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/graphicruleproduct")
 */
class GraphicRuleProductController extends AppController
{
    
    /**
     * @Route("/", name="admin.product.web.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param EntityManagerInterface $entityManager
     * @return GraphicRuleProductController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, EntityManagerInterface $entityManager )
    {
        $request->setParameter( 'products', $entityManager->find( Product::class, 'active = 1' ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="admin.product.web.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return GraphicRuleProductController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'graphicRuleProduct', $this->getEntity( GraphicRuleProduct::class, $request ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create/{id}", name="admin.product.web.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createGraphicRuleProduct
     * @param SectionRepository $sectionRepository
     * @param ProductRepository $productRepository
     * @return GraphicRuleProductController|Distributor|array
     * @throws NotFoundException
     */
    public function create(
        HttpResponse $response,
        HttpRequest $request,
        CreateInterface $createGraphicRuleProduct,
        SectionRepository $sectionRepository,
        ProductRepository $productRepository )
    {
        
        if( $this->manageCreate( $request, $createGraphicRuleProduct, new GraphicRuleProduct() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Votre produit sera publié dans quelques instants' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.product.web.index' );
        }
        
        $request->setParameter( 'sections', $sectionRepository->findAll() );
        $request->setParameter( 'products', $productRepository->findAllToRecommended( (int)$request->getParameter( 'id' ) ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="admin.product.web.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateGraphicRuleProduct
     * @param SectionRepository $sectionRepository
     * @param ProductRepository $productRepository
     * @return GraphicRuleProductController|Distributor|array
     * @throws NotFoundException
     */
    public function edit(
        HttpResponse $response,
        HttpRequest $request,
        UpdateInterface $updateGraphicRuleProduct,
        SectionRepository $sectionRepository,
        ProductRepository $productRepository )
    {
        /** @var GraphicRuleProduct $graphicRuleProduct */
        $graphicRuleProduct = $this->getEntity( GraphicRuleProduct::class, $request );
        
        if( $this->manageUpdate( $request, $updateGraphicRuleProduct, $graphicRuleProduct ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Règle graphique mise à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.product.web.show', [ 'id' => $graphicRuleProduct->getId() ] );
        }
        
        $request->setParameter( 'context', 'update' );
        $request->setParameter( 'sections', $sectionRepository->findAll() );
        $request->setParameter( 'products', $productRepository->findAllToRecommended( $graphicRuleProduct->getProduct()->getId() ) );
        $request->setParameter( 'graphicRuleProduct', $graphicRuleProduct );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.product.web.delete", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteGraphicRuleProduct
     * @return GraphicRuleProductController | Distributor | array
     * @throws NotFoundException
     */
    public function delete( HttpResponse $response, HttpRequest $request, DeleteInterface $deleteGraphicRuleProduct )
    {
        $this->setSearchMethod( 'GET' );
        if( $this->manageDelete( $request, $deleteGraphicRuleProduct, $this->getEntity( GraphicRuleProduct::class, $request ) ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Règle graphic supprimé' );
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.product.web.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/product/web/$templateName.html.twig";
    }
}
