<?php

namespace App\Controllers\Admin;

use App\Repositories\InvoiceRepository;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/invoice")
 */
class InvoiceController extends Distributor
{
    
    /**
     * @Route("/", name="admin.invoice.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return InvoiceController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, InvoiceRepository $invoiceRepository )
    {
        $request->setParameter( 'invoices', $invoiceRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/invoice/$templateName.html.twig";
    }
}
