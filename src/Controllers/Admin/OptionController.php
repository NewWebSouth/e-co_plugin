<?php

namespace App\Controllers\Admin;

use App\Entities\Option;
use App\Entities\Stock;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/product/option")
 */
class OptionController extends AppController
{
    
    /**
     * @Route("/edit/{id}", name="admin.product.option.edit", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param UpdateInterface $updateOption
     * @return OptionController|Distributor|array
     * @throws NotFoundException
     */
    public function edit( HttpResponse $response, HttpRequest $request, UpdateInterface $updateOption )
    {
        $option = $this->getEntity( Option::class, $request );
        
        if( $this->manageUpdate( $request, $updateOption, $option ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Option mise à jour' );
            
            return $this->forward( $request, $response )->redirectLocal( 'admin.product.show', [ 'id' => $option->getProduct()->getId() ] );
        }
        
        $request->setParameter( 'option', $option );
        $request->setParameter( 'stocks', $this->entityManager->find( Stock::class ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "manager/option/$templateName.html.twig";
    }
}
