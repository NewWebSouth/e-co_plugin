<?php

namespace App\Controllers\Admin;

use App\Entities\Memory;
use App\Repositories\MemoryRepository;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Annotations\Route;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/admin/memory")
 */
class MemoryController extends AppController
{
    
    /**
     * @Route("/", name="admin.memory.index", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param MemoryRepository $memoryRepository
     * @return MemoryController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, MemoryRepository $memoryRepository )
    {
        $request->setParameter( 'memories', $memoryRepository->findAll() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/create", name="admin.memory.create", methods="POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param CreateInterface $createMemory
     * @return MemoryController|Distributor|array
     * @throws NotFoundException
     */
    public function create( HttpResponse $response, HttpRequest $request, CreateInterface $createMemory )
    {
        if( $this->manageCreate( $request, $createMemory, new Memory() ) ) {
            $request->setSuccess( 'Memo créé' );
            $this->entityManager->register();
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.home' );
    }
    
    
    /**
     * @Route("/delete/{id}", name="admin.memory.delete", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param DeleteInterface $deleteMemory
     * @param EntityManagerInterface $entityManager
     * @return MemoryController | Distributor | array
     * @throws NotFoundException
     */
    public function delete( HttpResponse $response, HttpRequest $request, DeleteInterface $deleteMemory, EntityManagerInterface $entityManager )
    {
        $this->setSearchMethod( 'GET' );
        if( $this->manageDelete( $request, $deleteMemory, $this->getEntity( Memory::class, $request ) ) ) {
            $request->setSuccess( 'Memo supprimé' );
            $entityManager->register();
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'admin.home' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "memory/$templateName.html.twig";
    }
}
