<?php

namespace App\Controllers;

use App\Entities\Credit;
use App\Entities\Document;
use App\Entities\Invoice;
use Newwebsouth\Abstraction\Controller\AppController;
use Newwebsouth\Exception\UninitializedException;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Http\HttpSession;


class DocumentController extends AppController
{
    
    private const DC_STORAGE_INVOICE = 'local_storage_invoice';
    private const DC_STORAGE_CREDIT  = 'local_storage_credit';
    private const SESSION_CUSTOMER   = 'customer';
    /**
     * @Inject()
     */
    private HttpSession $session;
    
    
    /**
     * @format_off
     * @Route("/admin/document/invoice/{id}", name="admin.document.invoice.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @format_on
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @return void
     * @throws UninitializedException
     */
    public function adminInvoice( HttpResponse $response, HttpRequest $request ): void
    {
        /** @var Invoice $invoice */
        $invoice = $this->getEntity( Invoice::class, $request );
        $path    = $this->get( self::DC_STORAGE_INVOICE ) . $invoice->getDocumentName();
        
        $this->response( $path );
    }
    
    
    /**
     * @format_off
     * @Route("/admin/document/document/{id}", name="admin.document.document.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @format_on
     * @param HttpRequest $request
     * @return void
     * @throws UninitializedException
     */
    public function adminDocument( HttpRequest $request ): void
    {
        /** @var Document $document */
        $document = $this->getEntity( Document::class, $request );
        $path     = $document->getLocalPath();
        
        $this->response( $path );
    }
    
    
    /**
     * @format_off
     * @Route("/admin/document/credit/{id}", name="admin.document.credit.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @format_on
     * @param HttpRequest $request
     * @return void
     * @throws UninitializedException
     */
    public function adminCredit( HttpRequest $request ): void
    {
        /** @var Credit $credit */
        $credit = $this->getEntity( Credit::class, $request );
        $path   = $this->get( self::DC_STORAGE_CREDIT ) . $credit->getDocumentName();
        
        $this->response( $path );
    }
    
    
    /**
     * @format_off
     * @Route("/account/document/{id}", name="customer.document.invoice.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @format_on
     *
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @return void
     * @throws UninitializedException
     */
    public function customer( HttpResponse $response, HttpRequest $request )
    {
        /** @var Invoice $invoice */
        $invoice = $this->getEntity( Invoice::class, $request, [ 'customer->id' => $this->getCustomerId() ] );
        $path    = $this->get( self::DC_STORAGE_INVOICE ) . $invoice->getDocumentName();
        
        $this->response( $path );
    }
    
    
    private function response( string $filename ): void
    {
        header( "Content-type: application/pdf" );
        header( "Content-Length: " . filesize( $filename ) );
        
        readfile( $filename );
    }
    
    
    private function getCustomerId(): int
    {
        return $this->session->get( self::SESSION_CUSTOMER )->getId();
    }
}
