<?php

namespace App\Controllers\Customer;

use App\Controllers\Website\WebsiteController;
use App\Entities\Ordered;
use App\Packages\Cunsumption\Basket\RevalideBasketInterface;
use App\Packages\Website\Resolver\ResolverInterface;
use App\Repositories\InvoiceRepository;
use App\Repositories\OrderedRepository;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Newwebsouth\Exception\UninitializedException;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


class OrderedController extends WebsiteController
{
    
    private const LP_BASKET = 'basket';
    /**
     * @Inject()
     */
    private ResolverInterface $resolver;
    /**
     * @Inject()
     */
    private LightPersistsInterface $lightPersists;
    /**
     * @Inject()
     */
    private OrderedRepository $orderedRepository;
    
    
    /**
     * @Route("/account/ordered/", name="website.ordered.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return OrderedController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'orders', $this->orderedRepository->findByCustomer( $this->session->get( self::SESSION_CUSTOMER )->getId() ) );
        
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/account/ordered/{id}", name="website.ordered.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param InvoiceRepository $invoiceRepository
     * @return OrderedController|Distributor|array
     * @throws UninitializedException
     */
    public function show( HttpResponse $response, HttpRequest $request, InvoiceRepository $invoiceRepository )
    {
        
        $request->setParameter( 'ordered', $ordered = $this->getEntity( Ordered::class, $request, [ 'customer->id' => $this->getCustomerId() ] ) );
        $request->setParameter( 'invoice', $invoiceRepository->findByOrdered( $ordered->getId() ) );
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/ordered/create", name="website.ordered.create", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param RevalideBasketInterface $revalideBasket
     * @param CreateInterface $createOrdered
     * @return OrderedController|Distributor|array
     * @throws NotFoundException
     */
    public function create(
        HttpResponse $response,
        HttpRequest $request,
        RevalideBasketInterface $revalideBasket,
        CreateInterface $createOrdered )
    {
        if( !$this->session->has( self::SESSION_CUSTOMER ) ) {
            $request->setParameter( 'message', 'Vous devez être connecté pour passer une commande' );
            
            return $this->forward( $request, $response )->redirectLocal( 'website.ordered.redirect' );
        }
        
        if( $this->manageCreate( $request, $createOrdered, $ordered = new Ordered() ) ) {
            $this->entityManager->register();
            $request->setSuccess( 'Commande créé' );
            
            return $this->forward( $request, $response )->redirectLocal( 'website.ordered.show', [ 'id' => $ordered->getId() ] );
        }
        
        $revalideBasket->revalide();
        $this->bindData( $request );
        $request->setParameter( 'customer', $this->session->get( self::SESSION_CUSTOMER ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    private function bindData( HttpRequest $request ): void
    {
        $request->setParameter( 'front', $this->resolver->get( ResolverInterface::MENU ) );
        $request->setParameter( self::LP_BASKET, $this->lightPersists->get( self::LP_BASKET ) );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
    }
    
    
    private function getCustomerId(): int
    {
        return $this->session->get( self::SESSION_CUSTOMER )->getId();
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "website/ordered/$templateName.html.twig";
    }
}
