<?php

namespace App\Controllers\Customer;

use App\Controllers\Website\WebsiteController;
use App\Entities\Customer;
use App\Entities\Ordered;
use App\Packages\Cunsumption\Account\SLogin;
use App\Packages\Cunsumption\Account\SSubscribe;
use App\Packages\Website\Resolver\ResolverInterface;
use App\Repositories\ReductionRepository;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Newwebsouth\Exception\UninitializedException;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Http\HttpSession;
use Nomess\Manager\Distributor;


class AccountController extends WebsiteController
{
    
    private const LP_BASKET = 'basket';
    private const ERROR     = 'error';
    /**
     * @Inject()
     */
    private LightPersistsInterface $lightPersists;
    /**
     * @Inject()
     */
    private ResolverInterface $resolver;
    
    
    /**
     * @Route("/account/", name="account.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return AccountController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request )
    {
        $this->bindData( $request );
        
        $request->setParameter( 'orders', $this->entityManager->find( Ordered::class, 'customer_id = :customer_id AND status != 4 ANd status != 5', [ 'customer_id' => $this->getCustomer()->getId() ] ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/login", name="account.login", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return AccountController|Distributor|array
     * @throws NotFoundException
     */
    public function login( HttpResponse $response, HttpRequest $request, SLogin $login )
    {
        if( $request->isValidToken() && $request->isRequestMethod( 'POST' ) ) {
            if( $login->login( $request ) ) {
                $request->setSuccess( 'Vous êtes à présent connecté' );
                
                return $this->forward( $request, $response )->redirectLocal( 'account.index' );
            }
            
            $request->setError( $login->getRepository( self::ERROR ) );
        }
        
        $this->bindData( $request );
        
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'login' ) );
    }
    
    
    /**
     * @Route("/account/logout", name="account.logout", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param HttpSession $session
     * @return AccountController|Distributor|array
     * @throws NotFoundException
     */
    public function logout( HttpResponse $response, HttpRequest $request, HttpSession $session )
    {
        $session->kill();
        
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->redirectLocal( 'account.login' );
    }
    
    
    /**
     * @Route("/subscribe", name="account.subscribe", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SSubscribe $subscribe
     * @return AccountController|Distributor|array
     * @throws NotFoundException
     */
    public function subscribe( HttpResponse $response, HttpRequest $request, SSubscribe $subscribe )
    {
        if( $request->isValidToken() && $request->isRequestMethod( 'POST' ) ) {
            if( $subscribe->subscribe( $request, new Customer() ) ) {
                $request->setSuccess( 'Vous pouvez à présent vous connecter' );
                $this->entityManager->register();
                
                return $this->forward( $request, $response )->redirectLocal( 'account.login' );
            }
            
            $request->setError( $subscribe->getRepository( self::ERROR ) );
        }
        
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'subscribe' ) );
    }
    
    
    /**
     * @Route("/forgot", name="account.forgot", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $serviceForgotPassword
     * @return AccountController|Distributor|array
     * @throws NotFoundException
     */
    public function forgotPassword( HttpResponse $response, HttpRequest $request, ServiceInterface $serviceForgotPassword )
    {
        if( $this->manageService( $request, $serviceForgotPassword, new \stdClass() ) ) {
            $this->entityManager->register();
            
            $request->setSuccess( 'Vous allez recevoir un email' );
            
            return $this->forward( $request, $response )->redirectLocal( 'account.login' );
        }
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'forgot' ) );
    }
    
    
    /**
     * @Route("/reset/password/{id}/{key}", name="account.reset", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $serviceResetPassword
     * @return AccountController|Distributor|array
     * @throws UninitializedException
     * @throws NotFoundException
     */
    public function resetPassword( HttpResponse $response, HttpRequest $request, ServiceInterface $serviceResetPassword )
    {
        if( $request->isRequestMethod( 'POST' ) ) {
            if( $serviceResetPassword->service( $request, $this->getEntity( Customer::class, $request ) ) ) {
                $this->entityManager->register();
                
                $request->setSuccess( 'Vous pouvez à présent vous connecter' );
                
                return $this->forward( $request, $response )->redirectLocal( 'account.login' );
            } else {
                $request->setError( $serviceResetPassword->getRepository( 'error' ) );
            }
        }
        
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'reset' ) );
    }
    
    
    /**
     * @Route("/authentification", name="website.ordered.redirect", methods="POST,GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param HttpSession $session
     * @param SLogin $login
     * @param SSubscribe $subscribe
     * @return AccountController|Distributor
     * @throws NotFoundException
     */
    public function redirectedByOrdered(
        HttpRequest $request,
        HttpResponse $response,
        HttpSession $session,
        SLogin $login,
        SSubscribe $subscribe )
    {
        if( $request->isValidToken() && $request->isRequestMethod( 'POST' ) ) {
            if( empty( $request->getParameter( 'last_name' ) ) ) {
                if( $login->login( $request ) ) {
                    return $this->forward( $request, $response )->redirectLocal( 'website.ordered.create' );
                } else {
                    $request->setError( $login->getRepository( self::ERROR ) );
                }
            }
            
            if( $subscribe->subscribe( $request, $customer = new Customer() ) ) {
                $session->set( self::SESSION_CUSTOMER, $customer );
                
                return $this->forward( $request, $response )->redirectLocal( 'website.ordered.create' );
            } else {
                $request->setError( $subscribe->getRepository( self::ERROR ) );
            }
        }
        
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( 'website/ordered/connection.html.twig' );
    }
    
    
    /**
     * @Route("/account/data", name="website.account.data", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param ReductionRepository $reductionRepository
     * @return AccountController|Distributor
     */
    public function data( HttpRequest $request, HttpResponse $response, ReductionRepository $reductionRepository )
    {
        $customer = $this->session->get( self::SESSION_CUSTOMER );
        $this->bindData( $request );
        $request->setParameter( 'customer', $customer );
        $request->setParameter( 'reductions', $reductionRepository->findAllActiveForCustomer( $customer->getId() ) );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'data' ) );
    }
    
    
    private function bindData( HttpRequest $request ): void
    {
        $request->setParameter( 'front', $this->resolver->get( ResolverInterface::MENU ) );
        $request->setParameter( self::LP_BASKET, $this->lightPersists->get( self::LP_BASKET ) );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "website/account/$templateName.html.twig";
    }
    
    
    private function getCustomer(): Customer
    {
        return $this->session->get( self::SESSION_CUSTOMER );
    }
}
