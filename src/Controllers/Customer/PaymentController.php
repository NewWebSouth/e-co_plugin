<?php

namespace App\Controllers\Customer;

use App\Controllers\Website\WebsiteController;
use App\Entities\Ordered;
use App\Packages\Cunsumption\Payment\PaymentAdapterInterface;
use App\Packages\Website\Resolver\ResolverInterface;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Newwebsouth\Exception\UninitializedException;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/account/payment")
 */
class PaymentController extends WebsiteController
{
    
    /**
     * @Inject()
     */
    private ResolverInterface $resolver;
    
    
    /**
     * @Route("/{id}", name="website.payment.index", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $serviceControlOrdered
     * @return PaymentController|Distributor|array
     * @throws NotFoundException
     * @throws UninitializedException
     */
    public function index( HttpResponse $response, HttpRequest $request, ServiceInterface $serviceControlOrdered )
    {
        $this->setSearchMethod( 'GET' );
        
        if( !$this->manageService( $request, $serviceControlOrdered, $ordered = $this->getEntity( Ordered::class, $request, [ 'customer->id' => $this->getCustomerId() ] ) ) ) {
            $request->setError( 'Une erreur s\'est produite' );
            
            return $this->forward( $request, $response )->redirectLocal( 'website.ordered.show', [
                'id' => $ordered->getId()
            ] );
        }
        
        $request->setParameter( 'ordered', $ordered );
        $this->bindData( $request );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/intent/{id}", name="website.payment.intent", methods="POST", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param PaymentAdapterInterface $servicePaymentIntent
     * @return false|string
     * @throws UninitializedException
     */
    public function paymentIntent( HttpRequest $request, PaymentAdapterInterface $servicePaymentIntent )
    {
        if( $this->manageService( $request, $servicePaymentIntent, $this->getEntity( Ordered::class, $request, [ 'customer->id' => $this->getCustomerId() ] ) ) ) {
            return $this->sendData( json_encode( $servicePaymentIntent->getRepository( 'success' ) ) );
        }
        
        $this->response_code( 500 );
    }
    
    
    /**
     * @Route("/success/{id}", name="website.payment.success", methods="GET", requirements=["id" => "[0-9]+")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param UpdateInterface $updateSuspectSuccess
     * @return PaymentController|Distributor
     * @throws NotFoundException
     * @throws UninitializedException
     */
    public function paymentSuccess( HttpRequest $request, HttpResponse $response, UpdateInterface $updateSuspectSuccess )
    {
        $ordered = $this->getEntity( Ordered::class, $request, [ 'customer->id' => $this->getCustomerId() ] );
        
        $this->setSearchMethod( 'GET' );
        
        if( $this->manageUpdate( $request, $updateSuspectSuccess, $ordered ) ) {
            $this->entityManager->register();
        }
        
        return $this->forward( $request, $response )->redirectLocal( 'website.ordered.show', [ 'id' => $ordered->getId() ] );
    }
    
    
    private function bindData( HttpRequest $request ): void
    {
        $request->setParameter( 'front', $this->resolver->get( ResolverInterface::MENU ) );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
    }
    
    
    private function getCustomerId(): int
    {
        return $this->session->get( self::SESSION_CUSTOMER )->getId();
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "website/payment/$templateName.html.twig";
    }
}
