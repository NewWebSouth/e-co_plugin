<?php

namespace App\Controllers\Customer;

use App\Controllers\Website\WebsiteController;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/invoice")
 */
class InvoiceController extends WebsiteController
{
    
    /**
     * @Route("/", name="website.invoice.index", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return InvoiceController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'index' ) );
    }
    
    
    /**
     * @Route("/{id}", name="invoice.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return InvoiceController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    /**
     * @Route("/create", name="invoice.create", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return InvoiceController|Distributor|array
     */
    public function create( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'create' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="invoice.edit", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return InvoiceController|Distributor|array
     */
    public function edit( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="invoice.delete", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return InvoiceController|Distributor|array
     */
    public function delete( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->redirectLocal( 'invoice.index' );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "invoice/$templateName.html.twig";
    }
}
