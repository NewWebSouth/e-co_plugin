<?php

namespace App\Controllers\Website;

use App\Entities\Section;
use App\Packages\Website\Resolver\ResolverInterface;
use App\Packages\Website\Resolver\Tracker;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Components\EntityManager\EntityManagerInterface;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NotFoundException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/section")
 */
class SectionController extends WebsiteController
{
    
    private const LP_BASKET = 'basket';
    /**
     * @Inject()
     */
    private LightPersistsInterface $lightPersists;
    
    
    public function __construct( LightPersistsInterface $lightPersists )
    {
        $this->lightPersists = $lightPersists;
    }
    
    
    /**
     * @Route("/{id}/{slug}", name="website.section", methods="GET", requirements="["id" => "[0-9]+"]")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ResolverInterface $resolver
     * @return SectionController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, ResolverInterface $resolver )
    {
        $data = $resolver->get( ResolverInterface::SECTION, $request->getParameter( 'id' ) );
        $request->setParameter( 'front', $data );
        $request->setParameter( self::LP_BASKET, $this->getBasket() );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
        
        return $this->forward( $request, $response )->bindTwig( $data['template'] );
    }
    
    
    /**
     * @Route("/last", name="website.section.last", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param Tracker $tracker
     * @param EntityManagerInterface $entityManager
     * @return SectionController|Distributor
     * @throws InvalidParamException
     * @throws NotFoundException
     */
    public function last( HttpRequest $request, HttpResponse $response, Tracker $tracker, EntityManagerInterface $entityManager )
    {
        $lastSectionId = $tracker->getLast( Tracker::TRACE_SECTION );
        
        /** @var Section $section */
        $section = $entityManager->find( Section::class, $lastSectionId );
        
        if( $section instanceof Section ) {
            return $this->forward( $request, $response )->redirectLocal( 'website.section', [
                'id'   => $section->getId(),
                'slug' => $section->getSlug()
            ] );
        } else {
            return $this->forward( $request, $response )->redirectLocal( 'website.home' );
        }
    }
    
    
    private function getBasket(): ?array
    {
        return $this->lightPersists->get( self::LP_BASKET );
    }
}
