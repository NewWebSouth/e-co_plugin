<?php

namespace App\Controllers\Website;

use App\Packages\Website\Resolver\ResolverInterface;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/product")
 */
class ProductController extends WebsiteController
{
    
    /**
     * @Route("/{id}/{slug}", name="website.product", methods="GET", requirements="["id" => "[0-9]+")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ResolverInterface $resolver
     * @return ProductController|Distributor|array
     */
    public function index( HttpResponse $response, HttpRequest $request, ResolverInterface $resolver )
    {
        $request->setParameter( 'front', $resolver->get( ResolverInterface::PRODUCT, $request->getParameter( 'id' ) ) );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
        
        return $this->forward( $request, $response )->bindTwig( 'website/product/product.html.twig' );
    }
}
