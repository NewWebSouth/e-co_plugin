<?php

namespace App\Controllers\Website;

use App\Packages\Website\Resolver\ResolverInterface;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/article")
 */
class ArticleController extends WebsiteController
{
    
    /**
     * @Route("/{id}", name="website.article.show", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return ArticleController|Distributor|array
     */
    public function show( HttpResponse $response, HttpRequest $request, ResolverInterface $resolver )
    {
        $data = $resolver->get( ResolverInterface::ARTICLE, $request->getParameter( 'id' ) );
        $request->setParameter( 'front', $data );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'show' ) );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "website/article/$templateName.html.twig";
    }
}
