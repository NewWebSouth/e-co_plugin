<?php

namespace App\Controllers\Website;

use App\Packages\Website\Resolver\ResolverInterface;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/page")
 */
class PageController extends WebsiteController
{
    
    /**
     * @Inject()
     */
    private ResolverInterface $resolver;
    
    
    /**
     * @Route("/identity", name="website.page.identity", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PageController|Distributor|array
     */
    public function identity( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'front', $this->getMenu() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'identity' ) );
    }
    
    
    /**
     * @Route("/tos", name="website.page.tos", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PageController|Distributor|array
     */
    public function tos( HttpResponse $response, HttpRequest $request )
    {
        $request->setParameter( 'front', $this->getMenu() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'tos' ) );
    }
    
    
    /**
     * @Route("/contact", name="website.page.contact", methods="GET,POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param ServiceInterface $serviceContact
     * @return PageController|Distributor|array
     */
    public function contact( HttpResponse $response, HttpRequest $request, ServiceInterface $serviceContact )
    {
        
        if( $this->manageService( $request, $serviceContact, new \stdClass() ) ) {
            $request->setSuccess( 'Votre message a été envoyé' );
        }
        
        $request->setParameter( 'front', $this->getMenu() );
        
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'contact' ) );
    }
    
    
    /**
     * @Route("/edit/{id}", name="page.edit", methods="GET,POST")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PageController|Distributor|array
     */
    public function edit( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->bindTwig( $this->getTemplate( 'edit' ) );
    }
    
    
    /**
     * @Route("/delete/{id}", name="page.delete", methods="GET")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @return PageController|Distributor|array
     */
    public function delete( HttpResponse $response, HttpRequest $request )
    {
        return $this->forward( $request, $response )->redirectLocal( 'page.index' );
    }
    
    
    public function getMenu(): array
    {
        return $this->resolver->get( ResolverInterface::MENU );
    }
    
    
    private function getTemplate( string $templateName ): string
    {
        return "website/page/$templateName.html.twig";
    }
}
