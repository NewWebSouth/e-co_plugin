<?php

namespace App\Controllers\Website;

use App\Entities\Section;
use Nomess\Annotations\Route;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NotFoundException;
use Nomess\Helpers\DataHelper;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


class HomeController extends Distributor
{
    
    use DataHelper;
    
    private const DC_HOME_SECTION = 'section_home_id';
    
    
    /**
     * @Route("/", name="website.home", methods="GET")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param EntityManagerInterface $entityManager
     * @return HomeController|Distributor|array
     * @throws InvalidParamException
     * @throws NotFoundException
     */
    public function index( HttpResponse $response, HttpRequest $request, EntityManagerInterface $entityManager )
    {
        $section = $this->getHome( $entityManager );
        
        return $this->forward( $request, $response )->redirectLocal( 'website.section', [
            'id'   => $section->getId(),
            'slug' => $section->getSlug()
        ] );
    }
    
    
    private function getHome( EntityManagerInterface $entityManager ): Section
    {
        return $entityManager->find( Section::class, $this->get( self::DC_HOME_SECTION ) );
    }
}
