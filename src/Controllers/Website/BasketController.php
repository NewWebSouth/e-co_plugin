<?php

namespace App\Controllers\Website;

use App\Entities\Section;
use App\Packages\Cunsumption\Basket\SAdd;
use App\Packages\Cunsumption\Basket\SRemove;
use App\Packages\Website\Resolver\ResolverInterface;
use App\Packages\Website\Resolver\Tracker;
use App\Repositories\SectionRepository;
use Nomess\Annotations\Inject;
use Nomess\Annotations\Route;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NotFoundException;
use Nomess\Helpers\DataHelper;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpResponse;
use Nomess\Manager\Distributor;


/**
 * @Route("/basket")
 */
class BasketController extends WebsiteController
{
    
    use DataHelper;
    
    private const DC_SECTION_HOME = 'section_home_id';
    private const LP_BASKET       = 'basket';
    /**
     * @Inject()
     */
    private Tracker                 $tracker;
    /**
     * @Inject()
     */
    private LightPersistsInterface  $lightPersists;
    /**
     * @Inject()
     */
    private SectionRepository       $sectionRepository;
    
    
    /**
     * @Route("/", name="website.basket.index")
     * @param HttpRequest $request
     * @param HttpResponse $response
     * @param ResolverInterface $resolver
     * @return BasketController|Distributor
     */
    public function index( HttpRequest $request, HttpResponse $response, ResolverInterface $resolver )
    {
        
        $request->setParameter( self::LP_BASKET, $this->lightPersists->get( self::LP_BASKET ) );
        $request->setParameter( 'front', $resolver->get( ResolverInterface::MENU ) );
        $request->setParameter( self::KEY_CUSTOMER_CONNECTED, $this->isCustomerConnected() );
        
        return $this->forward( $request, $response )->bindTwig( 'website/basket.html.twig' );
    }
    
    
    /**
     * @Route("/add", name="website.basket.add", methods="POST")
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SAdd $addProduct
     * @return BasketController|Distributor
     * @throws NotFoundException
     * @throws InvalidParamException
     */
    public function add( HttpResponse $response, HttpRequest $request, SAdd $addProduct )
    {
        $addProduct->add( $request );
        $section = $this->getLastSection();
        
        return $this->forward( $request, $response )->redirectLocal( 'website.section', [
            'id'   => $section->getId(),
            'slug' => $section->getSlug()
        ] );
    }
    
    
    /**
     * @Route("/remove/{created}", name="website.basket.remove", methods="GET", requirements=["id" => "[0-9]+"])
     * @param HttpResponse $response
     * @param HttpRequest $request
     * @param SRemove $removeProduct
     * @return BasketController|Distributor|array
     * @throws NotFoundException
     * @throws InvalidParamException
     */
    public function remove( HttpResponse $response, HttpRequest $request, SRemove $removeProduct )
    {
        $removeProduct->remove( $request );
        $section = $this->getLastSection();
        
        return $this->forward( $request, $response )->redirectLocal( 'website.section', [
            'id'   => $section->getId(),
            'slug' => $section->getSlug()
        ] );
    }
    
    
    private function getLastSection(): Section
    {
        return $this->sectionRepository->findById(
            !empty( $id = $this->tracker->getLast( Tracker::TRACE_SECTION ) )
                ? $id
                : $this->get( self::DC_SECTION_HOME
            )
        );
    }
}
