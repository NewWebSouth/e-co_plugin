<?php


namespace App\Controllers\Website;


use App\Entities\Customer;
use Newwebsouth\Abstraction\Controller\AppController;
use Nomess\Annotations\Inject;
use Nomess\Http\HttpSession;

abstract class WebsiteController extends AppController
{
    
    protected const SESSION_CUSTOMER       = 'customer';
    protected const KEY_CUSTOMER_CONNECTED = 'customer_connected';
    /**
     * @Inject()
     */
    protected HttpSession $session;
    
    
    protected function isCustomerConnected(): bool
    {
        return $this->session->get( self::SESSION_CUSTOMER ) instanceof Customer;
    }
}
