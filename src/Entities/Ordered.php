<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use NoMess\Annotations\ManyToMany;
use Nomess\Annotations\ManyToOne;
use Nomess\Annotations\OneToMany;

class Ordered extends AbstractEntity
{
    
    public const STATUS = [
        0 => 'En attente de paiement',
        1 => 'Paiement en attente de validation',
        2 => 'Prise en charge',
        3 => 'Expédié',
        4 => 'Livré',
        5 => 'Avorté'
    ];
    private int          $id;
    /**
     * @OneToMany()
     */
    private Customer     $customer;
    private int          $status;
    private ?string      $data_customer;
    /**
     * @ManyToOne()
     * @var ProductCustomer[]
     */
    private array $product_customer;
    /**
     * @ManyToMany()
     * @var ReductionCustomer[]
     */
    private ?array  $reduction_customers;
    private float   $netToPay;
    private float   $total;
    private string  $address_billing;
    private string  $address_delivery;
    private int     $consulted;
    private ?string $comment;
    private string  $created_at;
    
    
    public function __construct()
    {
        $this->reduction_customers = array();
        $this->product_customer    = array();
        $this->data_customer       = NULL;
        $this->status              = 0;
        $this->consulted           = 0;
        $this->comment             = NULL;
        $this->created_at          = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }
    
    
    /**
     * @param Customer $customer
     * @return Ordered
     */
    public function setCustomer( Customer $customer ): Ordered
    {
        $this->customer = $customer;
        
        return $this;
    }
    
    
    public function getStatus(): ?int
    {
        return $this->status;
    }
    
    
    public function setStatus( int $status ): self
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    public function getStrStatus(): string
    {
        return self::STATUS[$this->status];
    }
    
    
    public function getDataCustomer(): ?string
    {
        return $this->data_customer;
    }
    
    
    public function setDataCustomer( ?string $dataCustomer ): self
    {
        $this->data_customer = $dataCustomer;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return ProductCustomer[]
     */
    public function getProductCustomer(): array
    {
        return $this->product_customer;
    }
    
    
    public function addProductCustomer( ProductCustomer $productCustomer ): self
    {
        
        if( !$this->arrayContainsValue( $productCustomer, $this->product_customer, TRUE ) ) {
            $this->product_customer[] = $productCustomer;
        }
        
        return $this;
    }
    
    
    public function removeProductCustomer( ProductCustomer $productCustomer ): self
    {
        $key = $this->indexOf( $productCustomer, $this->product_customer );
        
        if( !is_null( $key ) ) {
            unset( $this->product_customer[$key] );
        }
        
        return $this;
    }
    
    
    /**
     * @return ReductionCustomer[]|null
     */
    public function getReductionCustomers(): ?array
    {
        return $this->reduction_customers;
    }
    
    
    public function addReductionCustomer( ReductionCustomer $reductionCustomer ): self
    {
        if( !$this->arrayContainsValue( $reductionCustomer, $this->reduction_customers ) ) {
            $this->reduction_customers[] = $reductionCustomer;
        }
        
        return $this;
    }
    
    
    public function removeReductionCustomer( ReductionCustomer $reductionCustomer ): self
    {
        $key = $this->indexOf( $reductionCustomer, $this->reduction_customers );
        
        if( !is_null( $key ) ) {
            unset( $this->reduction_customers[$key] );
        }
        
        return $this;
    }
    
    
    /**
     * @return float
     */
    public function getNetToPay(): float
    {
        if( !isset( $this->netToPay ) ) {
            $this->setNetToPay();
        }
        
        return $this->netToPay;
    }
    
    
    /**
     * @return Ordered
     */
    public function setNetToPay(): Ordered
    {
        $netToPay = 0;
        
        if( !$this->isEmptyArray( $this->product_customer ) ) {
            foreach( $this->product_customer as $productCustomer ) {
                $netToPay += (float)$productCustomer->getNetToPay();
            }
        }
        
        if( !$this->isEmptyArray( $this->reduction_customers ) ) {
            if( !isset( $this->total ) ) {
                $this->setTotal();
            }
            
            foreach( $this->reduction_customers as $reductionCustomer ) {
                $netToPay -= (float)( $this->total * $reductionCustomer->getReduction()->getValue() ) / 100;
            }
        }
        
        
        $this->netToPay = round( $netToPay, 2 );
        
        return $this;
    }
    
    
    /**
     * @return float
     */
    public function getTotal(): float
    {
        if( !isset( $this->total ) ) {
            $this->setTotal();
        }
        
        return $this->total;
    }
    
    
    /**
     * @return Ordered
     */
    public function setTotal(): Ordered
    {
        $total = 0;
        
        if( !$this->isEmptyArray( $this->product_customer ) ) {
            foreach( $this->product_customer as $productCustomer ) {
                $total += (float)$productCustomer->getNetToPay();
            }
        }
        
        $this->total = $total;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getAddressBilling(): string
    {
        return $this->address_billing;
    }
    
    
    /**
     * @param string $address_billing
     * @return Ordered
     */
    public function setAddressBilling( string $address_billing ): Ordered
    {
        $this->address_billing = $address_billing;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getAddressDelivery(): string
    {
        return $this->address_delivery;
    }
    
    
    /**
     * @param string $address_delivery
     * @return Ordered
     */
    public function setAddressDelivery( string $address_delivery ): Ordered
    {
        $this->address_delivery = $address_delivery;
        
        return $this;
    }
    
    
    /**
     * @return int
     */
    public function wasConsulted(): int
    {
        return $this->consulted;
    }
    
    
    /**
     * @return Ordered
     */
    public function isConsulted(): Ordered
    {
        $this->consulted += 1;
        
        return $this;
    }
    
    
    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    
    
    /**
     * @param string|null $comment
     * @return Ordered
     */
    public function setComment( ?string $comment ): Ordered
    {
        $this->comment = $comment;
        
        return $this;
    }
    
    
    public function getParsedAddressDelivery(): ?array
    {
        return $this->parseAddress( $this->address_delivery );
    }
    
    
    public function getParsedAddressBilling(): ?array
    {
        return $this->parseAddress( $this->address_billing );
    }
    
    
    private function parseAddress( string $address ): ?array
    {
        if( preg_match( '/([0-9a-zA-Z-_]+) (.+)<br>( *[0-9 ]+) (.+)<br>(.+)/', $address, $data ) ) {
            return [
                'number_street' => trim( $data[1] ),
                'street'        => trim( $data[2] ),
                'postal_code'   => trim( $data[3] ),
                'city'          => trim( $data[4] ),
                'country'       => trim( $data[5] )
            ];
        }
        
        return NULL;
    }
}
