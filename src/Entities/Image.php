<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToMany;
use Nomess\Annotations\ManyToOne;

class Image extends AbstractEntity
{
    
    private int    $id;
    private string $filename;
    private string $name;
    private string $created_at;
    private string $public_filename;
    /**
     * @ManyToOne()
     * @var Article[]
     */
    private ?array $articles;
    /**
     * @ManyToOne()
     * @var Section[]
     */
    private ?array $sections;
    /**
     * @ManyToMany()
     * @var GraphicRuleProduct[]
     */
    private ?array $graphic_rule_product;
    /**
     * @ManyToMany()
     * @var GraphicRuleReduction[]
     */
    private ?array $graphic_rule_reduction;
    
    
    public function __construct()
    {
        $this->articles               = array();
        $this->sections               = array();
        $this->graphic_rule_reduction = array();
        $this->graphic_rule_product   = array();
        $this->created_at             = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getLocalFilename(): ?string
    {
        return $this->filename;
    }
    
    
    public function setLocalFilename( string $filename ): self
    {
        $this->filename = $filename;
        
        return $this;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    public function setName( string $name ): self
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getPublicFilename(): ?string
    {
        return $this->public_filename;
    }
    
    
    public function setPublicFilename( string $public_filename ): self
    {
        $this->public_filename = $public_filename;
        
        return $this;
    }
    
    
    /**
     * @return Article[]
     */
    public function getArticles(): ?array
    {
        return $this->articles;
    }
    
    
    public function addArticle( Article $article ): self
    {
        if( !$this->arrayContainsValue( $article, $this->articles ) ) {
            $this->addDependencyTo( $article, 'image', 'articles' )
                ->articles[] = $article;
        }
        
        return $this;
    }
    
    
    public function removeArticle( Article $article ): self
    {
        $key = $this->indexOf( $article, $this->articles );
        
        if( isset( $this->articles[$key] ) ) {
            
            $this->removeDependencyTo( $article, 'image' );
            unset( $this->articles[$key] );
        }
        
        
        return $this;
    }
    
    
    public function getSections(): ?array
    {
        return $this->sections;
    }
    
    
    public function addSection( Section $section ): self
    {
        if( !$this->arrayContainsValue( $section, $this->sections ) ) {
            $this->addDependencyTo( $section, 'image', 'sections' )
                ->sections[] = $section;
        }
        
        return $this;
    }
    
    
    public function removeSection( Section $section ): self
    {
        $key = $this->indexOf( $section, $this->sections );
        
        if( isset( $this->sections[$key] ) ) {
            $this->removeDependencyTo( $section, 'image' );
            
            unset( $this->sections[$key] );
        }
        
        return $this;
    }
    
    
    public function getGraphicRuleProduct(): ?array
    {
        return $this->graphic_rule_product;
    }
    
    
    public function addGraphicRuleProduct( GraphicRuleProduct $graphicRuleProduct ): self
    {
        
        if( !$this->arrayContainsValue( $graphicRuleProduct, $this->graphic_rule_product ) ) {
            $this->addDependencyTo( $graphicRuleProduct, 'images', 'graphic_rule_product' )
                ->graphic_rule_product[] = $graphicRuleProduct;
        }
        
        return $this;
    }
    
    
    public function removeGraphicRuleProduct( GraphicRuleProduct $graphicRuleProduct ): self
    {
        $key = $this->indexOf( $graphicRuleProduct, $this->graphic_rule_product );
        
        if( isset( $this->graphicRuleProduct[$key] ) ) {
            $this->removeDependencyTo( $graphicRuleProduct, 'images' );
            unset( $this->graphic_rule_product[$key] );
        }
        
        return $this;
    }
    
    
    public function getGraphicRuleReduction(): ?array
    {
        return $this->graphic_rule_reduction;
    }
    
    
    public function addGraphicRuleReduction( GraphicRuleReduction $graphicRuleReduction ): self
    {
        
        if( !$this->arrayContainsValue( $graphicRuleReduction, $this->graphic_rule_reduction ) ) {
            $this->addDependencyTo( $graphicRuleReduction, 'image', 'graphic_rule_reduction' )
                ->graphic_rule_reduction[] = $graphicRuleReduction;
        }
        
        return $this;
    }
    
    
    public function removeGraphicRuleReduction( GraphicRuleReduction $graphicRuleReduction ): self
    {
        $key = $this->indexOf( $graphicRuleReduction, $this->graphic_rule_reduction );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $graphicRuleReduction, 'image' );
            unset( $this->graphic_rule_reduction[$key] );
        }
        
        return $this;
    }
}
