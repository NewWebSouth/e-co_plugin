<?php

namespace App\Entities;

use Nomess\Annotations\OneToMany;
use Nomess\Exception\InvalidParamException;

class Section extends AbstractEntityFront
{
    
    private int     $id;
    private ?string $name;
    private ?string $description;
    /**
     * @OneToMany()
     */
    private ?Section $parent;
    private string   $created_at;
    /**
     * @OneToMany()
     */
    private ?Image  $image;
    private bool    $protected;
    private ?string $slug;
    private string  $template;
    private ?int    $priority = NULL;
    
    
    public function __construct()
    {
        $this->name        = NULL;
        $this->description = NULL;
        $this->image       = NULL;
        $this->parent      = NULL;
        $this->slug        = NULL;
        $this->protected   = FALSE;
        $this->template    = "website/section/section.html.twig";
        $this->created_at  = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    public function setName( string $name ): self
    {
        
        if( strlen( $name ) > 150 || strlen( $name ) < 3 ) {
            throw new InvalidParamException( 'Le nom d\'une section doit avoir au minimum 3 caractères et au maximum 150 caractères' );
        } elseif( empty( $name ) ) {
            throw new InvalidParamException( 'Un section doit avoir un nom' );
        }
        
        $this->slug = NULL;
        
        $this->name = $name;
        
        try {
            $this->getSlug();
        } catch( InvalidParamException $e ) {
        }
        
        return $this;
    }
    
    
    /**
     * @return string|null
     * @throws InvalidParamException
     */
    public function getSlug(): ?string
    {
        
        if( $this->slug === NULL ) {
            $this->slug = $this->generateSlug( [ 'getName' ] );
        }
        
        return $this->slug;
    }
    
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    
    public function setDescription( string $description ): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    
    public function getParent(): ?Section
    {
        return $this->parent;
    }
    
    
    public function setParent( ?Section $parent ): self
    {
        $this->parent = $parent;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getImage(): ?Image
    {
        return $this->image;
    }
    
    
    public function setImage( ?Image $image ): self
    {
        if( isset( $this->image ) ) {
            $this->removeDependencyTo( $this->image, 'sections' );
            $this->image = NULL;
        }
        
        if( !empty( $image ) ) {
            $this->addDependencyTo( $image, 'sections', 'image' )
                ->image = $image;
        }
        
        return $this;
    }
    
    
    public function getProtected(): ?bool
    {
        return $this->protected;
    }
    
    
    public function setProtected( bool $protected ): self
    {
        $this->protected = $protected;
        
        return $this;
    }
    
    
    public function getTemplate(): string
    {
        return $this->template;
    }
    
    
    public function setTemplate( string $template ): self
    {
        $this->template = $template;
        
        return $this;
    }
    
    
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    
    public function setPriority( ?int $priority ): self
    {
        $this->priority = $priority;
        
        return $this;
    }
}
