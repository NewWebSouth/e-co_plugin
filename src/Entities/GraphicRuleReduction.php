<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToMany;
use Nomess\Annotations\MappedBy;
use Nomess\Annotations\OneToMany;
use Nomess\Annotations\OneToOne;
use Nomess\Exception\InvalidParamException;

class GraphicRuleReduction extends AbstractEntity
{
    
    private int     $id;
    private string  $title;
    private ?string $description;
    /**
     * @ManyToMany()
     * @var Section[]
     */
    private array   $sections;
    private string  $created_at;
    /**
     * @OneToMany()
     */
    private ?Image $image;
    /**
     * @MappedBy("graphic_rule_reduction")
     * @OneToOne()
     */
    private Reduction $reduction;
    
    
    public function __construct()
    {
        $this->image       = NULL;
        $this->description = NULL;
        $this->sections    = array();
        $this->created_at  = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    
    public function setTitle( ?string $title ): self
    {
        
        if( empty( $title ) ) {
            throw new InvalidParamException( 'Une reduction doit avoir un titre pour etre visible sur le site' );
        } elseif( strlen( $title ) > 500 ) {
            throw new InvalidParamException( 'Le titre d\'un réduction ne peut pas dépasser 500 crfaractères' );
        }
        
        $this->title = $title;
        
        return $this;
    }
    
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    
    public function setDescription( ?string $description ): self
    {
        
        if( !empty( $description ) && strlen( $description ) > 10000 ) {
            throw new InvalidParamException( 'La déscription d\'une réduction ne peux pas dépasser 10 000 caractères' );
        }
        $this->description = $description;
        
        return $this;
    }
    
    
    /**
     * @return Section[]
     */
    public function getSections(): array
    {
        return $this->sections;
    }
    
    
    public function addSection( Section $section ): self
    {
        if( !$this->arrayContainsValue( $section, $this->sections ) ) {
            $this->sections[] = $section;
        }
        
        return $this;
    }
    
    
    public function removeSection( Section $section ): self
    {
        $key = $this->indexOf( $section, $this->sections );
        
        if( !is_null( $key ) ) {
            unset( $this->sections[$key] );
        }
        
        return $this;
    }
    
    
    public function purgeSection(): self
    {
        $this->sections = array();
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getImage(): ?Image
    {
        return $this->image;
    }
    
    
    public function setImage( ?Image $image ): self
    {
        if( isset( $this->image ) ) {
            $this->removeDependencyTo( $image, 'graphic_rule_product' );
        }
        
        $this->addDependencyTo( $image, 'graphic_rule_product', 'image' )
            ->image = $image;
        
        return $this;
    }
    
    
    public function getReduction(): Reduction
    {
        return $this->reduction;
    }
    
    
    public function setReduction( Reduction $reduction ): self
    {
        if( isset( $this->reduction ) ) {
            $this->removeDependencyTo( $reduction, 'graphic_rule_reduction' );
        }
        
        $this->addDependencyTo( $reduction, 'graphic_rule_reduction', 'reduction' )
            ->reduction = $reduction;
        
        
        return $this;
    }
}
