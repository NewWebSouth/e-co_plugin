<?php


namespace App\Entities;


interface CarriesGraphicRuleInterface
{
    
    /**
     * @return GraphicRuleReduction|GraphicRuleProduct|null
     */
    public function getGraphicRule();
}
