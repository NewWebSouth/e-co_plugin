<?php

namespace App\Entities;


use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToOne;
use Nomess\Annotations\OneToMany;
use Nomess\Exception\InvalidParamException;

class Credit extends AbstractEntity implements EntityPdfInterface
{
    
    private int         $id;
    /**
     * @OneToMany()
     */
    private Customer    $customer;
    /**
     * @OneToMany()
     */
    private Invoice     $invoice;
    private string      $way;
    private string      $created_at;
    /**
     * @ManyToOne()
     * @var ProductCredit[]
     */
    private array $products;
    private float $netToPay;
    private float $total;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getCustomer(): Customer
    {
        return $this->customer;
    }
    
    
    public function setCustomer( ?Customer $customer ): self
    {
        if( !empty( $customer ) ) {
            $this->customer = $customer;
        }
        
        return $this;
    }
    
    
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }
    
    
    public function setInvoice( ?Invoice $invoice ): self
    {
        if( !empty( $invoice ) ) {
            $this->invoice = $invoice;
        }
        
        return $this;
    }
    
    
    public function getWay(): ?string
    {
        return $this->way;
    }
    
    
    public function setWay( ?string $way ): self
    {
        if( empty( $way ) ) {
            throw new InvalidParamException( 'Merci de renseigner un moyen de paiement' );
        }
        
        $this->way = $way;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return ProductCredit[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }
    
    
    public function addProduct( ProductCredit $product ): self
    {
        if( !$this->arrayContainsValue( $product, $this->products ) ) {
            $this->addDependencyTo( $product, 'credit', 'products' )
                ->products[] = $product;
        }
        
        return $this;
    }
    
    
    public function removeProduct( ProductCredit $product ): self
    {
        $key = $this->indexOf( $product, $this->products );
        
        if( !is_null( $key ) ) {
            unset( $this->products[$key] );
        }
        
        return $this;
    }
    
    
    /**
     * @return float
     */
    public function getNetToPay(): float
    {
        return $this->netToPay;
    }
    
    
    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }
    
    
    /**
     * @return Credit
     */
    public function setTotal(): Credit
    {
        if( !empty( $this->products ) ) {
            $total = 0.0;
            
            foreach( $this->products as $product ) {
                
                $total += (float)$product->getNetToPay();
            }
            
            $this->total = $total;
        }
        
        $this->netToPay = $this->total;
        
        return $this;
    }
    
    
    public function getDocumentName(): string
    {
        return 'avoir-' . $this->getId() . '.pdf';
    }
}
