<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToOne;
use Nomess\Annotations\OneToMany;
use Nomess\Exception\InvalidParamException;

class ProductCustomer extends AbstractEntity
{
    
    private int         $id;
    /**
     * @OneToMany()
     */
    private Customer    $customer;
    /**
     * @OneToMany()
     */
    private Product $product;
    private int     $quantity;
    private float   $total;
    private float   $net_to_pay;
    /**
     * @OneToMany()
     */
    private ?Option $option;
    private ?string $data_customer;
    /**
     * @ManyToOne()
     * @var ReductionCustomer[]
     */
    private array    $reduction_customers;
    private string   $created_at;
    
    
    public function __construct()
    {
        $this->reduction_customers = array();
        $this->option              = NULL;
        $this->data_customer       = NULL;
        $this->created_at          = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }
    
    
    public function setQuantity( int $quantity ): self
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    
    public function getTotal(): ?float
    {
        if( !isset( $this->total ) ) {
            $this->setTotal();
        }
        
        return $this->total;
    }
    
    
    public function setTotal(): self
    {
        $this->total = (
                           $this->product->getPrice() +
                           ( !is_null( $this->option ) ? $this->option->getPrice() : 0 )
                       ) * $this->quantity;
        
        return $this;
    }
    
    
    public function getNetToPay(): ?float
    {
        if( !isset( $this->net_to_pay ) ) {
            $this->setNetToPay();
        }
        
        return $this->net_to_pay;
    }
    
    
    public function setNetToPay(): self
    {
        
        if( !isset( $this->total ) ) {
            $this->setTotal();
        }
        
        if( isset( $this->reduction_customers ) ) {
            $reduction = 0.0;
            
            if( !$this->isEmptyArray( $this->reduction_customers ) ) {
                /** @var ReductionCustomer $reductionCustomer */
                foreach( $this->reduction_customers as $reductionCustomer ) {
                    $reduction += $reductionCustomer->getReduction()->getValue();
                }
            }
            
            if( $reduction > 100 ) {
                throw new InvalidParamException( 'Le total des reductions appliquées à un produit ne peuvent pas dépasser 100%' );
            }
            
            $this->net_to_pay = round( $this->total - ( ( $this->total * $reduction ) / 100 ), 2 );
            
            return $this;
        }
        
        $this->net_to_pay = $this->total;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getProduct(): Product
    {
        return $this->product;
    }
    
    
    public function setProduct( Product $product ): self
    {
        $this->product = $product;
        
        return $this;
    }
    
    
    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }
    
    
    /**
     * @param Customer $customer
     * @return ProductCustomer
     */
    public function setCustomer( Customer $customer ): ProductCustomer
    {
        $this->customer = $customer;
        
        return $this;
    }
    
    
    /**
     * @return Option|null
     */
    public function getOption(): ?Option
    {
        return $this->option;
    }
    
    
    /**
     * @param Option|null $option
     * @return ProductCustomer
     */
    public function setOption( ?Option $option ): ProductCustomer
    {
        $this->option = $option;
        
        return $this;
    }
    
    
    /**
     * @return string|null
     */
    public function getDataCustomer(): ?string
    {
        return $this->data_customer;
    }
    
    
    /**
     * @param string|null $data_customer
     * @return ProductCustomer
     */
    public function setDataCustomer( ?string $data_customer ): ProductCustomer
    {
        $this->data_customer = $data_customer;
        
        return $this;
    }
    
    
    /**
     * @return ReductionCustomer[]|null
     */
    public function getReductionCustomers(): ?array
    {
        return $this->reduction_customers;
    }
    
    
    /**
     * @param ReductionCustomer $reduction_customer
     * @return ProductCustomer
     */
    public function addReductionCustomers( ?ReductionCustomer $reduction_customer ): ProductCustomer
    {
        if( !$this->arrayContainsValue( $reduction_customer, $this->reduction_customers ) && !empty( $reduction_customer ) ) {
            $this->reduction_customers[] = $reduction_customer;
        }
        
        return $this;
    }
    
    
    public function removeReductionCustomers( ReductionCustomer $reduction_customer ): ProductCustomer
    {
        $key = $this->indexOf( $reduction_customer, $this->reduction_customers );
        
        if( !is_null( $key ) ) {
            unset( $this->reduction_customers[$key] );
        }
        
        return $this;
    }
}
