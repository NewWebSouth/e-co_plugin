<?php


namespace App\Entities;


use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\MappedBy;
use Nomess\Annotations\OneToMany;
use Nomess\Annotations\OneToOne;

class Document extends AbstractEntity
{
    
    private int    $id;
    private string $document_name;
    private string $local_path;
    private string $mime_type;
    private string $type;
    private string $created_at;
    /**
     * @OneToOne()
     * @MappedBy("document")
     */
    private ?Invoice $invoice;
    /**
     * @OneToMany()
     */
    private ?Purchase $purchase;
    /**
     * @OneToOne()
     * @MappedBy("document")
     */
    private ?Credit $credit;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    /**
     * @return string
     */
    public function getDocumentName(): string
    {
        return $this->document_name;
    }
    
    
    /**
     * @param string $document_name
     * @return Document
     */
    public function setDocumentName( string $document_name ): Document
    {
        $this->document_name = $document_name;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getLocalPath(): string
    {
        return $this->local_path;
    }
    
    
    /**
     * @param string $local_path
     * @return Document
     */
    public function setLocalPath( string $local_path ): Document
    {
        $this->local_path = $local_path;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mime_type;
    }
    
    
    /**
     * @param string $mime_type
     * @return Document
     */
    public function setMimeType( string $mime_type ): Document
    {
        $this->mime_type = $mime_type;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    /**
     * @param string $created_at
     * @return Document
     */
    public function setCreatedAt( string $created_at ): Document
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return Invoice|null
     */
    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }
    
    
    /**
     * @param Invoice|null $invoice
     * @return Document
     */
    public function setInvoice( ?Invoice $invoice ): Document
    {
        if( isset( $this->invoice ) ) {
            $this->removeDependencyTo( $this->invoice, 'document' );
        }
        
        $this->addDependencyTo( $invoice, 'document', 'document' )
            ->invoice = $invoice;
        
        return $this;
    }
    
    
    /**
     * @return Purchase|null
     */
    public function getPurchase(): ?Purchase
    {
        return $this->purchase;
    }
    
    
    /**
     * @param Purchase|null $purchase
     * @return Document
     */
    public function setPurchase( ?Purchase $purchase ): Document
    {
        if( isset( $this->purchase ) ) {
            $this->removeDependencyTo( $this->purchase, 'document' );
        }
        
        $this->addDependencyTo( $purchase, 'document', 'document' )
            ->purchase = $purchase;
        
        return $this;
    }
    
    
    /**
     * @return Credit|null
     */
    public function getCredit(): ?Credit
    {
        return $this->credit;
    }
    
    
    /**
     * @param Credit|null $credit
     * @return Document
     */
    public function setCredit( ?Credit $credit ): Document
    {
        if( isset( $this->credit ) ) {
            $this->removeDependencyTo( $this->credit, 'document' );
        }
        
        $this->addDependencyTo( $credit, 'document', 'document' )
            ->credit = $credit;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    
    /**
     * @param string $type
     * @return Document
     */
    public function setType( string $type ): Document
    {
        $this->type = $type;
        
        return $this;
    }
}
