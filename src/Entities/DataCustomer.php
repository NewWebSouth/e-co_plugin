<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\OneToMany;
use Nomess\Exception\InvalidParamException;

class DataCustomer extends AbstractEntity
{
    
    private int    $id;
    private string $name;
    private bool   $required;
    private string $created_at;
    /**
     * @OneToMany()
     */
    private Product $product;
    private ?int    $priority;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    /**
     * @param string|null $name
     * @return $this
     * @throws InvalidParamException
     */
    public function setName( ?string $name ): self
    {
        
        if( empty( $name ) ) {
            throw new InvalidParamException( 'Le nom d\'une donnée client doit avoir un nom' );
        } elseif( strlen( $name ) > 500 ) {
            throw new InvalidParamException( 'Le nom d\'une donnée client ne peut pas contenir plus de 500 caractères' );
        }
        
        $this->name = $name;
        
        return $this;
    }
    
    
    public function isRequired(): ?bool
    {
        return $this->required;
    }
    
    
    /**
     * @param bool|null $required
     * @return $this
     * @throws InvalidParamException
     */
    public function setRequired( ?bool $required ): self
    {
        
        if( empty( $required ) ) {
            throw new InvalidParamException( 'Merci de spécifer une contrainte pour toute les données client' );
        }
        
        $this->required = $required;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getProduct(): Product
    {
        return $this->product;
    }
    
    
    public function setProduct( Product $product ): self
    {
        if( isset( $this->product ) ) {
            $this->removeDependencyTo( $this->product, 'data_customers' );
        }
        
        $this->addDependencyTo( $product, 'data_customers', 'product' )
            ->product = $product;
        
        return $this;
    }
    
    
    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    
    /**
     * @param int|null $priority
     * @return DataCustomer
     */
    public function setPriority( ?int $priority ): DataCustomer
    {
        $this->priority = $priority;
        
        return $this;
    }
}
