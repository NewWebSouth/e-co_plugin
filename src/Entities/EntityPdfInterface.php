<?php


namespace App\Entities;


interface EntityPdfInterface
{
    
    public function getDocumentName(): string;
}
