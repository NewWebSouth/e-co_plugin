<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToMany;
use Nomess\Annotations\MappedBy;
use Nomess\Annotations\OneToOne;
use Nomess\Annotations\Stateless;
use Nomess\Exception\ConflictException;
use Nomess\Exception\InvalidParamException;

class GraphicRuleProduct extends AbstractEntity
{
    
    private const DEFAULT_IMAGE = '/website/img/lazy-font.jpg';
    private int     $id;
    private ?string $description;
    /**
     * @ManyToMany()
     * @var Image[]
     */
    private ?array $images;
    /**
     * @ManyToMany()
     * @var Section[]
     */
    private array  $sections;
    private bool   $active;
    private string $created_at;
    /**
     * @MappedBy("graphic_rule_product")
     * @OneToOne()
     */
    private Product $product;
    private ?int    $priority;
    /**
     * @ManyToMany()
     * @var Product[]
     */
    private ?array  $product_recommended;
    /**
     * @Stateless()
     */
    private array $sortedImage;
    /**
     * @Stateless()
     */
    private array $formattedPrice;
    
    
    public function __construct()
    {
        $this->priority            = NULL;
        $this->product_recommended = array();
        $this->images              = array();
        $this->active              = TRUE;
        $this->sections            = array();
        $this->created_at          = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    
    public function setDescription( ?string $description ): self
    {
        
        if( !empty( $description ) && strlen( $description ) > 10000 ) {
            throw new InvalidParamException( 'La déscription d\'un produit ne peut pas dépasser 10 000 caractères' );
        }
        
        $this->description = $description;
        
        return $this;
    }
    
    
    /**
     * @return Image[]
     */
    public function getImages(): ?array
    {
        return $this->images;
    }
    
    
    public function addImage( Image $image ): self
    {
        if( !$this->arrayContainsValue( $image, $this->images ) ) {
            $this->addDependencyTo( $image, 'graphic_rule_product', 'images' )
                ->images[] = $image;
        }
        
        return $this;
    }
    
    
    public function removeImage( Image $image ): self
    {
        $key = $this->indexOf( $image, $this->images );
        
        if( !is_null( $key ) ) {
            
            $this->removeDependencyTo( $image, 'graphic_rule_product' );
            unset( $this->images[$key] );
        }
        
        return $this;
    }
    
    
    /**
     * @return Section[]
     */
    public function getSections(): array
    {
        return $this->sections;
    }
    
    
    public function addSection( Section $section ): self
    {
        if( !$this->arrayContainsValue( $section, $this->sections ) ) {
            $this->sections[] = $section;
        }
        
        return $this;
    }
    
    
    public function removeSection( Section $section ): self
    {
        $key = $this->indexOf( $section, $this->sections );
        
        if( !is_null( $key ) ) {
            unset( $this->sections[$key] );
        }
        
        return $this;
    }
    
    
    public function purgeSections(): self
    {
        $this->sections = array();
        
        return $this;
    }
    
    
    public function isActive(): ?bool
    {
        return $this->active;
    }
    
    
    public function setActive( bool $active ): self
    {
        $this->active = $active;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    
    public function setPriority( ?int $priority ): self
    {
        $this->priority = $priority;
        
        return $this;
    }
    
    
    /**
     * @return Product[]
     */
    public function getRecommended(): ?array
    {
        return $this->product_recommended;
    }
    
    
    public function addRecommended( Product $recommended ): self
    {
        if( !$this->arrayContainsValue( $recommended, $this->product_recommended, TRUE ) ) {
            $this->product_recommended[] = $recommended;
            
            if( $this->countArray( $this->product_recommended ) > 3 ) {
                throw new ConflictException( 'Vous ne pouvez recommander que 3 produit par règle graphique' );
            }
        }
        
        return $this;
    }
    
    
    public function removeRecommended( Product $recommended ): self
    {
        $key = $this->indexOf( $recommended, $this->product_recommended );
        
        if( !is_null( $key ) ) {
            unset( $this->product_recommended[$key] );
        }
        
        return $this;
    }
    
    
    public function clearRecommended(): GraphicRuleProduct
    {
        $this->product_recommended = array();
        
        return $this;
    }
    
    
    public function getProduct(): Product
    {
        return $this->product;
    }
    
    
    public function setProduct( Product $product ): self
    {
        
        if( isset( $this->product ) ) {
            $this->removeDependencyTo( $product, 'graphic_rule_products' );
        }
        
        $this->product = $product;
        
        return $this;
    }
    
    
    /*
     * =========================== UTILITY ================================
     */
    
    
    public function getSortedImage(): array
    {
        if( !isset( $this->sortedImage ) ) {
            
            if( !empty( $this->images ) ) {
                
                $toSorted       = array();
                $nonSortedImage = array();
                
                /** @var Image $image */
                foreach( $this->images as $image ) {
                    $imageName = $image->getName();
                    
                    preg_match( '/-[0-9]+-/', $imageName, $priority );
                    
                    if( !empty( $priority[0] ) ) {
                        $toSorted[$priority[0]] = $image;
                    } else {
                        $nonSortedImage[] = $image;
                    }
                }
                
                $this->sortedImage = $this->sort( $toSorted, $nonSortedImage );
                
                return $this->sortedImage;
            } else {
                $this->sortedImage    = array();
                $this->sortedImage[0] = ( new Image() )->setPublicFilename( self::DEFAULT_IMAGE );
                
                return $this->sortedImage;
            }
        }
        
        return $this->sortedImage;
    }
    
    
    private function sort( array $toSorted, array $noSorted ): array
    {
        sort( $toSorted );
        
        $j         = count( $toSorted );
        $iteration = count( $noSorted );
        
        $tmp = array();
        
        for( $i = 0; $i < $iteration; $i++ ) {
            $tmp[$j] = $noSorted[$i];
            
            $j++;
        }
        
        return array_merge( $toSorted, $noSorted );
    }
    
    
    /**
     * @Stateless()
     */
    private array $data_customer_sorted = array();
    
    
    public function getSortedDataCustomer(): array
    {
        if( !empty( $this->data_customer_sorted ) ) {
            return $this->data_customer_sorted;
        }
        
        if( !empty( $this->product->getDataCustomer() ) ) {
            
            /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
            $this->data_customer_sorted = $this->product->getDataCustomer();
            
            usort( $this->data_customer_sorted, function ( $a, $b ) {
                if( $a->getPriority() === $b->getPriority() ) {
                    return 0;
                }
                
                return ( $a->getPriority() > $b->getPriority() ) ? -1 : 1;
            } );
        }
        
        return $this->data_customer_sorted;
    }
    
    
    public function getFormattedPrice(): array
    {
        if( !isset( $this->formattedPrice ) ) {
            $productPrice = $this->priceFormatter( $this->getProduct()->getPrice() );
            
            $this->formattedPrice    = array();
            $this->formattedPrice[0] = $productPrice[0];
            $this->formattedPrice[1] = $productPrice[1];
            
            if( !empty( $this->getProduct()->getOptions() ) ) {
                $totalOptions = 0;
                
                foreach( $this->getProduct()->getOptions() as $option ) {
                    $totalOptions += $option->getPrice();
                }
                
                $totalOptions += $this->getProduct()->getPrice();
                
                $optionsPrice            = $this->priceFormatter( $totalOptions );
                $this->formattedPrice[3] = $optionsPrice[0];
                $this->formattedPrice[4] = $optionsPrice[1];
            }
        }
        
        return $this->formattedPrice;
    }
    
    
    private function priceFormatter( float $price ): array
    {
        $nonFormattedPrice = number_format( $price, 2, ',', '' );
        
        return explode( ',', $nonFormattedPrice );
    }
    
    
    /**
     * @Stateless()
     */
    private array $stocks_options = array();
    
    
    public function getStockOption(): array
    {
        if( !empty( $this->stocks_options ) ) {
            return $this->stocks_options;
        }
        
        if( !empty( $this->product->getOptions() ) ) {
            /** @var Option $option */
            foreach( $this->product->getOptions() as $option ) {
                
                $this->stocks_options[$option->getId()] = TRUE;
                
                if( !empty( $option->getStock() ) ) {
                    
                    $minQuantity = 1000000;
                    
                    foreach( $option->getStock() as $stock ) {
                        if( $stock->getQuantity() < $minQuantity ) {
                            $minQuantity = $stock->getQuantity();
                        }
                    }
                    
                    if( $minQuantity === 0 ) {
                        $this->stocks_options[$option->getId()] = FALSE;
                    }
                }
            }
        }
        
        return $this->stocks_options;
    }
    
    
    public function stockAvailable(): bool
    {
        
        if( $this->product->isOptionRequired() ) {
            $minQuantity = 1000000;
            
            if( !empty( $this->getStockOption() ) ) {
                foreach( $this->getStockOption() as $id => $quantity ) {
                    if( $quantity < $minQuantity ) {
                        $minQuantity = $quantity;
                    }
                }
            }
            
            return $minQuantity !== 0;
        }
        
        return TRUE;
    }
}
