<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToMany;
use Nomess\Exception\InvalidParamException;

class Stock extends AbstractEntity
{
    
    private int     $id;
    private ?float  $quantity;
    private ?string $name;
    private ?string $description;
    private string  $created_at;
    /**
     * @ManyToMany()
     * @var Purchase[]
     */
    private array $buy;
    /**
     * @ManyToMany()
     * @var Product[]
     */
    private array $products;
    /**
     * @ManyToMany()
     * @var Option[]
     */
    private array $options;
    
    
    public function __construct()
    {
        $this->quantity    = NULL;
        $this->name        = NULL;
        $this->description = NULL;
        $this->buy         = array();
        $this->products    = array();
        $this->options     = array();
        $this->created_at  = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    
    public function setQuantity( ?float $quantity ): self
    {
        if( $quantity === NULL ) {
            $quantity = 0.0;
        } elseif( $quantity < 0 ) {
            throw new InvalidParamException( 'Une quantité ne peut pas être inférieure a 0' );
        }
        
        $this->quantity = $quantity;
        
        return $this;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    public function setName( ?string $name ): self
    {
        
        if( empty( $name ) ) {
            throw new InvalidParamException( 'Un stock doit porter un nom' );
        } elseif( strlen( $name ) > 255 ) {
            throw new InvalidParamException( 'Le nom d\'un stock ne doit pas dépasser 255 caractères' );
        }
        
        $this->name = $name;
        
        return $this;
    }
    
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    
    public function setDescription( ?string $description ): self
    {
        
        if( !empty( $description ) && strlen( $description ) > 10000 ) {
            throw new InvalidParamException( 'La description d\'un stock ne doit pas dépasser 10 000 caractères' );
        }
        
        $this->description = $description;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getBuy(): ?array
    {
        return $this->buy;
    }
    
    
    public function addBuy( Purchase $buy ): self
    {
        
        if( !$this->arrayContainsValue( $buy, $this->buy ) ) {
            $this->addDependencyTo( $buy, 'stocks', 'buy' )
                ->buy[] = $buy;
        }
        
        return $this;
    }
    
    
    public function removeBuy( Purchase $buy ): self
    {
        $key = $this->indexOf( $buy, $this->buy );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $buy, 'stocks' );
            unset( $this->buy[$key] );
        }
        
        return $this;
    }
    
    
    public function getProduct(): ?array
    {
        return $this->products;
    }
    
    
    public function addProduct( Product $product ): self
    {
        if( !$this->arrayContainsValue( $product, $this->products ) ) {
            $this->addDependencyTo( $product, 'stocks', 'products' )
                ->products[] = $product;
        }
        
        return $this;
    }
    
    
    public function removeProduct( Product $product ): self
    {
        $key = $this->indexOf( $product, $this->products );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $product, 'stocks' );
            unset( $this->products[$key] );
        }
        
        return $this;
    }
    
    
    public function getOptions(): ?array
    {
        return $this->options;
    }
    
    
    public function addOptions( Option $option ): self
    {
        if( !$this->arrayContainsValue( $option, $this->options ) ) {
            $this->addDependencyTo( $option, 'stocks', 'options' )
                ->options[] = $option;
        }
        
        return $this;
    }
    
    
    public function removeOption( Option $option ): self
    {
        
        $key = $this->indexOf( $option, $this->options );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $option, 'stocks' );
            unset( $this->options[$key] );
        }
        
        return $this;
    }
    
    
    public function deduct( int $quantity ): Stock
    {
        $this->quantity -= $quantity;
        
        if( $this->quantity < 0 ) {
            throw new InvalidParamException( 'Un stock ne peut pas être inférieure' );
        }
        
        return $this;
    }
    
    
    public function add( int $quantity ): Stock
    {
        $this->quantity += $quantity;
        
        return $this;
    }
}
