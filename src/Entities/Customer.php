<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Exception\InvalidParamException;

class Customer extends AbstractEntity
{
    
    private int     $id;
    private string  $first_name;
    private string  $last_name;
    private string  $email;
    private ?string $tel;
    private string  $number_street;
    private string  $street;
    private string  $city;
    private string  $postal_code;
    private string  $country;
    private string  $password;
    private bool    $offer_accept;
    private ?string $comment;
    private bool    $active;
    private ?string $key;
    
    
    public function __construct()
    {
        $this->key     = NULL;
        $this->tel     = NULL;
        $this->comment = NULL;
        $this->active  = TRUE;
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }
    
    
    /**
     * @param string $first_name
     * @return $this
     * @throws InvalidParamException
     */
    public function setFirstName( ?string $first_name ): self
    {
        
        if( empty( $first_name ) || mb_strlen( $first_name ) < 3 ) {
            throw new InvalidParamException( 'Le prénom doit être composé d\'au moins 3 caractères' );
        }
        
        $this->first_name = $first_name;
        
        return $this;
    }
    
    
    public function getLastName(): ?string
    {
        return $this->last_name;
    }
    
    
    /**
     * @param string $last_name
     * @return $this
     * @throws InvalidParamException
     */
    public function setLastName( ?string $last_name ): self
    {
        if( empty( $last_name ) || mb_strlen( $last_name ) < 3 ) {
            throw new InvalidParamException( 'Le nom doit être composé d\'au moins 3 caractères' );
        }
        
        $this->last_name = $last_name;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    
    /**
     * @param string $email
     * @return Customer
     * @throws InvalidParamException
     */
    public function setEmail( ?string $email ): Customer
    {
        if( empty( $email ) || mb_strpos( $email, '@' ) === FALSE ) {
            throw new InvalidParamException( 'Merci de saisir un email valide' );
        }
        
        $this->email = $email;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getTel(): string
    {
        return $this->tel;
    }
    
    
    /**
     * @param string $tel
     * @return Customer
     * @throws InvalidParamException
     */
    public function setTel( ?string $tel ): Customer
    {
        if( !empty( $tel ) && !preg_match( '/0[0-9].([0-9]{2}\.){4}/', $tel ) ) {
            throw new InvalidParamException( 'Merci de saisir un numero de téléphone valide' );
        }
        $this->tel = $tel;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getNumberStreet(): string
    {
        return $this->number_street;
    }
    
    
    /**
     * @param string $number_street
     * @return Customer
     * @throws InvalidParamException
     */
    public function setNumberStreet( ?string $number_street ): Customer
    {
        if( empty( $number_street ) ) {
            throw new InvalidParamException( 'Merci de saisir un numero de rue' );
        }
        
        $this->number_street = $number_street;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }
    
    
    /**
     * @param string $street
     * @return Customer
     * @throws InvalidParamException
     */
    public function setStreet( ?string $street ): Customer
    {
        if( empty( $street ) ) {
            throw new InvalidParamException( 'Merci de saisir une rue' );
        }
        
        $this->street = $street;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }
    
    
    /**
     * @param string $city
     * @return Customer
     * @throws InvalidParamException
     */
    public function setCity( ?string $city ): Customer
    {
        if( empty( $city ) ) {
            throw new InvalidParamException( 'Merci de saisir une ville' );
        }
        
        $this->city = $city;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postal_code;
    }
    
    
    /**
     * @param string $postal_code
     * @return Customer
     * @throws InvalidParamException
     */
    public function setPostalCode( ?string $postal_code ): Customer
    {
        if( empty( $postal_code ) ) {
            throw new InvalidParamException( 'Merci de saisir un code postal' );
        }
        
        $this->postal_code = $postal_code;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
    
    
    /**
     * @param string $country
     * @return Customer
     * @throws InvalidParamException
     */
    public function setCountry( ?string $country ): Customer
    {
        if( empty( $country ) ) {
            throw new InvalidParamException( 'Merci de saisir un pays' );
        }
        
        $this->country = $country;
        
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
    
    
    /**
     * @param string $password
     * @return Customer
     */
    public function setPassword( string $password ): Customer
    {
        $this->password = $password;
        
        return $this;
    }
    
    
    /**
     * @return bool
     */
    public function isOfferAccept(): bool
    {
        return $this->offer_accept;
    }
    
    
    /**
     * @param bool $offer_accept
     * @return Customer
     */
    public function setOfferAccept( bool $offer_accept ): Customer
    {
        $this->offer_accept = $offer_accept;
        
        return $this;
    }
    
    
    public function getStrAddress(): string
    {
        return $this->number_street . ' ' . $this->street . '<br>' . $this->postal_code . ' ' . $this->city . '<br>' . $this->country;
    }
    
    
    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    
    
    /**
     * @param string|null $comment
     * @return Customer
     */
    public function setComment( ?string $comment ): Customer
    {
        $this->comment = $comment;
        
        return $this;
    }
    
    
    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }
    
    
    /**
     * @param bool $active
     * @return Customer
     */
    public function setActive( bool $active ): Customer
    {
        $this->active = $active;
        
        return $this;
    }
    
    
    public function getStrName(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }
    
    
    /**
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }
    
    
    /**
     * @param string|null $key
     * @return Customer
     */
    public function setKey( ?string $key ): Customer
    {
        $this->key = $key;
        
        return $this;
    }
}
