<?php

namespace App\Entities;

use Nomess\Annotations\ManyToMany;
use Nomess\Annotations\ManyToOne;
use Nomess\Annotations\OneToOne;
use Nomess\Annotations\Owner;
use Nomess\Exception\InvalidParamException;

class Product extends AbstractEntityFront implements CarriesGraphicRuleInterface
{
    
    private int     $id;
    private string  $name;
    private ?string $description;
    private float   $price;
    private bool    $active;
    /**
     * @ManyToMany()
     * @var Stock[]
     */
    private array $stocks;
    /**
     * @ManyToOne()
     * @var DataCustomer[]|null
     */
    private array $data_customers;
    /**
     * @ManyToMany()
     * @var DataComplement[]|null
     */
    private array $data_complements;
    /**
     * @ManyToOne()
     * @var Option[]
     */
    private array $options;
    private bool  $option_required;
    /**
     * @Owner()
     * @OneToOne()
     */
    private ?GraphicRuleProduct  $graphic_rule_product;
    private ?array               $composition_stocks;
    private string               $created_at;
    private ?string              $slug;
    
    
    public function __construct()
    {
        $this->description          = NULL;
        $this->active               = TRUE;
        $this->stocks               = array();
        $this->data_customers       = array();
        $this->data_complements     = array();
        $this->options              = array();
        $this->composition_stocks   = array();
        $this->graphic_rule_product = NULL;
        $this->option_required      = FALSE;
        $this->created_at           = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    public function setName( ?string $name ): self
    {
        
        if( empty( $name ) ) {
            throw new InvalidParamException( 'Un produit doit avoir un nom' );
        } elseif( strlen( $name ) > 300 ) {
            throw new InvalidParamException( 'Le nom d\'un produit doit avoir 5 caractères minimum et 300 maximum' );
        }
        
        $this->name = $name;
        $this->slug = NULL;
        
        try {
            $this->getSlug();
        } catch( InvalidParamException $e ) {
        }
        
        return $this;
    }
    
    
    /**
     * @return string|null
     * @throws InvalidParamException
     */
    public function getSlug(): ?string
    {
        if( $this->slug === NULL ) {
            $this->slug = $this->generateSlug( [ 'getName' ] );
        }
        
        return $this->slug;
    }
    
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    
    public function setDescription( ?string $description ): self
    {
        
        if( !empty( $description ) && strlen( $description ) > 10000 ) {
            throw new InvalidParamException( 'La description d\'un produit ne peut dépasser 10 000 caractères' );
        }
        
        $this->description = $description;
        
        return $this;
    }
    
    
    public function getPrice(): ?float
    {
        return $this->price;
    }
    
    
    public function setPrice( float $price ): self
    {
        
        if( $price <= 0.0 ) {
            throw new InvalidParamException( 'Le prix d\'un produit doit être supérieure à 0' );
        }
        
        $this->price = $price;
        
        return $this;
    }
    
    
    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }
    
    
    /**
     * @param bool $active
     * @return Product
     */
    public function setActive( bool $active ): Product
    {
        $this->active = $active;
        
        return $this;
    }
    
    
    /**
     * @return DataCustomer[]|null
     */
    public function getDataCustomers(): array
    {
        return $this->data_customers;
    }
    
    
    public function addDataCustomer( DataCustomer $dataCustomer ): self
    {
        if( !$this->arrayContainsValue( $dataCustomer, $this->data_customers ) ) {
            $this->addDependencyTo( $dataCustomer, 'product', 'data_customers' )
                ->data_customers[] = $dataCustomer;
        }
        
        return $this;
    }
    
    
    public function removeDataCustomer( DataCustomer $dataCustomer ): self
    {
        $key = array_search( $dataCustomer, $this->data_customers );
        
        if( isset( $this->data_customers[$key] ) ) {
            unset( $this->data_customers[$key] );
        }
        
        return $this;
    }
    
    
    /**
     * @return DataComplement[]|null
     */
    public function getDataComplements(): ?array
    {
        return $this->data_complements;
    }
    
    
    public function addDataComplement( DataComplement $dataComplement ): self
    {
        
        if( !$this->arrayContainsValue( $dataComplement, $this->data_complements ) ) {
            $this->addDependencyTo( $dataComplement, 'products', 'data_complements' )
                ->data_complements[] = $dataComplement;
        }
        
        return $this;
    }
    
    
    public function removeDataComplement( DataComplement $dataComplement ): self
    {
        $key = array_search( $dataComplement, $this->data_complements );
        
        if( isset( $this->data_complement[$key] ) ) {
            
            $this->removeDependencyTo( $dataComplement, 'products' );
            unset( $this->data_complements[$key] );
        }
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return Stock[]|null
     */
    public function getStocks(): array
    {
        return $this->stocks;
    }
    
    
    public function addStock( Stock $stock ): self
    {
        if( !$this->arrayContainsValue( $stock, $this->stocks ) ) {
            $this->addDependencyTo( $stock, 'products', 'stocks' )
                ->stocks[] = $stock;
        }
        
        return $this;
    }
    
    
    public function removeStock( Stock $stock ): self
    {
        $key = $this->indexOf( $stock, $this->stocks );
        
        if( !is_null( $key ) ) {
            
            $this->removeDependencyTo( $stock, 'products' );
            unset( $this->stocks[$key] );
        }
        
        return $this;
    }
    
    
    public function clearStock(): self
    {
        $this->stocks = array();
        
        return $this;
    }
    
    
    /**
     * @return Option[]|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }
    
    
    public function addOption( Option $option ): self
    {
        if( !$this->arrayContainsValue( $option, $this->options ) ) {
            $this->addDependencyTo( $option, 'product', 'options' )
                ->options[] = $option;
        }
        
        return $this;
    }
    
    
    public function removeOption( Option $option ): self
    {
        $key = $this->indexOf( $option, $this->options );
        
        if( !is_null( $key ) ) {
            unset( $this->options[$key] );
        }
        
        return $this;
    }
    
    
    public function getGraphicRule(): ?GraphicRuleProduct
    {
        return $this->graphic_rule_product;
    }
    
    
    public function setGraphicRule( ?GraphicRuleProduct $graphicRuleProduct ): self
    {
        $this->graphic_rule_product = $graphicRuleProduct;
        
        return $this;
    }
    
    
    /**
     * @return bool
     */
    public function isOptionRequired(): bool
    {
        return $this->option_required;
    }
    
    
    /**
     * @param bool $option_required
     * @return Product
     */
    public function setOptionRequired( bool $option_required ): Product
    {
        $this->option_required = $option_required;
        
        return $this;
    }
    
    
    /**
     * @param int $stockId
     * @return int|null
     */
    public function getCompositionStocks( int $stockId ): ?int
    {
        if( $this->arrayContainsKey( $stockId, $this->composition_stocks ) ) {
            return $this->composition_stocks[$stockId];
        }
        
        return NULL;
    }
    
    
    /**
     * @param int $stockId
     * @param int $quantity
     * @return Product
     */
    public function addCompositionStocks( int $stockId, int $quantity ): Product
    {
        $this->composition_stocks[$stockId] = $quantity;
        
        return $this;
    }
    
    
    /**
     * @param int $stockId
     * @return $this
     */
    public function removeCompositionStocks( int $stockId ): Product
    {
        if( $this->arrayContainsKey( $stockId, $this->composition_stocks ) ) {
            unset( $this->composition_stocks[$stockId] );
        }
        
        return $this;
    }
}
