<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;

class Memory extends AbstractEntity
{
    
    private int    $id;
    private string $content;
    private string $created_at;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getContent(): ?string
    {
        return $this->content;
    }
    
    
    public function setContent( string $content ): self
    {
        $this->content = $content;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
}
