<?php


namespace App\Entities;


use Cocur\Slugify\Slugify;
use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Exception\InvalidParamException;

abstract class AbstractEntityFront extends AbstractEntity
{
    
    protected function generateSlug( array $methods ): string
    {
        if( !empty( $methods ) ) {
            foreach( $methods as &$method ) {
                if( empty( $this->$method() ) ) {
                    throw new InvalidParamException( 'An property is empty, impossible of generate an consistancy slug' );
                } else {
                    $method = $this->$method();
                }
            }
        }
        
        return ( new Slugify() )->slugify( implode( '-', $methods ) );
    }
}
