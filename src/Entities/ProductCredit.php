<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\OneToMany;

class ProductCredit extends AbstractEntity
{
    
    private int    $id;
    private string $name;
    private float  $price;
    private float  $net_to_pay;
    private int    $quantity;
    private string $created_at;
    /**
     * @OneToMany()
     */
    private Credit $credit;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    public function setName( string $name ): self
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    public function getPrice(): ?float
    {
        return $this->price;
    }
    
    
    public function setPrice( float $price ): self
    {
        $this->price = $price;
        
        return $this;
    }
    
    
    public function getNetToPay(): ?float
    {
        return $this->net_to_pay;
    }
    
    
    public function setNetToPay( float $net_to_pay ): self
    {
        $this->net_to_pay = $net_to_pay;
        
        return $this;
    }
    
    
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }
    
    
    public function setQuantity( int $quantity ): self
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return Credit
     */
    public function getCredit(): Credit
    {
        return $this->credit;
    }
    
    
    /**
     * @param Credit $credit
     * @return ProductCredit
     */
    public function setCredit( Credit $credit ): ProductCredit
    {
        $this->addDependencyTo( $credit, 'products', 'credit' )
            ->credit = $credit;
        
        return $this;
    }
}
