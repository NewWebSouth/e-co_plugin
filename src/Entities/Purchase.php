<?php

namespace App\Entities;


use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToMany;
use Nomess\Annotations\ManyToOne;

class Purchase extends AbstractEntity
{
    
    private int     $id;
    private ?string $provider;
    /**
     * @ManyToMany()
     * @var Stock[]
     */
    private array     $stocks;
    private ?float    $total;
    /**
     * @ManyToOne()
     * @var Document[]
     */
    private array     $documents;
    private ?string   $date;
    private string    $created_at;
    
    
    public function __construct()
    {
        $this->stocks     = array();
        $this->total      = NULL;
        $this->document   = array();
        $this->date       = NULL;
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getProvider(): ?string
    {
        return $this->provider;
    }
    
    
    public function setProvider( ?string $provider ): self
    {
        $this->provider = $provider;
        
        return $this;
    }
    
    
    /**
     * @return Stock[]|null
     */
    public function getStocks(): ?array
    {
        return $this->stocks;
    }
    
    
    public function addStock( Stock $stock ): self
    {
        if( !$this->arrayContainsValue( $stock, $this->stocks ) ) {
            $this->addDependencyTo( $stock, 'buy', 'stocks' )
                ->stocks[] = $stock;
        }
        
        return $this;
    }
    
    
    public function removeStock( Stock $stock ): self
    {
        $key = $this->indexOf( $stock, $this->stocks );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $stock, 'stocks' );
            unset( $this->stocks[$key] );
        }
        
        return $this;
    }
    
    
    public function getTotal(): ?float
    {
        return $this->total;
    }
    
    
    public function setTotal( ?float $total ): self
    {
        $this->total = $total;
        
        return $this;
    }
    
    
    public function getDocuments(): array
    {
        return $this->documents;
    }
    
    
    public function addDocument( Document $document ): self
    {
        if( !$this->arrayContainsValue( $document, $this->documents ) ) {
            $this->addDependencyTo( $document, 'purchase', 'document' )
                ->documents[] = $document;
        }
        
        return $this;
    }
    
    
    public function removeDocument( Document $document ): self
    {
        $key = $this->indexOf( $document, $this->documents );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $document, 'document' );
            
            unset( $this->documents[$key] );
        }
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    
    
    /**
     * @param string|null $date
     * @return Purchase
     */
    public function setDate( ?string $date ): Purchase
    {
        $this->date = $date;
        
        return $this;
    }
}
