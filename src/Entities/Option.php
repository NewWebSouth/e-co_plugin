<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\OneToMany;
use Nomess\Exception\InvalidParamException;

class Option extends AbstractEntity
{
    
    private int    $id;
    private string $name;
    /**
     * @OneToMany()
     */
    private ?Stock  $stock;
    private array   $composition_stock;
    private ?float  $price;
    /**
     * @OneToMany()
     */
    private Product $product;
    
    
    public function __construct()
    {
        $this->stock              = NULL;
        $this->composition_stocks = array();
        $this->price              = NULL;
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    
    /**
     * @param string|null $name
     * @return $this
     * @throws InvalidParamException
     */
    public function setName( ?string $name ): self
    {
        
        if( empty( $name ) ) {
            throw new InvalidParamException( 'Une option doit avoir un nom' );
        } elseif( strlen( $name ) > 100 ) {
            throw new InvalidParamException( 'Le nom d\'une option ne peut pas dépasser 100 caractères' );
        }
        
        $this->name = $name;
        
        return $this;
    }
    
    
    /**
     * @return Stock[]|null
     */
    public function getStock(): ?Stock
    {
        return $this->stock;
    }
    
    
    public function setStock( Stock $stock ): self
    {
        $this->addDependencyTo( $stock, 'options', 'stocks' )
            ->stock = $stock;
        
        
        return $this;
    }
    
    
    public function clearStock(): self
    {
        $this->stock              = NULL;
        $this->composition_stocks = array();
        
        return $this;
    }
    
    
    public function getPrice(): ?float
    {
        return $this->price;
    }
    
    
    public function setPrice( ?float $price ): self
    {
        if( $price < 0 ) {
            throw new InvalidParamException( 'Le prix d\'une option ne peux pas être infèrieure à 0 €' );
        }
        
        $this->price = $price;
        
        return $this;
    }
    
    
    public function getProduct(): Product
    {
        return $this->product;
    }
    
    
    public function setProduct( Product $product ): self
    {
        if( isset( $this->product ) ) {
            $this->removeDependencyTo( $this->product, 'options' );
        }
        
        $this->addDependencyTo( $product, 'options', 'product' )
            ->product = $product;
        
        return $this;
    }
    
    
    /**
     * @return array
     */
    public function getCompositionStocks(): array
    {
        return $this->composition_stock;
    }
    
    
    /**
     * @param int $quantity
     * @return Option
     */
    public function setCompositionStock( int $quantity ): Option
    {
        $this->composition_stock[$this->stock->getId()] = $quantity;
        
        return $this;
    }
    
    
    public function getStrCompositionStock(): int
    {
        if( !empty( $this->composition_stock ) ) {
            return current( $this->composition_stock );
        }
        
        return 0;
    }
}
