<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\MappedBy;
use Nomess\Annotations\OneToMany;
use Nomess\Annotations\OneToOne;

class Payment extends AbstractEntity
{
    
    private int $id;
    /**
     * @OneToMany()
     */
    private Customer $customer;
    /**
     * @OneToOne()
     * @MappedBy("payment")
     */
    private ?Invoice $invoice;
    private string   $way;
    private string   $created_at;
    
    
    public function __construct()
    {
        $this->invoice    = NULL;
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }
    
    
    public function setCustomer( ?Customer $customer ): self
    {
        if( !empty( $customer ) ) {
            $this->customer = $customer;
        }
        
        return $this;
    }
    
    
    public function getWay(): ?string
    {
        return $this->way;
    }
    
    
    public function setWay( string $way ): self
    {
        $this->way = $way;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    /**
     * @return Invoice|null
     */
    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }
    
    
    /**
     * @param Invoice|null $invoice
     * @return Payment
     */
    public function setInvoice( ?Invoice $invoice ): Payment
    {
        if( !empty( $invoice ) ) {
            $this->addDependencyTo( $invoice, 'payment', 'invoice' )
                ->invoice = $invoice;
        }
        
        return $this;
    }
}
