<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\OneToMany;

class ReductionCustomer extends AbstractEntity
{
    
    private int       $id;
    /**
     * @OneToMany()
     */
    private Reduction $reduction;
    private string    $created_at;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getReduction(): Reduction
    {
        return $this->reduction;
    }
    
    
    public function setReduction( Reduction $reduction ): self
    {
        $this->reduction = $reduction;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
}
