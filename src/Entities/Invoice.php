<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\OneToMany;
use Nomess\Annotations\OneToOne;
use Nomess\Annotations\Owner;

class Invoice extends AbstractEntity implements EntityPdfInterface
{
    
    private int      $id;
    /**
     * @OneToMany()
     */
    private Customer $customer;
    private string   $dateExec1;
    private ?string  $dateExec2;
    private float    $total;
    private float    $netToPay;
    private string   $created_at;
    /**
     * @Owner()
     * @OneToOne()
     */
    private Ordered  $ordered;
    /**
     * @OneToOne()
     * @Owner()
     */
    private ?Payment $payment;
    
    
    public function __construct()
    {
        $this->dateExec2  = NULL;
        $this->payment    = NULL;
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }
    
    
    public function setCustomer( Customer $customer ): self
    {
        if( !empty( $customer ) ) {
            $this->customer = $customer;
        }
        
        return $this;
    }
    
    
    public function getDateExec1(): ?string
    {
        return $this->dateExec1;
    }
    
    
    public function setDateExec1( string $dateExec1 ): self
    {
        $this->dateExec1 = $dateExec1;
        
        return $this;
    }
    
    
    public function getDateExec2(): ?string
    {
        return $this->dateExec2;
    }
    
    
    public function setDateExec2( ?string $dateExec2 ): self
    {
        $this->dateExec2 = $dateExec2;
        
        return $this;
    }
    
    
    public function getTotal(): ?float
    {
        return $this->total;
    }
    
    
    public function setTotal( float $total ): self
    {
        $this->total = $total;
        
        return $this;
    }
    
    
    public function getNetToPay(): ?float
    {
        return $this->netToPay;
    }
    
    
    public function setNetToPay( float $netToPay ): self
    {
        $this->netToPay = $netToPay;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getOrdered(): Ordered
    {
        return $this->ordered;
    }
    
    
    public function setOrdered( Ordered $ordered ): self
    {
        
        $this->ordered = $ordered;
        
        return $this;
    }
    
    
    /**
     * @return Payment|null
     */
    public function getPayment(): ?Payment
    {
        return $this->payment;
    }
    
    
    /**
     * @param Payment|null $payment
     * @return Invoice
     */
    public function setPayment( ?Payment $payment ): Invoice
    {
        if( !empty( $payment ) ) {
            $this->addDependencyTo( $payment, 'invoice', 'payment' )
                ->payment = $payment;
        }
        
        return $this;
    }
    
    
    public function getDocumentName(): string
    {
        return 'Facture-' . $this->getId() . '.pdf';
    }
}
