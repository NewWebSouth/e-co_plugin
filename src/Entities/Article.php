<?php

namespace App\Entities;

use Nomess\Annotations\OneToMany;
use Nomess\Exception\InvalidParamException;

class Article extends AbstractEntityFront
{
    
    public const TYPE = [
        0 => 'Article',
        1 => 'Évènement'
    ];
    private int    $id;
    private int    $type;
    private string $content;
    private string $created_at;
    /**
     * @OneToMany()
     */
    private ?Image  $image;
    private ?string $title;
    private ?string $slug;
    
    
    public function __construct()
    {
        $this->image      = NULL;
        $this->title      = NULL;
        $this->slug       = NULL;
        $this->created_at = date( 'Y-m-d H:i:s' );
        $this->type       = 0;
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getType(): ?string
    {
        
        return self::TYPE[$this->type];
    }
    
    
    /**
     * @param int $type
     * @return $this
     * @throws InvalidParamException
     */
    public function setType( int $type ): self
    {
        
        if( !array_key_exists( $type, self::TYPE ) ) {
            throw new InvalidParamException( 'Merci de séléctionner un type d\'article valide (Évènement ou article)' );
        }
        
        $this->type = $type;
        
        return $this;
    }
    
    
    public function getContent(): string
    {
        return $this->content;
    }
    
    
    public function setContent( ?string $content ): self
    {
        if( empty( $content ) ) {
            throw new InvalidParamException( 'Le contenu d\'un article doit avoir une valeur' );
        } elseif( strlen( $content ) < 10 ) {
            throw new InvalidParamException( 'Le contenu d\'un article doit être composé d\'au moins 10 caractères' );
        }
        
        
        $this->content = $content;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getImage(): ?Image
    {
        return $this->image;
    }
    
    
    public function setImage( ?Image $image ): self
    {
        if( isset( $this->image ) ) {
            
            $this->removeDependencyTo( $this->image, 'image' );
        }
        
        $this->addDependencyTo( $image, 'articles', 'image' )
            ->image = $image;
        
        return $this;
    }
    
    
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    
    public function setTitle( string $title ): self
    {
        
        if( empty( $title ) ) {
            throw new InvalidParamException( 'Le titre d\'un article doit avoir une valeur' );
        } elseif( strlen( $title ) < 5 ) {
            throw new InvalidParamException( 'Le titre d\'un article doit être composé d\'au moins 5 caractères' );
        }
        
        $this->slug = NULL;
        
        try {
            $this->getSlug();
        } catch( InvalidParamException $e ) {
        }
        
        
        $this->title = $title;
        
        return $this;
    }
    
    
    /**
     * @return string|null
     * @throws InvalidParamException
     */
    public function getSlug(): ?string
    {
        if( $this->slug === NULL ) {
            $this->slug = $this->generateSlug( [ 'getTitle' ] );
        }
        
        return $this->slug;
    }
    
    
    public function getTypeInt(): int
    {
        return $this->type;
    }
}
