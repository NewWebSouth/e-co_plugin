<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\OneToMany;
use Nomess\Annotations\OneToOne;
use Nomess\Annotations\Owner;
use Nomess\Exception\InvalidParamException;

class Reduction extends AbstractEntity implements CarriesGraphicRuleInterface
{
    
    public const CURRENCY = [
        0 => '€',
        1 => '%'
    ];
    private int $id;
    /**
     * @OneToMany()
     */
    private ?Product $product;
    private float    $value;
    private int      $currency = 1;
    private bool     $active   = TRUE;
    private ?string  $code;
    private ?array   $rule;
    private string   $created_at;
    private ?string  $rule_translation;
    /**
     * @OneToMany()
     */
    private ?Customer $customer;
    /**
     * @Owner()
     * @OneToOne()
     */
    private ?GraphicRuleReduction $graphic_rule_reduction;
    
    
    public function __construct()
    {
        $this->product                = NULL;
        $this->customer               = NULL;
        $this->graphic_rule_reduction = NULL;
        $this->created_at             = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getProduct(): ?Product
    {
        return $this->product;
    }
    
    
    public function setProduct( ?Product $product ): self
    {
        $this->product = $product;
        
        return $this;
    }
    
    
    public function getValue(): ?float
    {
        return $this->value;
    }
    
    
    /**
     * @param float $value
     * @return $this
     * @throws InvalidParamException
     */
    public function setValue( float $value ): self
    {
        if( $value < 1 ) {
            throw new InvalidParamException( 'La valeur d\'une reduction ne peut pas être inférieure à 1' );
        }
        
        if( $value > 100 ) {
            throw new InvalidParamException( 'La valeur d\'une reduction ne peut pas être supérieure à 100' );
        }
        
        $this->value = $value;
        
        return $this;
    }
    
    
    public function getCurrency(): ?int
    {
        return $this->currency;
    }
    
    
    public function setCurrency( int $currency ): self
    {
        if( !array_key_exists( $currency, self::CURRENCY ) ) {
            throw new InvalidParamException( 'Merci de sélectionner un pourcentage ou l\'euro' );
        }
        
        $this->currency = $currency;
        
        return $this;
    }
    
    
    public function isActive(): ?bool
    {
        return $this->active;
    }
    
    
    public function setActive( bool $active ): self
    {
        $this->active = $active;
        
        return $this;
    }
    
    
    public function getCode(): ?string
    {
        return $this->code;
    }
    
    
    public function setCode( ?string $code ): self
    {
        
        if( !empty( $code ) && strlen( $code ) > 255 ) {
            throw new InvalidParamException( 'Le code promotionnel d\'une reduction ne peut pas dépasser 255 caractères' );
        }
        
        $this->code = $code;
        
        return $this;
    }
    
    
    public function getRule(): ?array
    {
        return $this->rule;
    }
    
    
    public function setRule( ?array $rule ): self
    {
        $this->rule = $rule;
        
        return $this;
    }
    
    
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getRuleTranslation(): ?string
    {
        return $this->rule_translation;
    }
    
    
    public function setRuleTranslation( ?string $rule_translation ): self
    {
        $this->rule_translation = $rule_translation;
        
        return $this;
    }
    
    
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }
    
    
    public function setCustomer( ?Customer $customer ): self
    {
        $this->customer = $customer;
        
        return $this;
    }
    
    
    public function getGraphicRule(): ?GraphicRuleReduction
    {
        return $this->graphic_rule_reduction;
    }
    
    
    public function setGraphicRule( ?GraphicRuleReduction $graphicRuleReduction ): self
    {
        if( is_null( $graphicRuleReduction ) ) {
            $this->graphic_rule_reduction = NULL;
            
            return $this;
        }
        
        $this->addDependencyTo( $graphicRuleReduction, 'reduction', 'graphic_rule_reduction' )
            ->graphic_rule_reduction = $graphicRuleReduction;
        
        return $this;
    }
}
