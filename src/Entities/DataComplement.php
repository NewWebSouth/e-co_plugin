<?php

namespace App\Entities;

use Newwebsouth\Abstraction\Entity\AbstractEntity;
use Nomess\Annotations\ManyToMany;
use Nomess\Exception\InvalidParamException;

class DataComplement extends AbstractEntity
{
    
    private int    $id;
    private string $title;
    private string $content;
    private string $created_at;
    /**
     * @ManyToMany()
     * @var Product[]
     */
    private array $products;
    
    
    public function __construct()
    {
        $this->created_at = date( 'Y-m-d H:i:s' );
    }
    
    
    public function getId(): int
    {
        return $this->id;
    }
    
    
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    
    /**
     * @param string|null $title
     * @return $this
     * @throws InvalidParamException
     */
    public function setTitle( ?string $title ): self
    {
        
        if( empty( $title ) ) {
            throw new InvalidParamException( 'Une donnée complémentaire doit avoir un titre' );
        } elseif( strlen( $title ) > 255 ) {
            throw new InvalidParamException( 'Le titre d\'une donnée complémentaire ne peut pas dépasser 255 caractères' );
        }
        
        $this->title = $title;
        
        return $this;
    }
    
    
    public function getContent(): ?string
    {
        return $this->content;
    }
    
    
    /**
     * @param string|null $content
     * @return $this
     * @throws InvalidParamException
     */
    public function setContent( ?string $content ): self
    {
        
        if( empty( $content ) ) {
            throw new InvalidParamException( 'Une donnée complémentaire doit avoir une valeur' );
        } elseif( strlen( $content ) > 255 ) {
            throw new InvalidParamException( 'La valeur d\'une donnée complémentaire ne peut pas dépasser 500 caractères' );
        }
        
        
        $this->content = $content;
        
        return $this;
    }
    
    
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }
    
    
    public function setCreatedAt( string $created_at ): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    
    public function getProducts(): array
    {
        return $this->products;
    }
    
    
    public function addProduct( Product $product ): self
    {
        if( !$this->arrayContainsValue( $product, $this->products ) ) {
            $this->addDependencyTo( $product, 'data_complements', 'products' )
                ->products[] = $product;
        }
        
        return $this;
    }
    
    
    public function removeProduct( Product $product ): self
    {
        $key = $this->indexOf( $product, $this->products );
        
        if( !is_null( $key ) ) {
            $this->removeDependencyTo( $product, 'data_complements' );
            unset( $this->products[$key] );
        }
        
        return $this;
    }
}
