<?php


namespace App\Packages\Administer\Account;


use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Helpers\DataHelper;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpSession;

class SLogin extends AbstractCrud implements ServiceInterface
{
    
    use DataHelper;
    
    private const DC_USERNAME    = 'login_username';
    private const DC_PASSWORD    = 'login_password';
    private const PARAM_EMAIL    = 'email';
    private const PARAM_PASSWORD = 'password';
    private const SESSION_ADMIN  = 'admin';
    private HttpSession $session;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        HttpSession $session )
    {
        parent::__construct( $entityManager );
        $this->session = $session;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     * @throws InvalidParamException
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        $email    = (string)$request->getParameter( self::PARAM_EMAIL );
        $password = (string)$request->getParameter( self::PARAM_PASSWORD );
        
        if( $this->isValidPassword( $password ) && $this->isValidEmail( $email ) ) {
            $this->session->installSecurityModules( TRUE, TRUE, TRUE, FALSE );
            $this->session->set( self::SESSION_ADMIN, TRUE );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'une erreur s\'est produite' );
        
        return FALSE;
    }
    
    
    private function isValidPassword( string $password ): bool
    {
        return password_verify( $password, $this->get( self::DC_PASSWORD ) );
    }
    
    
    private function isValidEmail( string $email ): bool
    {
        return $this->get( self::DC_USERNAME ) === $email;
    }
}
