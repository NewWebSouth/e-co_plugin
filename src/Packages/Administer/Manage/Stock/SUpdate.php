<?php


namespace App\Packages\Administer\Manage\Stock;


use App\Exception\DuplicationException;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractStock implements UpdateInterface
{
    
    private StockRepository $stockRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager );
        $this->stockRepository = $stockRepository;
    }
    
    
    public function update( HttpRequest $request, object $instance ): bool
    {
        
        try {
            $this->validData( $request, $instance );
        } catch( DuplicationException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Stock mis à jour' );
        
        return TRUE;
    }
    
    
    /**
     * Valid that this name is available
     *
     * @param string $nameStock
     * @throws DuplicationException
     */
    protected function validUniqueStock( string $nameStock ): void
    {
        $stock = $this->stockRepository->findByName( $nameStock );
        
        if( $stock !== NULL ) {
            throw new DuplicationException( 'Ce nom est déjà prit' );
        }
    }
}
