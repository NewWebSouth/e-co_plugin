<?php


namespace App\Packages\Administer\Manage\Stock;


use App\Exception\DuplicationException;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractStock implements CreateInterface
{
    
    private StockRepository $stockRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager );
        $this->stockRepository = $stockRepository;
    }
    
    
    public function create( HttpRequest $request, object $instance ): bool
    {
        
        try {
            $this->validData( $request, $instance );
        } catch( DuplicationException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Stock créé' );
        
        return TRUE;
    }
    
    
    /**
     * @param string $name
     * @throws DuplicationException
     */
    protected function validUniqueStock( string $name ): void
    {
        if( $this->stockRepository->findByName( $name ) !== NULL ) {
            throw new DuplicationException( 'Ce nom est déjà pris' );
        }
    }
}
