<?php


namespace App\Packages\Administer\Manage\Stock;


use App\Entities\Stock;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractFrontNotifier implements DeleteInterface
{
    
    public function delete( HttpRequest $request, object $instance ): bool
    {
        /** @var Stock $instance */
        
        $this->notifyFront();
        $this->entityManager->delete( $instance );
        $this->setRepository( self::SUCCESS, 'Stock supprimé' );
        
        return TRUE;
    }
}
