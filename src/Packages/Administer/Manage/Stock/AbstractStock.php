<?php


namespace App\Packages\Administer\Manage\Stock;


use App\Entities\Stock;
use App\Exception\DuplicationException;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

abstract class AbstractStock extends AbstractFrontNotifier
{

    private const PARAM_NAME        = 'name';

    private const PARAM_QUANTITY    = 'quantity';

    private const PARAM_DESCRIPTION = 'description';


    /**
     * @param HttpRequest $request
     * @param object $instance
     * @throws DuplicationException
     * @throws InvalidParamException
     */
    protected function validData( HttpRequest $request, $instance ): void
    {

        $name        = (string)$request->getParameter( self::PARAM_NAME );
        $quantity    = (float)$request->getParameter( self::PARAM_QUANTITY );
        $description = (string)$request->getParameter( self::PARAM_DESCRIPTION );

        /** @var Stock $instance */

        $instance->setName( $name );
        $instance->setQuantity( $quantity );
        $instance->setDescription( $description );

        $this->validUniqueStock( $name );
    }


    /**
     * Valid that this name is available
     *
     * @param string $nameStock
     * @throws DuplicationException
     */
    abstract protected function validUniqueStock( string $nameStock ): void;
}
