<?php


namespace App\Packages\Administer\Manage\Purchase;


use App\Entities\Purchase;
use App\Entities\Stock;
use App\Packages\Administer\Tools\System\Document\UploadDocumentInterface;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractCrud implements CreateInterface
{
    
    private const PARAM_PROVIDER       = 'provider';
    private const PARAM_DATE           = 'date';
    private const PARAM_TOTAL          = 'total';
    private const PARAM_DOCUMENT       = 'document';
    private const PARAM_STOCKS         = 'stocks';
    private const PARAM_STOCK_QUANTITY = 'stock_quantity';
    private UploadDocumentInterface $uploadDocument;
    private StockRepository         $stockRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadDocumentInterface $uploadDocument,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager );
        $this->uploadDocument  = $uploadDocument;
        $this->stockRepository = $stockRepository;
    }
    
    
    /**
     * Create a purchase
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        $document = (array)$request->getPart( self::PARAM_DOCUMENT );
        $provider = (string)$request->getParameter( self::PARAM_PROVIDER );
        $total    = (float)$request->getParameter( self::PARAM_TOTAL );
        $date     = (string)$request->getParameter( self::PARAM_DATE );
        $stocks   = (array)$request->getParameter( self::PARAM_STOCKS );
        $quantity = (array)$request->getParameter( self::PARAM_STOCK_QUANTITY );
        /** @var Purchase $instance */
        
        
        $instance->setTotal( $total )
                 ->setProvider( $provider )
                 ->setDate( date( 'd/m/Y', strtotime( $date ) ) );
        
        $this->attachDocument( $document, $instance );
        try {
            $this->attachStock( $stocks, $quantity, $instance );
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $instance );
        
        
        return TRUE;
    }
    
    
    /**
     * Attache one or many documents to purchases (if exists)
     *
     * @param array $parts
     * @param Purchase $instance
     */
    private function attachDocument( array $parts, Purchase $instance ): void
    {
        
        if( empty( $parts ) ) {
            return;
        }
        
        $parts = $this->uploadDocument->convertToMultipleArray( $parts );
        
        foreach( $parts as $part ) {
            $instance->addDocument( $this->uploadDocument->getDocument( $part, 'purchase' ) );
        }
    }
    
    
    /**
     * @param array $stocks
     * @param array $quantity
     * @param Purchase $instance
     * @throws NullPointerException
     */
    private function attachStock( array $stocks, array $quantity, Purchase $instance ): void
    {
        if( empty( $stocks ) || empty( $stocks[0] ) ) {
            return;
        }
        
        $iteration = count( $stocks );
        
        for( $i = 0; $i < $iteration; $i++ ) {
            /** @var Stock $stock */
            $stock = $this->stockRepository->findById( $stocks[$i], isset( $quantity[$i] ) );
            
            if( $stock === NULL ) {
                throw new NullPointerException( 'L\'un des stocks que vous tentez d\'attacher n\'a pas été trouvé' );
            }
            
            if( isset( $quantity[$i] ) ) {
                $stock->add( $quantity[$i] );
            }
            
            $instance->addStock( $stock );
        }
    }
}
