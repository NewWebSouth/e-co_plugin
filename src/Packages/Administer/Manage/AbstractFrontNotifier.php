<?php


namespace App\Packages\Administer\Manage;


use App\Packages\Website\Cache\FrontObserverInterface;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Annotations\Inject;

class AbstractFrontNotifier extends AbstractCrud implements FrontSubjectInterface
{
    
    protected FrontObserverInterface $frontObserver;
    
    
    /**
     * @Inject()
     * @param FrontObserverInterface $frontObserver
     */
    public function attachFront( FrontObserverInterface $frontObserver ): void
    {
        $this->frontObserver = $frontObserver;
    }
    
    
    /**
     * Notify front that the data has be updated
     * The front system will be purge her cache
     */
    public function notifyFront(): void
    {
        $this->frontObserver->notified();
    }
}
