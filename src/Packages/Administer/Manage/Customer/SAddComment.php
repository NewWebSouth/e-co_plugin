<?php


namespace App\Packages\Administer\Manage\Customer;


use App\Entities\Customer;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

class SAddComment extends AbstractCrud implements ServiceInterface
{
    
    private const PARAM_COMMENT = 'comment';
    
    
    /**
     * Add comment to customer
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        $comment = (string)$request->getParameter( self::PARAM_COMMENT );
        
        /** @var Customer $instance */
        
        $instance->setComment( $comment );
        
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
}
