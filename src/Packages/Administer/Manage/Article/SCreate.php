<?php


namespace App\Packages\Administer\Manage\Article;


use App\Entities\Article;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractFrontNotifier implements CreateInterface
{
    
    private const PARAM_TITLE   = 'title';
    private const PARAM_TYPE    = 'type';
    private const PARAM_CONTENT = 'content';
    private const PARAM_IMAGE   = 'image';
    private UploadImageInterface $uploadManager;
    
    
    /**
     * @param EntityManagerInterface $entityManager parent provider
     * @param UploadImageInterface $uploadManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploadManager = $uploadManager;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        
        //Get parameters
        $title   = (string)$request->getParameter( self::PARAM_TITLE );
        $type    = (int)$request->getParameter( self::PARAM_TYPE );
        $content = (string)$request->getParameter( self::PARAM_CONTENT );
        $image   = (array)$request->getPart( self::PARAM_IMAGE );
        
        /** @var Article $instance */
        
        //Trying to setting the article
        try {
            $instance->setTitle( $title );
            $instance->setType( $type );
            $instance->setContent( $content );
        } catch( InvalidParamException $exception ) {
            $this->setRepository( self::ERROR, $exception->getMessage() );
            
            return FALSE;
        }
        
        //Get image
        if( !empty( $image ) ) {
            $imageInstance = $this->uploadManager->getImage( $image );
            $instance->setImage( $imageInstance );
        }
        
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        
        $this->setRepository( self::SUCCESS, 'Article créé avec succès' );
        
        return TRUE;
    }
}
