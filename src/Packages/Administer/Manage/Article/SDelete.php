<?php


namespace App\Packages\Administer\Manage\Article;

use App\Entities\Article;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\CleanerImageInterface;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;


class SDelete extends AbstractFrontNotifier implements DeleteInterface, CleanerImageInterface
{
    
    private UploadImageInterface $uploaderManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager = $uploadManager;
    }
    
    
    /**
     * Purge image of article and delete it
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function delete( HttpRequest $request, object $instance ): bool
    {
        
        $this->purgeImage( $instance );
        
        $this->notifyFront();
        $this->entityManager->delete( $instance );
        
        $this->setRepository( self::SUCCESS, 'Article supprimé' );
        
        return TRUE;
    }
    
    
    /**
     * @param object $instance
     */
    public function purgeImage( object $instance ): void
    {
        /** @var Article $instance */
        
        if( $instance->getImage() !== NULL ) {
            $this->uploaderManager->cleanImageRepository( $instance->getImage() );
            $instance->setImage( NULL );
        }
    }
}
