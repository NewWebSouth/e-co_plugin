<?php
/** @noinspection DuplicatedCode */


namespace App\Packages\Administer\Manage\Article;

use App\Entities\Article;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\CleanerImageInterface;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractFrontNotifier implements UpdateInterface, CleanerImageInterface
{
    
    private const PARAM_TITLE   = 'title';
    private const PARAM_TYPE    = 'type';
    private const PARAM_CONTENT = 'content';
    private const PARAM_IMAGE   = 'image';
    private const PARAM_DETACH  = 'detach';
    private UploadImageInterface $uploaderManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager = $uploadManager;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function update( HttpRequest $request, object $instance ): bool
    {
        
        //Get parameters
        $title   = (string)$request->getParameter( self::PARAM_TITLE );
        $type    = (int)$request->getParameter( self::PARAM_TYPE );
        $content = (string)$request->getParameter( self::PARAM_CONTENT );
        $detach  = (string)$request->getParameter( self::PARAM_DETACH );
        $image   = (array)$request->getPart( self::PARAM_IMAGE );
        
        
        /** @var Article $instance */
        
        
        //Trying to setting the article
        try {
            $instance->setTitle( $title );
            $instance->setType( $type );
            $instance->setContent( $content );
        } catch( InvalidParamException $exception ) {
            $this->setRepository( self::ERROR, $exception->getMessage() );
            
            return FALSE;
        }
        
        
        // Detach the image
        if( !empty( $detach ) ) {
            $this->purgeImage( $instance );
        }
        
        //Get image
        if( !empty( $image ) ) {
            $this->purgeImage( $instance );
            $instance->setImage( $this->uploaderManager->getImage( $image ) );
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
    
    
    /**
     * Remove image of the section and launch cleaner
     *
     * @param object $article
     */
    public function purgeImage( object $article ): void
    {
        
        /** @var Article $article */
        if( $article->getImage() !== NULL ) {
            $this->uploaderManager->cleanImageRepository( $article->getImage() );
            $article->setImage( NULL );
        }
    }
}
