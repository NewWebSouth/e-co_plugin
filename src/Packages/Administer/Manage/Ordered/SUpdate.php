<?php


namespace App\Packages\Administer\Manage\Ordered;


use App\Entities\Ordered;
use App\Entities\ProductCustomer;
use App\Entities\Reduction;
use App\Entities\ReductionCustomer;
use App\Packages\Administer\Tools\Mail\Initializer\SentMailInterface;
use App\Packages\Cunsumption\Stock\StockRollbackInterface;
use App\Repositories\ReductionCustomerRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Helpers\ArrayHelper;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractCrud implements UpdateInterface
{
    
    use ArrayHelper;
    
    private const PARAM_ADDRESS_BILLING_NUMBER_STREET  = 'address_billing_number_street';
    private const PARAM_ADDRESS_BILLING_STREET         = 'address_billing_street';
    private const PARAM_ADDRESS_BILLING_CITY           = 'address_billing_city';
    private const PARAM_ADDRESS_BILLING_POSTAL_CODE    = 'address_billing_postal_code';
    private const PARAM_ADDRESS_BILLING_COUNTRY        = 'address_billing_country';
    private const PARAM_ADDRESS_DELIVERY_NUMBER_STREET = 'address_delivery_number_street';
    private const PARAM_ADDRESS_DELIVERY_STREET        = 'address_delivery_street';
    private const PARAM_ADDRESS_DELIVERY_CITY          = 'address_delivery_city';
    private const PARAM_ADDRESS_DELIVERY_POSTAL_CODE   = 'address_delivery_postal_code';
    private const PARAM_ADDRESS_DELIVERY_COUNTRY       = 'address_delivery_country';
    private const PARAM_QUANTITY                       = 'quantity_product_';
    private const PARAM_STATUS                         = 'status';
    private const PARAM_ADD_REDUCTION                  = 'add_reduction_';
    private const PARAM_REMOVE_REDUCTION               = 'remove_reduction_';
    private const PARAM_REMOVE_PRODUCT                 = 'remove_product_';
    private StockRollbackInterface      $stockRollback;
    private ReductionCustomerRepository $reductionCustomerRepository;
    private SentMailInterface           $sentMail;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        StockRollbackInterface $stockRollback,
        ReductionCustomerRepository $reductionCustomerRepository,
        SentMailInterface $sentMail )
    {
        parent::__construct( $entityManager );
        $this->stockRollback               = $stockRollback;
        $this->reductionCustomerRepository = $reductionCustomerRepository;
        $this->sentMail                    = $sentMail;
    }
    
    
    /**
     * Update the ordered (only by admin)
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function update( HttpRequest $request, object $instance ): bool
    {
        $adNumberStreet = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_NUMBER_STREET );
        $adStreet       = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_STREET );
        $adPostalCode   = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_POSTAL_CODE );
        $adCity         = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_CITY );
        $adCountry      = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_COUNTRY );
        $abNumberStreet = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_NUMBER_STREET );
        $abStreet       = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_STREET );
        $abPostalCode   = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_POSTAL_CODE );
        $abCity         = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_CITY );
        $abCountry      = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_COUNTRY );
        $status         = (int)$request->getParameter( self::PARAM_STATUS );
        
        /** @var Ordered $instance */
        if( !$this->isAlterable( $instance ) ) {
            $this->setRepository( self::ERROR, 'Vous ne pouvez plus modifier cette commande' );
            
            return FALSE;
        }
        
        try {
            $this->updateProductCustomers( $instance, $request );
            
            $instance->setAddressDelivery( $this->resolveAddress( $adNumberStreet, $adStreet, $adPostalCode, $adCity, $adCountry ) )
                     ->setAddressBilling( $this->resolveAddress( $abNumberStreet, $abStreet, $abPostalCode, $abCity, $abCountry ) )
                     ->setStatus( $this->validStatus( $status, $instance ) )
                     ->setTotal()
                     ->setNetToPay();
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
    
    
    /**
     * Return the good address for billing or delivery
     *
     * @param $numberStreet
     * @param $street
     * @param $postalCode
     * @param $city
     * @param $country
     * @return string
     * @throws InvalidParamException
     */
    private function resolveAddress( $numberStreet, $street, $postalCode, $city, $country ): string
    {
        if( empty( $numberStreet ) || empty( $street ) || empty( $postalCode ) || empty( $city ) || empty( $country ) ) {
            throw new InvalidParamException( 'Merci de saisir une adresse valide' );
        }
        
        return "$numberStreet $street<br>$postalCode $city<br>$country";
    }
    
    
    /**
     * If order is disabled, the stocks are redistributed
     * If status change to 3, send mail to customer
     *
     * @param int $status
     * @param Ordered $instance
     * @return int
     */
    private function validStatus( int $status, Ordered $instance ): int
    {
        if( $status === 5 && $instance->getStatus() !== 5 ) {
            $this->stockRollback->rollback( $instance );
        }
        
        if( $status === 3 && $instance->getStatus() !== 3 ) {
            $this->sentMail->send( $instance );
        }
        
        return $status;
    }
    
    
    /**
     * @param ProductCustomer[] $productCustomers
     * @param Ordered $instance
     * @param HttpRequest $request
     * @throws InvalidParamException
     */
    private function updateProductCustomers( Ordered $instance, HttpRequest $request ): void
    {
        foreach( $instance->getProductCustomer() as $productCustomer ) {
            $remove = (bool)$request->getParameter( self::PARAM_REMOVE_PRODUCT . $productCustomer->getId() );
            
            if( $remove ) {
                $instance->removeProductCustomer( $productCustomer );
                $this->entityManager->delete( $productCustomer );
            }
            
            $quantity = (int)$request->getParameter( self::PARAM_QUANTITY . $productCustomer->getId() );
            
            if( $quantity > 0 ) {
                $productCustomer->setQuantity( $quantity );
            }
            
            $addReduction = (float)$request->getParameter( self::PARAM_ADD_REDUCTION . $productCustomer->getId() );
            
            if( $addReduction > 0 ) {
                $reduction         = ( new Reduction() )->setValue( $addReduction )
                                                        ->setCustomer( $productCustomer->getCustomer() )
                                                        ->setProduct( $productCustomer->getProduct() )
                                                        ->setActive( FALSE );
                $reductionCustomer = ( new ReductionCustomer() )->setReduction( $reduction );
                
                $productCustomer->addReductionCustomers( $reductionCustomer );
            }
            
            $removeReductions = (array)$request->getParameter( self::PARAM_REMOVE_REDUCTION . $productCustomer->getId() );
            
            if( !empty( $removeReductions ) ) {
                foreach( $removeReductions as $reductionId ) {
                    $reduction = $this->reductionCustomerRepository->findById( (int)$reductionId );
                    
                    if( $this->arrayContainsValue( $reduction, $productCustomer->getReductionCustomers() ) ) {
                        $productCustomer->removeReductionCustomers( $reduction );
                        $this->entityManager->delete( $reduction );
                    }
                }
            }
            
            $productCustomer->setTotal()
                            ->setNetToPay();
        }
    }
    
    
    /**
     * If status is superior to 0, sh'es not alterable
     *
     * @param Ordered $ordered
     * @return bool
     */
    private function isAlterable( Ordered $ordered ): bool
    {
        return $ordered->getStatus() === 0;
    }
}
