<?php


namespace App\Packages\Administer\Manage\Ordered;


use App\Entities\Ordered;
use App\Packages\Cunsumption\Stock\StockRollbackInterface;
use App\Repositories\OrderedRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;

class SRevalide
{
    
    private EntityManagerInterface $entityManager;
    private StockRollbackInterface $stockRollback;
    private OrderedRepository      $orderedRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        StockRollbackInterface $stockRollback,
        OrderedRepository $orderedRepository )
    {
        $this->entityManager     = $entityManager;
        $this->stockRollback     = $stockRollback;
        $this->orderedRepository = $orderedRepository;
    }
    
    
    /**
     * Abort the orders passed two days ago
     */
    public function revalide(): void
    {
        $orders = $this->orderedRepository->findAllInWaitingPayment();
        
        if( !empty( $orders ) ) {
            /** @var Ordered $order */
            foreach( $orders as $order ) {
                if( strtotime( $order->getCreatedAt() ) + ( 60 * 60 * 24 * 2 ) <= time() ) {
                    $order->setStatus( 5 );//Avorté
                    $this->stockRollback->rollback( $order );
                    $this->entityManager->persists( $order );
                }
            }
        }
    }
}
