<?php


namespace App\Packages\Administer\Manage\Section;


use App\Entities\Section;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\CleanerImageInterface;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractFrontNotifier implements DeleteInterface, CleanerImageInterface
{
    
    private UploadImageInterface $uploaderManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager = $uploadManager;
    }
    
    
    /**
     * Try to remove image for clean repository and remove section
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function delete( HttpRequest $request, object $instance ): bool
    {
        $this->purgeImage( $instance );
        
        $this->entityManager->delete( $instance );
        $this->notifyFront();
        $this->setRepository( self::SUCCESS, 'Section supprimé' );
        
        return TRUE;
    }
    
    
    /**
     * Cleaner of image
     *
     * @param object $instance
     */
    public function purgeImage( object $instance ): void
    {
        /** @var Section $instance */
        
        if( $instance->getImage() !== NULL ) {
            $instance->setImage( NULL );
            $this->uploaderManager->cleanImageRepository( $instance->getImage() );
        }
    }
}
