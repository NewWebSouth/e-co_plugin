<?php


namespace App\Packages\Administer\Manage\Section;


use App\Entities\Section;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\NullPointerException;

abstract class AbstractSection extends AbstractFrontNotifier
{
    
    protected const PARAM_NAME        = 'name';
    protected const PARAM_DESCRIPTION = 'description';
    protected const PARAM_PRIORITY    = 'priority';
    protected const PARAM_IMAGE       = 'image';
    protected const PARAM_PARENT      = 'parent';
    protected UploadImageInterface     $uploaderManager;
    protected SectionRepository        $sectionRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager,
        SectionRepository $sectionRepository )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager   = $uploadManager;
        $this->sectionRepository = $sectionRepository;
    }
    
    
    protected function attachImage( array $part, Section $instance ): void
    {
        $instance->setImage( $this->uploaderManager->getImage( $part ) );
    }
    
    
    /**
     * Valid that an section have the id received and the id is ok
     *
     * @param int $parent
     * @return Section|null
     * @throws NullPointerException
     */
    protected function validParent( int $parent ): ?Section
    {
        if( $this->sectionRepository->findById( $parent ) !== NULL ) {
            return $this->sectionRepository->findById( $parent );
        }
        
        if( $parent !== 0 ) {
            throw new NullPointerException( 'La section parente spécifié n\'a pas été trouvé' );
        }
        
        return NULL;
    }
}
