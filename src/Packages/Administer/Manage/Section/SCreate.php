<?php


namespace App\Packages\Administer\Manage\Section;


use App\Entities\Section;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractSection implements CreateInterface
{
    
    /**
     * @param HttpRequest $request
     * @param object $section
     * @return bool
     */
    public function create( HttpRequest $request, object $section ): bool
    {
        $name     = (string)$request->getParameter( self::PARAM_NAME );
        $priority = (int)$request->getParameter( self::PARAM_PRIORITY );
        $parent   = (int)$request->getParameter( self::PARAM_PARENT );
        $image    = (array)$request->getPart( self::PARAM_IMAGE );
        
        /** @var Section $section */
        
        try {
            $section->setName( $name );
            $section->setPriority( $priority );
            $section->setParent( $this->validParent( $parent ) );
        } catch( InvalidParamException $exception ) {
            $this->setRepository( self::ERROR, $exception->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        if( !empty( $image ) ) {
            $this->attachImage( $image, $section );
        }
        
        $this->setRepository( self::SUCCESS, 'Section créé avec succès' );
        
        $this->notifyFront();
        $this->entityManager->persists( $section );
        
        return TRUE;
    }
}
