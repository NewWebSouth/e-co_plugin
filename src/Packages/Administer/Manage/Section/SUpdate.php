<?php
/** @noinspection DuplicatedCode */


namespace App\Packages\Administer\Manage\Section;


use App\Entities\Section;
use App\Packages\Administer\Tools\System\Image\CleanerImageInterface;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractSection implements UpdateInterface, CleanerImageInterface
{
    
    private const PARAM_DETACH = 'detach';
    
    
    /**
     * @param HttpRequest $request
     * @param object $section
     * @return bool
     */
    public function update( HttpRequest $request, object $section ): bool
    {
        $name     = (string)$request->getParameter( self::PARAM_NAME );
        $priority = (int)$request->getParameter( self::PARAM_PRIORITY );
        $parent   = (int)$request->getParameter( self::PARAM_PARENT );
        $detach   = (string)$request->getParameter( self::PARAM_DETACH );
        $image    = (array)$request->getPart( self::PARAM_IMAGE );
        
        /** @var Section $section */
        
        try {
            $section->setPriority( $priority );
            $section->setName( $name );
            $section->setParent( $this->validParent( $parent ) );
        } catch( InvalidParamException $exception ) {
            $this->setRepository( self::ERROR, $exception->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        if( !empty( $detach ) ) {
            $this->purgeImage( $section );
        }
        
        if( !empty( $image ) ) {
            
            $this->purgeImage( $section );
            
            $this->attachImage( $image, $section );
        }
        
        
        $this->setRepository( self::SUCCESS, 'Section mise à jour' );
        
        $this->notifyFront();
        $this->entityManager->persists( $section );
        
        return TRUE;
    }
    
    
    /**
     * Remove the image of section and launch the cleaner
     *
     * @param object $section
     */
    public function purgeImage( object $section ): void
    {
        
        /** @var Section $section */
        
        if( $section->getImage() !== NULL ) {
            $this->uploaderManager->cleanImageRepository( $section->getImage() );
            $section->setImage( NULL );
        }
    }
}
