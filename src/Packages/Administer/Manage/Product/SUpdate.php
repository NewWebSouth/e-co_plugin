<?php
/** @noinspection DuplicatedCode */


namespace App\Packages\Administer\Manage\Product;


use App\Entities\Product;
use App\Exception\InternalPackageException;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractProduct implements UpdateInterface
{
    
    private const PARAM_DESCRIPTION     = 'description';
    private const PARAM_PRICE           = 'price';
    private const PARAM_OPTION_REQUIRED = 'option_required';
    private const PARAM_DETACH_STOCK    = 'detach_stock';
    private DependencyFollowInterface $updateDataCustomer;
    private DependencyFollowInterface $updateDataComplement;
    private DependencyFollowInterface $createOptions;
    private DependencyFollowInterface $deleteOptions;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        DependencyFollowInterface $updateDataCustomer,
        DependencyFollowInterface $updateDataComplement,
        DependencyFollowInterface $createOptions,
        DependencyFollowInterface $deleteOptions,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager, $stockRepository );
        $this->updateDataComplement = $updateDataComplement;
        $this->updateDataCustomer   = $updateDataCustomer;
        $this->createOptions        = $createOptions;
        $this->deleteOptions        = $deleteOptions;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function update( HttpRequest $request, object $instance ): bool
    {
        $description     = (string)$request->getParameter( self::PARAM_DESCRIPTION );
        $price           = (float)$request->getParameter( self::PARAM_PRICE );
        $option_required = (bool)$request->getParameter( self::PARAM_OPTION_REQUIRED );
        
        /** @var Product $instance */
        
        try {
            $instance->setDescription( $description );
            $instance->setPrice( $price );
            $this->detachStock( $instance, $request );
            $this->addStock( $instance, $request );
            $instance->setOptionRequired( $option_required );
            $this->updateDataComplement->follow( $instance, $request );
            $this->updateDataCustomer->follow( $instance, $request );
            $this->createOptions->follow( $instance, $request );
            $this->deleteOptions->follow( $instance, $request );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InternalPackageException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Le produit a été mit à jour' );
        
        return TRUE;
    }
    
    
    private function detachStock( Product $instance, HttpRequest $request ): void
    {
        $detachStock = (array)$request->getParameter( self::PARAM_DETACH_STOCK );
        
        if( !empty( $detachStock ) ) {
            foreach( $detachStock as $stockId ) {
                $stock = $this->stockRepository->findById( $stockId );
                
                if( !is_null( $stock ) ) {
                    $instance->removeStock( $stock );
                } else {
                    throw new NullPointerException( 'L\'un des stocks que vous tentez de détacher n\'a pas été trouvé' );
                }
            }
        }
    }
}
