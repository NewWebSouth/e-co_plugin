<?php


namespace App\Packages\Administer\Manage\Product;


use App\Entities\Product;
use App\Exception\InternalPackageException;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractProduct implements CreateInterface
{
    
    private const PARAM_NAME            = 'name';
    private const PARAM_DESCRIPTION     = 'description';
    private const PARAM_PRICE           = 'price';
    private const PARAM_OPTION_REQUIRED = 'option_required';
    private DependencyFollowInterface $createDataCustomer;
    private DependencyFollowInterface $createDataComplement;
    private DependencyFollowInterface $createOptions;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        DependencyFollowInterface $createDataCustomer,
        DependencyFollowInterface $createDataComplement,
        DependencyFollowInterface $createOptions,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager, $stockRepository );
        $this->createDataCustomer   = $createDataCustomer;
        $this->createDataComplement = $createDataComplement;
        $this->createOptions        = $createOptions;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        $name            = (string)$request->getParameter( self::PARAM_NAME );
        $description     = (string)$request->getParameter( self::PARAM_DESCRIPTION );
        $price           = (float)$request->getParameter( self::PARAM_PRICE );
        $option_required = (bool)$request->getParameter( self::PARAM_OPTION_REQUIRED );
        
        /** @var Product $instance */
        
        try {
            $instance->setName( $name );
            $instance->setDescription( $description );
            $instance->setPrice( $price );
            $instance->setOptionRequired( $option_required );
            $this->addStock( $instance, $request );
            $this->createOptions->follow( $instance, $request );
            $this->createDataCustomer->follow( $instance, $request );
            $this->createDataComplement->follow( $instance, $request );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InternalPackageException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Produit créé' );
        
        return TRUE;
    }
}
