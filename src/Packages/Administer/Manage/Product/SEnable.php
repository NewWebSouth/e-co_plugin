<?php


namespace App\Packages\Administer\Manage\Product;


use App\Entities\Product;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

class SEnable extends AbstractFrontNotifier implements ServiceInterface
{
    
    public function service( HttpRequest $request, object $instance ): bool
    {
        
        /** @var Product $instance */
        if( $instance->isActive() === FALSE ) {
            $instance->setActive( TRUE );
            
            $this->notifyFront();
            $this->entityManager->persists( $instance );
            $this->setRepository( self::SUCCESS, 'Produit ' . $instance->getName() . ' activé' );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Le produit ' . $instance->getName() . ' est déjà actif' );
        
        return FALSE;
    }
}
