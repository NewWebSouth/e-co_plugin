<?php


namespace App\Packages\Administer\Manage\Product;


use App\Entities\Product;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

abstract class AbstractProduct extends AbstractFrontNotifier
{
    
    private const PARAM_STOCK         = 'stock';
    private const PARAM_STOCK_CONTENT = 'content_stock';
    protected StockRepository $stockRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager );
        $this->stockRepository = $stockRepository;
    }
    
    
    /**
     * Attach the stock associate to product
     * The stock have an composition for deduct many unities by order of product
     *
     * @param Product $instance
     * @param HttpRequest $request
     * @throws NullPointerException
     */
    protected function addStock( Product $instance, HttpRequest $request ): void
    {
        $stocks            = (array)$request->getParameter( self::PARAM_STOCK );
        $compositionsStock = (array)$request->getParameter( self::PARAM_STOCK_CONTENT );
        
        if( !empty( $stocks ) ) {
            $iteration = count( $stocks );
            
            for( $i = 0; $i < $iteration; $i++ ) {
                $stock = $this->stockRepository->findById( $stocks[$i] );
                
                if( !is_null( $stock ) ) {
                    $instance->addStock( $stock );
                    
                    if( isset( $compositionsStock[$i] ) && $compositionsStock[$i] !== 0 ) {
                        $instance->addCompositionStocks( $stock->getId(), $compositionsStock[$i] );
                    }
                } else {
                    throw new NullPointerException( 'L\'un des stocks que vous tentez d\'attacher au produit n\'existe pas' );
                }
            }
        }
    }
}
