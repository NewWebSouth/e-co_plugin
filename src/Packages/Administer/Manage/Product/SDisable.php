<?php


namespace App\Packages\Administer\Manage\Product;


use App\Entities\Product;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

class SDisable extends AbstractFrontNotifier implements ServiceInterface
{
    
    /**
     * Disable an product, and remove her graphic rule (if exists)
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        /** @var Product $instance */
        if( $instance->isActive() === TRUE ) {
            $instance->setActive( FALSE );
            
            $this->notifyFront();
            
            if( $instance->getGraphicRule() !== NULL ) {
                $this->entityManager->delete( $instance->getGraphicRule() );
                $instance->setGraphicRule( NULL );
            }
            
            $this->entityManager->persists( $instance );
            $this->setRepository( self::SUCCESS, 'Produit ' . $instance->getName() . ' désactivé' );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Le produit ' . $instance->getName() . ' est déjà désactivé' );
        
        return FALSE;
    }
}
