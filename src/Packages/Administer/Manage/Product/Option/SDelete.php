<?php


namespace App\Packages\Administer\Manage\Product\Option;


use App\Entities\Product;
use App\Exception\InternalPackageException;
use App\Packages\Administer\Manage\Product\DependencyFollowInterface;
use App\Repositories\OptionRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractCrud implements DependencyFollowInterface
{
    
    private const PARAM_DETACH = 'option_detach';
    private OptionRepository $optionRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        OptionRepository $optionRepository )
    {
        parent::__construct( $entityManager );
        $this->optionRepository = $optionRepository;
    }
    
    
    /**
     * @param Product $product
     * @param HttpRequest $request
     * @return bool
     * @throws InternalPackageException
     */
    public function follow( Product $product, HttpRequest $request ): bool
    {
        $options = (array)$request->getParameter( self::PARAM_DETACH );
        
        if( !empty( $options ) ) {
            foreach( $options as $id ) {
                $option = $this->optionRepository->findById( $id );
                
                if( $options !== NULL ) {
                    $product->removeOption( $option );
                    $this->entityManager->delete( $option );
                } else {
                    throw new InternalPackageException( 'Une erreur s\'est produite' );
                }
            }
        }
        
        return TRUE;
    }
}
