<?php


namespace App\Packages\Administer\Manage\Product\Option;


use App\Entities\Option;
use App\Exception\InternalPackageException;
use App\Repositories\StockRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Components\EntityManager\EntityManagerInterface;

abstract class AbstractOption extends AbstractCrud
{
    
    private StockRepository $stockRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        StockRepository $stockRepository )
    {
        parent::__construct( $entityManager );
        $this->stockRepository = $stockRepository;
    }
    
    
    /**
     * Attach the stock associate to product
     * The stock have an composition for deduct many unities by order of product
     *
     * @param int $stockId
     * @param int|null $compositionStock
     * @param Option $instance
     * @throws InternalPackageException
     */
    protected function attachStock( int $stockId, ?int $compositionStock, Option $instance ): void
    {
        $stock = $this->stockRepository->findById( $stockId );
        
        if( !is_null( $stock ) ) {
            $instance->setStock( $stock );
            
            if( !is_null( $compositionStock ) && $compositionStock !== 0 ) {
                $instance->setCompositionStock( $compositionStock );
            }
        } else {
            throw new InternalPackageException( 'L\'un des stocks que vous tentez d\'attacher n\'a pas été trouvé' );
        }
    }
}
