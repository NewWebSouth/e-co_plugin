<?php


namespace App\Packages\Administer\Manage\Product\Option;

use App\Entities\Option;
use App\Exception\InternalPackageException;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractOption implements UpdateInterface
{
    
    private const PARAM_NAME          = 'name';
    private const PARAM_PRICE         = 'price';
    private const PARAM_STOCK         = 'stock';
    private const PARAM_STOCK_CONTENT = 'content_stock';
    
    
    /**
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function update( HttpRequest $request, object $instance ): bool
    {
        $name              = (string)$request->getParameter( self::PARAM_NAME );
        $price             = (float)$request->getParameter( self::PARAM_PRICE );
        $stock             = (int)$request->getParameter( self::PARAM_STOCK );
        $compositionStocks = (int)$request->getParameter( self::PARAM_STOCK_CONTENT );
        
        /** @var Option $instance */
        
        try {
            $instance->setName( $name );
            $instance->setPrice( $price );
            $instance->clearStock();
            $this->attachStock( $stock, $compositionStocks, $instance );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InternalPackageException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Option modifié' );
        
        return TRUE;
    }
}
