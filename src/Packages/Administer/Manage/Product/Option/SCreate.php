<?php


namespace App\Packages\Administer\Manage\Product\Option;


use App\Entities\Option;
use App\Entities\Product;
use App\Exception\InternalPackageException;
use App\Packages\Administer\Manage\Product\DependencyFollowInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractOption implements DependencyFollowInterface
{
    
    private const PARAM_NAME          = 'option_name';
    private const PARAM_PRICE         = 'option_price';
    private const PARAM_STOCK         = 'option_stock';
    private const PARAM_STOCK_CONTENT = 'option_content_stock';
    
    
    /**
     * @param Product $product
     * @param HttpRequest $request
     * @return bool
     * @throws InternalPackageException
     */
    public function follow( Product $product, HttpRequest $request ): bool
    {
        $name              = $request->getParameter( self::PARAM_NAME );
        $price             = (array)$request->getParameter( self::PARAM_PRICE );
        $stocks            = (array)$request->getParameter( self::PARAM_STOCK );
        $compositionStocks = (array)$request->getParameter( self::PARAM_STOCK_CONTENT );
        
        
        if( !empty( $name ) || !empty( $price ) || !empty( $stocks ) ) {
            
            $iteration = count( $name );
            
            for( $i = 0; $i < $iteration; $i++ ) {
                
                $instance = $this->getInstance();
                
                try {
                    $instance->setName( $name[$i] );
                    
                    if( isset( $price[$i] ) ) {
                        $instance->setPrice( (float)$price[$i] );
                    }
                    
                    if( isset( $stocks[$i] ) && (int)$stocks[$i] !== 0 ) {
                        $this->attachStock( (int)$stocks[$i], isset( $compositionStocks[$i] ) ? (int)$compositionStocks[$i] : NULL, $instance );
                    }
                    
                    $instance->setProduct( $product );
                } catch( InvalidParamException $e ) {
                    throw new InternalPackageException( $e->getMessage() );
                }
                
                $product->addOption( $instance );
                $this->entityManager->persists( $instance );
            }
        }
        
        return TRUE;
    }
    
    
    /**
     * @return Option
     */
    private function getInstance(): Option
    {
        return new Option();
    }
}
