<?php


namespace App\Packages\Administer\Manage\Product;


use App\Entities\Product;
use App\Exception\InternalPackageException;
use Newwebsouth\Abstraction\Crud\CrudInterface;
use Nomess\Http\HttpRequest;

interface DependencyFollowInterface extends CrudInterface
{
    
    /**
     * Attach the dependency of product (typically: options, data of customer and data complement)
     *
     * @param Product $product
     * @param HttpRequest $request
     * @return bool
     * @throws InternalPackageException
     */
    public function follow( Product $product, HttpRequest $request ): bool;
}
