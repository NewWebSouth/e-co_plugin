<?php


namespace App\Packages\Administer\Manage\Product\DataCustomer;


use App\Entities\Product;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

abstract class AbstractDataCustomer extends AbstractCrud
{
    
    private const PARAM_NAME    = 'name_data_customer';
    private const PARAM_REQUIRE = 'require_data_customer';
    
    
    /**
     * Attach the data for customer, an field can required or facultative
     *
     * @param Product $product
     * @param HttpRequest $request
     * @throws InvalidParamException
     */
    protected function attachData( Product $product, HttpRequest $request ): void
    {
        $name    = (array)$request->getParameter( self::PARAM_NAME );
        $require = (array)$request->getParameter( self::PARAM_REQUIRE );
        
        
        if( !$this->isValidParameters( $name, $require ) ) {
            $this->throwInvalidParameters();
        }
        
        if( !empty( $name ) || !empty( $require ) ) {
            
            $iteration = count( $name );
            
            for( $i = 0; $i < $iteration; $i++ ) {
                
                // If name is null, we think that isn't error
                $instance = $this->getInstance();
                $instance->setName( $name[$i] );
                
                $instance->setRequired( (bool)$require[$i] );
                
                $product->addDataCustomer( $instance );
                $this->entityManager->persists( $instance );
            }
        }
    }
    
    
    /**
     * @param array $titles
     * @param array $contents
     * @return bool
     */
    private function isValidParameters( array $titles, array $contents ): bool
    {
        if( count( $titles ) !== count( $contents ) ) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    
    /**
     * @throws InvalidParamException
     */
    private function throwInvalidParameters(): void
    {
        throw new InvalidParamException( 'Le nom et la contrainte d\'une donnée client sont obligatoire' );
    }
}

