<?php


namespace App\Packages\Administer\Manage\Product\DataCustomer;

use App\Entities\DataCustomer;
use App\Entities\Product;
use App\Packages\Administer\Manage\Product\DependencyFollowInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractDataCustomer implements DependencyFollowInterface
{
    
    /**
     * Attach the data to product
     *
     * @param Product $product
     * @param HttpRequest $request
     * @return bool
     * @throws InvalidParamException
     */
    public function follow( Product $product, HttpRequest $request ): bool
    {
        $this->attachData( $product, $request );
        
        return TRUE;
    }
    
    
    /**
     * @return DataCustomer
     */
    protected function getInstance(): DataCustomer
    {
        return new DataCustomer();
    }
}
