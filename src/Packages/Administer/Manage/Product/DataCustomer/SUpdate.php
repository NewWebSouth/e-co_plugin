<?php


namespace App\Packages\Administer\Manage\Product\DataCustomer;


use App\Entities\DataCustomer;
use App\Entities\Product;
use App\Exception\InternalPackageException;
use App\Packages\Administer\Manage\Product\DependencyFollowInterface;
use App\Repositories\DataCustomerRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractDataCustomer implements DependencyFollowInterface
{
    
    private const PARAM_DETACH = 'detach_data_customer';
    private DataCustomerRepository $dataCustomerRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        DataCustomerRepository $dataCustomerRepository )
    {
        parent::__construct( $entityManager );
        $this->dataCustomerRepository = $dataCustomerRepository;
    }
    
    
    /**
     * Attach the data to product
     *
     * @param Product $product
     * @param HttpRequest $request
     * @return bool
     * @throws InternalPackageException
     */
    public function follow( Product $product, HttpRequest $request ): bool
    {
        $detach = (array)$request->getParameter( self::PARAM_DETACH );
        
        $this->detachDataCustomer( $detach, $product );
        
        $this->attachData( $product, $request );
        
        return TRUE;
    }
    
    
    /**
     * @param array $idsDataCustomer
     * @param Product $product
     * @throws InternalPackageException
     */
    private function detachDataCustomer( array $idsDataCustomer, Product $product ): void
    {
        if( !empty( $idsDataCustomer ) ) {
            
            foreach( $idsDataCustomer as $id ) {
                $dataCustomer = $this->dataCustomerRepository->findById( $id );
                
                if( $dataCustomer !== NULL ) {
                    $product->removeDataCustomer( $dataCustomer );
                    $this->entityManager->delete( $dataCustomer );
                } else {
                    throw new InternalPackageException( 'L\'une des données clientes que vous tentez de détachez n\'a pas été trouvé' );
                }
            }
        }
    }
    
    
    protected function getInstance(): DataCustomer
    {
        return new DataCustomer();
    }
}
