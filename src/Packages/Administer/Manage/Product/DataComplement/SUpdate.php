<?php


namespace App\Packages\Administer\Manage\Product\DataComplement;


use App\Entities\DataComplement;
use App\Entities\Product;
use App\Exception\InternalPackageException;
use App\Packages\Administer\Manage\Product\DependencyFollowInterface;
use App\Repositories\DataComplementRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractDataComplement implements DependencyFollowInterface
{
    
    private const PARAM_DETACH = 'detach_data_complement';
    private DataComplementRepository $dataComplementRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        DataComplementRepository $dataComplementRepository )
    {
        parent::__construct( $entityManager );
        $this->dataComplementRepository = $dataComplementRepository;
    }
    
    
    /**
     * Attach the data to product
     *
     * @param Product $product
     * @param HttpRequest $request
     * @return bool
     * @throws InternalPackageException
     */
    public function follow( Product $product, HttpRequest $request ): bool
    {
        $detach = (array)$request->getParameter( self::PARAM_DETACH );
        
        
        $this->detach( $detach, $product );
        $this->attachData( $product, $request );
        
        return TRUE;
    }
    
    
    /**
     * @param array $idsDataComplement
     * @param Product $product
     * @throws InternalPackageException
     */
    private function detach( array $idsDataComplement, Product $product ): void
    {
        if( !empty( $idsDataComplement ) ) {
            
            foreach( $idsDataComplement as $id ) {
                $dataComplement = $this->dataComplementRepository->findById( $id );
                
                if( $dataComplement !== NULL ) {
                    $product->removeDataComplement( $dataComplement );
                    $dataComplement->removeProduct( $product );
                    
                    if( empty( $dataComplement->getProducts() ) ) {
                        $this->entityManager->delete( $dataComplement );
                    }
                } else {
                    throw new InternalPackageException( 'L\'une des données complémentaire que vous tentez de détacher n\'a pas été trouvé' );
                }
            }
        }
    }
    
    
    /**
     * @return DataComplement
     */
    protected function getInstance(): DataComplement
    {
        return new DataComplement();
    }
}
