<?php


namespace App\Packages\Administer\Manage\Product\DataComplement;


use App\Entities\Product;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

abstract class AbstractDataComplement extends AbstractCrud
{
    
    private const PARAM_TITLE   = 'title_data_complement';
    private const PARAM_CONTENT = 'content_data_complement';
    
    
    /**
     * Attach the data complement to product
     *
     * @param Product $product
     * @param HttpRequest $request
     * @return void
     * @throws InvalidParamException
     */
    public function attachData( Product $product, HttpRequest $request ): void
    {
        $title   = (array)$request->getParameter( self::PARAM_TITLE );
        $content = (array)$request->getParameter( self::PARAM_CONTENT );
        
        if( !$this->isValidParameters( $title, $content ) ) {
            $this->throwInvalidParameters();
        }
        
        if( !empty( $title ) && !empty( $content ) ) {
            
            $iteration = count( $title );
            
            for( $i = 0; $i < $iteration; $i++ ) {
                
                
                // If title and content is null, an block of form is in excess
                $instance = $this->getInstance();
                
                $instance->setTitle( $title[$i] );
                $instance->setContent( $content[$i] );
                
                $product->addDataComplement( $instance );
                $this->entityManager->persists( $instance );
            }
        }
    }
    
    
    /**
     * @param array $titles
     * @param array $contents
     * @return bool
     */
    private function isValidParameters( array $titles, array $contents ): bool
    {
        if( count( $titles ) !== count( $contents ) ) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    
    /**
     * @throws InvalidParamException
     */
    private function throwInvalidParameters(): void
    {
        throw new InvalidParamException( 'Le titre et contenu d\'une donnée complémentaire sont obligatoire' );
    }
}
