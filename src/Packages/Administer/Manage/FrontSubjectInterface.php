<?php


namespace App\Packages\Administer\Manage;


use App\Packages\Website\Cache\FrontObserverInterface;

interface FrontSubjectInterface
{
    
    public function attachFront( FrontObserverInterface $frontObserver ): void;
    
    
    public function notifyFront(): void;
}
