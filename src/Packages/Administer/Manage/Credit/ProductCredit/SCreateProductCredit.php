<?php


namespace App\Packages\Administer\Manage\Credit\ProductCredit;


use App\Entities\Credit;
use App\Entities\ProductCredit;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SCreateProductCredit implements AttachProductCreditInterface
{
    
    private const PARAM_NAME     = 'name';
    private const PARAM_PRICE    = 'price';
    private const PARAM_QUANTITY = 'quantity';
    private EntityManagerInterface $entityManager;
    
    
    public function __construct( EntityManagerInterface $entityManager )
    {
        $this->entityManager = $entityManager;
    }
    
    
    public function attachProductCredit( HttpRequest $request, Credit $credit ): void
    {
        $name     = (array)$request->getParameter( self::PARAM_NAME );
        $price    = (array)$request->getParameter( self::PARAM_PRICE );
        $quantity = (array)$request->getParameter( self::PARAM_QUANTITY );
        
        for( $i = 0; $i < $this->getIteration( $name, $price, $quantity ); $i++ ) {
            
            if( empty( $name[$i] ) || empty( $price[$i] ) || empty( $quantity[$i] ) ) {
                $this->throwInvalidParameters();
            }
            
            $productCredit = ( new ProductCredit() )
                ->setName( $name[$i] )
                ->setPrice( $price[$i] )
                ->setQuantity( $quantity[$i] )
                ->setNetToPay( $price[$i] * $quantity[$i] )
                ->setCredit( $credit );
            
            $this->entityManager->persists( $productCredit );
        }
    }
    
    
    /**
     * Valid the data and return the number of iterations
     *
     * @param array $name
     * @param array $price
     * @param array $quantity
     * @return int
     * @throws InvalidParamException
     */
    private function getIteration( array $name, array $price, array $quantity ): int
    {
        if( count( $name ) !== count( $price )
            || count( $name ) !== count( $quantity )
            || count( $price ) !== count( $quantity ) ) {
            
            $this->throwInvalidParameters();
        }
        
        return count( $name );
    }
    
    
    /**
     * @throws InvalidParamException
     */
    private function throwInvalidParameters(): void
    {
        throw new InvalidParamException( 'Les champs nom et prix et quantité des produits sont obligatoire' );
    }
}
