<?php


namespace App\Packages\Administer\Manage\Credit\ProductCredit;


use App\Entities\Credit;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

interface AttachProductCreditInterface
{
    
    /**
     * @param HttpRequest $request
     * @param Credit $credit
     * @throws InvalidParamException
     */
    public function attachProductCredit( HttpRequest $request, Credit $credit ): void;
}
