<?php


namespace App\Packages\Administer\Manage\Credit;


use App\Entities\Credit;
use App\Entities\Invoice;
use App\Packages\Administer\Manage\Credit\ProductCredit\AttachProductCreditInterface;
use App\Packages\Administer\Report\PdfGeneratorInterface;
use App\Repositories\InvoiceRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SCreateCredit extends AbstractCrud implements CreateInterface
{
    
    private const PARAM_INVOICE = 'invoice';
    private const PARAM_WAY     = 'way';
    private InvoiceRepository            $invoiceRepository;
    private AttachProductCreditInterface $attachProductCredit;
    private PdfGeneratorInterface        $generator;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        InvoiceRepository $invoiceRepository,
        AttachProductCreditInterface $attachProductCredit,
        PdfGeneratorInterface $generator )
    {
        parent::__construct( $entityManager );
        $this->invoiceRepository   = $invoiceRepository;
        $this->attachProductCredit = $attachProductCredit;
        $this->generator           = $generator;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param Credit|object $instance
     * @return bool
     * @throws InvalidParamException
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        $invoiceId = (int)$request->getParameter( self::PARAM_INVOICE );
        $way       = (string)$request->getParameter( self::PARAM_WAY );
        
        try {
            $invoice = $this->getInvoice( $invoiceId );
            $instance->setCustomer( $invoice->getCustomer() )
                     ->setInvoice( $invoice )
                     ->setWay( $way );
            
            $this->attachProductCredit->attachProductCredit( $request, $instance );
            $instance->setTotal();
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->generator->generate( $instance );
        
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
    
    
    /**
     * @param int $id
     * @return Invoice
     * @throws NullPointerException
     */
    private function getInvoice( int $id ): Invoice
    {
        $invoice = $this->invoiceRepository->findById( $id );
        
        if( empty( $invoice ) ) {
            throw new NullPointerException( 'La facture associé à l\'avoir n\'a pas été trouvé' );
        }
        
        return $invoice;
    }
}
