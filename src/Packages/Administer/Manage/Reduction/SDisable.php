<?php


namespace App\Packages\Administer\Manage\Reduction;


use App\Entities\Reduction;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

class SDisable extends AbstractFrontNotifier implements ServiceInterface
{
    
    /**
     * Disable a reduction and remove graphic rule of reduction (if exists)
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        /** @var Reduction $instance */
        
        if( $instance->isActive() === TRUE ) {
            $instance->setActive( FALSE );
            
            $this->notifyFront();
            $this->entityManager->persists( $instance );
            
            
            if( $instance->getGraphicRule() !== NULL ) {
                $this->entityManager->delete( $instance->getGraphicRule() );
                $instance->setGraphicRule( NULL );
            }
            
            $this->setRepository( self::SUCCESS, 'Reduction désactivé' );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Cette réduction est déjà désactivé' );
        
        return FALSE;
    }
}
