<?php


namespace App\Packages\Administer\Manage\Reduction;


use App\Entities\Reduction;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

class SEnable extends AbstractFrontNotifier implements ServiceInterface
{
    
    public function service( HttpRequest $request, object $instance ): bool
    {
        /** @var Reduction $instance */
        
        if( $instance->isActive() === FALSE ) {
            $instance->setActive( TRUE );
            
            $this->notifyFront();
            $this->entityManager->persists( $instance );
            $this->setRepository( self::SUCCESS, 'Reduction activé' );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Cette réduction est déjà active' );
        
        return FALSE;
    }
}
