<?php
/** @noinspection ALL */


namespace App\Packages\Administer\Manage\Reduction;

use App\Entities\Reduction;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractFrontNotifier implements CreateInterface
{
    
    private const PARAM_CODE          = 'code';
    private const PARAM_VALUE         = 'value';
    private const PARAM_PRODUCT       = 'product';
    private const PARAM_CUSTOMER      = 'id_customer';
    private const PARAM_RULE_INIT     = 'rule_init';
    private const PARAM_RULE_PRODUCT  = 'rule_product';
    private const PARAM_RULE_OPERATOR = 'rule_operator';
    private const PARAM_RULE_VALUE    = 'rule_value';
    private ProductRepository  $productRepository;
    private CustomerRepository $customerRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        CustomerRepository $customerRepository )
    {
        parent::__construct( $entityManager );
        $this->productRepository  = $productRepository;
        $this->customerRepository = $customerRepository;
    }
    
    
    /**
     * Create reduction
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        $code         = (string)$request->getParameter( self::PARAM_CODE );
        $value        = (float)$request->getParameter( self::PARAM_VALUE );
        $product      = (int)$request->getParameter( self::PARAM_PRODUCT );
        $customer     = (int)$request->getParameter( self::PARAM_CUSTOMER );
        $ruleInit     = (string)$request->getParameter( self::PARAM_RULE_INIT );
        $ruleProduct  = (int)$request->getParameter( self::PARAM_RULE_PRODUCT );
        $ruleOperator = (string)$request->getParameter( self::PARAM_RULE_OPERATOR );
        $ruleValue    = (string)$request->getParameter( self::PARAM_RULE_VALUE );
        
        
        /** @var Reduction $instance */
        
        try {
            $instance->setCode( $code );
            $instance->setValue( $value );
            
            $this->attachProduct( $product, $instance );
            $this->attachCustomer( $customer, $instance );
            $this->attachRule( [ $ruleInit, $ruleProduct, $ruleOperator, $ruleValue ], $instance );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Reduction créé' );
        
        return TRUE;
    }
    
    
    /**
     * Attach the product's reduction (if exists)
     *
     * @param int $idProduct
     * @param Reduction $instance
     * @throws NullPointerException
     */
    private function attachProduct( int $idProduct, Reduction $instance ): void
    {
        if( $idProduct !== 0 ) {
            $product = $this->productRepository->findById( $idProduct );
            
            if( $product !== NULL ) {
                $instance->setProduct( $product );
            } else {
                throw new NullPointerException( 'Le produit que vous tentez d\'attacher n\'a pas été trouvé' );
            }
        }
    }
    
    
    /**
     * Attach the customer's reduction (if exists)
     *
     * @param int $idCustomer
     * @param Reduction $instance
     * @throws NullPointerException
     */
    private function attachCustomer( int $idCustomer, Reduction $instance ): void
    {
        if( $idCustomer !== 0 ) {
            $customer = $this->customerRepository->findById( $idCustomer );
            
            if( $customer !== NULL ) {
                $instance->setCustomer( $customer );
            } else {
                throw new NullPointerException( 'Le client que vous tentez d\'attacher n\'a pas été trouvé' );
            }
        }
    }
    
    
    /**
     * Attach the rule of reduction (if exists)
     *
     * @param array $rule
     * @param Reduction $instance
     * @throws InvalidParamException
     */
    private function attachRule( array $rule, Reduction $instance ): void
    {
        $empty = 0;
        
        foreach( $rule as $item ) {
            if( empty( $item ) ) {
                $empty++;
            }
        }
        
        /*
         * If One is empty, it's ok (no product)
         * If 2 or 3  it's error
         * 4 all is OK
         */
        if( $empty === 2 || $empty === 3 ) {
            throw new InvalidParamException( 'Tous les champ de la règle d\'attribution sont obligatoire' );
        } elseif( $empty <= 1 ) {
            $instance->setRule( $rule );
            $this->ruleToString( $rule, $instance );
        }
    }
    
    
    /**
     * Convert rule to string for front
     *
     * @param array|null $rule
     * @param Reduction $instance
     * @throws InvalidParamException
     */
    private function ruleToString( ?array $rule, Reduction $instance ): void
    {
        if( !empty( $rule ) ) {
            $content = $this->getRuleInit( $rule[0] );
            $content .= $this->getRuleProduct( $rule[1] );
            $content .= $this->getRuleOperator( $rule[2] );
            $content .= $rule[3];
            
            $instance->setRuleTranslation( $content );
        }
    }
    
    
    /**
     * @param string $data
     * @return string|null
     * @throws InvalidParamException
     */
    private function getRuleInit( string $data ): string
    {
        $list = [
            'PT' => 'Si le total du produit',
            'PQ' => 'Si la quantité du produit',
            'FT' => 'Si le total de la facture'
        ];
        
        if( array_key_exists( $data, $list ) ) {
            return $list[$data];
        }
        
        
        $this->throwRuleException();
    }
    
    
    /**
     * @param int $id
     * @return string|null
     * @throws InvalidParamException
     */
    private function getRuleProduct( int $id ): ?string
    {
        if( $id !== 0 ) {
            
            if( $this->productRepository->findById( $id ) === NULL ) {
                $this->throwRuleException();
            }
            
            return ' ayant pour référence: ' . $id;
        }
        
        return NULL;
    }
    
    
    /**
     * @param string $operator
     * @return string|null
     * @throws InvalidParamException
     */
    private function getRuleOperator( string $operator ): string
    {
        $list = [
            '>'  => ' est strictement supérieur à ',
            '<'  => ' est strictement inférieur à ',
            '>=' => ' est supérieur ou égale à ',
            '<=' => ' est inférieur ou égale à ',
            '==' => ' est égale à ',
            '!=' => ' n\'est pas égale à '
        ];
        
        if( array_key_exists( $operator, $list ) ) {
            return $list[$operator];
        }
        
        $this->throwRuleException();
    }
    
    
    /**
     * @throws InvalidParamException
     */
    private function throwRuleException(): void
    {
        throw new InvalidParamException( 'Une erreur est survenue pendant la génération de la règle d\'attribution' );
    }
}
