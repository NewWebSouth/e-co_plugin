<?php


namespace App\Packages\Administer\Manage\GraphicRule\Reduction;

use App\Entities\GraphicRuleReduction;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractFrontNotifier implements DeleteInterface
{
    
    private UploadImageInterface $uploadManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploadManager = $uploadManager;
    }
    
    
    public function delete( HttpRequest $request, object $instance ): bool
    {
        /** @var GraphicRuleReduction $instance */
        
        if( $instance->getImage() !== NULL ) {
            $image = $instance->getImage();
            
            $instance->getImage()->removeGraphicRuleReduction( $instance );
            $this->uploadManager->cleanImageRepository( $image );
        }
        
        $this->notifyFront();
        $this->entityManager->delete( $instance );
        $this->setRepository( self::SUCCESS, 'La réduction ne sera bientôt plus visible sur le site' );
        
        return TRUE;
    }
}
