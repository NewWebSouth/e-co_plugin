<?php


namespace App\Packages\Administer\Manage\GraphicRule\Reduction;


use App\Entities\GraphicRuleReduction;
use App\Entities\Reduction;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Repositories\ReductionRepository;
use App\Repositories\SectionRepository;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\ConflictException;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;


class SCreate extends AbstractGraphicRuleReduction implements CreateInterface
{
    
    private const PARAM_ID_REDUCTION = 'id';
    private ReductionRepository $reductionRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager,
        SectionRepository $sectionRepository,
        ReductionRepository $reductionRepository )
    {
        parent::__construct( $entityManager, $uploadManager, $sectionRepository );
        $this->reductionRepository = $reductionRepository;
    }
    
    
    /**
     * Build the graphic rule for push reduction in website
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        $title       = (string)$request->getParameter( self::PARAM_TITLE );
        $description = (string)$request->getParameter( self::PARAM_DESCRIPTION );
        $image       = (array)$request->getPart( self::PARAM_IMAGE );
        $sections    = (array)$request->getParameter( self::PARAM_SECTION );
        $reduction   = (int)$request->getParameter( self::PARAM_ID_REDUCTION );
        
        /** @var GraphicRuleReduction $instance */
        
        try {
            $instance->setTitle( $title );
            $instance->setDescription( $description );
            $this->attachSection( $sections, $instance );
            $this->attachImage( $image, $instance );
            $this->attachReduction( $reduction, $instance );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( ConflictException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        
        $this->setRepository( self::SUCCESS, 'La réduction sera bientôt visible sur le site' );
        
        return TRUE;
    }
    
    
    /**
     * Attach the reduction on graphic rule
     *
     * @param int $id
     * @param GraphicRuleReduction $instance
     * @throws NullPointerException
     * @throws ConflictException
     */
    private function attachReduction( int $id, GraphicRuleReduction $instance ): void
    {
        $reduction = $this->reductionRepository->findById( $id );
        
        
        if( $reduction !== NULL ) {
            
            if( !$this->isFree( $reduction ) ) {
                throw new ConflictException( 'Cette reduction appartient à ' . $reduction->getCustomer()->getStrName() . ', vous ne pouvez pas l\'afficher sur votre site' );
            }
            
            $instance->setReduction( $reduction );
        } else {
            throw new NullPointerException( 'Une erreur s\'est produite' );
        }
    }
    
    
    private function isFree( Reduction $reduction ): bool
    {
        return $reduction->getCustomer() === NULL;
    }
}
