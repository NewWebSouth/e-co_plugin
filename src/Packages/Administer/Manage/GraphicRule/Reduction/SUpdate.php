<?php


namespace App\Packages\Administer\Manage\GraphicRule\Reduction;


use App\Entities\GraphicRuleReduction;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractGraphicRuleReduction implements UpdateInterface
{
    
    public function update( HttpRequest $request, object $instance ): bool
    {
        $title       = (string)$request->getParameter( self::PARAM_TITLE );
        $description = (string)$request->getParameter( self::PARAM_DESCRIPTION );
        $image       = (array)$request->getPart( self::PARAM_IMAGE );
        $sections    = (array)$request->getParameter( self::PARAM_SECTION );
        
        /** @var GraphicRuleReduction $instance */
        
        try {
            $instance->setTitle( $title );
            $instance->setDescription( $description );
            
            if( $image !== NULL ) {
                $this->detachImage( $instance );
            }
            
            $this->attachImage( $image, $instance );
            $instance->purgeSection();
            $this->attachSection( $sections, $instance );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'La réduction a été mise à jour' );
        
        return TRUE;
    }
    
    
    /**
     * @param GraphicRuleReduction $instance
     */
    protected function detachImage( GraphicRuleReduction $instance ): void
    {
        if( $instance->getImage() !== NULL ) {
            $this->uploaderManager->cleanImageRepository( $instance->getImage() );
            $instance->setImage( NULL );
        }
    }
}
