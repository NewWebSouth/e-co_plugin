<?php


namespace App\Packages\Administer\Manage\GraphicRule\Reduction;


use App\Entities\GraphicRuleReduction;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;

class AbstractGraphicRuleReduction extends AbstractFrontNotifier
{
    
    protected const PARAM_TITLE       = 'title';
    protected const PARAM_DESCRIPTION = 'description';
    protected const PARAM_IMAGE       = 'image';
    protected const PARAM_SECTION     = 'sections';
    protected UploadImageInterface   $uploaderManager;
    private SectionRepository        $sectionRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager,
        SectionRepository $sectionRepository )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager   = $uploadManager;
        $this->sectionRepository = $sectionRepository;
    }
    
    
    /**
     * Attach the section specified by admin
     *
     * @param array $sections
     * @param GraphicRuleReduction $instance
     * @throws NullPointerException
     * @throws InvalidParamException
     */
    protected function attachSection( array $sections, GraphicRuleReduction $instance ): void
    {
        if( !empty( $sections ) ) {
            foreach( $sections as $id ) {
                $section = $this->sectionRepository->findById( $id );
                
                if( $section !== NULL ) {
                    $instance->addSection( $section );
                } else {
                    throw new NullPointerException( 'L\'une des sections que vous tentez d\'attacher n\'a pas été trouvé' );
                }
            }
            
            return;
        }
        
        throw new InvalidParamException( 'Merci de renseigner au moins une section' );
    }
    
    
    /**
     * Attach the image uploaded by admin
     *
     * @param array|null $part
     * @param GraphicRuleReduction $instance
     */
    protected function attachImage( ?array $part, GraphicRuleReduction $instance ): void
    {
        if( !empty( $part ) ) {
            
            if( $instance->getImage() !== NULL ) {
                $this->uploaderManager->cleanImageRepository( $instance->getImage() );
            }
            
            $imageInstance = $this->uploaderManager->getImage( $part );
            $instance->setImage( $imageInstance );
            $this->entityManager->persists( $imageInstance );
        }
    }
}
