<?php


namespace App\Packages\Administer\Manage\GraphicRule\Product;

use App\Entities\GraphicRuleProduct;
use App\Entities\Product;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Repositories\ProductRepository;
use App\Repositories\SectionRepository;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\ConflictException;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;


class SCreate extends AbstractFrontNotifier implements CreateInterface
{
    
    private const PARAM_SECTION     = 'sections';
    private const PARAM_DESCRIPTION = 'description';
    private const PARAM_PRODUCT     = 'id';
    private const PARAM_IMAGE       = 'attach_image';
    private const PARAM_PRIORITY    = 'priority';
    private const PARAM_RECOMMENDED = 'recommended';
    private UploadImageInterface   $uploaderManager;
    private ProductRepository      $productRepository;
    private SectionRepository      $sectionRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager,
        ProductRepository $productRepository,
        SectionRepository $sectionRepository )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager   = $uploadManager;
        $this->productRepository = $productRepository;
        $this->sectionRepository = $sectionRepository;
    }
    
    
    /**
     * Build the graphic rule of product for push on website
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        $sections    = (array)$request->getParameter( self::PARAM_SECTION );
        $description = (string)$request->getParameter( self::PARAM_DESCRIPTION );
        $priority    = (int)$request->getParameter( self::PARAM_PRIORITY );
        $product     = (int)$request->getParameter( self::PARAM_PRODUCT );
        $images      = (array)$request->getPart( self::PARAM_IMAGE );
        $recommended = (array)$request->getParameter( self::PARAM_RECOMMENDED );
        
        /** @var GraphicRuleProduct $instance */
        
        try {
            $instance->setDescription( $description );
            $instance->setPriority( $priority );
            $this->attachProduct( $product, $instance );
            $this->attachImages( $images, $instance );
            $this->attachSection( $sections, $instance );
            $this->attachRecommended( $recommended, $instance );
            $this->setPriorityDataCustomer( $request, $instance->getProduct() );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( ConflictException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'Règle graphique créé, le produit sera bientôt visible sur le site' );
        
        return TRUE;
    }
    
    
    /**
     * @param int $id
     * @param GraphicRuleProduct $instance
     * @throws NullPointerException
     */
    private function attachProduct( int $id, GraphicRuleProduct $instance ): void
    {
        $product = $this->productRepository->findById( $id );
        
        if( $product !== NULL ) {
            $instance->setProduct( $product );
        } else {
            throw new NullPointerException( 'Le produit à attacher à la règle graphique n\'a pas été trouvé' );
        }
    }
    
    
    /**
     * @param array $images
     * @param GraphicRuleProduct $instance
     */
    private function attachImages( array $images, GraphicRuleProduct $instance ): void
    {
        if( !empty( $images ) ) {
            
            foreach( $this->uploaderManager->convertToMultipleArray( $images ) as $image ) {
                
                $imageInstance = $this->uploaderManager->getImage( $image );
                
                if( $imageInstance !== NULL ) {
                    $instance->addImage( $imageInstance );
                }
            }
        }
    }
    
    
    /**
     * @param array $ids
     * @param GraphicRuleProduct $instance
     * @throws InvalidParamException
     * @throws NullPointerException
     */
    private function attachSection( array $ids, GraphicRuleProduct $instance ): void
    {
        if( !empty( $ids ) ) {
            foreach( $ids as $id ) {
                $section = $this->sectionRepository->findById( $id );
                
                if( $section !== NULL ) {
                    $instance->addSection( $section );
                } else {
                    throw new NullPointerException( 'L\'une des sections que vous tentez d\'attacher n\'a pas été trouvé' );
                }
            }
        } else {
            throw new InvalidParamException( 'Un produit doit être attaché à au moins une section pour pouvoir être visible sur le site' );
        }
    }
    
    
    /**
     * @param array $recommended
     * @param GraphicRuleProduct $instance
     * @throws NullPointerException
     * @throws ConflictException
     */
    private function attachRecommended( array $recommended, GraphicRuleProduct $instance ): void
    {
        foreach( $recommended as $id ) {
            
            if( $id === $instance->getProduct()->getId() ) {
                throw new ConflictException( 'Vous ne pouvez pas recommander le produit "' . $instance->getProduct()->getName() . '"' );
            }
            
            $product = $this->productRepository->findById( $id );
            
            if( $product !== NULL ) {
                $instance->addRecommended( $product );
            } else {
                throw new NullPointerException( 'L\'un des produit que vous souhaitez recommander n\'a pas été trouvé' );
            }
        }
    }
    
    
    private function setPriorityDataCustomer( HttpRequest $request, Product $product ): void
    {
        if( !empty( $product->getDataCustomers() ) ) {
            foreach( $product->getDataCustomers() as $dataCustomer ) {
                $dataCustomer->setPriority( $request->getParameter( 'data_customer_' . $dataCustomer->getId() ) );
            }
        }
    }
}
