<?php


namespace App\Packages\Administer\Manage\GraphicRule\Product;


use App\Entities\GraphicRuleProduct;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractFrontNotifier implements DeleteInterface
{
    
    public UploadImageInterface $uploaderManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager = $uploadManager;
    }
    
    
    public function delete( HttpRequest $request, object $instance ): bool
    {
        
        /** @var GraphicRuleProduct $instance */
        
        // Try to remove image
        if( !empty( $instance->getImages() ) ) {
            foreach( $instance->getImages() as $image ) {
                
                $image->removeGraphicRuleProduct( $instance );
                $this->uploaderManager->cleanImageRepository( $image );
            }
        }
        
        $this->notifyFront();
        $this->entityManager->delete( $instance );
        $this->setRepository( self::SUCCESS, 'Règle graphique supprimé' );
        
        return TRUE;
    }
}
