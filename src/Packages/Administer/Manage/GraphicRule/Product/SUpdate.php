<?php


namespace App\Packages\Administer\Manage\GraphicRule\Product;

use App\Entities\GraphicRuleProduct;
use App\Entities\Image;
use App\Entities\Product;
use App\Packages\Administer\Manage\AbstractFrontNotifier;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Repositories\ImageRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SectionRepository;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\ConflictException;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractFrontNotifier implements UpdateInterface
{
    
    private const PARAM_SECTION             = 'sections';
    private const PARAM_ATTACH_IMAGE        = 'attach_image';
    private const PARAM_DETACH_IMAGE        = 'detach_image';
    private const PARAM_DESCRIPTION         = 'description';
    private const PARAM_PRIORITY            = 'priority';
    private const PARAM_PRODUCT_RECOMMENDED = 'recommended';
    private UploadImageInterface   $uploaderManager;
    private SectionRepository      $sectionRepository;
    private ImageRepository        $imageRepository;
    private ProductRepository      $productRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager,
        SectionRepository $sectionRepository,
        ImageRepository $imageRepository,
        ProductRepository $productRepository )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager   = $uploadManager;
        $this->productRepository = $productRepository;
        $this->sectionRepository = $sectionRepository;
        $this->imageRepository   = $imageRepository;
    }
    
    
    public function update( HttpRequest $request, object $instance ): bool
    {
        $sections            = (array)$request->getParameter( self::PARAM_SECTION );
        $attachImage         = (array)$request->getPart( self::PARAM_ATTACH_IMAGE );
        $detachImage         = (array)$request->getParameter( self::PARAM_DETACH_IMAGE );
        $description         = (string)$request->getParameter( self::PARAM_DESCRIPTION );
        $priority            = (int)$request->getParameter( self::PARAM_PRIORITY );
        $productsRecommended = (array)$request->getParameter( self::PARAM_PRODUCT_RECOMMENDED );
        
        /** @var GraphicRuleProduct $instance */
        
        try {
            $instance->setDescription( $description );
            $instance->setPriority( $priority );
            $this->attachSection( $sections, $instance );
            $this->attachImage( $attachImage, $instance );
            $this->detachImage( $detachImage, $instance );
            $this->setPriorityDataCustomer( $request, $instance->getProduct() );
            $this->attachProductsRecommended( $productsRecommended, $instance );
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( ConflictException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->notifyFront();
        
        $this->entityManager->persists( $instance );
        $this->setRepository( self::SUCCESS, 'La règle graphique du produit a été mise à jour' );
        
        return TRUE;
    }
    
    
    /**
     * @param array $ids
     * @param GraphicRuleProduct $instance
     * @throws NullPointerException
     */
    private function attachSection( array $ids, GraphicRuleProduct $instance ): void
    {
        $instance->purgeSections();
        
        if( !empty( $ids ) ) {
            foreach( $ids as $id ) {
                $section = $this->sectionRepository->findById( $id );
                
                if( $section !== NULL ) {
                    $instance->addSection( $section );
                } else {
                    throw new NullPointerException( 'L\'une des sections que vous tentez d\'attacher a la règle graphic n\'à pas été trouvé' );
                }
            }
        }
    }
    
    
    /**
     * @param array $images
     * @param GraphicRuleProduct $instance
     */
    private function attachImage( array $images, GraphicRuleProduct $instance ): void
    {
        if( !empty( $images ) ) {
            
            foreach( $this->uploaderManager->convertToMultipleArray( $images ) as $image ) {
                $imageInstance = $this->uploaderManager->getImage( $image );
                
                if( $imageInstance !== NULL ) {
                    $instance->addImage( $imageInstance );
                }
            }
        }
    }
    
    
    /**
     * @param array $ids
     * @param GraphicRuleProduct $instance
     */
    private function detachImage( array $ids, GraphicRuleProduct $instance ): void
    {
        
        if( !empty( $ids ) ) {
            foreach( $ids as $id ) {
                /** @var Image $imageInstance */
                $imageInstance = $this->imageRepository->findById( $id );
                
                if( is_array( $imageInstance ) ) {
                    $imageInstance = $imageInstance[0];
                }
                
                if( $imageInstance !== NULL ) {
                    $instance->removeImage( $imageInstance );
                    $imageInstance->removeGraphicRuleProduct( $instance );
                    $this->uploaderManager->cleanImageRepository( $imageInstance );
                }
            }
        }
    }
    
    
    private function setPriorityDataCustomer( HttpRequest $request, Product $product ): void
    {
        if( !empty( $product->getDataCustomers() ) ) {
            foreach( $product->getDataCustomers() as $dataCustomer ) {
                $dataCustomer->setPriority( $request->getParameter( 'data_customer_' . $dataCustomer->getId() ) );
            }
        }
    }
    
    
    /**
     * @param array $productsRecommended
     * @param GraphicRuleProduct $graphicRuleProduct
     * @throws NullPointerException
     * @throws ConflictException
     */
    private function attachProductsRecommended( array $productsRecommended, GraphicRuleProduct $graphicRuleProduct ): void
    {
        // Reset recommended products
        $graphicRuleProduct->clearRecommended();
        
        if( !empty( $productsRecommended ) ) {
            
            foreach( $productsRecommended as $idProduct ) {
                
                if( $idProduct === $graphicRuleProduct->getProduct()->getId() ) {
                    throw new ConflictException( 'Vous ne pouvez pas recommander le produit "' . $graphicRuleProduct->getProduct()->getName() . '"' );
                }
                
                
                $product = $this->productRepository->findById( $idProduct );
                
                if( !is_null( $product ) ) {
                    $graphicRuleProduct->addRecommended( $product );
                } else {
                    throw new NullPointerException( 'L\'un des produits que vous souhaitez recommander n\'a pas été trouvé' );
                }
            }
        }
    }
}
