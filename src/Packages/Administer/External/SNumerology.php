<?php


namespace App\Packages\Administer\External;


use Nomess\Http\HttpRequest;

class SNumerology
{
    
    private const DATE   = 'date';
    private const PRENOM = 'prenom';
    private const PERE   = 'pere';
    private const MERE   = 'mere';
    
    
    public function service( HttpRequest $request ): void
    {
        //Controle des données reçues ***********************************************************
        $prenom  = array();
        $nomMere = array();
        $nomPere = array();
        $date    = self::control( $request->getParameter( self::DATE ) );
        
        $j = 0;
        
        foreach( $request->getParameter( self::PRENOM ) as $key => $value ) {
            $j++;
            
            $value = trim( $value );
            $value = strtolower( $value );
            
            if( $value === NULL || $value == "" ) {
                $value = NULL;
            }
            
            $prenom[$j] = $value;
        }
        
        $j = 0;
        
        foreach( $request->getParameter( self::PERE ) as $key => $value ) {
            $j++;
            
            $value = trim( $value );
            $value = strtolower( $value );
            
            if( empty( $value ) ) {
                $value = NULL;
            }
            
            $nomPere[$j] = $value;
        }
        
        $j = 0;
        
        foreach( $request->getParameter( self::MERE ) as $key => $value ) {
            $j++;
            
            $value = trim( $value );
            $value = strtolower( $value );
            
            if( $value === NULL || $value == "" ) {
                $value = NULL;
            }
            
            $nomMere[$j] = $value;
        }
        
        $valeurs = array();
        
        //Init erreur
        $erreur = NULL;
        //Calcul des valeurs numeriques ************************************************************
        if( $prenom != NULL && $nomMere != NULL && $nomPere != NULL && $date != NULL ) {
            $lettre = array(
                "a" => 1,
                "b" => 2,
                "c" => 3,
                "d" => 4,
                "e" => 5,
                "f" => 6,
                "g" => 7,
                "h" => 8,
                "i" => 9,
                "j" => 1,
                "k" => 2,
                "l" => 3,
                "m" => 4,
                "n" => 5,
                "o" => 6,
                "p" => 7,
                "q" => 8,
                "r" => 9,
                "s" => 1,
                "t" => 2,
                "u" => 3,
                "v" => 4,
                "w" => 5,
                "x" => 6,
                "y" => 7,
                "z" => 8
            );
            
            $voyelle = array(
                "a" => 1,
                "e" => 5,
                "i" => 9,
                "o" => 6,
                "u" => 3,
                "y" => 7
            );
            
            $consonne = array(
                "b" => 2,
                "c" => 3,
                "d" => 4,
                "f" => 6,
                "g" => 7,
                "h" => 8,
                "j" => 1,
                "k" => 2,
                "l" => 3,
                "m" => 4,
                "n" => 5,
                "p" => 7,
                "q" => 8,
                "r" => 9,
                "s" => 1,
                "t" => 2,
                "v" => 4,
                "w" => 5,
                "x" => 6,
                "z" => 8
            );
            
            /* Methode de calcul:
            * Se reporter au pdf fournie par Mme Salesse
            */
            
            $total1 = 0;
            $total2 = 0;
            $total3 = 0;
            $total4 = 0;
            $total5 = 0;
            $total6 = 0;
            $total7 = 0;
            $total8 = 0;
            
            $pierre1 = NULL;
            $pierre2 = NULL;
            $pierre3 = NULL;
            $pierre4 = NULL;
            $pierre5 = NULL;
            $pierre6 = NULL;
            $pierre7 = NULL;
            $pierre8 = NULL;
            
            //Calcul 1 et 2 (pierre de base, pierre de sommet) ************************************
            foreach( $lettre as $key => $value ) {
                foreach( $prenom as $keyP => $valueP ) {
                    //Calcul 1
                    $first = substr( $valueP, 0, 1 );
                    
                    if( $first === $key ) {
                        $total1 += $value;
                    }
                    
                    //Calcul 2
                    $last = substr( $valueP, -1, 1 );
                    
                    if( $last === $key ) {
                        $total2 += $value;
                    }
                }
                
                foreach( $nomPere as $keyP => $valueP ) {
                    //Calcul 1
                    $first = substr( $valueP, 0, 1 );
                    
                    if( $first === $key ) {
                        $total1 += $value;
                    }
                    
                    //Calcul 2
                    $last = substr( $valueP, -1, 1 );
                    
                    if( $last === $key ) {
                        $total2 += $value;
                    }
                }
                
                foreach( $nomMere as $keyP => $valueP ) {
                    //Calcul 1
                    $first = substr( $valueP, 0, 1 );
                    
                    if( $first === $key ) {
                        $total1 += $value;
                    }
                    
                    //Calcul 2
                    $last = substr( $valueP, -1, 1 );
                    
                    if( $last === $key ) {
                        $total2 += $value;
                    }
                }
            }
            
            //Calcul 3 (pierre de chemin de vie) ***************************************************
            $dateTemp = explode( "-", $date );
            $num1     = intval( $dateTemp[0] );
            $num2     = intval( $dateTemp[1] );
            $num3     = intval( $dateTemp[2] );
            
            $total3 = $num1 + $num2 + $num3;
            
            //Calcul 4 (pierre d'appel) ************************************************************
            foreach( $voyelle as $key => $value ) {
                foreach( $prenom as $keyP => $valueP ) {
                    for( $i = 0; $i < strlen( $valueP ); $i++ ) {
                        
                        if( $valueP[$i] === $key ) {
                            $total4 += $value;
                        }
                    }
                }
                
                foreach( $nomPere as $keyP => $valueP ) {
                    for( $i = 0; $i < strlen( $valueP ); $i++ ) {
                        
                        if( $valueP[$i] === $key ) {
                            $total4 += $value;
                        }
                    }
                }
                
                foreach( $nomMere as $keyP => $valueP ) {
                    for( $i = 0; $i < strlen( $valueP ); $i++ ) {
                        
                        if( $valueP[$i] === $key ) {
                            $total4 += $value;
                        }
                    }
                }
            }
            
            
            //Calcul 5 (pierre de personnalité) ****************************************************
            foreach( $consonne as $key => $value ) {
                foreach( $prenom as $keyP => $valueP ) {
                    for( $i = 0; $i < strlen( $valueP ); $i++ ) {
                        
                        if( $valueP[$i] === $key ) {
                            $total5 += $value;
                        }
                    }
                }
                
                foreach( $nomPere as $keyP => $valueP ) {
                    for( $i = 0; $i < strlen( $valueP ); $i++ ) {
                        
                        if( $valueP[$i] === $key ) {
                            $total5 += $value;
                        }
                    }
                }
                
                foreach( $nomMere as $keyP => $valueP ) {
                    for( $i = 0; $i < strlen( $valueP ); $i++ ) {
                        
                        if( $valueP[$i] === $key ) {
                            $total5 += $value;
                        }
                    }
                }
            }
            
            //Calcul 6 (pierre d'expression) *******************************************************
            $total6 = $total4 + $total5;
            
            //Calcul 7 (pierre de touche) **********************************************************
            $total7 = $total1 + $total2 + $total4 + $total5 + $total6;
            
            //Calcul 8 (pierre de voeux) ***********************************************************
            foreach( $prenom as $key => $value ) {
                $compte = FALSE;
                
                for( $i = 0; $i < strlen( $value ); $i++ ) {
                    foreach( $voyelle as $keyV => $valueV ) {
                        if( $value[$i] === $keyV ) {
                            $compte = TRUE;
                            $total8 += $valueV;
                            break;
                        }
                    }
                    
                    if( $compte == TRUE ) {
                        $compte = FALSE;
                        break;
                    }
                }
            }
            
            foreach( $nomPere as $key => $value ) {
                $compte = FALSE;
                
                for( $i = 0; $i < strlen( $value ); $i++ ) {
                    foreach( $voyelle as $keyV => $valueV ) {
                        if( $value[$i] === $keyV ) {
                            $compte = TRUE;
                            $total8 += $valueV;
                            break;
                        }
                    }
                    
                    if( $compte == TRUE ) {
                        $compte = FALSE;
                        break;
                    }
                }
            }
            
            foreach( $nomMere as $key => $value ) {
                $compte = FALSE;
                
                for( $i = 0; $i < strlen( $value ); $i++ ) {
                    foreach( $voyelle as $keyV => $valueV ) {
                        if( $value[$i] === $keyV ) {
                            $compte = TRUE;
                            $total8 += $valueV;
                            break;
                        }
                    }
                    
                    if( $compte === TRUE ) {
                        $compte = FALSE;
                        break;
                    }
                }
            }
            
            //Reduction des nombre > 33 *********************************************************
            $total1 = self::reduit( $total1 );
            $total2 = self::reduit( $total2 );
            $total3 = self::reduit( $total3 );
            $total4 = self::reduit( $total4 );
            $total5 = self::reduit( $total5 );
            $total6 = self::reduit( $total6 );
            $total7 = self::reduit( $total7 );
            $total8 = self::reduit( $total8 );
            
            $request->setParameter( 'total1', $total1 );
            $request->setParameter( 'total2', $total2 );
            $request->setParameter( 'total3', $total3 );
            $request->setParameter( 'total4', $total4 );
            $request->setParameter( 'total5', $total5 );
            $request->setParameter( 'total6', $total6 );
            $request->setParameter( 'total7', $total7 );
            $request->setParameter( 'total8', $total8 );
            
            $request->setParameter( 'pierre1', self::attrPierre( $total1 ) );
            $request->setParameter( 'pierre2', self::attrPierre( $total2 ) );
            $request->setParameter( 'pierre3', self::attrPierre( $total3 ) );
            $request->setParameter( 'pierre4', self::attrPierre( $total4 ) );
            $request->setParameter( 'pierre5', self::attrPierre( $total5 ) );
            $request->setParameter( 'pierre6', self::attrPierre( $total6 ) );
            $request->setParameter( 'pierre7', self::attrPierre( $total7 ) );
            $request->setParameter( 'pierre8', self::attrPierre( $total8 ) );
        } else {
            $request->setError( 'error' );
        }
    }
    
    
    //Controle des paramètres reçu
    private static function control( $param )
    {
        $param = trim( $param );
        $param = strtolower( $param );
        
        if( empty( $param ) ) {
            return NULL;
        } else {
            return $param;
        }
    }
    
    
    //Reduit les nombres > 33
    private static function reduit( $valueTemp )
    {
        if( $valueTemp > 33 ) {
            $value = (string)$valueTemp;
            $total = 0;
            
            for( $i = 0; $i < strlen( $value ); $i++ ) {
                $total += (int)$value[$i];
            }
            
            return $total;
        }
        
        return $valueTemp;
    }
    
    
    private static function attrPierre( $valueTemp )
    {
        
        $pierre = array(
            1  => "Quartz rose",
            2  => "Jaspe rouge",
            3  => "Calcédoine",
            4  => "Jade",
            5  => "Emeraude",
            6  => "Grenat",
            7  => "Critine",
            8  => "Obsidienne",
            9  => "Aigue marine",
            10 => "Rhodocrosite",
            11 => "Cornaline",
            12 => "Ambre",
            13 => "Hématite",
            14 => "Améthyste",
            15 => "Malachite",
            16 => "Opale",
            17 => "Turquoise",
            18 => "Pierre de lune",
            19 => "Topaze",
            20 => "Lapis lazuli",
            21 => "Tourmaline",
            22 => "Cristal de roche",
            23 => "Azurite",
            24 => "Amazonite",
            25 => "Œil de tigre",
            26 => "Pyrite",
            27 => "Fluorine ou fluorite",
            28 => "Perle",
            29 => "Sodalite",
            30 => "Quartz fumé",
            31 => "Soufre",
            32 => "Mercure",
            33 => "Sel"
        );
        
        foreach( $pierre as $key => $value ) {
            if( $valueTemp === $key ) {
                return $value;
            }
        }
    }
}
