<?php


namespace App\Packages\Administer\Report;


use App\Entities\Credit;
use App\Entities\EntityPdfInterface;
use App\Entities\Invoice;
use App\Packages\Administer\Report\Credit\CreditGenerator;
use App\Packages\Administer\Report\Invoice\InvoiceGenerator;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\MissingConfigurationException;

class Dispatcher implements PdfGeneratorInterface, TransactionObserverInterface
{
    
    private const MAPPER = [
        Invoice::class => InvoiceGenerator::class,
        Credit::class  => CreditGenerator::class
    ];
    private TransactionSubjectInterface $transactionSubject;
    private InvoiceGenerator            $invoiceGenerator;
    private CreditGenerator             $creditGenerator;
    private array                       $toGenerate = array();
    
    
    public function __construct(
        TransactionSubjectInterface $transactionSubject,
        InvoiceGenerator $invoiceGenerator,
        CreditGenerator $creditGenerator )
    {
        $this->transactionSubject = $transactionSubject;
        $this->invoiceGenerator   = $invoiceGenerator;
        $this->creditGenerator    = $creditGenerator;
        $this->subscribeToTransactionStatus();
    }
    
    
    /**
     * Store the object to generate if is supported
     *
     * @param EntityPdfInterface $entityPdf
     * @throws InvalidParamException
     */
    public function generate( EntityPdfInterface $entityPdf ): void
    {
        if( $this->isSupported( $entityPdf ) ) {
            $this->toGenerate[] = $entityPdf;
            
            return;
        }
        
        throw new InvalidParamException( 'The generator system doesn\'t support the class "' . get_class( $entityPdf ) . '"' );
    }
    
    
    /**
     * If status of transaction have a success, the file is generated
     *
     * @param bool $status
     */
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            foreach( $this->toGenerate as $object ) {
                $this->getGenerator( get_class( $object ) )->generate( $object );
            }
        }
    }
    
    
    /**
     * Subscribe to transaction state
     */
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
    
    
    /**
     * Control that this object is supported by generator system
     *
     * @param object $object
     * @return bool
     */
    private function isSupported( object $object ): bool
    {
        return array_key_exists( get_class( $object ), self::MAPPER );
    }
    
    
    /**
     * Return the good generator for object
     *
     * @param string $classname
     * @return object
     * @throws MissingConfigurationException
     */
    private function getGenerator( string $classname ): object
    {
        if( $classname === Invoice::class ) {
            return $this->invoiceGenerator;
        } elseif( $classname === Credit::class ) {
            return $this->creditGenerator;
        }
        
        throw new MissingConfigurationException( 'The generator for ' . $classname . ' was not found' );
    }
}
