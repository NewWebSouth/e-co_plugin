<?php


namespace App\Packages\Administer\Report;


use App\Entities\EntityPdfInterface;
use Nomess\Exception\InvalidParamException;

interface PdfGeneratorInterface
{
    
    /**
     * @param EntityPdfInterface $entityPdf
     * @return mixed
     * @throws InvalidParamException
     */
    public function generate( EntityPdfInterface $entityPdf );
}
