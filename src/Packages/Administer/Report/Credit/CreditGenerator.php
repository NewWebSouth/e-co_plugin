<?php


namespace App\Packages\Administer\Report\Credit;


use App\Entities\Credit;
use Nomess\Helpers\DataHelper;
use Spipu\Html2Pdf\Html2Pdf;

class CreditGenerator
{
    
    use DataHelper;
    
    private const DC_STORAGE_INVOICE = 'local_storage_credit';
    
    
    public function generate( Credit $credit ): void
    {
        $customer = $credit->getCustomer();
        
        ob_start();
        require_once ROOT . 'src/Packages/Administer/Report/layout/credit.php';
        $content = ob_get_clean();
        
        $pdf = new Html2Pdf( "p", "A4", "fr" );
        $pdf->pdf->SetAuthor( 'Une histoire de pierre' );
        $pdf->pdf->SetTitle( 'Avoir-' . $credit->getId() );
        $pdf->writeHTML( $content );
        
        ob_clean();
        $pdf->Output( $this->get( self::DC_STORAGE_INVOICE ) . $credit->getDocumentName(), 'F' );
    }
}
