<?php


namespace App\Packages\Administer\Report\Invoice;


use App\Entities\Invoice;
use Nomess\Helpers\DataHelper;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;

class InvoiceGenerator
{
    
    use DataHelper;
    
    private const DC_STORAGE_INVOICE = 'local_storage_invoice';
    
    
    /**
     * Créer une facture au format PDF
     *
     * @param Invoice $invoice
     * @return void
     * @throws Html2PdfException
     */
    public function generate( Invoice $invoice ): void
    {
        $customer          = $invoice->getCustomer();
        $productCustomer   = $invoice->getOrdered()->getProductCustomer();
        $reductionCustomer = $invoice->getOrdered()->getReductionCustomers();
        
        ob_start();
        require_once ROOT . 'src/Packages/Administer/Report/layout/invoice.php';
        $content = ob_get_clean();
        
        $pdf = new Html2Pdf( "p", "A4", "fr" );
        $pdf->pdf->SetAuthor( 'Une histoire de pierre' );
        $pdf->pdf->SetTitle( 'Facture-' . $invoice->getId() );
        $pdf->writeHTML( $content );
        
        ob_clean();
        $pdf->Output( $this->get( self::DC_STORAGE_INVOICE ) . $invoice->getDocumentName(), 'F' );
    }
}
