<page backtop="10mm" backleft="10mm" backright="10mm" backbottom="10mm" footer="page;">
    
    <style type="text/css">
        page {
            width: 100%;
        }
        
        .table-header {
            color: #333;
            font-family: helvetica;
            line-height: 5mm;
            border-collapse: collapse;
        }
        
        .table-header td {
            width: 250px;
        }
        
        .table-product {
            margin-top: 2%;
            color: #717375;
            font-family: helvetica;
            border-collapse: collapse;
            width: 650px;
        }
        
        .left {
            position: absolute;
            margin-top: 17%;
        }
        
        .right {
            position: absolute;
            width: 400px;
            margin-left: 450px;
            margin-top: 17%;
        }
        
        h2 {
            margin: 0;
            padding: 0;
        }
        
        p {
            margin: 5px;
            color: #717375;
        }
        
        .border th {
            border: 1px solid #baa499;
            color: white;
            background: #baa499;
            padding: 5px;
            font-weight: normal;
            font-size: 14px;
            text-align: center;
        }
        
        .border td {
            border-left: 1px solid #CFD1D2;
            border-right: 1px solid #CFD1D2;
            padding: 5px 10px;
            text-align: center;
            overflow-wrap: break-word;
            vertical-align: top;
        }
        
        .header-right {
            position: absolute;
            width: 400px;
            margin-left: 525px;
            font-size: 18px;
        }
        
        .hr {
            width: 525pt;
            border: 0.5px solid #7e8485;
            background: white;
        }
        
        .footer {
            margin-top: 30pt;
        }
    </style>
    
    <img style="display: block;" src="<?php
    echo ROOT . 'public/website/img/logo.png' ?>"/>
    <br>
    <div class="header-right">
        <p>Facture #<?php
            echo $invoice->getId() ?></p>
        <p>Du <?php
            echo ( new \DateTime( $invoice->getDateExec1() ) )->format( 'd/m/Y' ); ?></p>
    </div>
    <br>
    <br>
    <br>
    <div class="hr"></div>
    
    <table class="table-header left">
        <tr>
            <td><?php
                echo $customer->getFirstName() . " " . $customer->getLastName() ?></td>
        </tr>
        <tr>
            <td>
                <?php
                echo $customer->getNumberStreet() . " " . $customer->getStreet() . "<br>" . $customer->getPostalCode() . $customer->getCity() ?>
            </td>
        </tr>
    </table>
    <table class="table-header right">
        <tr>
            <td>Une histoire de pierres</td>
        </tr>
        <tr>
            <td>Sabrina Salesse</td>
        </tr>
        <tr>
            <td>
                2 rue Courbet <br>
                25510 PIERREFONTAINE LES VARANS
            </td>
        </tr>
    </table>
    <br>
    <div class="hr"></div>
    <table class="table-product border">
        <thead>
        <tr>
            <th>Ref</th>
            <th style="text-align: left;">Produit</th>
            <th>P.U.</th>
            <th>Quantité</th>
            <th>Réduction</th>
            <th>Total HT</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $totalProduct = count( $productCustomer );
        
        $i = 1;
        foreach( $productCustomer as $product ) {
            if( $i !== $totalProduct ) {
                ?>
                
                <tr>
                    <td style="width: 10pt;">#<?php
                        echo $product->getId() ?></td>
                    <td style="text-align: left;width: 200pt;"><?php
                        echo $product->getName() ?></td>
                    <td style="width: 30pt;"><?php
                        echo number_format( $product->getPrice(), 2, ',', '' ) ?> €
                    </td>
                    <td style="width: 30pt;"><?php
                        echo number_format( $product->getQuantity(), 2, ',', '' ) ?></td>
                    <td style="width: 30pt;">
                        <?php
                        $find = FALSE;
                        
                        foreach( $reductionCustomer as $reduction ) {
                            if( $reduction->getIdInternalProduct() === $product->getInternalId() ) {
                                echo number_format( $reduction->getValue(), 2, ',', '' );
                                $find = TRUE;
                            }
                        }
                        
                        if( $find === FALSE ) {
                            echo '0,00';
                        }
                        ?> %
                    </td>
                    <td style="width: 30pt;"><?php
                        echo number_format( $product->getNetToPay(), 2, ',', '' ) ?> €
                    </td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td style="width: 10pt; border-bottom: 1px solid #CFD1D2">#<?php
                        echo $product->getProduct()->getId() ?></td>
                    <td style="text-align: left;width: 200pt; border-bottom: 1px solid #CFD1D2"><?php
                        /** @var \App\Entities\ProductCustomer $product */
                        echo $product->getProduct()->getName() ?></td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo number_format( $product->getProduct()->getPrice(), 2, ',', '' ) ?> €
                    </td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo number_format( $product->getQuantity(), 2, ',', '' ) ?></td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2">
                        <?php
                        $find = FALSE;
                        
                        /** @var App\Packages\Cunsumption\ReductionCustomer\ $reduction */
                        foreach( $reductionCustomer as $reduction ) {
                            echo number_format( $reduction->getReduction()->getValue(), 2, ',', '' );
                            $find = TRUE;
                        }
                        
                        if( $find === FALSE ) {
                            echo '0,00';
                        }
                        ?> %
                    </td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo number_format( $product->getNetToPay(), 2, ',', '' ) ?> €
                    </td>
                </tr>
                <?php
            }
            $i++;
        }
        ?>
        <tr>
            <td style="border-left: 0px;" colspan="2"></td>
            <td colspan="2" style="text-align: right;">Total HT</td>
            <td colspan="2"><?php
                echo number_format( $invoice->getTotal(), 2, ',', '' ) ?> €
            </td>
        </tr>
        <tr>
            <td style="border-left: 0px;" colspan="2"></td>
            <td colspan="2" style="text-align: right;">Réduction</td>
            <td colspan="2">
                <?php
                $find = FALSE;
                
                foreach( $reductionCustomer as $reduction ) {
                    echo number_format( $reduction->getReduction()->getValue(), 2, ',', '' );
                    $find = TRUE;
                }
                
                if( $find === FALSE ) {
                    echo '0,00';
                }
                ?> %
            </td>
        </tr>
        <tr>
            <td style="border-left: 0px;" colspan="2"></td>
            <td style="border-bottom: 1px solid #CFD1D2; text-align: right;" colspan="2">Net à payer</td>
            <td style="border-bottom: 1px solid #CFD1D2" colspan="2"><?php
                echo number_format( $invoice->getNetToPay(), 2, ',', '' ) ?> €
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border: 0px;"></td>
            <td colspan="4" style="text-align: center; border: 0px;"> TVA non applicable, art. 293 B du CGI</td>
        </tr>
        </tbody>
    </table>
    
    <div class="footer">
        <p>Date d'éxecution:
            <?php
            if( $invoice->getDateExec2() === NULL ) {
                echo ( new \DateTime( $invoice->getDateExec1() ) )->format( 'd/m/Y' );
            } else {
                echo $invoice->getDateExec1() . ' au ' . $invoice->getDateExec2();
            }
            ?>
        </p>
        <p>phrase d'information en cas de non paiement</p>
    </div>

</page>
