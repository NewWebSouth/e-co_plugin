<page backtop="10mm" backleft="10mm" backright="10mm" backbottom="10mm" footer="page;">
    
    <style type="text/css">
        page {
            width: 100%;
        }
        
        .table-header {
            color: #333;
            font-family: helvetica;
            line-height: 5mm;
            border-collapse: collapse;
        }
        
        .table-header td {
            width: 250px;
        }
        
        .table-product {
            margin-top: 2%;
            color: #717375;
            font-family: helvetica;
            border-collapse: collapse;
            width: 650px;
        }
        
        .left {
            position: absolute;
            margin-top: 17%;
        }
        
        .right {
            position: absolute;
            width: 400px;
            margin-left: 450px;
            margin-top: 17%;
        }
        
        h2 {
            margin: 0;
            padding: 0;
        }
        
        p {
            margin: 5px;
            color: #717375;
        }
        
        .border th {
            border: 1px solid #baa499;
            color: white;
            background: #baa499;
            padding: 5px;
            font-weight: normal;
            font-size: 14px;
            text-align: center;
        }
        
        .border td {
            border-left: 1px solid #CFD1D2;
            border-right: 1px solid #CFD1D2;
            padding: 5px 10px;
            text-align: center;
            overflow-wrap: break-word;
            vertical-align: top;
        }
        
        .header-right {
            position: absolute;
            width: 400px;
            margin-left: 525px;
            font-size: 18px;
        }
        
        .hr {
            width: 525pt;
            border: 0.5px solid #7e8485;
            background: white;
        }
        
        .footer {
            margin-top: 30pt;
        }
    </style>
    
    <img style="display: block;" src="<?php
    echo ROOT . 'public/website/img/logo.png' ?>"/>
    <br>
    <div class="header-right">
        <?php
        /**
         * @var \App\Entities\Credit $credit
         */
        
        $customer = $credit->getCustomer();
        ?>
        
        <p>Avoir #<?php
            echo $credit->getId() ?></p>
        <p>Du <?php
            echo date( 'd/m/Y' ) ?></p>
    </div>
    <br>
    <br>
    <br>
    <div class="hr"></div>
    
    <table class="table-header left">
        <tr>
            <td><?php
                echo $customer->getFirstName() . " " . $customer->getLastName() ?></td>
        </tr>
        <tr>
            <td>
                <?php
                echo $customer->getStrAddress();
                ?>
            </td>
        </tr>
    </table>
    <table class="table-header right">
        <tr>
            <td>Une histoire de pierres</td>
        </tr>
        <tr>
            <td>Sabrina Salesse</td>
        </tr>
        <tr>
            <td>
                2 rue Courbet <br>
                25510 PIERREFONTAINE LES VARANS
            </td>
        </tr>
    </table>
    <br>
    <div class="hr"></div>
    <table class="table-product border">
        <thead>
        <tr>
            <th>Ref</th>
            <th style="text-align: left;">Produit</th>
            <th>P.U.</th>
            <th>Quantité</th>
            <th>Total HT</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $totalProduct = count( $credit->getProducts() );
        
        $i = 1;
        foreach( $credit->getProducts() as $product ) {
            if( $i !== $totalProduct ) {
                ?>
                
                <tr>
                    <td style="width: 10pt;">#<?php
                        echo $product->getId() ?></td>
                    <td style="text-align: left;width: 230pt;"><?php
                        echo $product->getName() ?></td>
                    <td style="width: 30pt;"><?php
                        echo number_format( $product->getPrice(), 2, ',', '' ) ?> €
                    </td>
                    <td style="width: 30pt;"><?php
                        echo number_format( $product->getQuantity(), 2, ',', '' ) ?></td>
                    <td style="width: 30pt;"><?php
                        echo number_format( $product->getNetToPay(), 2, ',', '' ) ?> €
                    </td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td style="width: 10pt; border-bottom: 1px solid #CFD1D2">#<?php
                        echo $product->getId() ?></td>
                    <td style="text-align: left;width: 230pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo $product->getName() ?></td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo number_format( $product->getPrice(), 2, ',', '' ) ?> €
                    </td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo number_format( $product->getQuantity(), 2, ',', '' ) ?></td>
                    <td style="width: 30pt; border-bottom: 1px solid #CFD1D2"><?php
                        echo number_format( $product->getNetToPay(), 2, ',', '' ) ?> €
                    </td>
                </tr>
                <?php
            }
            $i++;
        }
        ?>
        <tr>
            <td style="border-left: 0px;" colspan="2"></td>
            <td colspan="1" style="text-align: right;">Total HT</td>
            <td colspan="2"><?php
                echo number_format( $credit->getNetToPay(), 2, ',', '' ) ?> €
            </td>
        </tr>
        <tr>
            <td style="border-left: 0px;" colspan="2"></td>
            <td style="border-bottom: 1px solid #CFD1D2; text-align: right;" colspan="1">Net à payer</td>
            <td style="border-bottom: 1px solid #CFD1D2" colspan="2"><?php
                echo number_format( $credit->getNetToPay(), 2, ',', '' ) ?> €
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border: 0px;"></td>
            <td colspan="3" style="text-align: center; border: 0px;"> TVA non applicable, art. 293 B du CGI</td>
        </tr>
        </tbody>
    </table>
    
    <div class="footer">
        <p>Date d'éxecution: <?php
            echo date( 'd/m/Y' ) ?></p>
        <p>Moyen de paiement: <?php
            echo $credit->getWay() ?></p>
        <p>phrase d'information en cas de non paiement</p>
    </div>

</page>
