<?php


namespace App\Packages\Administer\Tools\Mail\Templates;


use Nomess\Exception\MissingConfigurationException;

interface TemplateInterface
{
    
    /**
     * Return the html code
     *
     * @param array $parameters Your parameters for template
     * @return string
     * @throws MissingConfigurationException
     */
    public function getTemplate( array $parameters ): string;
}
