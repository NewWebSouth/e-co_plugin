<?php


namespace App\Packages\Administer\Tools\Mail\Templates;


use Nomess\Exception\MissingConfigurationException;

class ForgotPasswordTemplate implements TemplateInterface
{
    
    public const PARAM_CLIENT_NAME = 'client_name';
    public const PARAM_LINK        = 'link';
    
    
    public function getTemplate( array $parameters ): string
    {
        $this->controlParameters( $parameters );
        
        $name = $parameters[self::PARAM_CLIENT_NAME];
        $link = $parameters[self::PARAM_LINK];
        
        return "
        <strong>Bonjour $name</strong><br><br>
        
        Cliquez sur ce <a href='$link'>lien</a> pour réinitialiser votre mot de passe.
        
        <div style='font-size: 10px'>
        Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce mail.
        </div>
        ";
    }
    
    
    private function controlParameters( array $parameters ): void
    {
        if( !isset( $parameters[self::PARAM_CLIENT_NAME] ) ) {
            $this->throwException( self::PARAM_CLIENT_NAME );
        }
        
        if( !isset( $parameters[self::PARAM_LINK] ) ) {
            $this->throwException( self::PARAM_LINK );
        }
    }
    
    
    /**
     * @param string $index
     * @throws MissingConfigurationException
     */
    private function throwException( string $index ): void
    {
        throw new MissingConfigurationException( 'Miss the parameter "' . $index . '" for forgot mail template' );
    }
}
