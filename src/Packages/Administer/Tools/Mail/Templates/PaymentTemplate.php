<?php


namespace App\Packages\Administer\Tools\Mail\Templates;


use App\Entities\ProductCustomer;
use Nomess\Exception\MissingConfigurationException;

class PaymentTemplate implements TemplateInterface
{
    
    public const  PARAM_PRODUCTS       = 'products';
    public const  PARAM_CLIENT_NAME    = 'client_name';
    public const  PARAM_ORDERED_REF    = 'ordered_ref';
    public const  PARAM_CLIENT_MESSAGE = 'message';
    
    
    /**
     * @inheritDoc
     */
    public function getTemplate( array $parameters ): string
    {
        $this->controlParameters( $parameters );
        $client_name    = $parameters[self::PARAM_CLIENT_NAME];
        $client_message = $parameters[self::PARAM_CLIENT_MESSAGE];
        $ordered_ref    = $parameters[self::PARAM_ORDERED_REF];
        $products       = $parameters[self::PARAM_PRODUCTS];
        
        return "
Validation d'une nouvelle commande de $client_name !<br><br><br>

<strong>Numéro de commande:</strong> $ordered_ref
<br><br><br>
" . $this->getArrayHtmlProduct( $products ) . "<br><br><strong>Message du client:</strong> $client_message";
    }
    
    
    /**
     * @param ProductCustomer[] $products
     * @return string
     */
    private function getArrayHtmlProduct( array $products ): string
    {
        $content = '<table style="border: 1px solid #baa499; width: 100%; text-align: center"><tr style="background-color: #baa499; border: 1px solid #baa499"><th>Ref</th><th>Nom</th><th>Quantité</th></tr>';
        
        foreach( $products as $productCustomer ) {
            $content .= '<tr><td>' . $productCustomer->getProduct()->getId() . '</td><td>' . $productCustomer->getProduct()->getName() . '</td><td>' . number_format( $productCustomer->getQuantity(), 2, ',', '' ) . '</td></tr>';
        }
        
        return $content . '</table>';
    }
    
    
    /**
     * @param array $parameters
     * @throws MissingConfigurationException
     */
    private function controlParameters( array $parameters ): void
    {
        if( !isset( $parameters[self::PARAM_CLIENT_NAME] ) ) {
            $this->throwMailException( self::PARAM_CLIENT_NAME );
        }
        
        if( !isset( $parameters[self::PARAM_PRODUCTS] ) ) {
            $this->throwMailException( self::PARAM_PRODUCTS );
        }
        
        if( !isset( $parameters[self::PARAM_ORDERED_REF] ) ) {
            $this->throwMailException( self::PARAM_ORDERED_REF );
        }
        
        if( !isset( $parameters[self::PARAM_CLIENT_MESSAGE] ) ) {
            $this->throwMailException( self::PARAM_CLIENT_MESSAGE );
        }
    }
    
    
    /**
     * @param string $index
     * @return string
     * @throws MissingConfigurationException
     */
    private function throwMailException( string $index ): string
    {
        throw new MissingConfigurationException( 'Miss the parameter "' . $index . '" for payment mail template' );
    }
}
