<?php


namespace App\Packages\Administer\Tools\Mail\Templates;


use Nomess\Exception\MissingConfigurationException;
use Nomess\Helpers\DataHelper;

class InvoiceTemplate implements TemplateInterface
{
    
    use DataHelper;
    
    private const DC_NUMBER_PHONE_COMPANY = 'number_phone_company';
    public const  PARAM_CLIENT_NAME       = 'client_name';
    public const  PARAM_ORDERED_REF       = 'ordered_ref';
    
    
    /**
     * @inheritDoc
     */
    public function getTemplate( array $parameters ): string
    {
        $this->controlParameters( $parameters );
        
        $client_name  = $parameters[self::PARAM_CLIENT_NAME];
        $ordered_ref  = $parameters[self::PARAM_ORDERED_REF];
        $phone_number = $this->get( self::DC_NUMBER_PHONE_COMPANY );
        
        return "<div style=\"background: #baa499; height: 100%; width: 100%; padding: 20px;font: 1em 'Open Sans', sans-serif;\">
<h1 style=\"color: #333333; font-size: inherit\"> $client_name, nous avons bien reçu votre commande n°$ordered_ref</h1>
<br>
<p>
    Vous trouverez la facture en piece jointe.
</p>
    <p style=\"font-size: 12px\">
        Si pour une raison quelconque il y avait un problème, nous restons a votre disposition au $phone_number.
    </p>
</div>
";
    }
    
    
    /**
     * @param array $parameters
     * @throws MissingConfigurationException
     */
    private function controlParameters( array $parameters ): void
    {
        if( !isset( $parameters[self::PARAM_CLIENT_NAME] ) ) {
            $this->throwMailException( self::PARAM_CLIENT_NAME );
        }
        
        if( !isset( $parameters[self::PARAM_ORDERED_REF] ) ) {
            $this->throwMailException( self::PARAM_ORDERED_REF );
        }
    }
    
    
    /**
     * @param string $index
     * @return string
     * @throws MissingConfigurationException
     */
    private function throwMailException( string $index ): string
    {
        throw new MissingConfigurationException( 'Miss the parameter "' . $index . '" for invoice mail template' );
    }
}
