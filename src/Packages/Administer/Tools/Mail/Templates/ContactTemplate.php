<?php


namespace App\Packages\Administer\Tools\Mail\Templates;


use Nomess\Exception\MissingConfigurationException;

class ContactTemplate implements TemplateInterface
{
    
    public const PARAM_NAME    = 'name';
    public const PARAM_EMAIL   = 'email';
    public const PARAM_MESSAGE = 'message';
    
    
    /**
     * @inheritDoc
     */
    public function getTemplate( array $parameters ): string
    {
        $this->controlParameters( $parameters );
        $email   = $parameters[self::PARAM_EMAIL];
        $name    = $parameters[self::PARAM_NAME];
        $message = $parameters[self::PARAM_MESSAGE];
        
        return "Page de contact:<br><br>
<strong>Email:</strong> $email
<br>
<strong>Nom:</strong> $name
<br>
<strong>Message:</strong> $message
        ";
    }
    
    
    private function controlParameters( array $parameters ): void
    {
        if( !isset( $parameters[self::PARAM_NAME] ) ) {
            $this->throwException( self::PARAM_NAME );
        }
        
        if( !isset( $parameters[self::PARAM_EMAIL] ) ) {
            $this->throwException( self::PARAM_EMAIL );
        }
        
        if( !isset( $parameters[self::PARAM_MESSAGE] ) ) {
            $this->throwException( self::PARAM_MESSAGE );
        }
    }
    
    
    /**
     * @param string $index
     * @throws MissingConfigurationException
     */
    private function throwException( string $index ): void
    {
        throw new MissingConfigurationException( 'Miss the parameter "' . $index . '" for forgot mail template' );
    }
}
