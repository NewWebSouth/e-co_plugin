<?php


namespace App\Packages\Administer\Tools\Mail;


interface MailAdapterInterface
{
    
    /**
     * @param string $email           Email target
     * @param string $name            name of target
     * @param string $object          object of mail
     * @param string $message         message of mail
     * @param array|null $attachments Join file [path, name, encoding, mime-type]
     * @return bool
     */
    public function sendMail( string $email, string $name, string $object, string $message, array $attachments = NULL ): bool;
}
