<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Invoice;
use App\Packages\Administer\Tools\Mail\MailAdapterInterface;
use App\Packages\Administer\Tools\Mail\Templates\InvoiceTemplate;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Container\ContainerInterface;
use Nomess\Helpers\DataHelper;

class InvoiceInitializer extends AbstractInitializer implements InvoiceMailInterface, TransactionObserverInterface
{
    
    use DataHelper;
    
    private const DC_STORAGE_INVOICE = 'local_storage_invoice';
    private TransactionSubjectInterface $transactionSubject;
    private Invoice                     $invoice;
    
    
    public function __construct(
        ContainerInterface $container,
        MailAdapterInterface $adapter,
        TransactionSubjectInterface $transactionSubject )
    {
        parent::__construct( $container, $adapter );
        $this->transactionSubject = $transactionSubject;
        $this->subscribeToTransactionStatus();
    }
    
    
    /**
     * @inheritDoc
     */
    public function send( Invoice $invoice ): void
    {
        $this->invoice = $invoice;
    }
    
    
    private function buildParameters( Invoice $invoice ): array
    {
        return [
            InvoiceTemplate::PARAM_CLIENT_NAME => $invoice->getCustomer()->getFirstName() . ' ' . $invoice->getCustomer()->getLastName(),
            InvoiceTemplate::PARAM_ORDERED_REF => $invoice->getOrdered()->getId()
        ];
    }
    
    
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            $this->adapter->sendMail(
                $this->invoice->getCustomer()->getEmail(),
                $this->invoice->getCustomer()->getFirstName() . ' ' . $this->invoice->getCustomer()->getLastName(),
                'Votre commande n°' . $this->invoice->getOrdered()->getId(),
                $this->loadTemplate( InvoiceTemplate::class, $this->buildParameters( $this->invoice ) ),
                [ $this->get( self::DC_STORAGE_INVOICE ) . $this->invoice->getDocumentName() ]
            );
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
}
