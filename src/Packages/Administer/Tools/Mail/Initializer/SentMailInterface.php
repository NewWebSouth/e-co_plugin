<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Ordered;

interface SentMailInterface
{
    
    public function send( Ordered $ordered ): void;
}
