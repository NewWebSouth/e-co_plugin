<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Customer;

interface ForgotPasswordMailInterface
{
    
    /**
     * @param Customer $customer
     */
    public function send( Customer $customer ): void;
}
