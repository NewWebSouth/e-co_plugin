<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Packages\Administer\Tools\Mail\Templates\ContactTemplate;
use Nomess\Helpers\DataHelper;

class ContactInitializer extends AbstractInitializer implements ContactMailInterface
{
    
    use DataHelper;
    
    private const DC_EMAIL_ADMIN = 'mail_sender';
    
    
    public function send( string $email, string $name, string $message ): void
    {
        $this->adapter->sendMail(
            NOMESS_CONTEXT === 'PROD' ? $this->get( self::DC_EMAIL_ADMIN ) : 'romainlavabre98@gmail.com',
            '',
            'Page de contact',
            $this->loadTemplate( ContactTemplate::class, [
                ContactTemplate::PARAM_EMAIL   => $email,
                ContactTemplate::PARAM_NAME    => $name,
                ContactTemplate::PARAM_MESSAGE => $message
            ] ) );
    }
}
