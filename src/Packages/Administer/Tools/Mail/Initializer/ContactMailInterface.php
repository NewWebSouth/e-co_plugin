<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


interface ContactMailInterface
{
    
    public function send( string $email, string $name, string $message ): void;
}
