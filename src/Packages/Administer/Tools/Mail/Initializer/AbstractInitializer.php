<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Packages\Administer\Tools\Mail\MailAdapterInterface;
use App\Packages\Administer\Tools\Mail\Templates\TemplateInterface;
use Nomess\Container\ContainerInterface;
use Nomess\Exception\MissingConfigurationException;

class AbstractInitializer
{
    
    protected ContainerInterface   $container;
    protected MailAdapterInterface $adapter;
    
    
    public function __construct(
        ContainerInterface $container,
        MailAdapterInterface $adapter )
    {
        $this->container = $container;
        $this->adapter   = $adapter;
    }
    
    
    /**
     * @param string $templateName
     * @param array $parameters
     * @return string
     * @throws MissingConfigurationException
     */
    protected function loadTemplate( string $templateName, array $parameters ): string
    {
        /** @var TemplateInterface $template */
        $template = $this->container->get( $templateName );
        
        return $template->getTemplate( $parameters );
    }
}
