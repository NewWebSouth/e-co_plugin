<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Payment;
use App\Packages\Administer\Tools\Mail\MailAdapterInterface;
use App\Packages\Administer\Tools\Mail\Templates\PaymentTemplate;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Container\ContainerInterface;
use Nomess\Exception\MissingConfigurationException;
use Nomess\Helpers\DataHelper;

class PaymentInitializer extends AbstractInitializer implements PaymentMailInterface, TransactionObserverInterface
{
    
    use DataHelper;
    
    private const DC_MAIL_SENDER = 'mail_sender';
    private TransactionSubjectInterface $transactionSubject;
    private Payment                     $payment;
    
    
    public function __construct(
        ContainerInterface $container,
        MailAdapterInterface $adapter,
        TransactionSubjectInterface $transactionSubject )
    {
        parent::__construct( $container, $adapter );
        $this->transactionSubject = $transactionSubject;
        $this->subscribeToTransactionStatus();
    }
    
    
    /**
     * @param Payment $payment
     */
    public function send( Payment $payment ): void
    {
        $this->payment = $payment;
    }
    
    
    private function buildParameters( Payment $payment ): array
    {
        return [
            PaymentTemplate::PARAM_CLIENT_NAME    => $payment->getCustomer()->getFirstName() . ' ' . $payment->getCustomer()->getLastName(),
            PaymentTemplate::PARAM_ORDERED_REF    => $payment->getInvoice()->getOrdered()->getId(),
            PaymentTemplate::PARAM_PRODUCTS       => $payment->getInvoice()->getOrdered()->getProductCustomer(),
            PaymentTemplate::PARAM_CLIENT_MESSAGE => $payment->getInvoice()->getOrdered()->getComment()
        ];
    }
    
    
    /**
     * @param bool $status
     * @throws MissingConfigurationException
     */
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            $this->adapter->sendMail(
                ( NOMESS_CONTEXT === 'PROD' ? $this->get( self::DC_MAIL_SENDER ) : 'romainlavabre98@gmail.com' ),
                '',
                'Nouvelle commande',
                $this->loadTemplate( PaymentTemplate::class, $this->buildParameters( $this->payment ) )
            );
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
}
