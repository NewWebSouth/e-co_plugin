<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Ordered;
use App\Packages\Administer\Tools\Mail\MailAdapterInterface;
use App\Packages\Administer\Tools\Mail\Templates\SentTemplate;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Container\ContainerInterface;
use Nomess\Exception\MissingConfigurationException;

class SentInitializer extends AbstractInitializer implements SentMailInterface, TransactionObserverInterface
{
    
    private TransactionSubjectInterface $transactionSubject;
    private Ordered                     $ordered;
    
    
    public function __construct(
        ContainerInterface $container,
        MailAdapterInterface $adapter,
        TransactionSubjectInterface $transactionSubject )
    {
        parent::__construct( $container, $adapter );
        $this->transactionSubject = $transactionSubject;
        
        $this->subscribeToTransactionStatus();
    }
    
    
    public function send( Ordered $ordered ): void
    {
        $this->ordered = $ordered;
    }
    
    
    private function buildParameters( Ordered $ordered ): array
    {
        return [
            SentTemplate::PARAM_CLIENT_NAME => $ordered->getCustomer()->getStrName(),
            SentTemplate::PARAM_ORDERED_REF => $ordered->getId(),
        ];
    }
    
    
    /**
     * @param bool $status
     * @throws MissingConfigurationException
     */
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            $this->adapter->sendMail(
                $this->ordered->getCustomer()->getEmail(),
                $this->ordered->getCustomer()->getStrName(),
                'Votre commande a été expédié',
                $this->loadTemplate( SentTemplate::class, $this->buildParameters( $this->ordered ) )
            );
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
}
