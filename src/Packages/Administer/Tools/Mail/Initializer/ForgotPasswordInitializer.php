<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Customer;
use App\Packages\Administer\Tools\Mail\MailAdapterInterface;
use App\Packages\Administer\Tools\Mail\Templates\ForgotPasswordTemplate;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Container\ContainerInterface;
use Nomess\Helpers\DataHelper;

class ForgotPasswordInitializer extends AbstractInitializer implements ForgotPasswordMailInterface, TransactionObserverInterface
{
    
    use DataHelper;
    
    private const DC_HOST     = 'hostname';
    private const DC_PROTOCOL = 'protocol';
    private TransactionSubjectInterface $transactionSubject;
    private Customer                    $customer;
    
    
    public function __construct(
        ContainerInterface $container,
        MailAdapterInterface $adapter,
        TransactionSubjectInterface $transactionSubject )
    {
        parent::__construct( $container, $adapter );
        $this->transactionSubject = $transactionSubject;
        $this->subscribeToTransactionStatus();
    }
    
    
    /**
     * @param Customer $customer
     */
    public function send( Customer $customer ): void
    {
        $this->customer = $customer;
    }
    
    
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            $this->adapter->sendMail(
                $this->customer->getEmail(),
                $this->customer->getStrName(),
                'Réinitialisation de votre mot de passe',
                $this->loadTemplate( ForgotPasswordTemplate::class, $this->buildParameters( $this->customer ) )
            );
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
    
    
    private function buildParameters( Customer $customer ): array
    {
        return [
            ForgotPasswordTemplate::PARAM_CLIENT_NAME => $customer->getStrName(),
            ForgotPasswordTemplate::PARAM_LINK        => $this->get( self::DC_PROTOCOL ) .
                                                         '://' . $this->get( self::DC_HOST ) .
                                                         '/reset/password/' . $customer->getId() . '/' . $customer->getKey()
        ];
    }
}
