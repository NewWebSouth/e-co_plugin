<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Invoice;
use App\Exception\MailException;
use Nomess\Exception\MissingConfigurationException;

interface InvoiceMailInterface
{
    
    /**
     * @param Invoice $invoice
     * @throws MissingConfigurationException
     * @throws MailException
     */
    public function send( Invoice $invoice ): void;
}
