<?php


namespace App\Packages\Administer\Tools\Mail\Initializer;


use App\Entities\Payment;
use App\Exception\MailException;
use Nomess\Exception\MissingConfigurationException;

interface PaymentMailInterface
{
    
    /**
     * @param Payment $payment
     * @throws MissingConfigurationException
     * @throws MailException
     */
    public function send( Payment $payment ): void;
}
