<?php


namespace App\Packages\Administer\Tools\Mail;


use App\Exception\MailException;
use Nomess\Helpers\DataHelper;
use Nomess\Helpers\ReportHelper;
use PHPMailer\PHPMailer\Exception;

class PhpMailer implements MailAdapterInterface
{
    
    use DataHelper;
    use ReportHelper;
    
    private const CONF_HOST      = 'mail_host';
    private const CONF_SMTP_AUTH = 'mail_smtp_auth';
    private const CONF_PORT      = 'mail_port';
    private const CONF_EMAIL     = 'mail_sender';
    private const CONF_PASSWORD  = 'mail_password_sender';
    private const CONF_NAME      = 'mail_name';
    private const CONF_ENCODING  = 'mail_encode';
    
    
    public function sendMail( string $email, string $name, string $object, string $message, array $attachments = NULL ): bool
    {
        $mail = $this->getInstance();
        $mail->isSMTP();
        $mail->Host     = $this->get( self::CONF_HOST );
        $mail->SMTPAuth = $this->get( self::CONF_SMTP_AUTH );
        $mail->Port     = $this->get( self::CONF_PORT );
        $mail->Username = $this->get( self::CONF_EMAIL );
        $mail->Password = $this->get( self::CONF_PASSWORD );
        $mail->setFrom( $this->get( self::CONF_EMAIL ), $this->get( self::CONF_NAME ) );
        $mail->addAddress( $email, $name );
        $mail->CharSet = $this->get( self::CONF_ENCODING );
        $mail->Subject = $object;
        $mail->msgHTML( $message );
        
        if( !is_null( $attachments ) ) {
            call_user_func_array( [ $mail, 'addAttachment' ], $attachments );
        }
        
        
        try {
            if( !$mail->send() ) {
                if( NOMESS_CONTEXT === 'DEV' ) {
                    throw new MailException( 'Un mail n\'a pas pu être envoyé' );
                } else {
                    $this->report( 'Un mail n\'a pas pu être envoyé' );
                }
                
                return FALSE;
            }
        } catch( Exception $e ) {
            if( NOMESS_CONTEXT === 'DEV' ) {
                throw new MailException( $e->getMessage() );
            } else {
                $this->report( $e->getMessage() );
            }
            
            return FALSE;
        }
        
        return TRUE;
    }
    
    
    private function getInstance(): \PHPMailer\PHPMailer\PHPMailer
    {
        return new \PHPMailer\PHPMailer\PHPMailer();
    }
}
