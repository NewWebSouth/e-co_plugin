<?php


namespace App\Packages\Administer\Tools\Memory;


use App\Entities\Memory;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractCrud implements CreateInterface
{
    
    private const PARAM_CONTENT = 'content';
    
    
    public function create( HttpRequest $request, object $instance ): bool
    {
        $content = (string)$request->getParameter( self::PARAM_CONTENT );
        
        /** @var Memory $instance */
        
        $instance->setContent( $content );
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
}
