<?php


namespace App\Packages\Administer\Tools\Memory;


use App\Entities\Memory;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractCrud implements DeleteInterface
{
    
    public function delete( HttpRequest $request, object $instance ): bool
    {
        /** @var Memory $instance */
        
        $this->entityManager->delete( $instance );
        
        return TRUE;
    }
}
