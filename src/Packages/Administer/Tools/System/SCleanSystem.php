<?php


namespace App\Packages\Administer\Tools\System;


use App\Packages\Administer\Manage\FrontSubjectInterface;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\ImageRepository;
use Nomess\Annotations\Inject;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Helpers\DataHelper;

class SCleanSystem implements FrontSubjectInterface, SystemCleanerInterface
{
    
    use DataHelper;
    
    private const DC_STORAGE_IMAGE      = 'local_storage_image';
    private const DC_LIFE_TIME_DOCUMENT = 'document_life_time';
    private FrontObserverInterface $frontObserver;
    private ImageRepository        $imageRepository;
    private EntityManagerInterface $entityManager;
    private UploadImageInterface   $uploadImageInterface;
    private array                  $associateImage = array();
    
    
    public function __construct(
        ImageRepository $imageRepository,
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadImageInterface )
    {
        $this->imageRepository      = $imageRepository;
        $this->entityManager        = $entityManager;
        $this->uploadImageInterface = $uploadImageInterface;
    }
    
    
    /**
     * @Inject()
     * @param FrontObserverInterface $frontObserver
     */
    public function attachFront( FrontObserverInterface $frontObserver ): void
    {
        $this->frontObserver = $frontObserver;
    }
    
    
    /**
     * The front system will be purge all caches
     */
    public function notifyFront(): void
    {
        $this->frontObserver->notified();
    }
    
    
    public function clean(): int
    {
        $recoveredMemory = 0;
        
        $images = $this->imageRepository->findAll();
        
        foreach( $images as $image ) {
            
            $filesize = $this->getFileSize( $image->getLocalFilename() );
            
            if( $this->uploadImageInterface->cleanImageRepository( $image ) ) {
                $recoveredMemory += $filesize;
            }
            
            $this->associateImage[$image->getLocalFilename()] = TRUE;
        }
        
        return $recoveredMemory += $this->purgeIsolatedImages();
    }
    
    
    private function purgeIsolatedImages(): int
    {
        $recoveredMemory = 0;
        
        $images = scandir( $this->get( self::DC_STORAGE_IMAGE ) );
        
        foreach( $images as $imageName ) {
            $filename = $this->get( self::DC_STORAGE_IMAGE ) . $imageName;
            
            if( $imageName !== '.' && $imageName !== '..'
                && !array_key_exists( $filename, $this->associateImage ) ) {
                
                if( file_exists( $filename ) ) {
                    $recoveredMemory += $this->getFileSize( $filename );
                    unlink( $filename );
                }
            }
        }
        
        return $recoveredMemory;
    }
    
    
    private function getFileSize( string $filename ): int
    {
        if( file_exists( $filename ) ) {
            try {
                chmod( $filename, 777 );
                
                return filesize( $filename );
            } catch( \Throwable $th ) {
            }
        }
        
        return 0;
    }
}
