<?php


namespace App\Packages\Administer\Tools\System\Image;


interface CleanerImageInterface
{

    /**
     * Cleaner of image
     *
     * @param object $instance
     */
    public function purgeImage( object $instance ): void;
}
