<?php


namespace App\Packages\Administer\Tools\System\Image;


use App\Entities\Image;

interface UploadImageInterface
{
    
    public function getImage( array $part ): Image;
    
    
    public function cleanImageRepository( Image $image ): bool;
    
    
    public function convertToMultipleArray( array $parts ): array;
}
