<?php


namespace App\Packages\Administer\Tools\System\Image;


use App\Entities\Image;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\DeleteInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SDelete extends AbstractCrud implements DeleteInterface
{
    
    private UploadImageInterface $uploaderManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager = $uploadManager;
    }
    
    
    public function delete( HttpRequest $request, object $instance ): bool
    {
        
        /** @var Image|null $instance */
        if( $this->uploaderManager->cleanImageRepository( $instance ) ) {
            return TRUE;
        }
        
        $this->setRepository( 'error', 'L\'image n\'a pas pu être supprimé, plusieurs éléments en dépendent' );
        
        return FALSE;
    }
}
