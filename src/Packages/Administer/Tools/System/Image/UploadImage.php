<?php


namespace App\Packages\Administer\Tools\System\Image;


use App\Entities\Image;
use App\Packages\Administer\Tools\System\AbstractUploader;
use App\Repositories\ImageRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Container\ContainerInterface;
use Nomess\Helpers\DataHelper;

class UploadImage extends AbstractUploader implements UploadImageInterface, TransactionObserverInterface
{
    
    use DataHelper;
    
    private const KEY_LOCAL_STORAGE_IMAGE  = 'local_storage_image';
    private const KEY_PUBLIC_STORAGE_IMAGE = 'public_storage_image';
    private EntityManagerInterface      $entityManager;
    private ContainerInterface          $container;
    private TransactionSubjectInterface $transactionSubject;
    private ImageRepository             $imageRepository;
    private array                       $imageToDelete = array();
    private array                       $imageToMove   = array();
    private bool                        $subscribed    = FALSE;
    
    
    /**
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface $container
     * @param TransactionSubjectInterface $transactionSubject
     * @param ImageRepository $imageRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container,
        TransactionSubjectInterface $transactionSubject,
        ImageRepository $imageRepository )
    {
        $this->entityManager      = $entityManager;
        $this->container          = $container;
        $this->transactionSubject = $transactionSubject;
        $this->imageRepository    = $imageRepository;
    }
    
    
    /**
     * Receive a part, if image already exists, she's keep else, create instance and store image
     *
     * @param array $part
     * @return Image
     */
    public function getImage( array $part ): Image
    {
        
        //Get all images for control that the name is unique
        $images = $this->imageRepository->findAll();
        
        $imageFound = $this->imageNotExists( $images, $part['name'] );
        
        $instance = $this->getInstance();
        
        if( $imageFound !== NULL ) {
            //Attach image already existant to new image instance
            $instance = $imageFound;
        } else {
            
            $originalImageName = $part['name'];
            
            //Setting image
            $instance->setName( $originalImageName );
            $instance->setLocalFilename( $this->getLocalDirectory( $originalImageName ) );
            $instance->setPublicFilename( $this->getPublicDirectory( $originalImageName ) );
            
            
            // Move the image of the tmp directory
            $this->imageToMove[] = $part;
            $this->subscribeToTransactionStatus();
        }
        
        $this->entityManager->persists( $instance );
        
        return $instance;
    }
    
    
    /**
     * Search by name, if exists, images is refused
     *
     * @param array|null $images
     * @param string $originalName
     * @return Image|null
     */
    private function imageNotExists( ?array $images, string $originalName ): ?Image
    {
        if( !empty( $images ) ) {
            foreach( $images as $image ) {
                
                if( $image->getName() === $originalName ) {
                    return $image;
                }
            }
        }
        
        return NULL;
    }
    
    
    /**
     * @return Image
     */
    private function getInstance(): Image
    {
        return new Image();
    }
    
    
    /**
     * Return the path of image
     *
     * @param string $imageName
     * @return string
     */
    private function getLocalDirectory( string $imageName ): string
    {
        return $this->get( self::KEY_LOCAL_STORAGE_IMAGE ) . $imageName;
    }
    
    
    /**
     * Return the public path of image
     *
     * @param string $imageName
     * @return string|null
     */
    private function getPublicDirectory( ?string $imageName ): string
    {
        return $this->get( self::KEY_PUBLIC_STORAGE_IMAGE ) . $imageName;
    }
    
    
    /**
     * Image image is unused, she's removed
     *
     * @param Image|null $image
     * @param bool $force
     * @return bool
     */
    public function cleanImageRepository( Image $image, bool $force = FALSE ): bool
    {
        $this->subscribeToTransactionStatus();
        
        if( $force === TRUE ||
            ( empty( $image->getArticles() )
              && empty( $image->getSections() )
              && empty( $image->getGraphicRuleProduct() )
              && empty( $image->getGraphicRuleReduction() ) ) ) {
            
            $this->imageToDelete[] = $image->getLocalFilename();
            $this->entityManager->delete( $image );
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    
    /**
     * If status have success, the image is uploaded or removed
     *
     * @param bool $status
     */
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status === TRUE ) {
            if( !empty( $this->imageToDelete ) ) {
                foreach( $this->imageToDelete as $path ) {
                    if( file_exists( $path ) ) {
                        unlink( $path );
                    }
                }
            }
            
            if( !empty( $this->imageToMove ) ) {
                foreach( $this->imageToMove as $part ) {
                    $this->move( $part, $this->getLocalDirectory( $part['name'] ) );
                }
            }
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        if( $this->subscribed === FALSE ) {
            $this->transactionSubject->addSubscriber( $this );
            $this->subscribed = TRUE;
        }
    }
}
