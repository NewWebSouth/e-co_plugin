<?php


namespace App\Packages\Administer\Tools\System\Image;


use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SCreate extends AbstractCrud implements CreateInterface
{
    
    private const PARAM_IMAGE = 'image';
    private UploadImageInterface $uploaderManager;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        UploadImageInterface $uploadManager )
    {
        parent::__construct( $entityManager );
        $this->uploaderManager = $uploadManager;
    }
    
    
    public function create( HttpRequest $request, object $instance ): bool
    {
        $image = (array)$request->getPart( self::PARAM_IMAGE );
        
        if( !empty( $image ) ) {
            $this->uploaderManager->getImage( $image );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Une erreur s\'est produite' );
        
        return FALSE;
    }
}
