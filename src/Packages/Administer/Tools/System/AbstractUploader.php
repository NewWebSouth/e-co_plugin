<?php


namespace App\Packages\Administer\Tools\System;


abstract class AbstractUploader
{
    
    protected function move( array $part, string $path ): bool
    {
        if( move_uploaded_file( $part['tmp_name'], $path ) ) {
            return TRUE;
        }
        
        return FALSE;
    }
    
    
    public function convertToMultipleArray( array $parts ): array
    {
        if( !is_array( $parts['name'] ) ) {
            return [ $parts ];
        }
        
        $list      = array();
        $iteration = count( $parts['name'] );
        
        for( $i = 0; $i < $iteration; $i++ ) {
            $list[] = [
                'name'     => $parts['name'][$i],
                'type'     => $parts['type'][$i],
                'tmp_name' => $parts['tmp_name'][$i],
                'error'    => $parts['error'][$i],
                'size'     => $parts['size'][$i],
            ];
        }
        
        return $list;
    }
}
