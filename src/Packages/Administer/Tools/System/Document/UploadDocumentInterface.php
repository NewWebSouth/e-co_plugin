<?php


namespace App\Packages\Administer\Tools\System\Document;


use App\Entities\Document;

interface UploadDocumentInterface
{
    
    public function getDocument( array $parts, string $for ): Document;
    
    
    public function convertToMultipleArray( array $parts ): array;
}
