<?php


namespace App\Packages\Administer\Tools\System\Document;


use App\Entities\Document;
use App\Packages\Administer\Tools\System\AbstractUploader;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NotFoundException;
use Nomess\Helpers\DataHelper;

class UploadDocument extends AbstractUploader implements UploadDocumentInterface, TransactionObserverInterface
{
    
    use DataHelper;
    
    private const DC_PATH_STORAGE_INVOICE  = 'local_storage_invoice';
    private const DC_PATH_STORAGE_CREDIT   = 'local_storage_credit';
    private const DC_PATH_STORAGE_PURCHASE = 'local_storage_purchase';
    private EntityManagerInterface      $entityManager;
    private TransactionSubjectInterface $transactionSubject;
    private array                       $documentToMove = array();
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        TransactionSubjectInterface $transactionSubject )
    {
        $this->entityManager      = $entityManager;
        $this->transactionSubject = $transactionSubject;
        $this->subscribeToTransactionStatus();
    }
    
    
    /**
     * Create a new document, if name is already used, is modified by increment
     *
     * @param array $parts
     * @param string $for
     * @return Document
     * @throws InvalidParamException
     * @throws NotFoundException
     */
    public function getDocument( array $parts, string $for ): Document
    {
        $document = $this->getInstance();
        
        $path = $this->getPath( $for );
        $name = $this->getNameOfDocument( $parts['name'], $path );
        
        $document->setDocumentName( $name )
                 ->setLocalPath( $path . $name )
                 ->setMimeType( $parts['type'] )
                 ->setType( $for );
        
        $parts['document'] = $document;
        
        $this->documentToMove[] = $parts;
        
        return $document;
    }
    
    
    /**
     * If name already exists, add ($i) after name
     *
     * @param string $name
     * @param string $path
     * @return string
     */
    private function getNameOfDocument( string $name, string $path ): string
    {
        if( file_exists( $path . $name ) ) {
            $iteration = count( scandir( $path ) );
            $tmp       = $this->decomposeFilename( $name );
            $name      = $tmp['name'];
            $extension = $tmp['extension'];
            
            for( $i = 1; $i < $iteration; $i++ ) {
                if( !file_exists( "$path$name($i).$extension" ) ) {
                    return "$name($i).$extension";
                }
            }
        }
        
        return $name;
    }
    
    
    /**
     * Return the good path for document type
     *
     * @param string $for
     * @return string
     * @throws InvalidParamException
     */
    private function getPath( string $for ): string
    {
        if( $for === 'invoice' ) {
            return $this->get( self::DC_PATH_STORAGE_INVOICE );
        }
        
        if( $for === 'credit' ) {
            return $this->get( self::DC_PATH_STORAGE_CREDIT );
        }
        
        if( $for === 'purchase' ) {
            return $this->get( self::DC_PATH_STORAGE_PURCHASE );
        }
        
        throw new InvalidParamException( 'The type of document must be for "invoice", "credit" or "purchase"' );
    }
    
    
    private function decomposeFilename( string $name ): array
    {
        $tmp       = explode( '.', $name );
        $extension = $tmp[count( $tmp ) - 1];
        unset( $tmp[count( $tmp ) - 1] );
        $name = implode( '.', $tmp );
        
        return [
            'name'      => $name,
            'extension' => $extension
        ];
    }
    
    
    private function getInstance(): Document
    {
        return new Document();
    }
    
    
    /**
     * If transaction validate, upload file
     *
     * @param bool $status
     */
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            foreach( $this->documentToMove as $part ) {
                $this->move( $part, $part['document']->getLocalPath() );
            }
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
}
