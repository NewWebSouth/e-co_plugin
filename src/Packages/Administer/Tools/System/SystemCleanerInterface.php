<?php


namespace App\Packages\Administer\Tools\System;


interface SystemCleanerInterface
{
    
    public function clean(): int;
}
