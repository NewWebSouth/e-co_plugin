<?php


namespace App\Packages\Cunsumption\Stock;


use App\Entities\Ordered;
use Nomess\Components\EntityManager\EntityManagerInterface;

class SRollback implements StockRollbackInterface
{
    
    private EntityManagerInterface $entityManager;
    
    
    public function __construct( EntityManagerInterface $entityManager )
    {
        $this->entityManager = $entityManager;
    }
    
    
    public function rollback( Ordered $ordered ): void
    {
        foreach( $ordered->getProductCustomer() as $productCustomer ) {
            if( !empty( $productCustomer->getProduct()->getStocks() ) ) {
                foreach( $productCustomer->getProduct()->getStocks() as $stock ) {
                    if( !is_null( $productCustomer->getProduct()->getCompositionStocks( $stock->getId() ) ) ) {
                        $stock->add( $productCustomer->getProduct()->getCompositionStocks( $stock->getId() ) );
                    } else {
                        $stock->add( 1 );
                    }
                    
                    $this->entityManager->persists( $stock );
                }
            }
        }
    }
}
