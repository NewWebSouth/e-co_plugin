<?php


namespace App\Packages\Cunsumption\Stock;


use App\Entities\Ordered;

interface StockRollbackInterface
{
    
    public function rollback( Ordered $ordered ): void;
}
