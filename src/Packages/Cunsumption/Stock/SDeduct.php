<?php


namespace App\Packages\Cunsumption\Stock;


use App\Entities\ProductCustomer;
use App\Entities\Stock;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;

class SDeduct
{
    
    private EntityManagerInterface $entityManager;
    private StockRepository        $stockRepository;
    
    
    public function __construct( EntityManagerInterface $entityManager, StockRepository $stockRepository )
    {
        $this->entityManager   = $entityManager;
        $this->stockRepository = $stockRepository;
    }
    
    
    /**
     * @param ProductCustomer[] $productCustomers
     * @throws InvalidParamException
     */
    public function deduct( array $productCustomers ): void
    {
        foreach( $productCustomers as $productCustomer ) {
            if( !empty( $productCustomer->getProduct()->getStocks() ) ) {
                foreach( $productCustomer->getProduct()->getStocks() as $stock ) {
                    /** @var Stock $stock */
                    $stock = $this->stockRepository->findById( $stock->getId(), TRUE );
                    
                    $stock->deduct( $productCustomer->getProduct()->getCompositionStocks( $stock->getId() ) );
                    
                    $this->entityManager->persists( $stock );
                }
            }
        }
    }
}
