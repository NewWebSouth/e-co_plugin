<?php


namespace App\Packages\Cunsumption\Payment\Stripe;


use App\Entities\Ordered;
use App\Packages\Cunsumption\Payment\PaymentAdapterInterface;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Helpers\DataHelper;
use Nomess\Helpers\ResponseHelper;
use Nomess\Http\HttpRequest;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class SPaymentIntent extends AbstractCrud implements PaymentAdapterInterface
{
    
    use ResponseHelper;
    use DataHelper;
    
    private const DC_SECRET_API_KEY = 'private_api_key';
    private ServiceInterface $serviceControlOrdered;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        ServiceInterface $serviceControlOrdered )
    {
        parent::__construct( $entityManager );
        $this->serviceControlOrdered = $serviceControlOrdered;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param Ordered|object $instance
     * @return bool
     * @throws ApiErrorException|InvalidParamException
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        if( !$this->serviceControlOrdered->service( $request, $instance ) ) {
            return FALSE;
        }
        
        
        $this->setKey();
        
        try {
            $output = [
                'clientSecret' => $this->createPaymentIntent( $instance )->client_secret,
            ];
            
            $this->setRepository( self::SUCCESS, $output );
            
            return TRUE;
        } catch( \Error $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
        }
        
        var_dump( 'pass' );
        
        return FALSE;
    }
    
    
    private function setKey(): void
    {
        Stripe::setApiKey( $this->get( self::DC_SECRET_API_KEY ) );
    }
    
    
    /**
     * @param object $jsonObj
     * @param Ordered $ordered
     * @return PaymentIntent
     * @throws ApiErrorException
     * @throws InvalidParamException
     */
    private function createPaymentIntent( Ordered $ordered ): PaymentIntent
    {
        return PaymentIntent::create( [
                                          'amount'      => $this->getAmount( $ordered ),
                                          'currency'    => 'eur',
                                          'description' => '[' . $ordered->getId() . '] Commande pour le client n°' .
                                                           $ordered->getCustomer()->getId() . ' = ' .
                                                           $ordered->getCustomer()->getFirstName() . ' ' .
                                                           $ordered->getCustomer()->getLastName(),
                                      ] );
    }
    
    
    /**
     * @param Ordered $ordered
     * @return float
     */
    private function getAmount( Ordered $ordered ): float
    {
        return $ordered->getNetToPay() * 100;
    }
}
