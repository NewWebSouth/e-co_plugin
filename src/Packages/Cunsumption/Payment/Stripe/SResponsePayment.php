<?php


namespace App\Packages\Cunsumption\Payment\Stripe;


use App\Entities\Ordered;
use App\Packages\Cunsumption\Invoice\AttachInvoiceInterface;
use App\Packages\Cunsumption\Payment\PaymentAdapterInterface;
use App\Repositories\OrderedRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Helpers\DataHelper;
use Nomess\Http\HttpRequest;
use Stripe\Event;
use Stripe\Stripe;

class SResponsePayment extends AbstractCrud implements PaymentAdapterInterface
{
    
    use DataHelper;
    
    private const DC_SECRET_API_KEY  = 'private_api_key';
    private const DC_SECRET_ENDPOINT = 'private_endpoint_key';
    private OrderedRepository      $orderedRepository;
    private AttachInvoiceInterface $attacheInvoice;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        OrderedRepository $orderedRepository,
        AttachInvoiceInterface $attachInvoice )
    {
        parent::__construct( $entityManager );
        $this->orderedRepository = $orderedRepository;
        $this->attacheInvoice    = $attachInvoice;
    }
    
    
    public function service( HttpRequest $request, object $instance ): bool
    {
        $this->setKey();
        $event   = $this->getEvent( $request );
        $ordered = $this->getOrdered( $event );
        
        $ordered->setStatus( 2 );
        $this->attacheInvoice->create( $ordered );
        
        return TRUE;
    }
    
    
    private function setKey(): void
    {
        Stripe::setApiKey( $this->get( self::DC_SECRET_API_KEY ) );
    }
    
    
    private function getEvent( HttpRequest $request ): Event
    {
        $endpoint_secret = $this->get( self::DC_SECRET_ENDPOINT );
        $payload         = @file_get_contents( 'php://input' );
        $sig_header      = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        
        try {
            return \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch( \UnexpectedValueException $e ) {
            http_response_code( 400 );
            exit();
        } catch( \Stripe\Exception\SignatureVerificationException $e ) {
            http_response_code( 400 );
            exit();
        }
    }
    
    
    private function getOrdered( Event $event ): Ordered
    {
        $description = $event->data->object->charges->data[0]->description;
        
        preg_match( '/^\[([0-9]+)] Commande .+/', $description, $id );
        
        return $this->orderedRepository->findById( $id[1] );
    }
}
