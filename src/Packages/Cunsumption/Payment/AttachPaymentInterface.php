<?php


namespace App\Packages\Cunsumption\Payment;


use App\Entities\Invoice;

interface AttachPaymentInterface
{
    
    public function create( Invoice $invoice ): void;
}
