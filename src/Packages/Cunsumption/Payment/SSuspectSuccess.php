<?php


namespace App\Packages\Cunsumption\Payment;


use App\Entities\Ordered;
use App\Packages\Cunsumption\Invoice\AttachInvoiceInterface;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Helpers\DataHelper;
use Nomess\Http\HttpRequest;

class SSuspectSuccess extends AbstractCrud implements UpdateInterface
{
    
    use DataHelper;
    
    private const DC_CONF_WAIT_WEBHOOK = 'conf_wait_webhook';
    private AttachInvoiceInterface $attachInvoice;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        AttachInvoiceInterface $attachInvoice )
    {
        parent::__construct( $entityManager );
        $this->attachInvoice = $attachInvoice;
    }
    
    
    /**
     * Update the status of ordered when payment is suspected to have success
     *
     * @param HttpRequest $request
     * @param Ordered|object $instance
     * @return bool
     */
    public function update( HttpRequest $request, object $instance ): bool
    {
        
        $instance->setStatus( 1 );
        
        $this->entityManager->persists( $instance );
        
        if( !$this->get( self::DC_CONF_WAIT_WEBHOOK ) ) {
            $instance->setStatus( 2 );
            $this->attachInvoice->create( $instance );
        }
        
        return TRUE;
    }
}
