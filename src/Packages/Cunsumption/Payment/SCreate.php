<?php


namespace App\Packages\Cunsumption\Payment;


use App\Entities\Invoice;
use App\Entities\Payment;
use Nomess\Components\EntityManager\EntityManagerInterface;

class SCreate implements AttachPaymentInterface
{
    
    private EntityManagerInterface $entityManager;
    
    
    public function __construct( EntityManagerInterface $entityManager )
    {
        $this->entityManager = $entityManager;
    }
    
    
    public function create( Invoice $invoice ): void
    {
        $payment = ( new Payment() )
            ->setCustomer( $invoice->getCustomer() )
            ->setInvoice( $invoice )
            ->setWay( 'Carte bancaire' );
        
        $this->entityManager->persists( $payment );
    }
}
