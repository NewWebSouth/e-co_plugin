<?php


namespace App\Packages\Cunsumption\Payment;


use App\Entities\Customer;
use App\Entities\Ordered;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpSession;

class SControlOrdered extends AbstractCrud implements ServiceInterface
{
    
    private const SESSION_CUSTOMER = 'customer';
    private HttpSession $session;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        HttpSession $session )
    {
        parent::__construct( $entityManager );
        $this->session = $session;
    }
    
    
    /**
     * @param HttpRequest $request
     * @param Ordered|object $instance
     * @return bool
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        return $this->isNeverPaid( $instance ) && $this->isValidCustomer( $instance->getCustomer() );
    }
    
    
    private function isNeverPaid( Ordered $ordered ): bool
    {
        return $ordered->getStatus() === 0;
    }
    
    
    /**
     * Valid that customer connected and ordered's customer is equal
     *
     * @param Customer $customer
     * @return bool
     */
    private function isValidCustomer( Customer $customer ): bool
    {
        return $this->session->get( self::SESSION_CUSTOMER )->getId() === $customer->getId();
    }
}
