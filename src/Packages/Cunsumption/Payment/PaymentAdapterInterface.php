<?php


namespace App\Packages\Cunsumption\Payment;


use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

interface PaymentAdapterInterface extends ServiceInterface
{
    
    public function getRepository( ?string $index = NULL );
    
    
    public function service( HttpRequest $request, object $instance ): bool;
}
