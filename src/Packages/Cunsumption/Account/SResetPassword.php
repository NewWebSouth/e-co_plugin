<?php


namespace App\Packages\Cunsumption\Account;


use App\Entities\Customer;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SResetPassword extends AbstractCrud implements ServiceInterface
{
    
    private const PARAM_KEY           = 'key';
    private const PARAM_PASSWORD      = 'password';
    private const PARAM_CONF_PASSWORD = 'conf_password';
    
    
    /**
     * @param HttpRequest $request
     * @param Customer|object $customer
     * @return bool
     */
    public function service( HttpRequest $request, object $customer ): bool
    {
        $key       = (string)$request->getParameter( self::PARAM_KEY );
        $password  = (string)$request->getParameter( self::PARAM_PASSWORD );
        $password2 = (string)$request->getParameter( self::PARAM_CONF_PASSWORD );
        
        if( $this->isValidKey( $key, $customer ) ) {
            try {
                $customer->setKey( NULL )
                         ->setPassword( $this->hetHashedPassword( $password, $password2 ) );
            } catch( InvalidParamException $e ) {
                $this->setRepository( self::ERROR, $e->getMessage() );
                
                return FALSE;
            }
            
            $this->entityManager->persists( $customer );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Une erreur s\'est produite' );
        
        return FALSE;
    }
    
    
    /**
     * @param string $password
     * @param string $password2
     * @return string
     * @throws InvalidParamException
     */
    private function hetHashedPassword( string $password, string $password2 ): string
    {
        if( empty( $password ) || empty( $password2 ) ) {
            throw new InvalidParamException( 'Merci de confirmer votre mot de passe' );
        } elseif( $password !== $password2 ) {
            throw new InvalidParamException( 'Vos mots de passe ne correspondent pas' );
        } elseif( mb_strlen( $password ) < 6 ) {
            throw new InvalidParamException( 'Votre mot de passe doit contenir au moins 6 caractères' );
        }
        
        return password_hash( $password, PASSWORD_DEFAULT );
    }
    
    
    /**
     * Valid that the key is ok
     *
     * @param string $key
     * @param Customer $customer
     * @return bool
     */
    private function isValidKey( string $key, Customer $customer ): bool
    {
        return $key === $customer->getKey();
    }
}
