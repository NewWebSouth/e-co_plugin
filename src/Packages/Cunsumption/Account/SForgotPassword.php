<?php


namespace App\Packages\Cunsumption\Account;


use App\Entities\Customer;
use App\Packages\Administer\Tools\Mail\Initializer\ForgotPasswordMailInterface;
use App\Repositories\CustomerRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SForgotPassword extends AbstractCrud implements ServiceInterface
{
    
    private const PARAM_EMAIL = 'email';
    private CustomerRepository          $customerRepository;
    private ForgotPasswordMailInterface $forgotPasswordMail;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        CustomerRepository $customerRepository,
        ForgotPasswordMailInterface $forgotPasswordMail )
    {
        parent::__construct( $entityManager );
        $this->customerRepository = $customerRepository;
        $this->forgotPasswordMail = $forgotPasswordMail;
    }
    
    
    /**
     * Generate key and send mail for reinitialization of customer's password
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function service( HttpRequest $request, object $instance ): bool
    {
        $email = (string)$request->getParameter( self::PARAM_EMAIL );
        
        try {
            $customer = $this->getCustomer( $email )
                             ->setKey( $this->generateKey() );
        } catch( NullPointerException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $customer );
        $this->forgotPasswordMail->send( $customer );
        
        return TRUE;
    }
    
    
    /**
     * @param string $email
     * @return Customer
     * @throws NullPointerException
     */
    private function getCustomer( string $email ): Customer
    {
        $customer = $this->customerRepository->findByEmail( $email );
        
        if( !empty( $customer ) ) {
            return $customer;
        }
        
        throw new NullPointerException( 'Une erreur s\'est produite' );
    }
    
    
    /**
     * Return the tmp key for reinitialization of password
     *
     * @return string
     */
    private function generateKey(): string
    {
        return md5( str_shuffle( 'azertyuiopqsdfghjklmwxcvbn123456789' ) );
    }
}
