<?php


namespace App\Packages\Cunsumption\Account;


use App\Entities\Customer;
use App\Repositories\CustomerRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpSession;

class SLogin extends AbstractCrud
{
    
    private const PARAM_EMAIL      = 'email';
    private const PARAM_PASSWORD   = 'password';
    private const SESSION_CUSTOMER = 'customer';
    private HttpSession        $session;
    private CustomerRepository $customerRepository;
    
    
    public function __construct( EntityManagerInterface $entityManager, HttpSession $session, CustomerRepository $customerRepository )
    {
        parent::__construct( $entityManager );
        $this->session            = $session;
        $this->customerRepository = $customerRepository;
    }
    
    
    public function login( HttpRequest $request ): bool
    {
        $email    = (string)$request->getParameter( self::PARAM_EMAIL );
        $password = (string)$request->getParameter( self::PARAM_PASSWORD );
        
        try {
            $this->vEmail( $email );;
            $this->vPassword( $password );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        /** @var Customer $customer */
        $customer = $this->customerRepository->findByEmail( $email );
        
        if( is_array( $customer ) ) {
            $customer = $customer[0];
        }
        
        if( password_verify( $password, $customer->getPassword() ) ) {
            $this->session->installSecurityModules( TRUE, TRUE, FALSE )
                          ->set( self::SESSION_CUSTOMER, $customer );
            
            return TRUE;
        }
        
        $this->setRepository( self::ERROR, 'Une erreur s\'est produite' );
        
        return FALSE;
    }
    
    
    /**
     * @param string|null $email
     * @throws InvalidParamException
     */
    private function vEmail( ?string $email ): void
    {
        if( empty( $email ) ) {
            throw new InvalidParamException( 'Merci de saisir un email valide' );
        }
    }
    
    
    /**
     * @param string $password
     * @throws InvalidParamException
     */
    private function vPassword( string $password ): void
    {
        if( empty( $password ) ) {
            throw new InvalidParamException( 'Merci de saisir un mot de passe' );
        }
    }
}
