<?php


namespace App\Packages\Cunsumption\Account;


use App\Entities\Customer;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Http\HttpRequest;

class SDisable extends AbstractCrud implements ServiceInterface
{
    
    public function service( HttpRequest $request, object $instance ): bool
    {
        /** @var Customer $instance */
        
        $instance->setActive( FALSE );
        
        return TRUE;
    }
}
