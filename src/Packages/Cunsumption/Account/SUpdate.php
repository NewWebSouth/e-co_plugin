<?php


namespace App\Packages\Cunsumption\Account;


use App\Entities\Customer;
use App\Repositories\CustomerRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\UpdateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SUpdate extends AbstractCrud implements UpdateInterface
{
    
    private const PARAM_FIRST_NAME    = 'first_name';
    private const PARAM_LAST_NAME     = 'last_name';
    private const PARAM_PHONE         = 'phone';
    private const PARAM_EMAIL         = 'email';
    private const PARAM_NUMBER_STREET = 'number_street';
    private const PARAM_STREET        = 'street';
    private const PARAM_POSTAL_CODE   = 'postal_code';
    private const PARAM_CITY          = 'city';
    private const PARAM_COUNTRY       = 'country';
    private const PARAM_COMMENT       = 'comment';
    private const PARAM_OFFER_ACCEPT  = 'offer_accept';
    private CustomerRepository $customerRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        CustomerRepository $customerRepository )
    {
        parent::__construct( $entityManager );
        $this->customerRepository = $customerRepository;
    }
    
    
    public function update( HttpRequest $request, object $instance ): bool
    {
        $firstName    = (string)$request->getParameter( self::PARAM_FIRST_NAME );
        $lastName     = (string)$request->getParameter( self::PARAM_LAST_NAME );
        $phone        = (string)$request->getParameter( self::PARAM_PHONE );
        $email        = (string)$request->getParameter( self::PARAM_EMAIL );
        $numberStreet = (string)$request->getParameter( self::PARAM_NUMBER_STREET );
        $street       = (string)$request->getParameter( self::PARAM_STREET );
        $postalCode   = (string)$request->getParameter( self::PARAM_POSTAL_CODE );
        $city         = (string)$request->getParameter( self::PARAM_CITY );
        $country      = (string)$request->getParameter( self::PARAM_COUNTRY );
        $comment      = (string)$request->getParameter( self::PARAM_COMMENT );
        $offerAccept  = (bool)$request->getParameter( self::PARAM_OFFER_ACCEPT );
        
        if( $instance->getEmail() !== $email && !$this->isUniqueEmail( $email ) ) {
            $this->setRepository( self::ERROR, 'Un compte porte déjà cet email' );
            
            return FALSE;
        }
        
        /** @var Customer $instance */
        
        try {
            $instance->setFirstName( $firstName )
                     ->setLastName( $lastName )
                     ->setTel( $phone )
                     ->setEmail( $this->validEmail( $email ) )
                     ->setNumberStreet( $numberStreet )
                     ->setStreet( $street )
                     ->setPostalCode( $postalCode )
                     ->setCity( $city )
                     ->setCountry( $country )
                     ->setComment( $comment )
                     ->setOfferAccept( $offerAccept );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
    
    
    private function validEmail( string $email ): string
    {
        if( !preg_match( '/.+@.+\..+/', $email ) ) {
            throw new InvalidParamException( 'Merci de saisir un email valide' );
        }
        
        return $email;
    }
    
    
    private function isUniqueEmail( string $email ): bool
    {
        return $this->customerRepository->findByEmail( $email ) === NULL;
    }
}
