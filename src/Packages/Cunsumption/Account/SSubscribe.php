<?php


namespace App\Packages\Cunsumption\Account;


use App\Entities\Customer;
use App\Repositories\CustomerRepository;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;

class SSubscribe extends AbstractCrud
{
    
    private const PARAM_FIRST_NAME    = 'first_name';
    private const PARAM_LAST_NAME     = 'last_name';
    private const PARAM_EMAIL         = 'email';
    private const PARAM_PASSWORD      = 'password';
    private const PARAM_CONF_PASSWORD = 'conf_password';
    private const PARAM_TEL           = 'phone';
    private const PARAM_NUMBER_STREET = 'number_street';
    private const PARAM_STREET        = 'street';
    private const PARAM_POSTAL_CODE   = 'postal_code';
    private const PARAM_CITY          = 'city';
    private const PARAM_COUNTRY       = 'country';
    private const PARAM_OFFER_ACCEPT  = 'offer_accept';
    private CustomerRepository $customerRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        CustomerRepository $customerRepository )
    {
        parent::__construct( $entityManager );
        $this->customerRepository = $customerRepository;
    }
    
    
    public function subscribe( HttpRequest $request, Customer $customer ): bool
    {
        $firstName    = (string)$request->getParameter( self::PARAM_FIRST_NAME );
        $lastName     = (string)$request->getParameter( self::PARAM_LAST_NAME );
        $email        = (string)$request->getParameter( self::PARAM_EMAIL );
        $password     = (string)$request->getParameter( self::PARAM_PASSWORD );
        $confPassword = (string)$request->getParameter( self::PARAM_CONF_PASSWORD );
        $phone        = (string)$request->getParameter( self::PARAM_TEL );
        $numberStreet = (string)$request->getParameter( self::PARAM_NUMBER_STREET );
        $street       = (string)$request->getParameter( self::PARAM_STREET );
        $postalCode   = (string)$request->getParameter( self::PARAM_POSTAL_CODE );
        $city         = (string)$request->getParameter( self::PARAM_CITY );
        $country      = (string)$request->getParameter( self::PARAM_COUNTRY );
        $offerAccept  = (bool)$request->getParameter( self::PARAM_OFFER_ACCEPT );
        
        if( !$this->isUniqueEmail( $email ) ) {
            $this->setRepository( self::ERROR, 'Vous avez déjà un compte' );
            
            return FALSE;
        }
        
        try {
            $customer->setFirstName( ucfirst( $firstName ) )
                     ->setLastName( ucfirst( $lastName ) )
                     ->setEmail( $this->validEmail( $email ) )
                     ->setTel( $phone )
                     ->setNumberStreet( $numberStreet )
                     ->setStreet( $street )
                     ->setPostalCode( $postalCode )
                     ->setCity( ucfirst( $city ) )
                     ->setCountry( ucfirst( $country ) )
                     ->setPassword( $this->vPassword( $password, $confPassword ) )
                     ->setOfferAccept( $offerAccept );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $customer );
        
        return TRUE;
    }
    
    
    /**
     * @param string $password
     * @param string $password2
     * @return string
     * @throws InvalidParamException
     */
    private function vPassword( string $password, string $password2 ): string
    {
        if( empty( $password ) || empty( $password2 ) ) {
            throw new InvalidParamException( 'Merci de confirmer votre mot de passe' );
        } elseif( $password !== $password2 ) {
            throw new InvalidParamException( 'Vos mots de passe ne correspondent pas' );
        } elseif( mb_strlen( $password ) < 6 ) {
            throw new InvalidParamException( 'Votre mot de passe doit contenir au moins 6 caractères' );
        }
        
        return password_hash( $password, PASSWORD_DEFAULT );
    }
    
    
    private function validEmail( string $email ): string
    {
        if( !preg_match( '/.+@.+\..+/', $email ) ) {
            throw new InvalidParamException( 'Merci de saisir un email valide' );
        }
        
        return $email;
    }
    
    
    private function isUniqueEmail( string $email ): bool
    {
        return $this->customerRepository->findByEmail( $email ) === NULL;
    }
}
