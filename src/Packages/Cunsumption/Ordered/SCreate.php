<?php


namespace App\Packages\Cunsumption\Ordered;


use App\Entities\Customer;
use App\Entities\Ordered;
use App\Entities\ProductCustomer;
use App\Packages\Cunsumption\Basket\RevalideBasketInterface;
use App\Packages\Cunsumption\ReductionCustomer\AttachReductionInterface;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\CreateInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Components\EntityManager\TransactionObserverInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpSession;

class SCreate extends AbstractCrud implements CreateInterface, TransactionObserverInterface
{
    
    private const PARAM_ADDRESS_BILLING_NUMBER_STREET  = 'address_billing_number_street';
    private const PARAM_ADDRESS_BILLING_STREET         = 'address_billing_street';
    private const PARAM_ADDRESS_BILLING_CITY           = 'address_billing_city';
    private const PARAM_ADDRESS_BILLING_POSTAL_CODE    = 'address_billing_postal_code';
    private const PARAM_ADDRESS_BILLING_COUNTRY        = 'address_billing_country';
    private const PARAM_ADDRESS_DELIVERY_NUMBER_STREET = 'address_delivery_number_street';
    private const PARAM_ADDRESS_DELIVERY_STREET        = 'address_delivery_street';
    private const PARAM_ADDRESS_DELIVERY_CITY          = 'address_delivery_city';
    private const PARAM_ADDRESS_DELIVERY_POSTAL_CODE   = 'address_delivery_postal_code';
    private const PARAM_ADDRESS_DELIVERY_COUNTRY       = 'address_delivery_country';
    private const PARAM_COMMENT                        = 'comment';
    private const SESSION_CUSTOMER                     = 'customer';
    private const LP_BASKET                            = 'basket';
    private LightPersistsInterface      $lightPersists;
    private HttpSession                 $session;
    private RevalideBasketInterface     $revalideBasket;
    private AttachReductionInterface    $attachReduction;
    private TransactionSubjectInterface $transactionSubject;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        LightPersistsInterface $lightPersists,
        HttpSession $session,
        RevalideBasketInterface $revalideBasket,
        AttachReductionInterface $attachReduction,
        TransactionSubjectInterface $transactionSubject )
    {
        parent::__construct( $entityManager );
        $this->lightPersists      = $lightPersists;
        $this->session            = $session;
        $this->revalideBasket     = $revalideBasket;
        $this->attachReduction    = $attachReduction;
        $this->transactionSubject = $transactionSubject;
        $this->subscribeToTransactionStatus();
    }
    
    
    /**
     * Use case of creating ordered
     *
     * @param HttpRequest $request
     * @param object $instance
     * @return bool
     */
    public function create( HttpRequest $request, object $instance ): bool
    {
        /** @var Ordered $instance */
        
        $abNumberStreet = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_NUMBER_STREET );
        $abStreet       = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_STREET );
        $abCity         = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_CITY );
        $abPostalCode   = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_POSTAL_CODE );
        $abCountry      = (string)$request->getParameter( self::PARAM_ADDRESS_BILLING_COUNTRY );
        $adNumberStreet = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_NUMBER_STREET );
        $adStreet       = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_STREET );
        $adCity         = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_CITY );
        $adPostalCode   = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_POSTAL_CODE );
        $adCountry      = (string)$request->getParameter( self::PARAM_ADDRESS_DELIVERY_COUNTRY );
        $comment        = (string)$request->getParameter( self::PARAM_COMMENT );
        
        
        if( !$this->isValidBasket() ) {
            $this->setRepository( self::ERROR, 'Votre panier est vide' );
            
            return FALSE;
        }
        
        try {
            $this->injectProductCustomers( $instance, $request, $this->session->get( self::SESSION_CUSTOMER ) );
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        } catch( \TypeError $error ) {
            $this->setRepository( self::ERROR, 'Une erreur s\'est produite' );
            
            return FALSE;
        }
        
        $this->injectReductionCustomers( $instance, $this->session->get( self::SESSION_CUSTOMER ), $request );
        
        /** @var Ordered $instance */
        try {
            $instance->setCustomer( $this->session->get( self::SESSION_CUSTOMER ) )
                     ->setAddressBilling( $this->resolveAddress( $abNumberStreet, $abStreet, $abPostalCode, $abCity, $abCountry ) )
                     ->setAddressDelivery( $this->resolveAddress( $adNumberStreet, $adStreet, $adPostalCode, $adCity, $adCountry ) )
                     ->setDataCustomer( $this->getDataCustomer( $instance->getProductCustomer() ) )
                     ->setComment( $comment )
                     ->setTotal()
                     ->setNetToPay()
                     ->setStatus( 0 );//Waiting payment
        } catch( InvalidParamException $e ) {
            $this->setRepository( self::ERROR, $e->getMessage() );
            
            return FALSE;
        }
        
        $this->entityManager->persists( $instance );
        
        return TRUE;
    }
    
    
    /**
     * Valid that address is complete
     * Parse address for a string
     *
     * @param $numberStreet
     * @param $street
     * @param $postalCode
     * @param $city
     * @param $country
     * @return string
     * @throws InvalidParamException
     */
    private function resolveAddress( $numberStreet, $street, $postalCode, $city, $country ): string
    {
        if( empty( $numberStreet ) || empty( $street ) || empty( $postalCode ) || empty( $city ) || empty( $country ) ) {
            throw new InvalidParamException( 'Merci de saisir une adresse valide' );
        }
        
        return "$numberStreet $street<br>$postalCode $city<br>$country";
    }
    
    
    /**
     * Inject the products customer in ordered with her reduction
     *
     * @param Ordered $ordered
     * @param HttpRequest $request
     * @param Customer $customer
     * @throws InvalidParamException
     */
    private function injectProductCustomers( Ordered $ordered, HttpRequest $request, Customer $customer ): void
    {
        /** @var ProductCustomer $productCustomer */
        foreach( $this->lightPersists->get( self::LP_BASKET ) as $productCustomer ) {
            $productCustomer->addReductionCustomers( $this->attachReduction->attachForProduct( $request, $productCustomer, $ordered ) )
                            ->setCustomer( $customer )
                            ->setTotal()
                            ->setNetToPay();
            $ordered->addProductCustomer( $productCustomer );
        }
    }
    
    
    /**
     * Inject the reductions when she's eligible to ordered or to customer
     *
     * @param Ordered $ordered
     * @param Customer $customer
     * @param HttpRequest $request
     */
    private function injectReductionCustomers( Ordered $ordered, Customer $customer, HttpRequest $request ): void
    {
        $this->attachReduction->attachForOrdered( $request, $ordered );
        $this->attachReduction->attachForCustomer( $request, $customer, $ordered );
    }
    
    
    /**
     * Parse the data customer for a string
     *
     * @param ProductCustomer[] $productCustomers
     * @return string|null
     */
    private function getDataCustomer( array $productCustomers ): ?string
    {
        $content = NULL;
        
        foreach( $productCustomers as $productCustomer ) {
            if( !empty( $productCustomer->getDataCustomer() ) ) {
                $content .= 'Pour ' . $productCustomer->getProduct()->getName() . ':<br>' . $productCustomer->getDataCustomer();
            }
        }
        
        return $content;
    }
    
    
    /**
     * Launch an revalidation of basket and valid that the basket is not empty
     *
     * @return bool
     */
    private function isValidBasket(): bool
    {
        $this->revalideBasket->revalide();
        
        return !empty( $this->lightPersists->get( self::LP_BASKET ) );
    }
    
    
    /**
     * Purge basket if the transaction was successful
     */
    private function purgeBasket(): void
    {
        $this->lightPersists->set( self::LP_BASKET, NULL );
    }
    
    
    public function statusTransactionNotified( bool $status ): void
    {
        if( $status ) {
            $this->purgeBasket();
        }
    }
    
    
    public function subscribeToTransactionStatus(): void
    {
        $this->transactionSubject->addSubscriber( $this );
    }
}
