<?php


namespace App\Packages\Cunsumption\ReductionCustomer;


use App\Entities\Customer;
use App\Entities\Ordered;
use App\Entities\ProductCustomer;
use App\Entities\Reduction;
use App\Entities\ReductionCustomer;
use App\Repositories\ReductionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SAttach implements AttachReductionInterface
{
    
    private const PARAM_CODE = 'reduction_code';
    private EntityManagerInterface $entityManager;
    private ReductionRepository    $reductionRepository;
    private array                  $operator = [
        '>'  => 'condStrictlySup',
        '<'  => 'condStrictlyInf',
        '>=' => 'condSupOrEqual',
        '<=' => 'condInfOrEqual',
        '==' => 'condIsEqual',
        '!=' => 'condIsDifferent'
    ];
    
    
    public function __construct( EntityManagerInterface $entityManager, ReductionRepository $reductionRepository )
    {
        $this->entityManager       = $entityManager;
        $this->reductionRepository = $reductionRepository;
    }
    
    
    public function attachForProduct( HttpRequest $request, ProductCustomer $productCustomer, Ordered $ordered ): ?ReductionCustomer
    {
        $reductions = $this->getReductions();
        
        if( !empty( $reductions ) ) {
            foreach( $reductions as $reduction ) {
                if( !empty( $reduction->getProduct() )
                    && $reduction->getProduct()->getId() === $productCustomer->getProduct()->getId() ) {
                    
                    if( $this->isEligible( $reduction, $ordered )
                        && $this->isValidCode( $request, $reduction )
                        && $this->isFree( $reduction ) ) {
                        
                        
                        $reductionCustomer = new ReductionCustomer();
                        $reductionCustomer->setReduction( $reduction );
                        
                        $productCustomer->addReductionCustomers( $reductionCustomer );
                        
                        return $reductionCustomer;
                    }
                }
            }
        }
        
        return NULL;
    }
    
    
    public function attachForOrdered( HttpRequest $request, Ordered $ordered ): void
    {
        $reductions = $this->getReductions();
        
        if( !empty( $reductions ) ) {
            foreach( $reductions as $reduction ) {
                if( $this->isEligible( $reduction, $ordered )
                    && $this->isValidCode( $request, $reduction )
                    && $this->isFree( $reduction ) ) {
                    
                    $reductionCustomer = new ReductionCustomer();
                    $reductionCustomer->setReduction( $reduction );
                    
                    $ordered->addReductionCustomer( $reductionCustomer );
                }
            }
        }
    }
    
    
    public function attachForCustomer( HttpRequest $request, Customer $customer, Ordered $ordered ): void
    {
        $reductions = $this->getReductions();
        
        if( !empty( $reductions ) ) {
            foreach( $reductions as $reduction ) {
                if( $this->isOwner( $customer, $reduction )
                    && $this->isEligible( $reduction, $ordered )
                    && $this->isValidCode( $request, $reduction ) ) {
                    
                    $reductionCustomer = new ReductionCustomer();
                    $reductionCustomer->setReduction( $reduction );
                    
                    $ordered->addReductionCustomer( $reductionCustomer );
                }
            }
        }
    }
    
    
    private function isValidCode( HttpRequest $request, Reduction $reduction ): bool
    {
        if( !empty( $reduction->getCode() ) ) {
            $codes = (array)$request->getParameter( self::PARAM_CODE );
            
            if( !empty( $codes ) ) {
                foreach( $codes as $code ) {
                    if( mb_strtolower( trim( $code ) ) === mb_strtolower( trim( $reduction->getCode() ) ) ) {
                        return TRUE;
                    }
                }
                
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    
    private function isEligible( Reduction $reduction, Ordered $ordered ): bool
    {
        if( !empty( $reduction->getRule() ) ) {
            $init     = $reduction->getRule()[0];
            $id       = $reduction->getRule()[1];
            $operator = $reduction->getRule()[2];
            $comp     = $reduction->getRule()[3];
            
            $funcname = $this->operator[$operator];
            
            if( strpos( $init, 'PT' ) !== FALSE ) {
                
                foreach( $ordered->getProductCustomer() as $productCustomer ) {
                    if( $productCustomer->getProduct()->getId() === $id ) {
                        
                        return $this->$funcname( $productCustomer->getTotal(), $comp );
                    }
                }
            } elseif( strpos( $init, 'PQ' ) !== FALSE ) {
                foreach( $ordered->getProductCustomer() as $productCustomer ) {
                    if( $productCustomer->getProduct()->getId() === $id ) {
                        return $this->$funcname( $productCustomer->getQuantity(), $comp );
                    }
                }
            } else {
                return $this->$funcname( $ordered->getTotal(), $comp );
            }
        }
        
        return TRUE;
    }
    
    
    private function isFree( Reduction $reduction ): bool
    {
        return empty( $reduction->getCustomer() );
    }
    
    
    private function isOwner( Customer $customer, Reduction $reduction ): bool
    {
        return !empty( $reduction->getCustomer() ) && $reduction->getCustomer()->getId() === $customer->getId();
    }
    
    
    /**
     * @return Reduction[]|null
     */
    public function getReductions(): ?array
    {
        $reductions = $this->reductionRepository->findAllActive();
        
        return $reductions;
    }
    
    
    public function condSupOrEqual( int $value1, int $value2 ): bool
    {
        return $value1 >= $value2;
    }
    
    
    private function condInfOrEqual( int $value1, int $value2 ): bool
    {
        return $value1 <= $value2;
    }
    
    
    private function condStrictlySup( int $value1, int $value2 ): bool
    {
        return $value1 > $value2;
    }
    
    
    private function condStrictlyInf( int $value1, int $value2 ): bool
    {
        return $value1 < $value2;
    }
    
    
    private function condIsEqual( int $value1, int $value2 ): bool
    {
        return $value1 === $value2;
    }
    
    
    private function confIsDifferent( int $value1, int $value2 ): bool
    {
        return $value1 !== $value2;
    }
}
