<?php


namespace App\Packages\Cunsumption\ReductionCustomer;


use App\Entities\Customer;
use App\Entities\Ordered;
use App\Entities\ProductCustomer;
use App\Entities\ReductionCustomer;
use Nomess\Http\HttpRequest;

interface AttachReductionInterface
{
    
    public function attachForProduct( HttpRequest $request, ProductCustomer $productCustomer, Ordered $ordered ): ?ReductionCustomer;
    
    
    public function attachForOrdered( HttpRequest $request, Ordered $ordered ): void;
    
    
    public function attachForCustomer( HttpRequest $request, Customer $customer, Ordered $ordered ): void;
}
