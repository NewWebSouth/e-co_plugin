<?php


namespace App\Packages\Cunsumption\Basket;


use App\Entities\Product;
use App\Entities\ProductCustomer;
use App\Entities\Stock;
use App\Repositories\ProductRepository;
use App\Repositories\StockRepository;
use NoMess\Components\LightPersists\LightPersistsInterface;

class SRevalide implements RevalideBasketInterface
{
    
    private const LP_BASKET = 'basket';
    private LightPersistsInterface $lightPersists;
    private ProductRepository      $productRepository;
    private StockRepository        $stockRepository;
    
    
    public function __construct(
        LightPersistsInterface $lightPersists,
        ProductRepository $productRepository,
        StockRepository $stockRepository )
    {
        $this->lightPersists     = $lightPersists;
        $this->stockRepository   = $stockRepository;
        $this->productRepository = $productRepository;
    }
    
    
    public function revalide(): void
    {
        if( $this->lightPersists->has( self::LP_BASKET ) ) {
            $productCustomers = &$this->lightPersists->getReference( self::LP_BASKET );
            
            /** @var ProductCustomer $productCustomer */
            foreach( $productCustomers as $key => &$productCustomer ) {
                /** @var Product $productOrigin */
                $productOrigin = $this->productRepository->findById( $productCustomer->getProduct()->getId() );
                
                if( is_null( $productOrigin ) || !$productOrigin->isActive() || !$this->isQuantitySufficient( $productCustomer ) ) {
                    unset( $productCustomers[$key] );
                }
            }
        }
    }
    
    
    private function isQuantitySufficient( ProductCustomer $productCustomer ): bool
    {
        if( !empty( $productCustomer->getProduct()->getStocks() ) ) {
            $minQuantity = 1000000;
            
            foreach( $productCustomer->getProduct()->getStocks() as $stock ) {
                
                /** @var Stock $stockOrigin */
                $stockOrigin = $this->stockRepository->findById( $stock->getId() );
                
                if( $stockOrigin->getQuantity() < $minQuantity ) {
                    $minQuantity = $stockOrigin->getQuantity();
                }
            }
            
            return $productCustomer->getQuantity() <= $minQuantity;
        }
        
        return TRUE;
    }
}
