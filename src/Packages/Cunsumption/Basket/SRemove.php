<?php


namespace App\Packages\Cunsumption\Basket;


use App\Entities\ProductCustomer;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Http\HttpRequest;

class SRemove
{
    
    private const PARAM_CREATED = 'created';
    
    private const LP_BASKET     = 'basket';
    
    private LightPersistsInterface $lightPersists;
    
    
    public function __construct( LightPersistsInterface $lightPersists )
    {
        $this->lightPersists = $lightPersists;
    }
    
    
    public function remove( HttpRequest $request ): void
    {
        $created = (string)$request->getParameter( self::PARAM_CREATED );
        
        
        $basket = &$this->lightPersists->getReference( self::LP_BASKET );
        
        if( !empty( $basket ) ) {
            /** @var ProductCustomer $productCustomer */
            foreach( $basket as $key => $productCustomer ) {
                if( $productCustomer->getCreatedAt() === $created ) {
                    unset( $basket[$key] );
                }
            }
        }
    }
}
