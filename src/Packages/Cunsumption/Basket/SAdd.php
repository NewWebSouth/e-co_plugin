<?php


namespace App\Packages\Cunsumption\Basket;


use App\Packages\Cunsumption\ProductCustomer\SAttach;
use NoMess\Components\LightPersists\LightPersistsInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SAdd
{
    
    private LightPersistsInterface $lightPersists;
    private SAttach                $attachProduct;
    
    
    public function __construct( LightPersistsInterface $lightPersists, SAttach $attach )
    {
        $this->lightPersists = $lightPersists;
        $this->attachProduct = $attach;
    }
    
    
    public function add( HttpRequest $request ): void
    {
        $productCustomer = NULL;
        
        try {
            $productCustomer = $this->attachProduct->attach( $request );
        } catch( InvalidParamException $e ) {
        } catch( NullPointerException $e ) {
        }
        
        if( !is_null( $productCustomer ) ) {
            $this->lightPersists->set( 'basket', array_merge( is_null( $this->lightPersists->get( 'basket' ) ) ? [] : $this->lightPersists->get( 'basket' ), [ $productCustomer ] ) );
        }
    }
}
