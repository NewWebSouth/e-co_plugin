<?php


namespace App\Packages\Cunsumption\Basket;


interface RevalideBasketInterface
{
    
    public function revalide(): void;
}
