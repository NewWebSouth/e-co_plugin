<?php


namespace App\Packages\Cunsumption\ProductCustomer;


use App\Entities\Option;
use App\Entities\Product;
use App\Entities\ProductCustomer;
use App\Repositories\ProductRepository;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;

class SAttach
{
    
    private const PARAM_PRODUCT_ID = 'product_id';
    private const PARAM_OPTION     = 'option';
    private const PARAM_QUANTITY   = 'quantity';
    private ProductRepository       $productRepository;
    
    
    public function __construct( ProductRepository $productRepository )
    {
        $this->productRepository = $productRepository;
    }
    
    
    /**
     * Build the product customer entity
     *
     * @param HttpRequest $request
     * @return ProductCustomer
     * @throws InvalidParamException
     * @throws NullPointerException
     */
    public function attach( HttpRequest $request ): ProductCustomer
    {
        $productId = (int)$request->getParameter( self::PARAM_PRODUCT_ID );
        $optionId  = (int)$request->getParameter( self::PARAM_OPTION );
        $quantity  = (int)$request->getParameter( self::PARAM_QUANTITY );
        
        $product = $this->getProduct( $productId );
        $this->isQuantitySufficient( $quantity, $product );
        $option = $this->attachOption( $optionId, $product );
        
        $productCustomer = new ProductCustomer();
        $productCustomer->setProduct( $product )
                        ->setQuantity( $quantity )
                        ->setProduct( $product )
                        ->setOption( $option )
                        ->setDataCustomer( $this->attachDataCustomer( $request, $product ) )
                        ->setTotal()
                        ->setNetToPay();
        
        return $productCustomer;
    }
    
    
    /**
     * Return product corespondent to id
     *
     * @param int $id
     * @return Product
     * @throws NullPointerException
     */
    private function getProduct( int $id ): Product
    {
        $product = $this->productRepository->findById( $id );
        
        if( !is_null( $product ) ) {
            return $product;
        }
        
        throw new NullPointerException( 'Une erreur s\'est produite' );
    }
    
    
    /**
     * Return option selected by customer
     *
     * @param int $optionId
     * @param Product $product
     * @return Option|null
     * @throws InvalidParamException
     */
    private function attachOption( int $optionId, Product $product ): ?Option
    {
        if( !empty( $product->getOptions() ) ) {
            foreach( $product->getOptions() as $option ) {
                if( $option->getId() === $optionId ) {
                    return $option;
                }
            }
            
            if( $product->isOptionRequired() ) {
                throw new InvalidParamException( 'L\'option pour le produit est requise' );
            }
        }
        
        return NULL;
    }
    
    
    /**
     * Build the data of customer
     *
     * @param HttpRequest $request
     * @param Product $product
     * @return string|null
     * @throws InvalidParamException
     */
    private function attachDataCustomer( HttpRequest $request, Product $product ): ?string
    {
        $content = NULL;
        
        if( !empty( $product->getDataCustomers() ) ) {
            foreach( $product->getDataCustomers() as $dataCustomer ) {
                $str = (string)$request->getParameter( 'data_customer_' . $dataCustomer->getId() );
                
                if( $dataCustomer->isRequired() && empty( $str ) ) {
                    throw new InvalidParamException( 'La donnée ' . $dataCustomer->getName() . ' est requise' );
                }
                
                $content .= '<strong>' . $dataCustomer->getName() . '</strong>: ' . ( empty( $str ) ? 'Non spécifié' : $str ) . '<br><br>';
            }
        }
        
        return $content;
    }
    
    
    /**
     * @param int $quantity
     * @param Product $product
     * @throws InvalidParamException
     */
    private function isQuantitySufficient( int $quantity, Product $product ): void
    {
        if( !empty( $product->getStocks() ) ) {
            $minQuantity = 1000000;
            
            foreach( $product->getStocks() as $stock ) {
                if( $stock->getQuantity() < $minQuantity ) {
                    $minQuantity = $stock->getQuantity();
                }
            }
            
            if( $minQuantity < $quantity ) {
                throw new InvalidParamException( 'La quantité demandé est supérieur aux stocks disponible' );
            }
        }
    }
}
