<?php


namespace App\Packages\Cunsumption\Invoice;


use App\Entities\Invoice;
use App\Entities\Ordered;
use App\Packages\Administer\Report\PdfGeneratorInterface;
use App\Packages\Administer\Tools\Mail\Initializer\InvoiceMailInterface;
use App\Packages\Administer\Tools\Mail\Initializer\PaymentMailInterface;
use App\Packages\Cunsumption\Payment\AttachPaymentInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;

class SCreate implements AttachInvoiceInterface
{
    
    private AttachPaymentInterface $attachPayment;
    private EntityManagerInterface $entityManager;
    private PdfGeneratorInterface  $generator;
    private PaymentMailInterface   $paymentMail;
    private InvoiceMailInterface   $invoiceMail;
    
    
    public function __construct(
        AttachPaymentInterface $attachPayment,
        EntityManagerInterface $entityManager,
        PdfGeneratorInterface $generator,
        PaymentMailInterface $paymentMail,
        InvoiceMailInterface $invoiceMail )
    {
        $this->attachPayment = $attachPayment;
        $this->entityManager = $entityManager;
        $this->generator     = $generator;
        $this->paymentMail   = $paymentMail;
        $this->invoiceMail   = $invoiceMail;
    }
    
    
    public function create( Ordered $ordered ): Invoice
    {
        $invoice = ( new Invoice() )
            ->setOrdered( $ordered )
            ->setCustomer( $ordered->getCustomer() )
            ->setDateExec1( date( 'Y-m-d H:i:s' ) )
            ->setNetToPay( $ordered->getNetToPay() )
            ->setTotal( $ordered->getTotal() );
        
        $this->attachPayment->create( $invoice );
        
        $this->generator->generate( $invoice );
        $this->entityManager->persists( $invoice );
        $this->paymentMail->send( $invoice->getPayment() );
        $this->invoiceMail->send( $invoice );
        
        return $invoice;
    }
}
