<?php


namespace App\Packages\Cunsumption\Invoice;


use App\Entities\Invoice;
use App\Entities\Ordered;

interface AttachInvoiceInterface
{
    
    public function create( Ordered $ordered ): Invoice;
}
