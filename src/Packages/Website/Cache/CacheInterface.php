<?php


namespace App\Packages\Website\Cache;


interface CacheInterface
{
    
    public const CACHE_KEY_SECTION           = 'front_cache_section';
    public const CACHE_KEY_PRODUCT           = 'front_cache_product';
    public const CACHE_KEY_MENU              = 'front_cache_menu';
    public const CACHE_KEY_ARTICLE           = 'front_cache_article';
    public const CACHE_KEY_PRODUCT_SECTION   = 'font_cache_product_section';
    public const CACHE_KEY_REDUCTION_SECTION = 'front_cache_reduction_section';
    public const CACHE_KEY_ARTICLE_SECTION   = 'front_cache_article_section';
    
    
    public function add( string $key, object $object ): void;
    
    
    public function addGroup( string $key, array $group ): void;
    
    
    public function get( string $key, int $id ): object;
    
    
    public function getGroup( string $key ): array;
    
    
    public function has( string $key, int $id ): bool;
    
    
    public function hasGroup( string $key ): bool;
}
