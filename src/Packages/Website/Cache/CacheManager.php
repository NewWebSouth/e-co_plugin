<?php


namespace App\Packages\Website\Cache;


use NoMess\Components\ApplicationScope\ApplicationScopeInterface;

/**
 * Manage cache of data front end
 * Removed when the administrator modify, add or delete an data
 * Agreement when the clients visit the website
 */
class CacheManager implements FrontObserverInterface, CacheInterface
{
    
    private ApplicationScopeInterface $applicationScope;
    
    
    public function __construct( ApplicationScopeInterface $applicationScope )
    {
        $this->applicationScope = $applicationScope;
    }
    
    
    public function add( string $key, object $object ): void
    {
        if( NOMESS_CONTEXT === 'PROD' ) {
            $this->applicationScope->set( $key, [ $object->getId() => $object ] );
        }
    }
    
    
    public function get( string $key, int $id ): object
    {
        return $this->applicationScope->get( $key )[$id];
    }
    
    
    public function has( string $key, int $id ): bool
    {
        if( $this->applicationScope->get( $key ) !== NULL ) {
            return array_key_exists( $id, $this->applicationScope->get( $key ) )
                   && $this->applicationScope->get( $key )[$id] !== NULL;
        }
        
        return FALSE;
    }
    
    
    public function addGroup( string $key, array $group ): void
    {
        if( NOMESS_CONTEXT === 'PROD' ) {
            $this->applicationScope->set( $key, $group );
        }
    }
    
    
    public function getGroup( string $key ): array
    {
        return $this->applicationScope->get( $key );
    }
    
    
    public function hasGroup( string $key ): bool
    {
        return $this->applicationScope->get( $key ) !== NULL;
    }
    
    
    public function notified(): void
    {
        $this->applicationScope->set( self::CACHE_KEY_PRODUCT, NULL );
        $this->applicationScope->set( self::CACHE_KEY_SECTION, NULL );
        $this->applicationScope->set( self::CACHE_KEY_REDUCTION_SECTION, NULL );
        $this->applicationScope->set( self::CACHE_KEY_PRODUCT_SECTION, NULL );
        $this->applicationScope->set( self::CACHE_KEY_MENU, NULL );
        $this->applicationScope->set( self::CACHE_KEY_ARTICLE_SECTION, NULL );
        $this->applicationScope->set( self::CACHE_KEY_ARTICLE, NULL );
    }
}
