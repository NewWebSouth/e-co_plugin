<?php


namespace App\Packages\Website\Cache;


interface FrontObserverInterface
{
    
    public function notified(): void;
}
