<?php


namespace App\Packages\Website\Contact;


use App\Packages\Administer\Tools\Mail\Initializer\ContactMailInterface;
use Newwebsouth\Abstraction\Crud\AbstractCrud;
use Newwebsouth\Abstraction\Crud\ServiceInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SContact extends AbstractCrud implements ServiceInterface
{
    
    private const PARAM_NAME    = 'name';
    private const PARAM_EMAIL   = 'email';
    private const PARAM_MESSAGE = 'message';
    private const PARAM_SPAM    = 'bad';
    private ContactMailInterface $contactMail;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        ContactMailInterface $contactMail )
    {
        parent::__construct( $entityManager );
        $this->contactMail = $contactMail;
    }
    
    
    public function service( HttpRequest $request, object $instance ): bool
    {
        $name    = (string)$request->getParameter( self::PARAM_NAME );
        $email   = (string)$request->getParameter( self::PARAM_EMAIL );
        $message = (string)$request->getParameter( self::PARAM_MESSAGE );
        $spam    = (bool)$request->getParameter( self::PARAM_SPAM );
        
        if( $spam ) {
            $this->setRepository( self::ERROR, 'Votre message n\'a pas pu être envoyé' );
            
            return FALSE;
        }
        
        $this->contactMail->send( $email, $name, $message );
        
        return TRUE;
    }
}
