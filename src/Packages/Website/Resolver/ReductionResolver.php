<?php


namespace App\Packages\Website\Resolver;


use App\Entities\GraphicRuleReduction;
use App\Entities\Section;
use App\Packages\Website\Cache\CacheInterface;
use App\Repositories\GraphicRuleReductionRepository;
use Nomess\Annotations\Inject;

class ReductionResolver
{
    
    /**
     * @Inject()
     */
    private CacheInterface $cache;
    /**
     * @Inject()
     */
    private GraphicRuleReductionRepository $graphicRuleReductionRepository;
    
    
    /**
     * @param Section $section
     * @return array
     */
    public function getReductionForSection( Section $section ): array
    {
        
        if( $this->cache->hasGroup( CacheInterface::CACHE_KEY_REDUCTION_SECTION )
            && array_key_exists( $section->getId(), $this->cache->getGroup( CacheInterface::CACHE_KEY_REDUCTION_SECTION ) ) ) {
            
            return $this->cache->getGroup( CacheInterface::CACHE_KEY_REDUCTION_SECTION )[$section->getId()];
        }
        
        $graphicRuleReductions = $this->graphicRuleReductionRepository->findAll();
        
        $list = array();
        
        if( !empty( $graphicRuleReductions ) ) {
            
            foreach( $graphicRuleReductions as $graphicRuleReduction ) {
                /** @var GraphicRuleReduction $graphicRuleReduction */
                
                if( in_array( $section, $graphicRuleReduction->getSections() ) ) {
                    $list[] = $graphicRuleReduction;
                }
            }
        }
        
        $this->cache->addGroup( CacheInterface::CACHE_KEY_REDUCTION_SECTION, [ $section->getId() => $list ] );
        
        return $list;
    }
}
