<?php


namespace App\Packages\Website\Resolver;


use App\Entities\GraphicRuleProduct;
use App\Entities\Product;
use App\Entities\Section;
use App\Packages\Website\Cache\CacheInterface;
use App\Repositories\GraphicRuleProductRepository;
use App\Repositories\ProductRepository;
use Nomess\Annotations\Inject;

class ProductResolver
{
    
    /**
     * @Inject()
     */
    private CacheInterface $cache;
    /**
     * @Inject()
     */
    private GraphicRuleProductRepository $graphicRuleProductRepository;
    /**
     * @Inject()
     */
    private ProductRepository $productRepository;
    
    
    /**
     * If graphic rule is in cache, she's returned
     * Else database is called
     *
     * @param int $id
     * @return GraphicRuleProduct
     */
    public function getProductForProduct( int $id ): GraphicRuleProduct
    {
        if( $this->cache->has( CacheInterface::CACHE_KEY_PRODUCT, $id ) ) {
            /** @noinspection PhpIncompatibleReturnTypeInspection */
            return $this->cache->get( CacheInterface::CACHE_KEY_PRODUCT, $id );
        }
        
        $graphicRuleProduct = $this->productRepository->findById( $id )->getGraphicRule();
        
        $this->cache->add( CacheInterface::CACHE_KEY_PRODUCT, $graphicRuleProduct );
        
        return $graphicRuleProduct;
    }
    
    
    /**
     * If graphic rule have 3 products recommended, return that
     * Else it's completed by section's product
     * It's possible to graft an suggestion algorithme by coupling the tracker
     *
     * @param GraphicRuleProduct $graphicRuleProduct
     * @param Section $section
     * @return Product[]
     */
    public function getRecommendedForProduct( GraphicRuleProduct $graphicRuleProduct, Section $section ): ?array
    {
        if( !empty( $graphicRuleProduct->getRecommended() ) ) {
            
            if( count( $graphicRuleProduct->getRecommended() ) === 3 ) {
                return $graphicRuleProduct->getRecommended();
            } else {
                
                return array_merge(
                    $graphicRuleProduct->getRecommended(),
                    !empty( $result = $this->randomRecommended( $section, 3 - count( $graphicRuleProduct->getRecommended() ), $graphicRuleProduct->getRecommended() ) ) ? $result : []
                );
            }
        }
        
        return $this->randomRecommended( $section, 3, $graphicRuleProduct->getRecommended() );
    }
    
    
    /**
     * @param Section $section      GraphicRuleProduct's section for get only the same product's categories
     * @param int $numberOccurrence Number of occurrence to take
     * @param Product[] $exclude    Product already recommended
     * @return Product[]
     */
    private function randomRecommended( Section $section, int $numberOccurrence, ?array $exclude ): ?array
    {
        $graphicRuleProducts = $this->getProductForRecommended( $exclude, $section );
        
        if( is_array( $graphicRuleProducts ) && !empty( $graphicRuleProducts ) && count( $graphicRuleProducts ) >= $numberOccurrence ) {
            $list = array();
            
            $rand = is_array( $data = array_rand( $graphicRuleProducts, $numberOccurrence ) ) ? $data : [ $data ];
            
            foreach( $rand as $id ) {
                $list[] = $graphicRuleProducts[$id]->getProduct();
            }
            
            return $list;
        }
        
        return NULL;
    }
    
    
    public function getProductsForSection( Section $section ): array
    {
        $graphicRuleProducts = $this->graphicRuleProductRepository->findAll();
        $list                = array();
        
        if( !empty( $graphicRuleProducts ) ) {
            foreach( $graphicRuleProducts as $graphicRuleProduct ) {
                foreach( $graphicRuleProduct->getSections() as $grSection ) {
                    if( $grSection->getId() === $section->getId() ) {
                        $list[] = $graphicRuleProduct;
                    }
                }
            }
        }
        
        return $this->sortProduct( $list, $section->getId() );
    }
    
    
    /**
     * Sort the product list by priority
     * If product is present in cache, return it.
     *
     * @param GraphicRuleProduct[] $graphicRuleProducts
     * @param int $idSection
     * @return array
     */
    private function sortProduct( array $graphicRuleProducts, int $idSection ): array
    {
        usort( $graphicRuleProducts, function ( $a, $b ) {
            if( $a->getPriority() === $b->getPriority() ) {
                return 0;
            }
            
            return ( $a->getPriority() > $b->getPriority() ) ? -1 : 1;
        }
        );
        
        $this->cache->addGroup( CacheInterface::CACHE_KEY_PRODUCT_SECTION, [ $idSection => $graphicRuleProducts ] );
        
        return $graphicRuleProducts;
    }
    
    
    /**
     * Remove the products already present in recommended
     * And return list
     *
     * @param Product[]|null $products Product to exclude
     * @param Section $section         Section of target product
     * @return array|null
     */
    private function getProductForRecommended( ?array $products, Section $section ): ?array
    {
        $graphicRuleProducts = $this->getProductsForSection( $section );
        
        if( !empty( $products ) ) {
            foreach( $products as $product ) {
                foreach( $graphicRuleProducts as $key => $graphicRuleProduct ) {
                    if( $graphicRuleProduct->getProduct()->getId() === $product->getId() ) {
                        unset( $graphicRuleProducts[$key] );
                    }
                }
            }
        }
        
        return $graphicRuleProducts;
    }
}
