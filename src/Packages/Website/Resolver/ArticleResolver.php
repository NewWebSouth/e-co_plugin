<?php


namespace App\Packages\Website\Resolver;


use App\Entities\Article;
use App\Packages\Website\Cache\CacheInterface;
use App\Repositories\ArticleRepository;
use Nomess\Annotations\Inject;

class ArticleResolver
{
    
    /**
     * @Inject()
     */
    private CacheInterface    $cache;
    /**
     * @Inject()
     */
    private ArticleRepository $articleRepository;
    
    
    /**
     * @return array
     */
    public function getArticlesForSection(): array
    {
        
        if( $this->cache->hasGroup( CacheInterface::CACHE_KEY_ARTICLE_SECTION ) ) {
            return $this->cache->getGroup( CacheInterface::CACHE_KEY_ARTICLE_SECTION );
        }
        
        $array  = array();
        $result = array();
        
        $articles = $this->articleRepository->findAll();
        
        if( !empty( $articles ) ) {
            foreach( $articles as $article ) {
                if( $article->getTypeInt() !== 1 ) {
                    $array[$article->getId()] = $article;
                }
            }
            
            for( $i = 3; $i !== 0; $i-- ) {
                if( !empty( $array ) ) {
                    $key      = array_key_last( $array );
                    $result[] = $array[$key];
                    unset( $array[$key] );
                }
            }
        }
        
        $this->cache->addGroup( CacheInterface::CACHE_KEY_ARTICLE_SECTION, $result );
        
        return $result;
    }
    
    
    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public function getArticleForArticle( int $id ): Article
    {
        if( $this->cache->has( CacheInterface::CACHE_KEY_ARTICLE, $id ) ) {
            return $this->cache->get( CacheInterface::CACHE_KEY_ARTICLE, $id );
        }
        
        $article = $this->articleRepository->findById( $id );
        
        $this->cache->add( CacheInterface::CACHE_KEY_ARTICLE, $article );
        
        return $article;
    }
    
    
    public function getArticlesForArticle(): array
    {
        return is_array( $result = $this->articleRepository->findAll() ) ? $result : [];
    }
}
