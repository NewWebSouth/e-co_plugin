<?php


namespace App\Packages\Website\Resolver;


use App\Entities\GraphicRuleProduct;
use App\Entities\Section;
use App\Packages\Website\Cache\CacheInterface;
use App\Repositories\SectionRepository;
use Nomess\Annotations\Inject;
use Nomess\Components\EntityManager\EntityManagerInterface;

class SectionResolver
{
    
    /**
     * @Inject()
     */
    private CacheInterface $cache;
    /**
     * @Inject()
     */
    private EntityManagerInterface $entityManager;
    /**
     * @Inject()
     */
    private Tracker $tracker;
    /**
     * @Inject()
     */
    private SectionRepository $sectionRepository;
    
    
    /**
     * Search section in cache or database
     *
     * @param int $id
     * @return object|null
     */
    public function getSectionForSection( int $id ): ?Section
    {
        if( $this->cache->has( CacheInterface::CACHE_KEY_SECTION, $id ) ) {
            return $this->cache->get( CacheInterface::CACHE_KEY_SECTION, $id );
        }
        
        /** @var Section $section */
        $section = $this->sectionRepository->findById( $id );
        
        $this->cache->add( CacheInterface::CACHE_KEY_SECTION, $section );
        
        return $section;
    }
    
    
    public function getSectionForProduct( GraphicRuleProduct $graphicRuleProduct ): Section
    {
        if( count( $graphicRuleProduct->getSections() ) === 1 ) {
            return current( $graphicRuleProduct->getSections() );
        }
        
        $idLastSection = $this->tracker->getLast( Tracker::TRACE_SECTION );
        
        if( $idLastSection !== NULL ) {
            
            $section = NULL;
            
            if( $this->cache->has( CacheInterface::CACHE_KEY_SECTION, $idLastSection ) ) {
                $section = $this->cache->get( CacheInterface::CACHE_KEY_SECTION, $idLastSection );
            } else {
                $section = $this->sectionRepository->findById( $idLastSection );
            }
            
            if( $section !== NULL ) {
                /** @var Section $section */
                return $section;
            }
        }
        
        return current( $graphicRuleProduct->getSections() );
    }
}
