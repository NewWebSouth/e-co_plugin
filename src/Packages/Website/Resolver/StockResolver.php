<?php


namespace App\Packages\Website\Resolver;


use App\Entities\Product;
use App\Entities\Stock;
use App\Repositories\StockRepository;
use Nomess\Annotations\Inject;
use Nomess\Components\EntityManager\EntityManagerInterface;

class StockResolver
{
    
    /**
     * @Inject()
     */
    private EntityManagerInterface $entityManager;
    /**
     * @Inject()
     */
    private StockRepository $stockRepository;
    
    
    public function getStockForProduct( Product $product ): int
    {
        /** @var Stock[] $stocks */
        $stocks   = $this->stockRepository->findAll();
        $quantity = 1000000;
        
        if( !empty( $stocks ) ) {
            foreach( $stocks as $stock ) {
                foreach( $stock->getProduct() as $productOfStock ) {
                    if( $productOfStock->getId() === $product->getId() && $stock->getQuantity() < $quantity ) {
                        $quantity = $stock->getQuantity();
                    }
                }
            }
        }
        
        return $quantity;
    }
}
