<?php


namespace App\Packages\Website\Resolver;


use App\Entities\Section;
use App\Packages\Website\Cache\CacheInterface;
use App\Repositories\GraphicRuleProductRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;

abstract class AbstractResolver
{
    
    protected EntityManagerInterface       $entityManager;
    protected CacheInterface               $cache;
    protected MenuResolver                 $menuResolver;
    protected GraphicRuleProductRepository $graphicRuleProductRepository;
    
    
    public function __construct(
        EntityManagerInterface $entityManager,
        CacheInterface $cache,
        MenuResolver $menuResolver )
    {
        $this->entityManager = $entityManager;
        $this->cache         = $cache;
        $this->menuResolver  = $menuResolver;
    }
    
    
    /**
     * @param Section $section
     * @return array
     */
    protected function getProducts( Section $section ): array
    {
        $graphicRuleProducts = $this->graphicRuleProductRepository->findAll();
        $list                = array();
        
        if( !empty( $graphicRuleProducts ) ) {
            foreach( $graphicRuleProducts as $graphicRuleProduct ) {
                foreach( $graphicRuleProduct->getSections() as $grSection ) {
                    if( $grSection->getId() === $section->getId() ) {
                        $list[] = $graphicRuleProduct;
                    }
                }
            }
        }
        
        return $list;
    }
}
