<?php


namespace App\Packages\Website\Resolver;


use App\Entities\Section;
use App\Packages\Website\Cache\CacheInterface;
use App\Repositories\SectionRepository;
use Nomess\Annotations\Inject;
use Nomess\Components\EntityManager\EntityManagerInterface;

class MenuResolver
{
    
    /**
     * @Inject()
     */
    private EntityManagerInterface $entityManager;
    /**
     * @Inject()
     */
    private CacheInterface $cache;
    /**
     * @Inject()
     */
    private SectionRepository $sectionRepository;
    
    
    public function getMenu(): array
    {
        if( $this->cache->hasGroup( CacheInterface::CACHE_KEY_MENU ) ) {
            return $this->cache->getGroup( CacheInterface::CACHE_KEY_MENU );
        }
        
        /** @var Section[] $sections */
        $sections = $this->sectionRepository->findAll();
        
        $list = [
            'parent' => array(),
            'child'  => array()
        ];
        
        if( !empty( $sections ) ) {
            foreach( $sections as $section ) {
                if( !empty( $section->getParent() ) ) {
                    $list['child'][] = $section;
                } else {
                    $list['parent'][] = $section;
                }
            }
        }
        
        usort( $list['parent'], function ( $a, $b ) {
            if( $a->getPriority() === $b->getPriority() ) {
                return 0;
            }
            
            return ( $a->getPriority() > $b->getPriority() ) ? -1 : 1;
        }
        );
        
        usort( $list['child'], function ( $a, $b ) {
            if( $a->getPriority() === $b->getPriority() ) {
                return 0;
            }
            
            return ( $a->getPriority() > $b->getPriority() ) ? -1 : 1;
        }
        );
        
        $this->cache->addGroup( CacheInterface::CACHE_KEY_MENU, $list );
        
        return $list;
    }
}
