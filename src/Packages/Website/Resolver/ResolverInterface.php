<?php


namespace App\Packages\Website\Resolver;


interface ResolverInterface
{
    
    public const SECTION = 'getSection';
    public const PRODUCT = 'getProduct';
    public const MENU    = 'getMenu';
    public const ARTICLE = 'getArticle';
    
    
    public function get( string $type, int $id = 0 ): array;
}
