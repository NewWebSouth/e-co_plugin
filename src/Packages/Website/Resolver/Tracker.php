<?php


namespace App\Packages\Website\Resolver;


use Nomess\Http\HttpSession;

class Tracker
{
    
    public const TRACE_SECTION = 'trace_section';
    public const TRACE_PRODUCT = 'trace_product';
    private array       $trace = [
        self::TRACE_PRODUCT => array(),
        self::TRACE_SECTION => array()
    ];
    private HttpSession $session;
    
    
    public function __construct( HttpSession $session )
    {
        $this->session                    = $session;
        $this->trace[self::TRACE_SECTION] = ( $this->session->has( self::TRACE_SECTION ) )
            ? $this->session->get( self::TRACE_SECTION ) : array();
        $this->trace[self::TRACE_PRODUCT] = ( $this->session->has( self::TRACE_PRODUCT ) )
            ? $this->session->get( self::TRACE_PRODUCT ) : array();
    }
    
    
    public function setSection( int $id ): void
    {
        $this->trace[self::TRACE_SECTION][] = $id;
        $this->session->set( self::TRACE_SECTION, $this->trace[self::TRACE_SECTION], TRUE );
    }
    
    
    public function setProduct( $id ): void
    {
        $this->trace[self::TRACE_PRODUCT] = $id;
        $this->session->set( self::TRACE_PRODUCT, $id, TRUE );
    }
    
    
    public function getLast( string $key ): ?int
    {
        $lastKey = array_key_last( $this->trace[$key] );
        
        var_dump( $lastKey );
        
        return isset( $this->trace[$key][$lastKey] )
            ? $this->trace[$key][$lastKey]
            : NULL;
    }
    
    
    public function getAll( string $key ): array
    {
        return $this->trace[$key];
    }
}
