<?php


namespace App\Packages\Website\Resolver;


use Nomess\Container\ContainerInterface;

class Dispatcher implements ResolverInterface
{
    
    private ContainerInterface  $container;
    private Tracker             $tracker;
    private SectionResolver     $sectionResolver;
    private ProductResolver     $productResolver;
    private ReductionResolver   $reductionResolver;
    private StockResolver       $stockResolver;
    private MenuResolver        $menuResolver;
    private ArticleResolver     $articleResolver;
    
    
    public function __construct(
        ContainerInterface $container,
        Tracker $tracker,
        SectionResolver $sectionResolver,
        ProductResolver $productResolver,
        ReductionResolver $reductionResolver,
        StockResolver $stockResolver,
        MenuResolver $menuResolver,
        ArticleResolver $articleResolver )
    {
        $this->container         = $container;
        $this->tracker           = $tracker;
        $this->sectionResolver   = $sectionResolver;
        $this->productResolver   = $productResolver;
        $this->reductionResolver = $reductionResolver;
        $this->stockResolver     = $stockResolver;
        $this->menuResolver      = $menuResolver;
        $this->articleResolver   = $articleResolver;
    }
    
    
    public function get( string $type, int $id = 0 ): array
    {
        return $this->$type( $id );
    }
    
    
    private function getProduct( int $id ): array
    {
        $this->tracker->setProduct( $id );
        
        
        $graphicRuleProduct = $this->productResolver->getProductForProduct( $id );
        $section            = $this->sectionResolver->getSectionForProduct( $graphicRuleProduct );
        $stock              = $this->stockResolver->getStockForProduct( $graphicRuleProduct->getProduct() );
        $recommended        = $this->productResolver->getRecommendedForProduct( $graphicRuleProduct, $section );
        $menu               = $this->menuResolver->getMenu();
        
        
        return [
            'section'            => $section,
            'graphicRuleProduct' => $graphicRuleProduct,
            'stock'              => $stock,
            'recommended'        => $recommended,
            'menu'               => $menu
        ];
    }
    
    
    private function getSection( int $id ): array
    {
        $this->tracker->setSection( $id );
        
        $section              = $this->sectionResolver->getSectionForSection( $id );
        $graphicRuleProduct   = $this->productResolver->getProductsForSection( $section );
        $graphicRuleReduction = $this->reductionResolver->getReductionForSection( $section );
        $articles             = $this->articleResolver->getArticlesForSection();
        $menu                 = $this->menuResolver->getMenu();
        
        return [
            'section'              => $section,
            'article'              => $articles,
            'graphicRuleProduct'   => $graphicRuleProduct,
            'graphicRuleReduction' => $graphicRuleReduction,
            'menu'                 => $menu,
            'template'             => $section->getTemplate()
        ];
    }
    
    
    private function getMenu( int $id ): array
    {
        return [
            'menu' => $this->menuResolver->getMenu()
        ];
    }
    
    
    private function getArticle( int $id ): array
    {
        return [
            'article'  => $this->articleResolver->getArticleForArticle( $id ),
            'articles' => $this->articleResolver->getArticlesForSection(),
            'menu'     => $this->menuResolver->getMenu()
        ];
    }
}
