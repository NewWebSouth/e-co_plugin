<?php

namespace App\Filters;

use Nomess\Annotations\Filter;
use Nomess\Annotations\Inject;
use Nomess\Helpers\ResponseHelper;
use Nomess\Http\HttpSession;
use Nomess\Manager\FiltersInterface;

/**
 * @Filter("^account")
 */
class CustomerFilter implements FiltersInterface
{
    
    use ResponseHelper;
    
    private const SESSION_CUSTOMER = 'customer';
    /**
     * @Inject()
     */
    private HttpSession $session;
    
    
    public function filtrate(): void
    {
        /* 
         * TODO create your rule
         *  You can use the dependency injection
         *  Use ResponseHelper for send an response
         */
        
        if( is_null( $this->session->get( self::SESSION_CUSTOMER ) ) ) {
            $this->redirectToLocalResource( 'account.login', [] );
        }
    }
}
