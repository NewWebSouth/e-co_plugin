<?php

namespace App\Filters;

use Nomess\Annotations\Filter;
use Nomess\Annotations\Inject;
use Nomess\Helpers\ResponseHelper;
use Nomess\Http\HttpSession;
use Nomess\Manager\FiltersInterface;

/**
 * @Filter("^admin\/")
 */
class AdminFilter implements FiltersInterface
{
    
    use ResponseHelper;
    
    private const SESSION_ADMIN = 'admin';
    /**
     * @Inject()
     */
    private HttpSession $session;
    
    
    public function filtrate(): void
    {
        if( !$this->session->has( self::SESSION_ADMIN ) ) {
            $this->redirectToLocalResource( 'admin.account.login', [] );
        }
    }
}
