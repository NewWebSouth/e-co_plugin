<?php


namespace App\Repositories;


use App\Entities\Payment;

class PaymentRepository extends AbstractRepository
{
    
    /**
     * @return Payment[]
     */
    public function findAll(): ?array
    {
        return parent::find( Payment::class );
    }
    
    
    /**
     * @param int $id
     * @return Payment|null
     */
    public function findById( int $id ): ?Payment
    {
        return parent::find( Payment::class, $id );
    }
}
