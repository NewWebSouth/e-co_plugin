<?php


namespace App\Repositories;


use App\Entities\Stock;

class StockRepository extends AbstractRepository
{
    
    /**
     * @return Stock[]
     */
    public function findAll(): ?array
    {
        return parent::find( Stock::class );
    }
    
    
    /**
     * @param int $id
     * @param bool $lock
     * @return Stock|null
     */
    public function findById( int $id, bool $lock = FALSE ): ?Stock
    {
        return $this->entityManager->find( Stock::class, $id, NULL, $lock );
    }
    
    
    public function findByName( string $name ): ?Stock
    {
        $result = $this->entityManager->find( Stock::class, 'name = :name', [ 'name' => $name ] );
        
        if( is_array( $result ) ) {
            return $result[0];
        }
        
        return NULL;
    }
}
