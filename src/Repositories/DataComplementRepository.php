<?php


namespace App\Repositories;


use App\Entities\DataComplement;

class DataComplementRepository extends AbstractRepository
{
    
    /**
     * @return DataComplement[]
     */
    public function findAll(): ?array
    {
        return parent::find( DataComplement::class );
    }
    
    
    /**
     * @param int $id
     * @return DataComplement[]|null
     */
    public function findById( int $id ): ?DataComplement
    {
        return parent::find( DataComplement::class, $id );
    }
}
