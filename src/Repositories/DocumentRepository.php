<?php


namespace App\Repositories;


use App\Entities\Document;

class DocumentRepository extends AbstractRepository
{
    
    /**
     * @return Document[]
     */
    public function findAll(): ?array
    {
        return parent::find( Document::class );
    }
    
    
    /**
     * @param int $id
     * @return Document|null
     */
    public function findById( int $id ): ?Document
    {
        return parent::find( Document::class, $id );
    }
}
