<?php


namespace App\Repositories;


use App\Entities\Ordered;

class OrderedRepository extends AbstractRepository
{
    
    /**
     * @return Ordered[]
     */
    public function findAll(): ?array
    {
        return parent::find( Ordered::class );
    }
    
    
    /**
     * @param int $id
     * @return Ordered|null
     */
    public function findById( int $id ): ?Ordered
    {
        return parent::find( Ordered::class, $id );
    }
    
    
    /**
     * @param int $id
     * @return Ordered[]|null
     */
    public function findByCustomer( int $id ): ?array
    {
        return $this->entityManager->find( Ordered::class, 'customer_id = :customer_id', [
            'customer_id' => $id
        ] );
    }
    
    
    /**
     * @return Ordered[]|null
     */
    public function findAllActive(): ?array
    {
        return $this->entityManager->find( Ordered::class, 'status != 4 AND status != 5' );
    }
    
    
    /**
     * @return Ordered[]|null
     */
    public function findAllInWaitingPayment(): ?array
    {
        return $this->entityManager->find( Ordered::class, 'status = 0' );
    }
}
