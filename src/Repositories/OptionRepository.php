<?php


namespace App\Repositories;


use App\Entities\Option;

class OptionRepository extends AbstractRepository
{
    
    /**
     * @return Option[]
     */
    public function findAll(): ?array
    {
        return parent::find( Option::class );
    }
    
    
    /**
     * @param int $id
     * @return Option|null
     */
    public function findById( int $id ): ?Option
    {
        return parent::find( Option::class, $id );
    }
}
