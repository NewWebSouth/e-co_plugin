<?php


namespace App\Repositories;


use App\Entities\Memory;

class MemoryRepository extends AbstractRepository
{
    
    /**
     * @return Memory[]
     */
    public function findAll(): ?array
    {
        return parent::find( Memory::class );
    }
    
    
    /**
     * @param int $id
     * @return Memory|null
     */
    public function findById( int $id ): ?Memory
    {
        return parent::find( Memory::class, $id );
    }
}
