<?php


namespace App\Repositories;


use App\Entities\DataCustomer;

class DataCustomerRepository extends AbstractRepository
{
    
    /**
     * @return DataCustomer[]
     */
    public function findAll(): ?array
    {
        return parent::find( DataCustomer::class );
    }
    
    
    /**
     * @param int $id
     * @return DataCustomer[]|null
     */
    public function findById( int $id ): ?DataCustomer
    {
        return parent::find( DataCustomer::class, $id );
    }
}
