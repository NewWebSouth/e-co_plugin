<?php


namespace App\Repositories;


use App\Entities\ReductionCustomer;

class ReductionCustomerRepository extends AbstractRepository
{
    
    /**
     * @return ReductionCustomer[]
     */
    public function findAll(): ?array
    {
        return parent::find( ReductionCustomer::class );
    }
    
    
    /**
     * @param int $id
     * @return ReductionCustomer|null
     */
    public function findById( int $id ): ?ReductionCustomer
    {
        return parent::find( ReductionCustomer::class, $id );
    }
}
