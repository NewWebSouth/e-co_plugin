<?php


namespace App\Repositories;


use App\Entities\Purchase;

class PurchaseRepository extends AbstractRepository
{
    
    /**
     * @return Purchase[]
     */
    public function findAll(): ?array
    {
        return parent::find( Purchase::class );
    }
    
    
    /**
     * @param int $id
     * @return Purchase|null
     */
    public function findById( int $id ): ?Purchase
    {
        return parent::find( Purchase::class, $id );
    }
}
