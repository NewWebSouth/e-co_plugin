<?php


namespace App\Repositories;


use App\Entities\Section;

class SectionRepository extends AbstractRepository
{
    
    /**
     * @return Section[]
     */
    public function findAll(): ?array
    {
        return parent::find( Section::class );
    }
    
    
    /**
     * @param int $id
     * @return Section|null
     */
    public function findById( int $id ): ?Section
    {
        return parent::find( Section::class, $id );
    }
    
    
    /**
     * @param int $id
     * @return Section[]|null
     */
    public function findAllWithout( int $id ): ?array
    {
        return $this->entityManager->find( Section::class, 'id != :id', [
            'id' => $id
        ] );
    }
}
