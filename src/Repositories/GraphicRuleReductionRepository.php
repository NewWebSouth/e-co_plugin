<?php


namespace App\Repositories;


use App\Entities\GraphicRuleReduction;

class GraphicRuleReductionRepository extends AbstractRepository
{
    
    /**
     * @return GraphicRuleReduction[]
     */
    public function findAll(): ?array
    {
        return parent::find( GraphicRuleReduction::class );
    }
    
    
    /**
     * @param int $id
     * @return GraphicRuleReduction[]|null
     */
    public function findById( int $id ): ?GraphicRuleReduction
    {
        return parent::find( GraphicRuleReduction::class, $id );
    }
}
