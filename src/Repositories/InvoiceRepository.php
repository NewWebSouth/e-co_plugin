<?php


namespace App\Repositories;


use App\Entities\Invoice;
use App\EntitiesInvoice;

class InvoiceRepository extends AbstractRepository
{
    
    /**
     * @return Invoice[]|null
     */
    public function findAll(): ?array
    {
        return parent::find( Invoice::class );
    }
    
    
    /**
     * @param int $id
     * @return Invoice|null
     */
    public function findById( int $id ): ?Invoice
    {
        return parent::find( Invoice::class, $id );
    }
    
    
    /**
     * @param int $id
     * @return Invoice[]|null
     */
    public function findAllForCustomer( int $id ): ?array
    {
        return $this->entityManager->find( Invoice::class, 'customer_id = :customer_id', [
            'customer_id' => $id
        ] );
    }
    
    
    public function findByOrdered( int $id ): ?Invoice
    {
        $result = $this->entityManager->find( Invoice::class, 'ordered_id = :ordered_id', [
            'ordered_id' => $id
        ] );
        
        if( is_array( $result ) ) {
            return $result[0];
        }
        
        return NULL;
    }
}
