<?php


namespace App\Repositories;


use App\Entities\Reduction;

class ReductionRepository extends AbstractRepository
{
    
    /**
     * @return Reduction[]
     */
    public function findAll(): ?array
    {
        return parent::find( Reduction::class );
    }
    
    
    /**
     * @param int $id
     * @return Reduction|null
     */
    public function findById( int $id ): ?Reduction
    {
        return parent::find( Reduction::class, $id );
    }
    
    
    /**
     * @param int $id
     * @return Reduction[]|null
     */
    public function findAllActiveForCustomer( int $id ): ?array
    {
        return $this->entityManager->find( Reduction::class, 'customer_id = :customer_id AND active = 1', [
            'customer_id' => $id
        ] );
    }
    
    
    /**
     * @return Reduction[]|null
     */
    public function findAllActive(): ?array
    {
        return $this->entityManager->find( Reduction::class, 'active = 1' );
    }
}
