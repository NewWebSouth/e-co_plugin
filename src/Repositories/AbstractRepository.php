<?php


namespace App\Repositories;


use Nomess\Annotations\Inject;
use Nomess\Components\EntityManager\EntityManagerInterface;

abstract class AbstractRepository
{
    
    /**
     * @Inject()
     */
    protected EntityManagerInterface $entityManager;
    
    
    /**
     * @param string $classname
     * @param int|null $id
     * @return array|object|null
     */
    protected function find( string $classname, int $id = NULL )
    {
        return $this->entityManager->find( $classname, $id );
    }
}
