<?php


namespace App\Repositories;


use App\Entities\Credit;

class CreditRepository extends AbstractRepository
{
    
    /**
     * @return Credit[]
     */
    public function findAll(): ?array
    {
        return parent::find( Credit::class );
    }
    
    
    /**
     * @param int $id
     * @return Credit[]|null
     */
    public function findById( int $id ): ?Credit
    {
        return parent::find( Credit::class, $id );
    }
}
