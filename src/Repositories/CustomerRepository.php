<?php


namespace App\Repositories;


use App\Entities\Customer;

class CustomerRepository extends AbstractRepository
{
    
    /**
     * @return Customer[]
     */
    public function findAll(): ?array
    {
        return parent::find( Customer::class );
    }
    
    
    /**
     * @param int $id
     * @return Customer[]|null
     */
    public function findById( int $id ): ?Customer
    {
        return parent::find( Customer::class, $id );
    }
    
    
    public function findByEmail( string $email ): ?Customer
    {
        $result = $this->entityManager->find( Customer::class, 'email = :email', [
            'email' => $email
        ] );
        
        return is_array( $result ) ? $result[0] : NULL;
    }
}
