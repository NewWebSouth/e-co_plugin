<?php


namespace App\Repositories;


use App\Entities\GraphicRuleProduct;

class GraphicRuleProductRepository extends AbstractRepository
{
    
    /**
     * @return GraphicRuleProduct[]
     */
    public function findAll(): ?array
    {
        return parent::find( GraphicRuleProduct::class );
    }
    
    
    /**
     * @param int $id
     * @return GraphicRuleProduct[]|null
     */
    public function findById( int $id ): ?GraphicRuleProduct
    {
        return parent::find( GraphicRuleProduct::class, $id );
    }
}
