<?php
/** @noinspection PhpIncompatibleReturnTypeInspection */


namespace App\Repositories;


use App\Entities\Article;

class ArticleRepository extends AbstractRepository
{
    
    /**
     * @return Article[]
     */
    public function findAll(): ?array
    {
        return parent::find( Article::class );
    }
    
    
    /**
     * @param int $id
     * @return Article[]|null
     */
    public function findById( int $id ): ?Article
    {
        return parent::find( Article::class, $id );
    }
}
