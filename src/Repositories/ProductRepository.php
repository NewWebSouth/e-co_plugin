<?php


namespace App\Repositories;


use App\Entities\Product;
use App\EntitiesProduct;

class ProductRepository extends AbstractRepository
{
    
    /**
     * @return Product[]
     */
    public function findAll(): ?array
    {
        return parent::find( Product::class );
    }
    
    
    /**
     * @param int $id
     * @return Product|null
     */
    public function findById( int $id ): ?Product
    {
        return parent::find( Product::class, $id );
    }
    
    
    /**
     * @param int $excludeId
     * @return Product[]|null
     */
    public function findAllToRecommended( int $excludeId ): ?array
    {
        return $this->entityManager->find( Product::class, 'active = 1 AND id != :id_exclude AND graphicruleproduct_id IS NOT NULL', [
            'id_exclude' => $excludeId
        ] );
    }
    
    
    /**
     * @return Product[]|null
     */
    public function findAllActive(): ?array
    {
        return $this->entityManager->find( Product::class, 'active = 1' );
    }
}
