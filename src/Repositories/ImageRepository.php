<?php


namespace App\Repositories;


use App\Entities\Image;

class ImageRepository extends AbstractRepository
{
    
    /**
     * @return Image[]|null
     */
    public function findAll(): ?array
    {
        return parent::find( Image::class );
    }
    
    
    /**
     * @param int $id
     * @return Image|null
     */
    public function findById( int $id ): ?Image
    {
        return parent::find( Image::class, $id );
    }
    
    
    public function findByPublicFilename( string $publicFilename ): ?Image
    {
        $result = $this->entityManager->find( Image::class, 'public_filename = :name', [
            'name' => $publicFilename
        ] );
        
        if( is_array( $result ) ) {
            return $result[0];
        }
        
        return NULL;
    }
}
