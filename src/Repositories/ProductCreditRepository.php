<?php


namespace App\Repositories;


use App\Entities\ProductCredit;

class ProductCreditRepository extends AbstractRepository
{
    
    /**
     * @return ProductCredit[]
     */
    public function findAll(): ?array
    {
        return parent::find( ProductCredit::class );
    }
    
    
    /**
     * @param int $id
     * @return ProductCredit|null
     */
    public function findById( int $id ): ?ProductCredit
    {
        return parent::find( ProductCredit::class, $id );
    }
}
