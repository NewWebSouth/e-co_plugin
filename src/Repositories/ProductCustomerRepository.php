<?php


namespace App\Repositories;


use App\Entities\ProductCustomer;

class ProductCustomerRepository extends AbstractRepository
{
    
    /**
     * @return ProductCustomer[]|null
     */
    public function findAll(): ?array
    {
        return $this->find( ProductCustomer::class );
    }
    
    
    public function findById( int $id ): ?ProductCustomer
    {
        return $this->find( ProductCustomer::class, $id );
    }
}
