<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NumberExtension extends AbstractExtension
{
    
    public function getFunctions()
    {
        return [
            new TwigFunction( 'str_number', [ $this, 'number' ] ),
            new TwigFunction( 'str_price', [ $this, 'price' ] ),
            new TwigFunction( 'str_percent', [ $this, 'percent' ] )
        ];
    }
    
    
    public function number( float $number ): string
    {
        return number_format( $number, 2, ',', '' );
    }
    
    
    public function price( float $number ): string
    {
        return $this->number( $number ) . ' €';
    }
    
    
    public function percent( float $number ): string
    {
        return $this->number( $number ) . ' %';
    }
}
