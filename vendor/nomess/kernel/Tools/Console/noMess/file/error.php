<?php

return [
    '404' => 'path/to/404_error',
    '403' => 'path/to/403_error',
    '500' => 'path/to/500_error'
];