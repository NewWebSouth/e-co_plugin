<?php


namespace Nomess\Manager;


interface FiltersInterface
{
    public function filtrate(): void;
}
