<?php

namespace Tests;

define( 'ROOT', '/var/www/histoire-de-pierre.nws/' );

use App\Entities\Customer;
use App\Entities\DataComplement;
use App\Entities\DataCustomer;
use App\Entities\Option;
use App\Entities\Product;
use App\Entities\ProductCustomer;
use App\Entities\Reduction;
use App\Entities\ReductionCustomer;
use App\Entities\Section;
use App\Entities\Stock;
use Nomess\Exception\InvalidParamException;
use PHPUnit\Framework\TestCase;

abstract class UnitTest extends TestCase
{
    
    private Customer $customer;
    private array    $reductionCustomer = array();
    private array    $productCustomers  = array();
    private array    $dataComplements   = array();
    private array    $dataCustomers     = array();
    private array    $options           = array();
    private array    $stocks            = array();
    
    
    /**
     * Add data in POST variable
     *
     * @param array $parameters
     */
    protected function hydratePost( array $parameters ): void
    {
        $_POST = array();
        
        foreach( $parameters as $key => $value ) {
            $_POST[$key] = $value;
        }
    }
    
    
    /**
     * Return a basically customer
     *
     * @return Customer
     * @throws InvalidParamException
     */
    protected function getGenericCustomer(): Customer
    {
        
        if( isset( $this->customer ) ) {
            return $this->customer;
        }
        
        $customer = ( new Customer() )->setFirstName( 'Romain' )
                                      ->setLastName( 'Lavabre' )
                                      ->setNumberStreet( '20' )
                                      ->setStreet( 'rue de la croix blanche' )
                                      ->setCity( 'Peyrolles en provence' )
                                      ->setPostalCode( '13 860' )
                                      ->setCountry( 'France' )
                                      ->setEmail( 'romainlavabre98@gmail.com' )
                                      ->setOfferAccept( FALSE );
        
        $this->setId( $customer, 1 );
        
        return $this->customer = $customer;
    }
    
    
    protected function generateProduct( int $id = NULL ): Product
    {
        $number = random_int( 20, 150 );
        $id     = empty( $id ) ? $number : $id;
        
        $product = ( new Product() )->setName( 'Produit ' . $id )
                                    ->setPrice( $number )
                                    ->setDescription( 'Déscription du produit ' . $id )
                                    ->setOptionRequired( FALSE );
        
        $this->setId( $product, $id );
        
        return $product;
    }
    
    
    protected function generateProductCustomer( int $id = NULL ): ProductCustomer
    {
        
        if( isset( $this->productCustomers[$id] ) ) {
            return $this->productCustomers[$id];
        }
        
        $number = random_int( 1, 50 );
        $id     = empty( $id ) ? $number : $id;
        
        $product = $this->generateProduct( $id );
        
        $productCustomer = ( new ProductCustomer() )->setCustomer( $this->getGenericCustomer() )
                                                    ->setProduct( $product )
                                                    ->setQuantity( $number )
                                                    ->setNetToPay()
                                                    ->setTotal();
        
        $this->setId( $productCustomer, $id );
        $this->productCustomers[$id] = $productCustomer;
        
        return $productCustomer;
    }
    
    
    protected function generateReduction( int $id = NULL ): Reduction
    {
        $number = random_int( 1, 100 );
        $id     = empty( $id ) ? $number : $id;
        
        $reduction = ( new Reduction() )->setCustomer( $this->getGenericCustomer() )
                                        ->setValue( $number );
        
        $this->setId( $reduction, $id );
        
        return $reduction;
    }
    
    
    protected function generateReductionCustomer( int $id = NULL ): ReductionCustomer
    {
        
        if( array_key_exists( $id, $this->reductionCustomer ) ) {
            return $this->reductionCustomer[$id];
        }
        
        $number = random_int( 1, 100 );
        $id     = empty( $id ) ? $number : $id;
        
        $reductionCustomer = ( new ReductionCustomer() )->setReduction( $this->generateReduction( 1 ) );
        $this->setId( $reductionCustomer, $id );
        
        $this->reductionCustomer[$id] = $reductionCustomer;
        
        return $reductionCustomer;
    }
    
    
    protected function generateDataComplement( int $id ): DataComplement
    {
        
        if( isset( $this->dataComplements[$id] ) ) {
            return $this->dataComplements[$id];
        }
        
        $dataComplement = ( new DataComplement() )->setTitle( 'Titre de la donnée' )
                                                  ->setContent( 'Contenu de la donnée' );
        
        $this->setId( $dataComplement, $id );
        
        $this->dataComplements[$id] = $dataComplement;
        
        return $dataComplement;
    }
    
    
    protected function generateOption( int $id ): Option
    {
        if( isset( $this->options[$id] ) ) {
            return $this->options[$id];
        }
        
        $option = ( new Option() )->setName( 'Option name' )
                                  ->setPrice( 20 );
        
        $this->setId( $option, $id );
        
        $this->options[$id] = $option;
        
        return $option;
    }
    
    
    protected function generateDataCustomer( int $id ): DataCustomer
    {
        if( isset( $this->dataCustomers[$id] ) ) {
            return $this->dataCustomers[$id];
        }
        
        $dataCustomer = ( new DataCustomer() )->setName( 'Donnée client name' )
                                              ->setRequired( TRUE )
                                              ->setPriority( 10 );
        
        $this->setId( $dataCustomer, $id );
        
        $this->dataCustomers[$id] = $dataCustomer;
        
        return $dataCustomer;
    }
    
    
    protected function generateStock( int $id ): Stock
    {
        
        if( isset( $this->stocks[$id] ) ) {
            return $this->stocks[$id];
        }
        
        $stock = ( new Stock() )->setName( 'Stock' )
                                ->setDescription( 'The of product' )
                                ->setQuantity( 10 );
        
        $this->setId( $stock, $id );
        
        $this->stocks[$id] = $stock;
        
        return $stock;
    }
    
    
    protected function generateSection( int $id ): Section
    {
        $section = ( new Section() )->setName( 'Nam of section' )
                                    ->setDescription( 'Description of section' )
                                    ->setPriority( 15 );
        
        $this->setId( $section, $id );
        
        return $section;
    }
    
    
    protected function setId( object $object, int $id ): void
    {
        $reflectionProperty = new \ReflectionProperty( get_class( $object ), 'id' );
        $reflectionProperty->setAccessible( TRUE );
        $reflectionProperty->setValue( $object, $id );
    }
}
