<?php


namespace Tests\Administer\Manage\Ordered;


use App\Entities\Ordered;
use App\Entities\Product;
use App\Entities\ProductCustomer;
use App\Packages\Administer\Manage\Ordered\SUpdate;
use App\Packages\Administer\Tools\Mail\Initializer\SentMailInterface;
use App\Packages\Cunsumption\Stock\StockRollbackInterface;
use App\Repositories\ReductionCustomerRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class OrderedTest extends UnitTest
{
    
    private function getService(): SUpdate
    {
        $entityManager               = $this->createMock( EntityManagerInterface::class );
        $stockRollback               = $this->createMock( StockRollbackInterface::class );
        $sentMail                    = $this->createMock( SentMailInterface::class );
        $reductionCustomerRepository = $this->createMock( ReductionCustomerRepository::class );
        $reductionCustomerRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            if( $id === 1 ) {
                return $this->generateReductionCustomer( 1 );
            }
            
            return NULL;
        } );
        
        return new SUpdate(
            $entityManager,
            $stockRollback,
            $reductionCustomerRepository,
            $sentMail
        );
    }
    
    
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $ordered = ( new Ordered() )->setStatus( 0 )
                                    ->setCustomer( $customer = $this->getGenericCustomer() )
                                    ->addProductCustomer( ( new ProductCustomer() )->setCustomer( $customer )
                                                                                   ->setQuantity( 2 )
                                                                                   ->setProduct( ( new Product() )->setName( 'Produit 1' )
                                                                                                                  ->setPrice( 20 ) ) );
        
        $state = $this->getService()->update( new HttpRequest(), $ordered );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @dataProvider providerUpdate_isFalse
     * @test
     * @param array $parameters
     * @param Ordered|null $ordered
     */
    public function update_isFalse( array $parameters, Ordered $ordered = NULL ): void
    {
        $this->hydratePost( $parameters );
        
        if( $ordered === NULL ) {
            $ordered = new Ordered();
        }
        
        $state = $this->getService()->update( new HttpRequest(), $ordered );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @dataProvider providerUpdate_isRemovedProductCustomer
     * @test
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function update_isRemovedProductCustomer( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $productCustomer1 = $this->generateProductCustomer( 1 );
        
        $ordered = ( new Ordered() )->setCustomer( $this->getGenericCustomer() )
                                    ->setStatus( 0 )
                                    ->addProductCustomer( $productCustomer1 );
        
        $this->getService()->update( new HttpRequest(), $ordered );
        
        $this->assertEmpty( $ordered->getProductCustomer() );
    }
    
    
    /**
     * @dataProvider providerUpdate_isValidTotalWithoutReduction
     * @test
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function update_isValidTotalWithoutReduction( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $productCustomer1 = $this->generateProductCustomer( 1 );
        
        $ordered = ( new Ordered() )->setCustomer( $this->getGenericCustomer() )
                                    ->setStatus( 0 )
                                    ->addProductCustomer( $productCustomer1->addReductionCustomers( $this->generateReductionCustomer( 1 ) ) )
                                    ->setNetToPay()
                                    ->setTotal();
        
        $this->getService()->update( new HttpRequest(), $ordered );
        
        $this->assertEquals(
            $productCustomer1->getProduct()->getPrice() * $parameters['quantity_product_1'],
            $ordered->getNetToPay()
        );
    }
    
    
    /**
     * @dataProvider providerUpdate_isValidTotalWithReduction
     * @test
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function update_isValidTotalWithReduction( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $productCustomer1   = $this->generateProductCustomer( 1 );
        $reductionCustomer1 = $this->generateReductionCustomer( 1 );
        
        $ordered = ( new Ordered() )->setCustomer( $this->getGenericCustomer() )
                                    ->setStatus( 0 )
                                    ->addProductCustomer( $productCustomer1->addReductionCustomers( $reductionCustomer1 ) );
        
        $this->getService()->update( new HttpRequest(), $ordered );
        $total = $productCustomer1->getProduct()->getPrice() * $parameters['quantity_product_1'];
        
        $this->assertEquals(
            round( $total - ( ( $total * $reductionCustomer1->getReduction()->getValue() ) / 100 ), 2 ),
            $ordered->getNetToPay()
        );
    }
    
    
    /**
     * @dataProvider providerUpdate_isValidTotalWithNewReduction
     * @test
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function update_isValidTotalWithNewReduction( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $productCustomer1 = $this->generateProductCustomer( 1 );
        
        $ordered = ( new Ordered() )->setCustomer( $this->getGenericCustomer() )
                                    ->setStatus( 0 )
                                    ->addProductCustomer( $productCustomer1 );
        
        $this->getService()->update( new HttpRequest(), $ordered );
        $total = $productCustomer1->getProduct()->getPrice() * $parameters['quantity_product_1'];
        
        $this->assertEquals(
            round( $total - ( ( $total * $parameters['add_reduction_1'] ) / 100 ), 2 ),
            $ordered->getNetToPay()
        );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'Peyrolles en pce',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'France',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'Peyrolles en pce',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'France',
                    'status'                         => 2
                ],
                ( new Ordered() )->setStatus( 1 ) // failure
            ],
            [
                [
                    'address_billing_number_street'  => NULL,
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'Peyrolles en pce',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'France',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'Peyrolles en pce',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'France',
                    'status'                         => 2
                ],
                ( new Ordered() )->setStatus( 0 )
            ],
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'Peyrolles en pce',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'France',
                    'address_delivery_number_street' => NULL,
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'Peyrolles en pce',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'France',
                    'status'                         => 2
                ],
                ( new Ordered() )->setStatus( 0 )
            ]
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'Peyrolles en pce',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'France',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'Peyrolles en pce',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'France',
                    'status'                         => 1
                ],
                ( new Ordered() )->setStatus( 0 )
            ],
        ];
    }
    
    
    public function providerUpdate_isRemovedProductCustomer(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'Peyrolles en pce',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'France',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'Peyrolles en pce',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'France',
                    'status'                         => 1,
                    'remove_product_1'               => 'true'
                ],
                ( new Ordered() )->setStatus( 0 )
            ],
        ];
    }
    
    
    public function providerUpdate_isValidTotalWithoutReduction(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'Peyrolles en pce',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'France',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'Peyrolles en pce',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'France',
                    'status'                         => 1,
                    'quantity_product_1'             => 10,
                    'remove_reduction_1'             => [ 1 ]
                ],
                ( new Ordered() )->setStatus( 0 )
            ],
        ];
    }
    
    
    public function providerUpdate_isValidTotalWithReduction(): array
    {
        return [
            [ // Try with existing reduction
              [
                  'address_billing_number_street'  => '20',
                  'address_billing_street'         => 'rue de la croix blanche',
                  'address_billing_city'           => 'Peyrolles en pce',
                  'address_billing_postal_code'    => '13 860',
                  'address_billing_country'        => 'France',
                  'address_delivery_number_street' => '20',
                  'address_delivery_street'        => 'rue de la croix blanche',
                  'address_delivery_city'          => 'Peyrolles en pce',
                  'address_delivery_postal_code'   => '13 860',
                  'address_delivery_country'       => 'France',
                  'status'                         => 1,
                  'quantity_product_1'             => 10,
              ],
              ( new Ordered() )->setStatus( 0 )
            ],
        ];
    }
    
    
    public function providerUpdate_isValidTotalWithNewReduction(): array
    {
        return [
            
            [ // Try with existing reduction + new Reduction
              [
                  'address_billing_number_street'  => '20',
                  'address_billing_street'         => 'rue de la croix blanche',
                  'address_billing_city'           => 'Peyrolles en pce',
                  'address_billing_postal_code'    => '13 860',
                  'address_billing_country'        => 'France',
                  'address_delivery_number_street' => '20',
                  'address_delivery_street'        => 'rue de la croix blanche',
                  'address_delivery_city'          => 'Peyrolles en pce',
                  'address_delivery_postal_code'   => '13 860',
                  'address_delivery_country'       => 'France',
                  'status'                         => 1,
                  'quantity_product_1'             => 10,
                  'add_reduction_1'                => 50.0 // value of new reduction
              ],
              ( new Ordered() )->setStatus( 0 )
            ],
        ];
    }
}
