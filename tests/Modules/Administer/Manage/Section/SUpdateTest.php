<?php


namespace Tests\Administer\Manage\Section;


use App\Entities\Section;
use App\Packages\Administer\Manage\Section\SUpdate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SUpdateTest extends UnitTest
{
    
    private function getService(): SUpdate
    {
        $entityManager     = $this->createMock( EntityManagerInterface::class );
        $frontObserver     = $this->createMock( FrontObserverInterface::class );
        $uploadImage       = $this->createMock( UploadImageInterface::class );
        $sectionRepository = $this->createMock( SectionRepository::class );
        
        $sectionRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateSection( 1 ) : NULL;
        } );
        
        $service = new SUpdate(
            $entityManager,
            $uploadImage,
            $sectionRepository
        );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isFalse
     * @param array $parameters
     */
    public function update_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), new Section() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isTrue
     * @param array $parameters
     */
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), new Section() );
        
        $this->assertTrue( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isValidParent
     * @param array $parameters
     */
    public function update_isValidParent( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService()->update( new HttpRequest(), $section = new Section() );
        
        $this->assertNotNull( $section->getParent() );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'name'        => '', //Failure
                    'description' => NULL,
                    'priority'    => NULL,
                    'image'       => NULL,
                    'parent'      => NULL,
                ]
            ],
            [
                [
                    'name'        => 'section',
                    'description' => NULL,
                    'priority'    => NULL,
                    'image'       => NULL,
                    'parent'      => 2,
                ]
            ]
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'name'        => 'Le nom de la section',
                    'description' => 'La déscription de la section',
                    'priority'    => 20,
                    'image'       => NULL,
                    'parent'      => 1,
                ]
            ],
        ];
    }
    
    
    public function providerUpdate_isValidParent(): array
    {
        return [
            [
                [
                    'name'        => 'Le nom de la section',
                    'description' => 'La déscription de la section',
                    'priority'    => 20,
                    'image'       => NULL,
                    'parent'      => 1,
                ]
            ],
        ];
    }
}
