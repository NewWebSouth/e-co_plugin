<?php


namespace Tests\Administer\Manage\Section;


use App\Entities\Section;
use App\Packages\Administer\Manage\Section\SCreate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SCreateTest extends \Tests\UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager     = $this->createMock( EntityManagerInterface::class );
        $frontObserver     = $this->createMock( FrontObserverInterface::class );
        $uploadImage       = $this->createMock( UploadImageInterface::class );
        $sectionRepository = $this->createMock( SectionRepository::class );
        
        $sectionRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateSection( 1 ) : NULL;
        } );
        
        $service = new SCreate(
            $entityManager,
            $uploadImage,
            $sectionRepository
        );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Section() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Section() );
        
        $this->assertTrue( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isValidParent
     * @param array $parameters
     */
    public function create_isValidParent( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService()->create( new HttpRequest(), $section = new Section() );
        
        $this->assertNotNull( $section->getParent() );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'name'        => '', //Failure
                    'description' => NULL,
                    'priority'    => NULL,
                    'image'       => NULL,
                    'parent'      => NULL,
                ]
            ],
            [
                [
                    'name'        => 'section',
                    'description' => NULL,
                    'priority'    => NULL,
                    'image'       => NULL,
                    'parent'      => 2,
                ]
            ]
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'name'        => 'Le nom de la section',
                    'description' => 'La déscription de la section',
                    'priority'    => 20,
                    'image'       => NULL,
                    'parent'      => 1,
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isValidParent(): array
    {
        return [
            [
                [
                    'name'        => 'Le nom de la section',
                    'description' => 'La déscription de la section',
                    'priority'    => 20,
                    'image'       => NULL,
                    'parent'      => 1,
                ]
            ],
        ];
    }
}
