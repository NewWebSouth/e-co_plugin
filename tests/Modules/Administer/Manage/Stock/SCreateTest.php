<?php


namespace Tests\Administer\Manage\Stock;


use App\Entities\Stock;
use App\Packages\Administer\Manage\Stock\SCreate;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager   = $this->createMock( EntityManagerInterface::class );
        $frontObserver   = $this->createMock( FrontObserverInterface::class );
        $stockRepository = $this->createMock( StockRepository::class );
        
        $stockRepository->method( 'findByName' )->willReturnCallback( function ( string $name ) {
            if( $name === 'Nom du stock' ) {
                return new Stock();
            }
            
            return NULL;
        } );
        
        $service = new SCreate( $entityManager, $stockRepository );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Stock() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Stock() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'name'     => '', // Failure
                    'quantity' => 10,
                ]
            ],
            [
                [
                    'name'     => 'Nom du stock', // Failure : already exists with this name
                    'quantity' => 10,
                ]
            ]
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'name'     => 'Name of stock',
                    'quantity' => 0,
                ]
            ],
            [
                [
                    'name'     => 'Name of stock',
                    'quantity' => 10,
                ]
            ]
        ];
    }
}
