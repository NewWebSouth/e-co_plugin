<?php


namespace Tests\Administer\Manage\Stock;


use App\Entities\Stock;
use App\Packages\Administer\Manage\Stock\SUpdate;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SUpdateTest extends UnitTest
{
    
    private function getService(): SUpdate
    {
        $entityManager   = $this->createMock( EntityManagerInterface::class );
        $frontObserver   = $this->createMock( FrontObserverInterface::class );
        $stockRepository = $this->createMock( StockRepository::class );
        
        $stockRepository->method( 'findByName' )->willReturnCallback( function ( string $name ) {
            if( $name === 'Nom du stock' ) {
                return new Stock();
            }
            
            return NULL;
        } );
        
        $service = new SUpdate( $entityManager, $stockRepository );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isFalse
     * @param array $parameters
     */
    public function update_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), new Stock() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isTrue
     * @param array $parameters
     */
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), new Stock() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'name'     => '', // Failure
                    'quantity' => 10,
                ]
            ],
            [
                [
                    'name'     => 'Nom du stock', // Failure : already exists with this name
                    'quantity' => 10,
                ]
            ]
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'name'     => 'Name of stock',
                    'quantity' => 0,
                ]
            ],
            [
                [
                    'name'     => 'Name of stock',
                    'quantity' => 10,
                ]
            ]
        ];
    }
}
