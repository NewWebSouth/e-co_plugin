<?php


namespace Tests\Administer\Manage\Credit;


use App\Entities\Credit;
use App\Entities\Invoice;
use App\Packages\Administer\Manage\Credit\ProductCredit\SCreateProductCredit;
use App\Packages\Administer\Manage\Credit\SCreateCredit;
use App\Packages\Administer\Report\PdfGeneratorInterface;
use App\Repositories\InvoiceRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateCreditTest extends UnitTest
{
    
    private function getService( bool $withoutInvoice ): SCreateCredit
    {
        $entityManager     = $this->createMock( EntityManagerInterface::class );
        $pdfGenerator      = $this->createMock( PdfGeneratorInterface::class );
        $invoiceRepository = $this->createMock( InvoiceRepository::class );
        
        // For test a crash when invoice not found
        if( !$withoutInvoice ) {
            $invoiceRepository->method( 'findById' )->willReturn( ( new Invoice() )->setCustomer( $this->getGenericCustomer() ) );
        }
        
        return new SCreateCredit( $entityManager, $invoiceRepository, new SCreateProductCredit( $entityManager ), $pdfGenerator );
    }
    
    
    /**
     * @dataProvider providerCreate_isFalse
     * @test
     * @param array $parameters
     * @param bool $withoutInvoice
     * @throws InvalidParamException
     */
    public function create_isFalse( array $parameters, bool $withoutInvoice = FALSE ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService( $withoutInvoice )->create( new HttpRequest(), new Credit() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @dataProvider providerCreate_isTrue
     * @test
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService( FALSE )->create( new HttpRequest(), new Credit() );
        
        $this->assertTrue( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isGoodTotal
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function create_isGoodTotal( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( FALSE )->create( new HttpRequest(), $credit = new Credit() );
        
        $this->assertEquals( 40, $credit->getTotal() );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'invoice'  => 1,
                    'way'      => '', //Failure
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        2,
                        2
                    ],
                ]
            ],
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1', // Failure (miss one name)
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        2,
                        2
                    ]
                ]
            ],
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20, // Failure
                    ],
                    'quantity' => [
                        2,
                        2
                    ]
                ]
            ],
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        2, // Failure
                    ]
                ]
            ],
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        2,
                        2
                    ]
                ],
                TRUE // Without invoice
            ],
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        2,
                        0, // failure
                    ]
                ]
            ],
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        0 // Failure
                    ],
                    'quantity' => [
                        2,
                        2,
                    ]
                ],
            ],
            [
                [
                    [
                        'invoice'  => 1,
                        'way'      => 'Carte bleu',
                        'name'     => [
                            'Produit 1',
                            '' // Failure
                        ],
                        'price'    => [
                            20,
                            20
                        ],
                        'quantity' => [
                            2,
                            2,
                        ]
                    ]
                ],
            ]
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        2,
                        2
                    ]
                ]
            ]
        ];
    }
    
    
    public function providerCreate_isGoodTotal(): array
    {
        return [
            [
                [
                    'invoice'  => 1,
                    'way'      => 'Carte bleu',
                    'name'     => [
                        'Produit 1',
                        'Produit 2'
                    ],
                    'price'    => [
                        20,
                        20
                    ],
                    'quantity' => [
                        1,
                        1
                    ]
                ]
            ]
        ];
    }
}
