<?php


namespace Tests\Administer\Manage\Purchase;


use App\Entities\Purchase;
use App\Packages\Administer\Manage\Purchase\SCreate;
use App\Packages\Administer\Tools\System\Document\UploadDocumentInterface;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager   = $this->createMock( EntityManagerInterface::class );
        $uploadDocument  = $this->createMock( UploadDocumentInterface::class );
        $stockRepository = $this->createMock( StockRepository::class );
        $stockRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateStock( 1 ) : NULL;
        } );
        
        return new SCreate(
            $entityManager,
            $uploadDocument,
            $stockRepository
        );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Purchase() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Purchase() );
        
        $this->assertTrue( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isValidQuantityStock
     * @param array $parameters
     */
    public function create_isValidQuantityStock( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $expect = $this->generateStock( 1 )->getQuantity() + 15;
        
        $this->getService()->create( new HttpRequest(), $purchase = new Purchase() );
        
        $this->assertEquals( $expect, $purchase->getStocks()[0]->getQuantity() );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'provider'       => 'Fournisseur',
                    'date'           => '09/09/2015',
                    'total'          => 50.0,
                    'stocks'         => [
                        2 // Failure
                    ],
                    'stock_quantity' => [
                        15
                    ]
                ]
            ]
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'provider'       => 'Fournisseur',
                    'date'           => '09/09/2015',
                    'total'          => 50.0,
                    'stocks'         => [
                        1
                    ],
                    'stock_quantity' => [
                        15
                    ]
                ]
            ],
            [
                [
                    'provider'       => '',
                    'date'           => '09/09/2015',
                    'total'          => 50.0,
                    'stocks'         => [
                        1
                    ],
                    'stock_quantity' => [
                        15
                    ]
                ]
            ],
            [
                [
                    'provider'       => 'Fournisseur',
                    'date'           => '',
                    'total'          => 50.0,
                    'stocks'         => [
                        1
                    ],
                    'stock_quantity' => [
                        15
                    ]
                ]
            ],
            [
                [
                    'provider'       => 'Fournisseur',
                    'date'           => '09/09/2015',
                    'total'          => NULL,
                    'stocks'         => [
                        1
                    ],
                    'stock_quantity' => [
                        15
                    ]
                ]
            ]
        ];
    }
    
    
    public function providerCreate_isValidQuantityStock(): array
    {
        return [
            [
                [
                    'provider'       => 'Fournisseur',
                    'date'           => '09/09/2015',
                    'total'          => 50.0,
                    'stocks'         => [
                        1
                    ],
                    'stock_quantity' => [
                        15
                    ]
                ]
            ]
        ];
    }
}
