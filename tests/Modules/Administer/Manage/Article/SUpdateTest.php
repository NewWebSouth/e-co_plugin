<?php


namespace Tests\Administer\Manage\Article;


use App\Entities\Article;
use App\Packages\Administer\Manage\Article\SUpdate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SUpdateTest extends UnitTest
{
    
    public function getService(): SUpdate
    {
        $entityManager = $this->createMock( EntityManagerInterface::class );
        $uploadImage   = $this->createMock( UploadImageInterface::class );
        $frontObserver = $this->createMock( FrontObserverInterface::class );
        
        $service = new SUpdate( $entityManager, $uploadImage );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @dataProvider providerUpdate_isFalse
     * @test
     * @param array $parameters
     */
    public function update_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), new Article() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isTrue
     * @param array $parameters
     */
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), new Article() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'title'   => 'Un titre valide',
                    'type'    => 3, // failure
                    'content' => 'Contenu valide'
                ]
            ],
            [
                [
                    'title'   => 'shrt', // failure
                    'type'    => 1,
                    'content' => 'Contenu valide'
                ]
            ],
            [
                [
                    'title'   => 'Un titre valide',
                    'type'    => 0,
                    'content' => 'Contenu'// failure
                ]
            ],
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'title'   => 'Un titre valide',
                    'type'    => 0, // failure
                    'content' => 'Contenu valide'
                ]
            ],
            [
                [
                    'title'   => 'valide title', // failure
                    'type'    => 1,
                    'content' => 'Contenu valide'
                ]
            ],
        ];
    }
}
