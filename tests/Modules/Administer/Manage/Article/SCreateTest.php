<?php


namespace Tests\Administer\Manage\Article;


use App\Entities\Article;
use App\Packages\Administer\Manage\Article\SCreate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    public function getService(): SCreate
    {
        $entityManager = $this->createMock( EntityManagerInterface::class );
        $uploadImage   = $this->createMock( UploadImageInterface::class );
        $frontObserver = $this->createMock( FrontObserverInterface::class );
        
        $service = new SCreate( $entityManager, $uploadImage );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @dataProvider providerCreate_isFalse
     * @test
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Article() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Article() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'title'   => 'Un titre valide',
                    'type'    => 3, // failure
                    'content' => 'Contenu valide'
                ]
            ],
            [
                [
                    'title'   => 'shrt', // failure
                    'type'    => 1,
                    'content' => 'Contenu valide'
                ]
            ],
            [
                [
                    'title'   => 'Un titre valide',
                    'type'    => 0,
                    'content' => 'Contenu'// failure
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'title'   => 'Un titre valide',
                    'type'    => 0, // failure
                    'content' => 'Contenu valide'
                ]
            ],
            [
                [
                    'title'   => 'valide title', // failure
                    'type'    => 1,
                    'content' => 'Contenu valide'
                ]
            ],
        ];
    }
}
