<?php


namespace Tests\Administer\Manage\Product;


use App\Entities\Stock;
use App\Packages\Administer\Manage\Product\Option\SCreate;
use App\Packages\Administer\Manage\Product\Option\SDelete;
use App\Packages\Administer\Manage\Product\SUpdate;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\DataComplementRepository;
use App\Repositories\DataCustomerRepository;
use App\Repositories\OptionRepository;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SUpdateTest extends \Tests\UnitTest
{
    
    private function getService(): SUpdate
    {
        $entityManager            = $this->createMock( EntityManagerInterface::class );
        $stockRepository          = $this->createMock( StockRepository::class );
        $dataComplementRepository = $this->createMock( DataComplementRepository::class );
        $dataCustomerRepository   = $this->createMock( DataCustomerRepository::class );
        $optionRepository         = $this->createMock( OptionRepository::class );
        $frontObserver            = $this->createMock( FrontObserverInterface::class );
        
        $stockRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? new Stock() : NULL;
        } );
        
        $dataComplementRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateDataComplement( 1 ) : NULL;
        } );
        
        $dataCustomerRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateDataCustomer( 1 ) : NULL;
        } );
        
        $optionRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateOption( 1 ) : NULL;
        } );
        
        $service = new SUpdate(
            $entityManager,
            new \App\Packages\Administer\Manage\Product\DataCustomer\SUpdate( $entityManager, $dataCustomerRepository ),
            new \App\Packages\Administer\Manage\Product\DataComplement\SUpdate( $entityManager, $dataComplementRepository ),
            new SCreate( $entityManager, $stockRepository ),
            new SDelete( $entityManager, $optionRepository ),
            $stockRepository
        );
        
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isFalse
     * @param array $parameters
     */
    public function update_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        $product = $this->generateProduct( 1 )
                        ->addOption( $this->generateOption( 1 ) )
                        ->addDataComplement( $this->generateDataComplement( 1 ) )
                        ->addDataCustomer( $this->generateDataCustomer( 1 ) );
        
        $state = $this->getService()->update( new HttpRequest(), $product );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isTrue
     * @param array $parameters
     */
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        $product = $this->generateProduct( 1 )
                        ->addOption( $this->generateOption( 1 ) )
                        ->addDataComplement( $this->generateDataComplement( 1 ) )
                        ->addDataCustomer( $this->generateDataCustomer( 1 ) );
        
        $state = $this->getService()->update( new HttpRequest(), $product );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'description'     => 'The description of product',
                    'price'           => 0, // failure
                    'option_required' => FALSE
                ]
            ],
            [
                [
                    'description'             => 'The description of product',
                    'price'                   => 25.0,
                    'option_required'         => FALSE,
                    'title_data_complement'   => [
                        'data_complement 1',
                        'data_complement 2'
                    ],
                    'content_data_complement' => [
                        'content_data_complement 1' // Failure : miss content for data_comp 2
                    ],
                ]
            ],
            [
                [
                    'description'             => 'The description of product',
                    'price'                   => 25.0,
                    'option_required'         => FALSE,
                    'title_data_complement'   => [
                        'data_complement 1',
                        'data_complement 2'
                    ],
                    'content_data_complement' => [
                        'content_data_complement 1', // Failure : miss content for data_comp 2
                        ''
                    ],
                ]
            ],
            [
                [
                    'price'                 => 25.0,
                    'name_data_customer'    => [
                        'name_data_customer 1',
                        'name_data_customer 2'
                    ],
                    'require_data_customer' => [
                        'require_data_customer 1' // Failure : miss require information for n_d_c 2
                    ],
                ]
            ],
            [
                [
                    'price'                 => 25.0,
                    'name_data_customer'    => [
                        'name_data_customer 1',
                        'name_data_customer 2'
                    ],
                    'require_data_customer' => [
                        'require_data_customer 1', // Failure : miss require information for n_d_c 2
                        ''
                    ],
                ]
            ],
            [
                [
                    'price'        => 25.0,
                    'option_name'  => [
                        'option 1'
                    ],
                    'option_stock' => [
                        2 // failure
                    ],
                ]
            ],
            [
                [
                    'price'                  => 25.0,
                    'detach_data_complement' => [
                        2
                    ],
                ]
            ],
            [
                [
                    'price'                => 25.0,
                    'detach_data_customer' => [
                        2
                    ],
                ]
            ],
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'description'             => '',
                    'price'                   => 25,
                    'option_required'         => FALSE,
                    'title_data_complement'   => [
                        'data_complement 1',
                        'data_complement 2'
                    ],
                    'content_data_complement' => [
                        'content_data_complement 1',
                        'content_data_complement 2'
                    ],
                    'name_data_customer'      => [
                        'name_data_customer 1',
                        'name_data_customer 2'
                    ],
                    'require_data_customer'   => [
                        'require_data_customer 1',
                        'require_data_customer 2'
                    ],
                    'option_name'             => [
                        'option 1'
                    ],
                    'option_stock'            => [
                        1
                    ],
                    'detach_data_complement'  => [
                        1
                    ],
                    'detach_data_customer'    => [
                        1
                    ],
                ]
            ],
        ];
    }
}
