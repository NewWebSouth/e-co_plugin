<?php


namespace Tests\Administer\Manage\Product;


use App\Entities\Product;
use App\Entities\Stock;
use App\Packages\Administer\Manage\Product\SCreate;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\StockRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager   = $this->createMock( EntityManagerInterface::class );
        $stockRepository = $this->createMock( StockRepository::class );
        $frontObserver   = $this->createMock( FrontObserverInterface::class );
        $stockRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? new Stock() : NULL;
        } );
        
        
        $service = new SCreate(
            $entityManager,
            new \App\Packages\Administer\Manage\Product\DataCustomer\SCreate( $entityManager ),
            new \App\Packages\Administer\Manage\Product\Option\SCreate( $entityManager, $stockRepository ),
            new \App\Packages\Administer\Manage\Product\DataComplement\SCreate( $entityManager ),
            $stockRepository
        );
        
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Product() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Product() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'name'            => 'Name of product',
                    'description'     => 'The description of product',
                    'price'           => 0, // failure
                    'option_required' => FALSE
                ]
            ],
            [
                [
                    'name'            => '', //Failure
                    'description'     => 'The description of product',
                    'price'           => 25.0,
                    'option_required' => FALSE,
                ]
            ],
            [
                [
                    'name'                    => 'Name of product',
                    'description'             => 'The description of product',
                    'price'                   => 25.0,
                    'option_required'         => FALSE,
                    'title_data_complement'   => [
                        'data_complement 1',
                        'data_complement 2'
                    ],
                    'content_data_complement' => [
                        'content_data_complement 1' // Failure : miss content for data_comp 2
                    ],
                ]
            ],
            [
                [
                    'name'                    => 'Name of product',
                    'description'             => 'The description of product',
                    'price'                   => 25.0,
                    'option_required'         => FALSE,
                    'title_data_complement'   => [
                        'data_complement 1',
                        'data_complement 2'
                    ],
                    'content_data_complement' => [
                        'content_data_complement 1', // Failure : miss content for data_comp 2
                        ''
                    ],
                ]
            ],
            [
                [
                    'name'                  => 'Name of product',
                    'price'                 => 25.0,
                    'name_data_customer'    => [
                        'name_data_customer 1',
                        'name_data_customer 2'
                    ],
                    'require_data_customer' => [
                        'require_data_customer 1' // Failure : miss require information for n_d_c 2
                    ],
                ]
            ],
            [
                [
                    'name'                  => 'Name of product',
                    'price'                 => 25.0,
                    'name_data_customer'    => [
                        'name_data_customer 1',
                        'name_data_customer 2'
                    ],
                    'require_data_customer' => [
                        'require_data_customer 1', // Failure : miss require information for n_d_c 2
                        ''
                    ],
                ]
            ],
            [
                [
                    'name'         => 'Name of product',
                    'price'        => 25.0,
                    'option_name'  => [
                        'option 1'
                    ],
                    'option_stock' => [
                        2
                    ],
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'name'                    => 'Name',
                    'description'             => '',
                    'price'                   => 25,
                    'option_required'         => FALSE,
                    'title_data_complement'   => [
                        'data_complement 1',
                        'data_complement 2'
                    ],
                    'content_data_complement' => [
                        'content_data_complement 1',
                        'content_data_complement 2'
                    ],
                    'name_data_customer'      => [
                        'name_data_customer 1',
                        'name_data_customer 2'
                    ],
                    'require_data_customer'   => [
                        'require_data_customer 1',
                        'require_data_customer 2'
                    ],
                    'option_name'             => [
                        'option 1'
                    ],
                    'option_stock'            => [
                        1
                    ],
                ]
            ],
        ];
    }
}
