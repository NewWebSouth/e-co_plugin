<?php


namespace Tests\Administer\Manage\GraphicRule\Reduction;


use App\Entities\Customer;
use App\Entities\GraphicRuleReduction;
use App\Entities\Reduction;
use App\Entities\Section;
use App\Packages\Administer\Manage\GraphicRule\Reduction\SCreate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\ReductionRepository;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager       = $this->createMock( EntityManagerInterface::class );
        $uploadImage         = $this->createMock( UploadImageInterface::class );
        $frontObserver       = $this->createMock( FrontObserverInterface::class );
        $reductionRepository = $this->createMock( ReductionRepository::class );
        $sectionRepository   = $this->createMock( SectionRepository::class );
        
        $reductionRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            
            if( $id !== 0 ) {
                // insert id
                $reduction          = new Reduction();
                $reflectionProperty = new \ReflectionProperty( Reduction::class, 'id' );
                
                $reflectionProperty->setAccessible( TRUE );
                $reflectionProperty->setValue( $reduction, $id );
                
                return $reduction;
            }
            
            if( $id === 15 ) {
                return ( new Reduction() )->setCustomer( new Customer() );
            }
            
            return NULL;
        } );
        
        $sectionRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            return $id === 0 ? NULL : new Section();
        } );
        
        $service = new SCreate(
            $entityManager,
            $uploadImage,
            $sectionRepository,
            $reductionRepository
        );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @dataProvider providerCreate_isFalse
     * @test
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new GraphicRuleReduction() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @dataProvider providerCreate_isTrue
     * @test
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new GraphicRuleReduction() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'id'          => 0, // failure
                    'title'       => 'Titre de la reduction',
                    'description' => 'Description de la reduction',
                    'sections'    => [ 1 ]
                ]
            ],
            [
                [
                    'id'          => 1,
                    'title'       => 'Titre de la reduction',
                    'description' => 'Description de la reduction',
                    'sections'    => NULL // failure
                ]
            ],
            [
                [
                    'id'          => 1,
                    'title'       => '',
                    'description' => 'Description de la reduction',
                    'sections'    => [ 1 ]
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'id'          => 1,
                    'title'       => 'Titre de la reduction',
                    'description' => 'Description de la reduction',
                    'sections'    => [ 1 ]
                ]
            ],
            [
                [
                    'id'          => 1,
                    'title'       => 'Titre de la reduction',
                    'description' => '',
                    'sections'    => [ 1 ]
                ]
            ]
        ];
    }
}
