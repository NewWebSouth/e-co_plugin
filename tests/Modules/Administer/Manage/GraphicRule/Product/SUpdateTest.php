<?php


namespace Tests\Administer\Manage\GraphicRule\Product;


use App\Entities\GraphicRuleProduct;
use App\Entities\Product;
use App\Entities\Section;
use App\Packages\Administer\Manage\GraphicRule\Product\SUpdate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\ImageRepository;
use App\Repositories\ProductRepository;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SUpdateTest extends UnitTest
{
    
    private function getService(): SUpdate
    {
        $entityManager     = $this->createMock( EntityManagerInterface::class );
        $uploadImage       = $this->createMock( UploadImageInterface::class );
        $frontObserver     = $this->createMock( FrontObserverInterface::class );
        $productRepository = $this->createMock( ProductRepository::class );
        $sectionRepository = $this->createMock( SectionRepository::class );
        $imageRepository   = $this->createMock( ImageRepository::class );
        
        $productRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            
            if( $id !== 0 ) {
                // insert id
                $product            = new Product();
                $reflectionProperty = new \ReflectionProperty( Product::class, 'id' );
                
                $reflectionProperty->setAccessible( TRUE );
                $reflectionProperty->setValue( $product, $id );
                
                return $product->setName( 'Name of product' );
            }
            
            return NULL;
        } );
        $sectionRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            return $id !== 1 ? NULL : new Section();
        } );
        
        $service = new SUpdate(
            $entityManager,
            $uploadImage,
            $sectionRepository,
            $imageRepository,
            $productRepository,
        );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @dataProvider providerUpdate_isFalse
     * @test
     * @param array $parameters
     * @return void
     */
    public function update_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), ( new GraphicRuleProduct() )->setProduct( $this->generateProduct( 1 ) ) );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @dataProvider providerUpdate_isTrue
     * @test
     * @param array $parameters
     * @return void
     */
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), ( new GraphicRuleProduct() )->setProduct( $this->generateProduct( 1 ) ) );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'sections'    => [ 10 ], // failure
                    'description' => 'La description du produit',
                    'priority'    => 10,
                    'recommended' => NULL
                ]
            ],
            [
                [
                    'sections'    => [ 1, 0 ], // failure
                    'description' => 'La description du produit',
                    'priority'    => 10,
                    'recommended' => NULL
                ]
            ],
            [
                [
                    'sections'    => [ 1 ],
                    'description' => 'La description du produit',
                    'priority'    => 10,
                    'recommended' => [ 1, 0 ] // failure
                ]
            ],
            [
                [
                    'sections'    => [ 1 ],
                    'description' => 'La description du produit',
                    'priority'    => 10,
                    'recommended' => [ 2, 1 ] // failure
                ]
            ],
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'sections'    => [ 1 ],
                    'description' => '',
                    'priority'    => 10,
                    'recommended' => [ 2 ]
                ]
            ],
        ];
    }
}
