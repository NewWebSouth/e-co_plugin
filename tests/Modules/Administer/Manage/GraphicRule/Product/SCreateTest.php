<?php


namespace Tests\Administer\Manage\GraphicRule\Product;


use App\Entities\GraphicRuleProduct;
use App\Entities\Product;
use App\Entities\Section;
use App\Packages\Administer\Manage\GraphicRule\Product\SCreate;
use App\Packages\Administer\Tools\System\Image\UploadImageInterface;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\ProductRepository;
use App\Repositories\SectionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager     = $this->createMock( EntityManagerInterface::class );
        $uploadImage       = $this->createMock( UploadImageInterface::class );
        $frontObserver     = $this->createMock( FrontObserverInterface::class );
        $productRepository = $this->createMock( ProductRepository::class );
        $sectionRepository = $this->createMock( SectionRepository::class );
        
        $productRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            
            if( $id !== 0 ) {
                // insert id
                $product            = new Product();
                $reflectionProperty = new \ReflectionProperty( Product::class, 'id' );
                
                $reflectionProperty->setAccessible( TRUE );
                $reflectionProperty->setValue( $product, $id );
                
                return $product->setName( 'Name of product' );
            }
            
            return NULL;
        } );
        $sectionRepository->method( 'findById' )->willReturnCallback( function ( $id ) {
            return $id === 0 ? NULL : new Section();
        } );
        
        $service = new SCreate(
            $entityManager,
            $uploadImage,
            $productRepository,
            $sectionRepository
        );
        $service->attachFront( $frontObserver );
        
        return $service;
    }
    
    
    /**
     * @dataProvider providerCreate_isFalse
     * @test
     * @param array $parameters
     * @return void
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new GraphicRuleProduct() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @dataProvider providerCreate_isTrue
     * @test
     * @param array $parameters
     * @return void
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new GraphicRuleProduct() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'sections'    => [ 0 ], // failure
                    'description' => 'La description du produit',
                    'id'          => 1, // Id of product
                    'priority'    => 10,
                    'recommended' => NULL
                ]
            ],
            [
                [
                    'sections'    => [ 1, 0 ], // failure
                    'description' => 'La description du produit',
                    'id'          => 1, // Id of product
                    'priority'    => 10,
                    'recommended' => NULL
                ]
            ],
            [
                [
                    'sections'    => [ 1 ],
                    'description' => 'La description du produit',
                    'id'          => 0, // failure
                    'priority'    => 10,
                    'recommended' => NULL
                ]
            ],
            [
                [
                    'sections'    => [ 1 ],
                    'description' => 'La description du produit',
                    'id'          => 1,
                    'priority'    => 10,
                    'recommended' => [ 1, 0 ] // failure
                ]
            ],
            [
                [
                    'sections'    => [ 1 ],
                    'description' => 'La description du produit',
                    'id'          => 1,
                    'priority'    => 10,
                    'recommended' => [ 2, 1 ] // failure
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'sections'    => [ 1 ],
                    'description' => '',
                    'id'          => 1, // Id of product
                    'priority'    => 10,
                    'recommended' => [ 2 ]
                ]
            ],
        ];
    }
}
