<?php


namespace Tests\Administer\Manage\Reduction;


use App\Entities\Reduction;
use App\Packages\Administer\Manage\Reduction\SCreate;
use App\Packages\Website\Cache\FrontObserverInterface;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private function getService(): SCreate
    {
        $entityManager      = $this->createMock( EntityManagerInterface::class );
        $frontObserr        = $this->createMock( FrontObserverInterface::class );
        $productRepository  = $this->createMock( ProductRepository::class );
        $customerRepository = $this->createMock( CustomerRepository::class );
        
        $customerRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->getGenericCustomer() : NULL;
        } );
        
        $productRepository->method( 'findById' )->willReturnCallback( function ( int $id ) {
            return $id === 1 ? $this->generateProduct( 1 ) : NULL;
        } );
        
        $service = new SCreate( $entityManager, $productRepository, $customerRepository );
        $service->attachFront( $frontObserr );
        
        return $service;
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Reduction() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Reduction() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'code'          => NULL,
                    'value'         => -10, // Failure
                    'product'       => NULL,
                    'id_customer'   => NULL,
                    'rule_init'     => NULL,
                    'rule_product'  => NULL,
                    'rule_operator' => NULL,
                    'rule_value'    => NULL,
                ]
            ],
            [
                [
                    'value' => 150, // Failure
                ]
            ],
            [
                [
                    'value' => 0, // Failure
                ]
            ],
            [
                [
                    'value'       => 5.0,
                    'id_customer' => 2, //Failure
                ]
            ],
            [
                [
                    'value'   => 5.0,
                    'product' => 2, //Failure
                ]
            ],
            [
                [
                    'value'         => 5.0,
                    'rule_init'     => 'A', // Failure
                    'rule_product'  => 'B', // Failure
                    'rule_operator' => 'C', // Failure
                    'rule_value'    => 'D', // Failure
                ]
            ],
            [
                [
                    'value'         => 5.0,
                    'product'       => 1,
                    'rule_init'     => 'PQ',
                    'rule_product'  => 2, // Failure
                    'rule_operator' => '>',
                    'rule_value'    => 4,
                ]
            ],
            [
                [
                    'value'         => 5.0,
                    'product'       => 1,
                    'rule_init'     => 'PQ',
                    'rule_operator' => '>',
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'code'          => NULL,
                    'value'         => 5.0,
                    'product'       => 1,
                    'id_customer'   => NULL,
                    'rule_init'     => NULL,
                    'rule_product'  => NULL,
                    'rule_operator' => NULL,
                    'rule_value'    => NULL,
                ]
            ],
            [
                [
                    'value'       => 5.0,
                    'id_customer' => 1,
                ]
            ],
            [
                [
                    'code'        => 'ABCD',
                    'value'       => 5.0,
                    'product'     => 1,
                    'id_customer' => 1,
                ]
            ],
            [
                [
                    'value'         => 5.0,
                    'product'       => 1,
                    'rule_init'     => 'PT',
                    'rule_product'  => 1,
                    'rule_operator' => '>',
                    'rule_value'    => 150,
                ]
            ],
            [
                [
                    'value'         => 5.0,
                    'product'       => 1,
                    'rule_init'     => 'FT',
                    'rule_operator' => '>',
                    'rule_value'    => 150,
                ]
            ],
        ];
    }
}
