<?php


namespace Tests\Cunsumption\Ordered;


use App\Entities\Ordered;
use App\Packages\Cunsumption\Basket\RevalideBasketInterface;
use App\Packages\Cunsumption\Ordered\SCreate;
use App\Packages\Cunsumption\ReductionCustomer\AttachReductionInterface;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Components\EntityManager\TransactionSubjectInterface;
use Nomess\Components\LightPersists\LightPersistsInterface;
use Nomess\Http\HttpRequest;
use Nomess\Http\HttpSession;
use Tests\UnitTest;

class SCreateTest extends UnitTest
{
    
    private const WITH_CUSTOMER    = FALSE;
    private const WITH_BASKET      = FALSE;
    private const WITHOUT_CUSTOMER = TRUE;
    private const WITHOUT_BASKET   = TRUE;
    
    
    private function getService( bool $withoutBasket = FALSE, bool $withoutCustomer = FALSE ): SCreate
    {
        $entityManager      = $this->createMock( EntityManagerInterface::class );
        $lightPersists      = $this->createMock( LightPersistsInterface::class );
        $revalideBasket     = $this->createMock( RevalideBasketInterface::class );
        $session            = $this->createMock( HttpSession::class );
        $attachReduction    = $this->createMock( AttachReductionInterface::class );
        $transactionSubject = $this->createMock( TransactionSubjectInterface::class );
        
        if( !$withoutBasket ) {
            $lightPersists->method( 'get' )->willReturn(
                [
                    1 => $this->generateProductCustomer( 1 )->setDataCustomer( 'Les TU c\'est cool' ),
                    2 => $this->generateProductCustomer( 2 ),
                    3 => $this->generateProductCustomer( 3 )
                ]
            );
        }
        
        if( !$withoutCustomer ) {
            $session->method( 'get' )->willReturn( $this->getGenericCustomer() );
        }
        
        return new SCreate(
            $entityManager,
            $lightPersists,
            $session,
            $revalideBasket,
            $attachReduction,
            $transactionSubject
        );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     * @param bool $withoutBasket
     * @param bool $withoutCustomer
     */
    public function create_isFalse( array $parameters, bool $withoutBasket = FALSE, bool $withoutCustomer = FALSE ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService( $withoutBasket, $withoutCustomer )->create( new HttpRequest(), new Ordered() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->create( new HttpRequest(), new Ordered() );
        
        $this->assertTrue( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isValidTotal
     * @param array $parameters
     */
    public function create_isValidTotal( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService()->create( new HttpRequest(), $ordered = new Ordered() );
        
        // With total
        $this->assertEquals(
            $this->generateProductCustomer( 1 )->getTotal() + $this->generateProductCustomer( 2 )->getTotal() + $this->generateProductCustomer( 3 )->getTotal(),
            $ordered->getTotal()
        );
        
        // With net to pay
        $this->assertEquals(
            $this->generateProductCustomer( 1 )->getNetToPay() + $this->generateProductCustomer( 2 )->getNetToPay() + $this->generateProductCustomer( 3 )->getNetToPay(),
            $ordered->getNetToPay()
        );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'peyrolles en provence',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'france',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'peyrolles en provence',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'france',
                    'comment'                        => 'Facking blue boy',
                ],
                self::WITH_BASKET,
                self::WITHOUT_CUSTOMER // Failure
            ],
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'peyrolles en provence',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'france',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'peyrolles en provence',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'france',
                    'comment'                        => 'Facking blue boy',
                ],
                self::WITHOUT_BASKET, // Failure
                self::WITH_CUSTOMER
            ],
            [
                [
                    'address_billing_number_street'  => '', // Failure
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'peyrolles en provence',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'france',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'peyrolles en provence',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'france',
                    'comment'                        => 'Facking blue boy',
                ],
                self::WITH_BASKET,
                self::WITH_CUSTOMER
            ],
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'peyrolles en provence',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'france',
                    'address_delivery_number_street' => '', //Failure
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'peyrolles en provence',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'france',
                    'comment'                        => 'Facking blue boy',
                ],
                self::WITH_BASKET,
                self::WITH_CUSTOMER
            ]
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'peyrolles en provence',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'france',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'peyrolles en provence',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'france',
                    'comment'                        => 'Facking blue boy',
                ],
                self::WITH_BASKET,
                self::WITH_CUSTOMER
            ],
        ];
    }
    
    
    public function providerCreate_isValidTotal(): array
    {
        return [
            [
                [
                    'address_billing_number_street'  => '20',
                    'address_billing_street'         => 'rue de la croix blanche',
                    'address_billing_city'           => 'peyrolles en provence',
                    'address_billing_postal_code'    => '13 860',
                    'address_billing_country'        => 'france',
                    'address_delivery_number_street' => '20',
                    'address_delivery_street'        => 'rue de la croix blanche',
                    'address_delivery_city'          => 'peyrolles en provence',
                    'address_delivery_postal_code'   => '13 860',
                    'address_delivery_country'       => 'france',
                    'comment'                        => 'Facking blue boy',
                ],
                self::WITH_BASKET,
                self::WITH_CUSTOMER
            ],
        ];
    }
}
