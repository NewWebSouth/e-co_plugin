<?php


namespace Tests\Cunsumption\Account;


use App\Packages\Cunsumption\Account\SResetPassword;
use App\Repositories\CustomerRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SResetPasswordTest extends UnitTest
{
    
    private function getService(): SResetPassword
    {
        $entityManager      = $this->createMock( EntityManagerInterface::class );
        $customerRepository = $this->createMock( CustomerRepository::class );
        $customerRepository->method( 'findByEmail' )->willReturnCallback( function ( string $email ) {
            if( $email === 'romainlavabre98@gmail.com' ) {
                return $this->getGenericCustomer()->setKey( 'ABCD' );
            }
            
            return NULL;
        } );
        
        return new SResetPassword( $entityManager );
    }
    
    
    /**
     * @test
     * @dataProvider provideService_isFalse
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function service_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->service( new HttpRequest(), $this->getGenericCustomer()->setKey( 'ABCD' ) );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerService_isTrue
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function service_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->service( new HttpRequest(), $this->getGenericCustomer()->setKey( 'ABCD' ) );
        
        $this->assertTrue( $state );
    }
    
    
    public function provideService_isFalse(): array
    {
        return [
            [
                [
                    'key'           => 'EFGH', // Failure
                    'password'      => '000000',
                    'conf_password' => '000000',
                ]
            ],
            [
                [
                    'key'           => 'ABCD',
                    'password'      => '111111',
                    'conf_password' => '000000', // Failure
                ]
            ],
        
        ];
    }
    
    
    public function providerService_isTrue(): array
    {
        return [
            [
                [
                    'key'           => 'ABCD', // Failure
                    'password'      => '000000',
                    'conf_password' => '000000',
                ]
            ],
        ];
    }
}
