<?php


namespace Tests\Cunsumption\Account;


use App\Entities\Customer;
use App\Packages\Cunsumption\Account\SUpdate;
use App\Repositories\CustomerRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Exception\InvalidParamException;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SUpdateTest extends UnitTest
{
    
    private function getService(): SUpdate
    {
        $entityManager      = $this->createMock( EntityManagerInterface::class );
        $customerRepository = $this->createMock( CustomerRepository::class );
        $customerRepository->method( 'findByEmail' )->willReturnCallback( function ( string $email ) {
            if( $email === 'already_exists@gmail.com' ) {
                return new Customer();
            }
            
            return NULL;
        } );
        
        return new SUpdate( $entityManager, $customerRepository );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isFalse
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function update_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), ( new Customer() )->setEmail( 'romainlavabre98@gmail.com' ) );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerUpdate_isTrue
     * @param array $parameters
     * @throws InvalidParamException
     */
    public function update_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->update( new HttpRequest(), ( new Customer() )->setEmail( 'romainlavabre98@gmail.com' ) );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerUpdate_isFalse(): array
    {
        return [
            [
                [
                    'first_name'    => NULL, //Failure
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => '', // Failure
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => NULL, // Failure
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => NULL,
                    'number_street' => NULL, // Failure
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => '06.54', // Failure
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => '0641825412', // Failure
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => NULL,
                    'number_street' => NULL,// failure
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@', // Failure
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => NULL, // Failure
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'already_exists@gmail.com', // Failure
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
        ];
    }
    
    
    public function providerUpdate_isTrue(): array
    {
        return [
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'phone'         => '06.41.82.54.12.',
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
        ];
    }
}
