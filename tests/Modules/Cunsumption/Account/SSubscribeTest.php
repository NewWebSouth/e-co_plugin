<?php


namespace Tests\Cunsumption\Account;


use App\Entities\Customer;
use App\Packages\Cunsumption\Account\SSubscribe;
use App\Repositories\CustomerRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;

class SSubscribeTest extends \Tests\UnitTest
{
    
    private function getService(): SSubscribe
    {
        $entityManager      = $this->createMock( EntityManagerInterface::class );
        $customerRepository = $this->createMock( CustomerRepository::class );
        $customerRepository->method( 'findByEmail' )->willReturnCallback( function ( string $email ) {
            if( $email === 'already_exists@gmail.com' ) {
                return new Customer();
            }
            
            return NULL;
        } );
        
        return new SSubscribe( $entityManager, $customerRepository );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isFalse
     * @param array $parameters
     */
    public function create_isFalse( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->subscribe( new HttpRequest(), new Customer() );
        
        $this->assertFalse( $state );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_isTrue
     * @param array $parameters
     */
    public function create_isTrue( array $parameters ): void
    {
        $this->hydratePost( $parameters );
        
        $state = $this->getService()->subscribe( new HttpRequest(), new Customer() );
        
        $this->assertTrue( $state );
    }
    
    
    public function providerCreate_isFalse(): array
    {
        return [
            [
                [
                    'first_name'    => NULL, //Failure
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => '', // Failure
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => NULL, // Failure
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '11111111', // Failure
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => NULL, // Failure
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => '06.54', // Failure
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => '0641825412', // Failure
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => NULL,// failure
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@', // Failure
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => NULL, // Failure
                ]
            ],
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'already_exists@gmail.com', // Failure
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => NULL,
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
        ];
    }
    
    
    public function providerCreate_isTrue(): array
    {
        return [
            [
                [
                    'first_name'    => 'romain',
                    'last_name'     => 'lavabre',
                    'email'         => 'romainlavabre98@gmail.com',
                    'password'      => '000000',
                    'conf_password' => '000000',
                    'phone'         => '06.41.82.54.12.',
                    'number_street' => '20',
                    'street'        => 'rue de la croix blanche',
                    'postal_code'   => '13 820',
                    'city'          => 'peyrolles en pce',
                    'country'       => 'france',
                ]
            ],
        ];
    }
}
