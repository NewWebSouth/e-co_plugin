<?php


namespace Tests\Cunsumption\ProductCustomer;


use App\Entities\DataCustomer;
use App\Entities\Option;
use App\Entities\ProductCustomer;
use App\Packages\Cunsumption\ProductCustomer\SAttach;
use App\Repositories\ProductRepository;
use Nomess\Exception\InvalidParamException;
use Nomess\Exception\NullPointerException;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SAttachTest extends UnitTest
{
    
    private function getService( bool $withOption = FALSE, bool $withDataCustomer = FALSE ): SAttach
    {
        $productRepository = $this->createMock( ProductRepository::class );
        $productRepository->method( 'findById' )->willReturnCallback( function ( int $id ) use ( $withOption, $withDataCustomer ) {
            if( $id === 1 ) {
                $product = $this->generateProduct( 1 );
                
                if( $withOption ) {
                    $product->addOption( $option = ( new Option() )->setName( 'option' ) );
                    $product->setOptionRequired( TRUE );
                    $this->setId( $option, 1 );
                }
                
                if( $withDataCustomer ) {
                    $product->addDataCustomer( $dataCustomer = ( new DataCustomer() )->setName( 'data_customer1' )
                                                                                     ->setRequired( TRUE )
                    );
                    $this->setId( $dataCustomer, 1 );
                }
                
                return $product;
            }
            
            return NULL;
        } );
        
        return new SAttach(
            $productRepository
        );
    }
    
    
    /**
     * @test
     * @throws InvalidParamException
     * @throws NullPointerException
     */
    public function create_throwNullPointerException(): void
    {
        $this->hydratePost( [
                                'product_id' => 2, // Failure
                                'option'     => NULL,
                                'quantity'   => 10,
                            ] );
        
        $this->expectException( NullPointerException::class );
        
        $this->getService()->attach( new HttpRequest() );
    }
    
    
    /**
     * @test
     * @dataProvider providerCreate_throwInvalidParameterException
     * @param array $parameters
     * @param bool $withOption
     * @param bool $withDataCustomer
     * @throws InvalidParamException
     * @throws NullPointerException
     */
    public function create_throwInvalidParameterException( array $parameters, bool $withOption = FALSE, bool $withDataCustomer = FALSE ): void
    {
        $this->hydratePost( $parameters );
        
        $this->expectException( InvalidParamException::class );
        
        $this->getService( $withOption, $withDataCustomer )->attach( new HttpRequest() );
    }
    
    
    /**
     * @test
     * @throws InvalidParamException
     * @throws NullPointerException
     */
    public function create_returnInstanceOfProductCustomer(): void
    {
        $this->hydratePost( [
                                'product_id'      => 1,
                                'option'          => 1,
                                'quantity'        => 10,
                                'data_customer_1' => 'Une donnée client'
                            ] );
        
        $result = $this->getService( TRUE, TRUE )->attach( new HttpRequest() );
        
        $this->assertInstanceOf( ProductCustomer::class, $result );
    }
    
    
    public function providerCreate_throwInvalidParameterException(): array
    {
        return [
            [
                [
                    'product_id' => 1,
                    'option'     => 2, // Failure
                    'quantity'   => 10,
                ],
                TRUE
            ],
            [
                [
                    'product_id' => 1, // Failure
                    'option'     => NULL,
                    'quantity'   => 10,
                ],
                FALSE,
                TRUE
            ],
        ];
    }
}
