<?php


namespace Tests\Cunsumption\ReductionCustomer;


use App\Entities\Customer;
use App\Entities\Ordered;
use App\Entities\ProductCustomer;
use App\Entities\Reduction;
use App\Packages\Cunsumption\ReductionCustomer\SAttach;
use App\Repositories\ReductionRepository;
use Nomess\Components\EntityManager\EntityManagerInterface;
use Nomess\Http\HttpRequest;
use Tests\UnitTest;

class SAttachTest extends UnitTest
{
    
    private function getService( Reduction $reduction = NULL ): SAttach
    {
        $entityManager       = $this->createMock( EntityManagerInterface::class );
        $reductionRepository = $this->createMock( ReductionRepository::class );
        $reductionRepository->method( 'findAllActive' )->willReturnCallback( function () use ( $reduction ) {
            
            return !empty( $reduction ) ? [ $reduction ] : NULL;
        } );
        
        return new SAttach(
            $entityManager,
            $reductionRepository
        );
    }
    
    
    /**
     * @test
     * @dataProvider providerAttachForProduct_hasReduction
     * @param array $parameters
     * @param ProductCustomer $productCustomer
     * @param Ordered $ordered
     * @param Reduction $reduction
     */
    public function attachForProduct_hasReduction( array $parameters, ProductCustomer $productCustomer, Ordered $ordered, Reduction $reduction ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( $reduction )->attachForProduct( new HttpRequest(), $productCustomer, $ordered );
        
        $this->assertNotEmpty( $productCustomer->getReductionCustomers() );
    }
    
    
    /**
     * @test
     * @dataProvider providerAttachForProduct_hasNotReduction
     * @param array $parameters
     * @param ProductCustomer $productCustomer
     * @param Ordered $ordered
     * @param Reduction $reduction
     */
    public function attachForProduct_hasNotReduction( array $parameters, ProductCustomer $productCustomer, Ordered $ordered, Reduction $reduction ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( $reduction )->attachForProduct( new HttpRequest(), $productCustomer, $ordered );
        
        $this->assertEmpty( $ordered->getReductionCustomers() );
    }
    
    
    /**
     * @test
     * @dataProvider providerAttachForOrdered_hasReduction
     * @param array $parameters
     * @param Ordered $ordered
     * @param Reduction $reduction
     */
    public function attachForOrdered_hasReduction( array $parameters, Ordered $ordered, Reduction $reduction ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( $reduction )->attachForOrdered( new HttpRequest(), $ordered );
        
        $this->assertNotEmpty( $ordered->getReductionCustomers() );
    }
    
    
    /**
     * @test
     * @dataProvider providerAttachForOrdered_hasNotReduction
     * @param array $parameters
     * @param Ordered $ordered
     * @param Reduction $reduction
     */
    public function attachForOrdered_hasNotReduction( array $parameters, Ordered $ordered, Reduction $reduction ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( $reduction )->attachForOrdered( new HttpRequest(), $ordered );
        
        $this->assertEmpty( $ordered->getReductionCustomers() );
    }
    
    
    /**
     * @test
     * @dataProvider providerAttachForCustomer_hasReduction
     * @param array $parameters
     * @param Customer $customer
     * @param Ordered $ordered
     * @param Reduction $reduction
     */
    public function attachForCustomer_hasReduction( array $parameters, Customer $customer, Ordered $ordered, Reduction $reduction ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( $reduction )->attachForCustomer( new HttpRequest(), $customer, $ordered );
        
        $this->assertNotEmpty( $ordered->getReductionCustomers() );
    }
    
    
    /**
     * @test
     * @dataProvider providerAttachForCustomer_hasNotReduction
     * @param array $parameters
     * @param Customer $customer
     * @param Ordered $ordered
     * @param Reduction $reduction
     */
    public function attachForCustomer_hasNotReduction( array $parameters, Customer $customer, Ordered $ordered, Reduction $reduction ): void
    {
        $this->hydratePost( $parameters );
        
        $this->getService( $reduction )->attachForCustomer( new HttpRequest(), $customer, $ordered );
        
        $this->assertEmpty( $ordered->getReductionCustomers() );
    }
    
    
    public function providerAttachForProduct_hasReduction(): array
    {
        return [
            [
                [],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( NULL )
                                   ->setProduct( $this->generateProduct( 1 ) )
            ],
            [
                [
                    'reduction_code' => [
                        'ABCD'  // Valid the control of code
                    ]
                ],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( 'ABCD' )
                                   ->setProduct( $this->generateProduct( 1 ) )
            ],
            [
                [],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                ( new Ordered() )->addProductCustomer( $this->generateProductCustomer( 1 )->setQuantity( 3 ) ),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PQ', 1, '>', 2 ] )
                                   ->setCode( NULL )
                                   ->setProduct( $this->generateProduct( 1 ) )
            ],
        ];
    }
    
    
    public function providerAttachForProduct_hasNotReduction(): array
    {
        return [
            [
                [],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setCustomer( $this->getGenericCustomer() ) // Failure: customer is owner
                                   ->setRule( NULL )
                                   ->setCode( NULL )
                                   ->setProduct( $this->generateProduct( 1 ) )
            ],
            [
                [
                    'reduction_code' => [
                        'ABCD'
                    ]
                ],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( 'EFGH' ) // Failure: invalid code of reduction
                                   ->setProduct( $this->generateProduct( 1 ) )
            ],
            [
                [],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                ( new Ordered() )->addProductCustomer( $this->generateProductCustomer( 1 )->setQuantity( 1 ) ), // Failure: low quantity
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PQ', 1, '>', 2 ] )
                                   ->setCode( NULL )
                                   ->setProduct( $this->generateProduct( 1 ) )
            ],
            [
                [],
                $this->generateProductCustomer( 1 )
                     ->setProduct( $this->generateProduct( 1 ) ),
                ( new Ordered() )->addProductCustomer(
                    $this->generateProductCustomer( 1 )
                         ->setQuantity( 1 )
                         ->setProduct(
                             $this->generateProduct( 1 )
                                  ->setPrice( 15 )
                         )
                         ->setTotal()
                ), // Failure: low total
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PT', 1, '>', 16 ] )
                                   ->setCode( NULL )
            ],
        ];
    }
    
    
    public function providerAttachForOrdered_hasReduction(): array
    {
        return [
            [
                [],
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( NULL )
            ],
            [
                [
                    'reduction_code' => [
                        'ABCD'  // Valid the control of code
                    ]
                ],
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( 'ABCD' )
            ],
            [
                [],
                ( new Ordered() )->addProductCustomer( $this->generateProductCustomer( 1 )->setQuantity( 3 ) ),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PQ', 1, '>', 2 ] )
                                   ->setCode( NULL )
            ],
        ];
    }
    
    
    public function providerAttachForOrdered_hasNotReduction(): array
    {
        return [
            [ // Failure: Customer is not owner
              [],
              new Ordered(),
              ( new Reduction() )->setActive( TRUE )
                                 ->setValue( 5 )
                                 ->setCustomer( $this->getGenericCustomer() ) // Failure: customer is owner
                                 ->setRule( NULL )
                                 ->setCode( NULL )
            ],
            [
                [
                    'reduction_code' => [
                        'ABCD'
                    ]
                ],
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( 'EFGH' ) // Failure: invalid code of reduction
            ],
            [
                [],
                ( new Ordered() )->addProductCustomer( $this->generateProductCustomer( 1 )->setQuantity( 1 ) ), // Failure: low quantity
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PQ', 1, '>', 2 ] )
                                   ->setCode( NULL )
            ],
            [
                [],
                ( new Ordered() )->addProductCustomer(
                    $this->generateProductCustomer( 1 )
                         ->setQuantity( 1 )
                         ->setProduct(
                             $this->generateProduct( 1 )
                                  ->setPrice( 15 )
                         )
                         ->setTotal()
                ), // Failure: low total
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PT', 1, '>', 16 ] )
                                   ->setCode( NULL )
            ],
        ];
    }
    
    
    public function providerAttachForCustomer_hasReduction(): array
    {
        return [
            [
                [],
                $this->getGenericCustomer(),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( NULL )
            ],
            [
                [
                    'reduction_code' => [
                        'ABCD'  // Valid the control of code
                    ]
                ],
                $this->getGenericCustomer(),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( 'ABCD' )
            ],
            [
                [],
                $this->getGenericCustomer(),
                ( new Ordered() )->addProductCustomer( $this->generateProductCustomer( 1 )->setQuantity( 3 ) ),
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PQ', 1, '>', 2 ] )
                                   ->setCode( NULL )
            ],
        ];
    }
    
    
    public function providerAttachForCustomer_hasNotReduction(): array
    {
        return [
            [ // Failure: Customer is not owner
              [],
              $this->getGenericCustomer(),
              new Ordered(),
              ( new Reduction() )->setActive( TRUE )
                                 ->setValue( 5 )
                                 ->setRule( NULL )
                                 ->setCode( NULL )
            ],
            [
                [
                    'reduction_code' => [
                        'ABCD'
                    ]
                ],
                $this->getGenericCustomer(),
                new Ordered(),
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( NULL )
                                   ->setCode( 'EFGH' ) // Failure: invalid code of reduction
            ],
            [
                [],
                $this->getGenericCustomer(),
                ( new Ordered() )->addProductCustomer( $this->generateProductCustomer( 1 )->setQuantity( 1 ) ), // Failure: low quantity
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PQ', 1, '>', 2 ] )
                                   ->setCode( NULL )
            ],
            [
                [],
                $this->getGenericCustomer(),
                ( new Ordered() )->addProductCustomer(
                    $this->generateProductCustomer( 1 )
                         ->setQuantity( 1 )
                         ->setProduct(
                             $this->generateProduct( 1 )
                                  ->setPrice( 15 )
                         )
                         ->setTotal()
                ), // Failure: low total
                ( new Reduction() )->setActive( TRUE )
                                   ->setCustomer( $this->getGenericCustomer() )
                                   ->setValue( 5 )
                                   ->setRule( [ 'PT', 1, '>', 16 ] )
                                   ->setCode( NULL )
            ],
        ];
    }
}
