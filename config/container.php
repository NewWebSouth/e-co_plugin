<?php


return [
    // ====================================== INTERNAL SERVICE ======================================
    
    \NoMess\Database\IPDOFactory::class                                  => \NoMess\Database\PDOFactory::class,
    \Nomess\Components\EntityManager\EntityManagerInterface::class       => \Nomess\Components\EntityManager\EntityManager::class,
    \Nomess\Components\EntityManager\Event\CreateEventInterface::class   => \Nomess\Components\EntityManager\Event\CreateEvent::class,
    \Nomess\Components\EntityManager\TransactionSubjectInterface::class  => \Nomess\Components\EntityManager\EntityManager::class,
    \Nomess\Container\ContainerInterface::class                          => \Nomess\Container\Container::class,
    \NoMess\Components\ApplicationScope\ApplicationScopeInterface::class => \Nomess\Components\ApplicationScope\ApplicationScope::class,
    \NoMess\Components\LightPersists\LightPersistsInterface::class       => \Nomess\Components\LightPersists\LightPersists::class,
    
    
    \Newwebsouth\Abstraction\Crud\CreateInterface::class => [
        'createArticle'              => \App\Packages\Administer\Manage\Article\SCreate::class,
        'createSection'              => \App\Packages\Administer\Manage\Section\SCreate::class,
        'createGraphicRuleProduct'   => \App\Packages\Administer\Manage\GraphicRule\Product\SCreate::class,
        'createGraphicRuleReduction' => \App\Packages\Administer\Manage\GraphicRule\Reduction\SCreate::class,
        'createProduct'              => \App\Packages\Administer\Manage\Product\SCreate::class,
        'createStock'                => \App\Packages\Administer\Manage\Stock\SCreate::class,
        'createReduction'            => \App\Packages\Administer\Manage\Reduction\SCreate::class,
        'createImage'                => \App\Packages\Administer\Tools\System\Image\SCreate::class,
        'createOrdered'              => \App\Packages\Cunsumption\Ordered\SCreate::class,
        'createMemory'               => \App\Packages\Administer\Tools\Memory\SCreate::class,
        'createPurchase'             => \App\Packages\Administer\Manage\Purchase\SCreate::class,
        'createCredit'               => \App\Packages\Administer\Manage\Credit\SCreateCredit::class
    ],
    
    \Newwebsouth\Abstraction\Crud\UpdateInterface::class => [
        'updateArticle'              => \App\Packages\Administer\Manage\Article\SUpdate::class,
        'updateSection'              => \App\Packages\Administer\Manage\Section\SUpdate::class,
        'updateProduct'              => \App\Packages\Administer\Manage\Product\SUpdate::class,
        'updateStock'                => \App\Packages\Administer\Manage\Stock\SUpdate::class,
        'updateGraphicRuleProduct'   => \App\Packages\Administer\Manage\GraphicRule\Product\SUpdate::class,
        'updateGraphicRuleReduction' => \App\Packages\Administer\Manage\GraphicRule\Reduction\SUpdate::class,
        'updateOption'               => \App\Packages\Administer\Manage\Product\Option\SUpdate::class,
        'updateOrdered'              => \App\Packages\Administer\Manage\Ordered\SUpdate::class,
        'updateCustomer'             => \App\Packages\Cunsumption\Account\SUpdate::class,
        'updateSuspectSuccess'       => \App\Packages\Cunsumption\Payment\SSuspectSuccess::class
    ],
    
    \Newwebsouth\Abstraction\Crud\DeleteInterface::class => [
        'deleteSection'              => \App\Packages\Administer\Manage\Section\SDelete::class,
        'deleteStock'                => \App\Packages\Administer\Manage\Stock\SDelete::class,
        'deleteImage'                => \App\Packages\Administer\Tools\System\Image\SDelete::class,
        'deleteGraphicRuleProduct'   => \App\Packages\Administer\Manage\GraphicRule\Product\SDelete::class,
        'deleteGraphicRuleReduction' => \App\Packages\Administer\Manage\GraphicRule\Reduction\SDelete::class,
        'deleteMemory'               => \App\Packages\Administer\Tools\Memory\SDelete::class
    ],
    
    \Newwebsouth\Abstraction\Crud\ServiceInterface::class => [
        'enableProduct'         => \App\Packages\Administer\Manage\Product\SEnable::class,
        'disableProduct'        => \App\Packages\Administer\Manage\Product\SDisable::class,
        'enableReduction'       => \App\Packages\Administer\Manage\Reduction\SEnable::class,
        'disableReduction'      => \App\Packages\Administer\Manage\Reduction\SDisable::class,
        'addComment'            => \App\Packages\Administer\Manage\Customer\SAddComment::class,
        'disableCustomer'       => \App\Packages\Cunsumption\Account\SDisable::class,
        'serviceControlOrdered' => \App\Packages\Cunsumption\Payment\SControlOrdered::class,
        'serviceForgotPassword' => \App\Packages\Cunsumption\Account\SForgotPassword::class,
        'serviceResetPassword'  => \App\Packages\Cunsumption\Account\SResetPassword::class,
        'serviceLoginAdmin'     => \App\Packages\Administer\Account\SLogin::class,
        'serviceContact'        => \App\Packages\Website\Contact\SContact::class
    ],
    
    \App\Packages\Administer\Manage\Product\DependencyFollowInterface::class => [
        'createDataCustomer'   => \App\Packages\Administer\Manage\Product\DataCustomer\SCreate::class,
        'createDataComplement' => \App\Packages\Administer\Manage\Product\DataComplement\SCreate::class,
        'createOptions'        => \App\Packages\Administer\Manage\Product\Option\SCreate::class,
        'updateDataCustomer'   => \App\Packages\Administer\Manage\Product\DataCustomer\SUpdate::class,
        'updateDataComplement' => \App\Packages\Administer\Manage\Product\DataComplement\SUpdate::class,
        'deleteOptions'        => \App\Packages\Administer\Manage\Product\Option\SDelete::class
    ],
    
    \App\Packages\Cunsumption\Payment\PaymentAdapterInterface::class                         => [
        'servicePaymentIntent'   => \App\Packages\Cunsumption\Payment\Stripe\SPaymentIntent::class,
        'serviceResponsePayment' => \App\Packages\Cunsumption\Payment\Stripe\SResponsePayment::class
    ],
    
    /* --------------------------------- MAIL ------------------------------------ */
    \App\Packages\Administer\Tools\Mail\MailAdapterInterface::class                          => \App\Packages\Administer\Tools\Mail\PhpMailer::class,
    \App\Packages\Administer\Tools\Mail\Initializer\PaymentMailInterface::class              => \App\Packages\Administer\Tools\Mail\Initializer\PaymentInitializer::class,
    \App\Packages\Administer\Tools\Mail\Initializer\InvoiceMailInterface::class              => \App\Packages\Administer\Tools\Mail\Initializer\InvoiceInitializer::class,
    \App\Packages\Administer\Tools\Mail\Initializer\ForgotPasswordMailInterface::class       => \App\Packages\Administer\Tools\Mail\Initializer\ForgotPasswordInitializer::class,
    \App\Packages\Administer\Tools\Mail\Initializer\SentMailInterface::class                 => \App\Packages\Administer\Tools\Mail\Initializer\SentInitializer::class,
    \App\Packages\Administer\Tools\Mail\Initializer\ContactMailInterface::class              => \App\Packages\Administer\Tools\Mail\Initializer\ContactInitializer::class,
    
    /* -------------------------------- OTHER -------------------------------------*/
    \App\Controllers\Admin\Error\ErrorInterface::class                                       => \App\Controllers\Admin\Error\Error::class,
    \App\Packages\Administer\Tools\System\Image\UploadImageInterface::class                  => \App\Packages\Administer\Tools\System\Image\UploadImage::class,
    \App\Packages\Administer\Tools\System\Document\UploadDocumentInterface::class            => \App\Packages\Administer\Tools\System\Document\UploadDocument::class,
    \App\Packages\Website\Cache\FrontObserverInterface::class                                => \App\Packages\Website\Cache\CacheManager::class,
    \App\Packages\Website\Cache\CacheInterface::class                                        => \App\Packages\Website\Cache\CacheManager::class,
    \App\Packages\Website\Resolver\ResolverInterface::class                                  => \App\Packages\Website\Resolver\Dispatcher::class,
    \App\Packages\Cunsumption\Basket\RevalideBasketInterface::class                          => \App\Packages\Cunsumption\Basket\SRevalide::class,
    \App\Packages\Cunsumption\ReductionCustomer\AttachReductionInterface::class              => \App\Packages\Cunsumption\ReductionCustomer\SAttach::class,
    \App\Packages\Cunsumption\Stock\StockRollbackInterface::class                            => \App\Packages\Cunsumption\Stock\SRollback::class,
    \App\Packages\Cunsumption\Invoice\AttachInvoiceInterface::class                          => \App\Packages\Cunsumption\Invoice\SCreate::class,
    \App\Packages\Cunsumption\Payment\AttachPaymentInterface::class                          => \App\Packages\Cunsumption\Payment\SCreate::class,
    \App\Packages\Administer\Report\PdfGeneratorInterface::class                             => \App\Packages\Administer\Report\Dispatcher::class,
    \App\Packages\Administer\Manage\Credit\ProductCredit\AttachProductCreditInterface::class => \App\Packages\Administer\Manage\Credit\ProductCredit\SCreateProductCredit::class,
    \App\Packages\Administer\Tools\System\SystemCleanerInterface::class                      => \App\Packages\Administer\Tools\System\SCleanSystem::class
];
