<?php

return [
    \App\Twig\DateExtension::class,
    \App\Twig\NumberExtension::class
];
