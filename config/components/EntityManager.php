<?php

return [
    'enable'    => TRUE,
    'life_time' => 60 * 60 * 6,
    \App\Entities\Customer::class,
    \App\Entities\Article::class,
    \App\Entities\Product::class,
    \App\Entities\Purchase::class,
    \App\Entities\Credit::class,
    \App\Entities\DataCustomer::class,
    \App\Entities\DataComplement::class,
    \App\Entities\GraphicRuleProduct::class,
    \App\Entities\GraphicRuleReduction::class,
    \App\Entities\Image::class,
    \App\Entities\Invoice::class,
    \App\Entities\Memory::class,
    \App\Entities\Option::class,
    \App\Entities\Ordered::class,
    \App\Entities\Payment::class,
    \App\Entities\ProductCredit::class,
    \App\Entities\ProductCustomer::class,
    \App\Entities\Reduction::class,
    \App\Entities\ReductionCustomer::class,
    \App\Entities\Section::class,
    \App\Entities\Stock::class,
    \App\Entities\Document::class

];
