<?php


return [
    /* ------------------------------ CONFIG -------------------------- */
    'error_data_manager'       => 'Une erreur s\'est produite',
    'orm_error'                => 'Une erreur s\'est produite',
    'nws_abstraction_redirect' => 'error.index', //Configuration for newwebsouth package
    
    'conf_wait_webhook'        => FALSE, //Waiting stripe event for generate invoice
    'hostname'                 => 'histoire-de-pierre.nws',
    'protocol'                 => 'http',
    'number_phone_company'     => '06.00.00.00.00',
    'google_analytic_id'       => '858391269733-94ob03f2imhkajj6vqnmv31nus2leads.apps.googleusercontent.com',
    'login_username'           => 'romainlavabre98@gmail.com',
    'login_password'           => '$2y$10$DMMo5PsR4OXyQOzohRJRPu2QYKjIgF7QmDEEzCD3EMN1sKFIIU6Ju',
    
    /* ------------------------------- MAIL ---------------------------- */
    'mail_host'                => 'ssl0.ovh.net',
    'mail_smtp_auth'           => TRUE,
    'mail_port'                => 587,
    'mail_sender'              => 'contact@newwebsouth.fr',
    'mail_password_sender'     => 'Romain98@',
    'mail_name'                => 'Romain Lavabre',
    'mail_encode'              => 'UTF-8',
    
    
    /* ------------------------------ UPLOAD ----------------------------- */
    'local_storage_image'      => ROOT . 'public/storage/images/',
    'public_storage_image'     => '/storage/images/',
    'local_storage_invoice'    => '/var/nws/histoire/invoices/',
    'local_storage_purchase'   => '/var/nws/histoire/purchases/',
    'local_storage_credit'     => '/var/nws/histoire/credits/',
    
    /* ------------------------------ UTILITY ----------------------------- */
    'section_home_id'          => 2,
    
    /* ------------------------------- STRIPE ------------------------------ */
    
    'private_api_key'      => 'sk_test_51HByNvLqYj4cn9Vm4z73gKLN9tgq5pjNo1ykku4O9xHTh964RyiJYgV4FZRW8e7hrDgtZHuYDFBjVVJAF8H9aEBy00zN7fNfEQ',
    'private_endpoint_key' => ''
];
