<?php

return [
    'default' => [
        'server' => 'mysql',
        'port' => '3306',
        'host' => 'localhost',
        'dbname' => 'uhp_nomess',
        'user' => 'root',
        'password' => 'root'
    ]
];
